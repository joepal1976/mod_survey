Changes as compared with earlier installation scripts
-----------------------------------------------------
People who have installed Mod_Survey earlier will notice that some 
things have changed in the installation script. One of the most 
noticable changes is that the script will no longer attempt to 
configure apache automatically. The reason for this is that different
platforms and distributions look very different in where they want
configuration files placed or included, and rather than doing 
something wrong, the script now leaves to the administrator to 
figure out where the final configuration should be set. 

Further, some external dependencies has been removed, so the module
detection only looks for CGI and DBI.


Apache 1 vs Apache 2 compatibility
----------------------------------
Mod_Survey was initially developed on Apache 1.3.2x, but during the
later stages of development, all default options presume Apache 2.0.x.
Thus Apache version 2.0.49 or higher, and mod_perl 1.99.09 or higher
is recommended. However, running Mod_Survey on Apache 1.3.26 or higher
and mod_perl 1.27 or higher should present no large difficulties. 

If you are using a newer linux distribution, chances are that you have
Apache 2 installed.


Post-install configuration steps
--------------------------------
After having run the installation script you will have a file called
"survey.conf", both in the destination directory and where you ran
the installation script. 

ENABLING MOD_SURVEY IN APACHE 2.0.x: Most linux platforms have a kind
of plug-in directory for apache modules. This directory can usually
be found in /etc/apache2 or /etc/httpd or similar. To enable Mod_Survey,
simply move "survey.conf" from the installation directory to the 
plug-in directory. You may also want to rename "survey.conf" to look
like the standard on your platform, for example "99_mod_survey.conf" or 
something. It is generally a good thing if Mod_Survey is the last module
to be plugged in. If things do not look this way on your platform, the
instructions for apache 1.3.x below should work.

ENABLING MOD_SURVEY IN APACHE 1.3.x: Installations of 1.3.x look very
different, and it is possible that there is a plug-in directory as 
described in the instructions for 2.0.x. However, in the default 
installation of source-based apache, you will get a file httpd.conf 
which contains all configuration settings. This file usually resides in
/etc/apache/conf, /etc/httpd/conf or /usr/local/apache/conf. To enable
Mod_Survey in this file, add an include line at the bottom of the file:

  Include "/usr/local/mod_survey/survey.conf"

(adjust path to fit the location where you installed Mod_Survey). If
there are several configuration files, add the include line at the 
bottom of the file which looks most general. 

CONFIGURATING MAIL SETTINGS: The MAILCOPY tag supports sending copies
of submitted data via mail. However, to enable this, you need valid
setting for mail servers and similar. Manually edit your file 
"survey.conf" and change the settings for _SURVEY_SMTP_HOST and 
_SURVEY_MAIL_ADMIN.


Installing local cocumentation
------------------------------
The documentation is not distributed with the installation tarball. You
can find a tarball with documentation on Mod_Survey's homepage. To
install this, simply untar it where you installed Mod_Survey. This will
overwrite webroot/docs/index.html and place a number of PDF files in
webroot/docs/.



