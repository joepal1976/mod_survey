#!/usr/bin/perl

# We find all Mod_Survey-related modules in the installation
# directory.
use lib $ENV{"_SURVEY_HOME"};

# Assume 2.0.0 if we cannot read mp version
$mp_version = 20;

$find_mp_version = "use mod_perl; \$mp_version = \$mod_perl::VERSION; \$mp_version = int(\$mp_version * 10);";
eval($find_mp_version);

if (!$mp_version) { $mp_version = 20; }

sub mpCompatUse
{
    print "MOD_SURVEY -- Mod_perl version: $mp_version\n";

    my ($evalstr) = "use Apache2 (); use Apache::compat; use Apache::Const";

    if ($mp_version < 19)
    {

        # mod_perl 1
        print "MOD_SURVEY -- Assuming mod_perl for apache 1\n";
        $evalstr = "use Apache (); use Apache::Constants qw (:common)";
    }

    if ($mp_version == 19)
    {

        # mod_perl 2 beta
        print "MOD_SURVEY -- Assuming mod_perl version 1.99.x\n";
        $evalstr = "use Apache2 (); use Apache::compat; use Apache::Constants qw(:common);";
    }

    if ($mp_version > 19)
    {

        # mod_perl 2 release
        print "MOD_SURVEY -- Assuming mod_perl 2.0.x\n";
        $evalstr = "use Apache2::Const qw(:common); use Apache2::Access (); ";
    }

    if ($evalstr)
    {
        print "MOD_SURVEY -- Compat eval: $evalstr\n";
        eval($evalstr);
        if ($@)
        {
            print "MOD_SURVEY -- Compat error: " . $@;
            die "MOD_SURVEY -- Compat error: " . $@;
        }
    }
}

sub showDebug
{
    my ($line);
    foreach $line (@INC)
    {
        print $line . "\n";
    }
}

use Survey::Language;

my ($sld)   = $ENV{"_SURVEY_LANG_DIRECTORY"};
my ($sl)    = $ENV{"_SURVEY_LANG"};
my ($slf)   = "$sld/$sl.sl";
my ($error) = Survey::Language->setupLanguage($slf);
if ($error)
{
    print "\n\nERROR: $error\n\n\n";
    die;
}

# "use" the parameter module and die if this caused an error
sub loadModule
{
    my ($module) = shift;
    eval "use $module;";
    if ($@)
    {
        print "\n\nERROR WHILE STARTING MOD_SURVEY:\n--------------------------------\n\n" . $@
          . "\n\n";

        die "\n\nERROR WHILE STARTING MOD_SURVEY:\n--------------------------------\n\n" . $@
          . "\n\n";
    }

    1;
}

# Iterate through module array and call loadModule for each
sub loadModules
{
    my (@modules) = @_;
    my ($module);

    foreach $module (@modules)
    {
        &loadModule($module);
    }

    1;
}

# These modules has to be loaded always
@requiredmodules = ("CGI",
                    "CGI::Cookie",
                    "Survey::Admin",
                    "Survey::Argument",
                    "Survey::Constants",
                    "Survey::Data",
                    "Survey::Debug",
                    "Survey::Display",
                    "Survey::Document",
                    "Survey::Handler",
                    "Survey::Persistance",
                    "Survey::PersistanceArg",
                    "Survey::Session",
                    "Survey::SessionArg",
                    "Survey::Slask",
                    "Survey::Submit",
                    "Survey::System",
                    "Survey::Template",
                    "Survey::Upload",
                    "Survey::Auth::Auth",
                    "Survey::Auth::DBIAuth",
                    "Survey::Auth::TokenAuth",
                    "Survey::Auth::FileAuth");

@componentmodules = ("Survey::Component::Boolean",
                     "Survey::Component::Caseroute",
                     "Survey::Component::Choice",
                     "Survey::Component::Comment",
                     "Survey::Component::Component",
                     "Survey::Component::Constant",
                     "Survey::Component::Datepicker",
                     "Survey::Component::Custom",
                     "Survey::Component::DateTime",
                     "Survey::Component::Env",
                     "Survey::Component::Ifroute",
                     "Survey::Component::Import",
                     "Survey::Component::Lickert",
                     "Survey::Component::List",
                     "Survey::Component::MailCopy",
                     "Survey::Component::Matrix",
                     "Survey::Component::Memo",
                     "Survey::Component::Newline",
                     "Survey::Component::Randomroute",
                     "Survey::Component::Route",
                     "Survey::Component::Security",
                     "Survey::Component::Sequence",
                     "Survey::Component::Submit",
                     "Survey::Component::SubmitError",
                     "Survey::Component::Text",
                     "Survey::Component::Timer");

@exportmodules =
  ("Survey::Export::Export", "Survey::Export::StatUtils", "Survey::Export::EventBasedExport");

# Patch from MB, compatibility with old survey.conf files
if ($ENV{"_SURVEY_ALLOWED_EXPORTS"})
{
    my ($exp) = $ENV{"_SURVEY_ALLOWED_EXPORTS"};
    my (@lst) = split(/,/, $exp);
    foreach $cell (@lst)
    {
        $inc = $ENV{"_SURVEY_EXPORT_$cell"};
        push(@exportmodules, $inc);
    }
}

if ($ENV{"_SURVEY_OPTIONAL_EXPORTS"})
{
    my ($exp) = $ENV{"_SURVEY_OPTIONAL_EXPORTS"};
    my (@lst) = split(/,/, $exp);
    foreach $cell (@lst)
    {
        $inc = $ENV{"_SURVEY_EXPORT_$cell"};
        push(@exportmodules, $inc);
    }
}

&mpCompatUse();

&loadModules(@componentmodules);
&loadModules(@requiredmodules);
&loadModules(@exportmodules);

if ($ENV{"_SURVEY_USEDBI"})
{
    eval "use Survey::Component::Import";
    if ($@)
    {
        die "\n\nERROR WHILE STARTING MOD_SURVEY:\n--------------------------------\n\n" . $@
          . "\n\n";
    }

    eval "use Survey::DBCommon";
    if ($@)
    {
        die "\n\nERROR WHILE STARTING MOD_SURVEY:\n--------------------------------\n\n" . $@
          . "\n\n";
    }

    &loadModule("DBI");
}

1;

