#!/usr/bin/perl

$min = 1;
$max = 100;
$pad = 5;

sub getAnswer()
{
    my ($default) = @_;
    my ($temp);
    $temp = scalar(<STDIN>);
    chop($temp);
    if (($temp ne "") && ($temp ne $default))
    {
        return $temp;
    }
    else
    {
        return $default;
    }
}

sub yesno()
{
    my ($default) = @_;

    $default = lc($default);

    my ($yn) = lc(&getAnswer($default));

    if ($yn eq "yes" || $yn eq "y")
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

sub mkPass
{
    my ($char, $chr, $line, $i);

    $line = "";

    for ($i = 0 ; $i < 6 ; $i++)
    {
        $chr  = int(rand(26)) + 97;
        $char = chr($chr);
        $line .= $char;
    }

    return $line;
}

print "\n\nEnter filename to generate: ";
$file = &getAnswer("");

if (!$file)
{
    die "you must enter a file name";
}

if (-e $file)
{
    print "\nFile $file exists. Do you want to overwrite it? (y/N) ";
    if (!&yesno("no"))
    {
        die "Please restart and enter another file name";
    }
}

print "\nHow many names do you want to generate? [100] ";
$max = &getAnswer(100);

print "\nEnter a name prefix: [N] ";
$prefix = &getAnswer("N");

$password = "";
$plain    = "";

print "\nDo you want to randomize passwords? (y/N) ";
if (&yesno("no"))
{

    # random
    $random = 1;

    print "\nEnter a filename for storing plaintext passwords: ";
    $plain = &getAnswer("");
    if (!$plain) { die "You must enter a filename"; }
}
else
{
    print "\nEnter a common password: ";
    $password = &getAnswer("");
}

print "\n\n";

print "min:       $min\n";
print "max:       $max\n";
print "pad:       $pad\n";
print "file:      $file\n";
print "prefix:    $prefix\n";
print "random:    $random\n";
print "password:  $password\n";
print "plain:     $plain\n";

open(FIL, ">$file") || die "Could not open $file for writing";

if (!$random)
{
    for ($i = $min ; $i <= $max ; $i++)
    {
        $n = "0" . $i;
        $n = "0" x ($pad - length($n)) . $n;

        print FIL "$prefix$n:" . crypt($password, "xx") . "\n";
    }
}
else
{
    open(PLAIN, ">$plain") || die "Could not open $plain for writing";

    for ($i = $min ; $i <= $max ; $i++)
    {
        $n = "0" . $i;
        $n = "0" x ($pad - length($n)) . $n;

        $name = "$prefix$n";
        $pass = &mkPass();

        print FIL "$name:" . crypt($pass, "xx") . "\n";
        print PLAIN "$name:$pass\n";
    }

    print "\n\nFile $plain successfully generated.\n\n";

    close(PLAIN);
}

close(FIL);

print "\n\nFile $file successfully generated.\n\n";

