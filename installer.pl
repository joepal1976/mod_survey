#!/usr/bin/perl

use CPAN;

@directories = ("Lang",
                "Lang/src",
                "Survey",
                "Survey/Auth",
                "Survey/Component",
                "Survey/Export",
                "data",
                "docs",
                "webroot",
                "webroot/docs",
                "webroot/images",
                "webroot/examples",
                "webroot/examples/basic",
                "webroot/examples/markup",
                "webroot/examples/routing",
                "webroot/system",
                "templates",
                "templates/default");

@corefiles = ("Lang/en.sl",
              "Lang/de.sl",
              "Lang/fr.sl",
              "Lang/sw.sl",
              "Lang/it.sl",
              "Lang/nl.sl",
              "Lang/src/en.po",
              "Lang/src/de.po",
              "Lang/src/fr.po",
              "Lang/src/sw.po",
              "Lang/src/it.po",
              "Lang/src/nl.po",
              "Lang/src/template.po",
              "Lang/po2sl.pl",
              "Lang/makepotemplate.pl",
              "Lang/mergepos.pl",
              "Lang/translate.pl",
              "docs/CHANGELOG.txt",
              "docs/CHANGELOG.txt.old",
              "docs/INSTALL.txt",
              "docs/INSTALL-WINDOWS.txt",
              "docs/Todo.txt",
              "docs/REPORTS.txt",
              "docs/Dynamic_content.txt",
              "docs/Pushed.txt",
              "docs/LICENSE.txt",
              "README.txt",
              "startup.pl",
              "docinterpreter.pl", 
              "genauth.pl", 
              "example-for-docinterpreter.txt",
              "survey.conf.sample",
              "survey-v3.0.0.dtd",
              "Survey/Admin.pm",
              "Survey/Argument.pm",
              "Survey/Constants.pm",
              "Survey/Data.pm",
              "Survey/DataAggressiveCache.pm",
              "Survey/DataEntry.pm",
              "Survey/DBCommon.pm",
              "Survey/Display.pm",
              "Survey/Debug.pm",
              "Survey/Language.pm",
              "Survey/Document.pm",
              "Survey/Slask.pm",
              "Survey/Handler.pm",
              "Survey/Persistance.pm",
              "Survey/PersistanceArg.pm",
              "Survey/Session.pm",
              "Survey/SessionArg.pm",
              "Survey/Statistics.pm",
              "Survey/Submit.pm",
              "Survey/System.pm",
              "Survey/Template.pm",
              "Survey/Upload.pm",
              "Survey/Auth/Auth.pm",
              "Survey/Auth/DBIAuth.pm",
              "Survey/Auth/FileAuth.pm",
              "Survey/Auth/TokenAuth.pm",
              "Survey/Component/Calculated.pm",
              "Survey/Component/Cati.pm",
              "Survey/Component/Env.pm",
              "Survey/Component/Matrix.pm",
              "Survey/Component/Lickert.pm",
              "Survey/Component/Route.pm",
              "Survey/Component/Custom.pm",
              "Survey/Component/Boolean.pm",
              "Survey/Component/List.pm",
              "Survey/Component/Ifroute.pm",
              "Survey/Component/Comment.pm",
              "Survey/Component/Memo.pm",
              "Survey/Component/Geo.pm",
              "Survey/Component/Component.pm",
              "Survey/Component/Newline.pm",
              "Survey/Component/Constant.pm",
              "Survey/Component/Text.pm",
              "Survey/Component/Choice.pm",
              "Survey/Component/Caseroute.pm",
              "Survey/Component/Randomroute.pm",
              "Survey/Component/Sequence.pm",
              "Survey/Component/Security.pm",
              "Survey/Component/Submit.pm",
              "Survey/Component/SubmitError.pm",
              "Survey/Component/Import.pm",
              "Survey/Component/MailCopy.pm",
              "Survey/Component/DateTime.pm",
              "Survey/Component/Timer.pm",
              "Survey/Export/Aggregate.pm",
              "Survey/Export/DelimitedFields.pm",
              "Survey/Export/EventBasedExport.pm",
              "Survey/Export/GroupedFrequency.pm",
              "Survey/Export/Frequency.pm",
              "Survey/Export/Descriptive.pm",
              "Survey/Export/StatUtils.pm",
              "Survey/Export/CaseBrowser.pm",
              "Survey/Export/CaseReport.pm",
              "Survey/Export/OverviewReport.pm",
              "Survey/Export/Export.pm",
              "Survey/Export/FixedColumns.pm",
              "Survey/Export/HtmlTables.pm",
              "Survey/Export/SPSS.pm",
              "Survey/Export/SQL.pm",
              "Survey/Export/Template.pm",
              "Survey/Export/XML.pm",
              "Survey/Export/R.pm",
              "Survey/Export/Tag.pm",
              "windows.conf",
              "survey.conf",
              "webroot/examples/example.survey",
              "webroot/examples/examples.html",
              "webroot/examples/basic/choice.survey",
              "webroot/examples/basic/lickert.survey",
              "webroot/examples/basic/list.survey",
              "webroot/examples/basic/matrix.survey",
              "webroot/examples/basic/memo.survey",
              "webroot/examples/basic/text.survey",
              "webroot/examples/markup/markup.survey",
              "webroot/examples/markup/custom.survey",
              "webroot/examples/markup/angry.png",
              "webroot/examples/markup/extatic.png",
              "webroot/examples/markup/happy.png",
              "webroot/examples/markup/indifferent.png",
              "webroot/examples/markup/logo1.png",
              "webroot/examples/markup/logo2.png",
              "webroot/examples/markup/logo3.png",
              "webroot/examples/markup/logo4.png",
              "webroot/examples/markup/mh.gif",
              "webroot/examples/markup/sour.png",
              "webroot/examples/routing/random1.survey",
              "webroot/examples/routing/alt1.survey",
              "webroot/examples/routing/alt2.survey",
              "webroot/examples/routing/alt3.survey",
              "webroot/examples/routing/alt4.survey",
              "webroot/examples/routing/alt5.survey",
              "webroot/examples/routing/rfinal.survey",
              "webroot/examples/routing/boss.survey",
              "webroot/examples/routing/case-final.survey",
              "webroot/examples/routing/case.survey",
              "webroot/examples/routing/route-part2.survey",
              "webroot/examples/routing/route.survey",
              "webroot/examples/routing/worker.survey",
              "webroot/examples/routing/if.survey",
              "webroot/examples/routing/female0-15.survey",
              "webroot/examples/routing/error.html",
              "webroot/examples/routing/male0-15.survey",
              "webroot/examples/routing/male16-25.survey",
              "webroot/examples/routing/female16-25.survey",
              "webroot/examples/routing/if-final.survey",
              "webroot/examples/invisible.survey",
              "webroot/examples/dynamic.survey",
              "webroot/examples/dynamic2.survey",
              "webroot/index.html",
              "webroot/docs/index.html",
              "webroot/survey-3.2.5.dtd",
              "webroot/main.css",
              "webroot/system/slate.css",
              "webroot/system/eveca.css",
              "webroot/system/invert.css",
              "webroot/system/rose.css",
              "webroot/system/null.css",
              "webroot/system/msu.css",
              "webroot/system/data.css",
              "webroot/system/cloud.css",
              "webroot/system/eveca.css",
              "webroot/system/formal.css",
              "webroot/images/windows.jpg",
              "webroot/images/windows.jpg",
              "templates/default/Lickert.tmpl",
              "templates/default/Matrix.tmpl",
              "templates/default/List.tmpl",
              "templates/default/Choice.tmpl",
              "templates/default/ChoiceMulti.tmpl",
              "templates/default/Boolean.tmpl",
              "templates/default/Text.tmpl",
              "templates/default/Memo.tmpl");

$cpcommand                 = "cp -f";
$windows                   = 0;
$distrib                   = "unix/generic";
$source                    = "./";
$destination               = "/usr/local/mod_survey/";
$creator                   = 1;
$conf{"_SURVEY_ALLOWAUTO"} = 1;

$webuser  = "apache";
$webgroup = "apache";

sub isWindows()
{
    return (-e "c:\\");
}

sub osFix()
{
    my ($path) = @_;

    if ($windows)
    {
        $path =~ s/\//\\\\/g;
    }

    return $path;
}

sub makeDir()
{
    my ($destdir) = shift;
    my ($dir);

    foreach $dir (@directories)
    {
        print "mkdir " . &osFix($destdir . $dir) . "\n";
        system "mkdir " . &osFix($destdir . $dir) . "\n";
    }
}

sub copyCoreFiles()
{
    my ($srcdir, $destdir) = @_;
    my ($file);

    foreach $file (@corefiles)
    {
        print $cpcommand . " " . &osFix($srcdir . $file) . " " . &osFix($destdir . $file) . "\n";
        system $cpcommand . " " . &osFix($srcdir . $file) . " " . &osFix($destdir . $file) . "\n";
    }
}

sub guessDistrib()
{
    my ($distro) = "unix/generic";

    if ((-e "/proc") && (-r "/proc/version"))
    {
        if (open(VER, "/proc/version"))
        {
            my ($inlin) = <VER>;
            close(VER);
            lc($inlin);
            if (index($inlin, "mandriva") > 0)  { $distro = "linux/mandriva"; }
            if (index($inlin, "mandrake") > 0)  { $distro = "linux/mandrake"; }
            if (index($inlin, "fedora") > 0)    { $distro = "linux/fedora"; }
            if (index($inlin, "redhat") > 0)    { $distro = "linux/redhat"; }
            if (index($inlin, "suse") > 0)      { $distro = "linux/suse"; }
            if (index($inlin, "SUSE") > 0)      { $distro = "linux/suse"; }
            if (index($inlin, "debian") > 0)    { $distro = "linux/debian"; }
            if (index($inlin, "slackware") > 0) { $distro = "linux/slackware"; }
            if (index($inlin, "gentoo") > 0)    { $distro = "linux/gentoo"; }
            if (index($inlin, "ubuntu") > 0)    { $distro = "linux/ubuntu"; }
        }
        if ($distro eq "unix/generic")
        {
            if (open(VER, "uname -a |"))
            {
                my ($inlin) = <VER>;
                close(VER);
                lc($inlin);
                if (index($inlin, "sunos") > 0) { $distro = "unix/solaris"; }
                if (index($inlin, "bsd") > 0)   { $distro = "unix/bsd"; }
                if (index($inlin, "macos") > 0) { $distro = "unix/macosx"; }
            }
        }
        if ($distro eq "unix/generic")
        {
            if (-e "/etc/gentoo-release")
            {
                $distro = "linux/gentoo";
            }

            # Possibly more distros have /etc/*-release
        }
    }

    return $distro;
}

sub partTitle()
{
    my ($now, $title) = @_;
    my ($max) = 13;
    $title = uc($title);
    my ($out) = "(Step $now of $max) $title";
    print $out . "\n";
    print "-" x length($out) . "\n";
}

sub getAnswer()
{
    my ($default) = @_;
    my ($temp);
    $temp = scalar(<STDIN>);
    chop($temp);
    if (($temp ne "") && ($temp ne $default))
    {
        return $temp;
    }
    else
    {
        return $default;
    }
}

sub moduleExists()
{
    my ($module) = @_;

    my ($pth);
    my ($exists) = 0;

    foreach $pth (@INC)
    {
        my ($mod) = &osFix($pth . "/" . $module . ".pm");
        if (-e $mod) { $exists = 1; }
    }

    return $exists;
}

sub getUidGid()
{
    my ($uid, $gid);
    if ($distrib eq "linux/mandriva")  { ($uid, $gid) = ("apache", "apache"); }
    if ($distrib eq "linux/mandrake")  { ($uid, $gid) = ("apache", "apache"); }
    if ($distrib eq "linux/gentoo")    { ($uid, $gid) = ("apache", "apache"); }
    if ($distrib eq "linux/fedora")    { ($uid, $gid) = ("apache", "apache"); }
    if ($distrib eq "linux/redhat")    { ($uid, $gid) = ("apache", "apache"); }
    if ($distrib eq "linux/suse")      { ($uid, $gid) = ("wwwrun", "www"); }
    if ($distrib eq "linux/debian")    { ($uid, $gid) = ("www-data","www-data"); }
    if ($distrib eq "linux/ubuntu")    { ($uid, $gid) = ("www-data","www-data"); }
    if ($distrib eq "linux/slackware") { ($uid, $gid) = ("nobody", "nogroup"); }
    if ($distrib eq "unix/solaris")    { ($uid, $gid) = ("nobody", "nogroup"); }
    if ($distrib eq "unix/bsd")        { ($uid, $gid) = ("nobody", "nogroup"); }
    if ($distrib eq "unix/macosx")     { ($uid, $gid) = ("www",    "www"); }
    if ($distrib eq "unix/generic")    { ($uid, $gid) = ("nobody", "nogroup"); }

    return ($uid, $gid);
}

sub askRestart()
{
    print "\n\n\b";
    print "A choice you made above made the script feel it would need to start\n";
    print "all over again. If this is ok with you, then just press enter.\n";
    print "Otherwise say you don't, and the script will exit.\n\n";

    print "Do you want to restart the installer script? [yes] ";
    my ($yn) = lc(&getAnswer("yes"));
    if (($yn eq "yes") || ($yn eq "y"))
    {
        print "\nRestarting script...\n\n";
        exec("perl installer.pl");
    }
    else
    {
        print "\n\n";
        die "User aborted installation";
    }
}

sub installModule()
{
    my ($mod) = @_;

    print "\n\n(Install of new module)\n\n";
    print "Using the CPAN installer may take some time, and produce a lot of\n";
    print "output. When the CPAN installer have finished, you will have to\n";
    print "re-run Mod_Survey\'s installer (by invoking \"perl installer.pl\".\n";

    print "\nOk, I understand. [ok] ";
    &getAnswer("");

    CPAN::Shell->install($mod);

    print "\n\n\nCPAN::Shell->install() finished.";

    &askRestart();
}

# --- INSTALLATION PROCEDURE BEGINS HERE

$windows = &isWindows();

if ($windows)
{
    $cpcommand                = "copy";
    $distrib                  = "windows";
    $conf{"_SURVEY_SENSIBLE"} = "0";
    $destination              = "c:\\mod_survey\\";
}
else
{
    $distrib = &guessDistrib();
    $conf{"_SURVEY_SENSIBLE"} = "1";
}

$nowpart = 1;

# --- BEFORE INSTALLING

&partTitle($nowpart, "BEFORE INSTALLING");

print "Before running this script, you should have read the file README.txt in\n";
print "the root of the tarball. It contains important info about things you\n";
print "should be aware of before installing, and a few post-install config\n";
print "settings you will need to do manually.\n\n";
print "If you have not read the README.txt file, you should press ctrl-c now\n";
print "and read it before continuing.\n\n";

print "Ok, I understand [ok] ";
&getAnswer("ok");

$nowpart++;
print "\n\n\n";

# --- CHECK REQUIRED AND OPTIONAL MODULES

&partTitle($nowpart, "Checking perl modules");

print "Checking for required perl modules:\n\n";

print "  * CGI ........ ";
if (!&moduleExists("CGI"))
{
    print "nope\n\n";
    print "Mod_Survey needs the perl module CGI.pm, which unfortunately does\n";
    print "not seem to exist in your system. Before configuring Mod_Survey\n";
    print "you should get the CGI module(s) from www.CPAN.org.\n\n";

    print "If you want, the script can start the proper installation\n";
    print "procedure for you (via CPAN::Shell->install).\n\n";

    print "Do you want to install CGI now? [yes] ";

    $yesno = lc(&getAnswer("yes"));
    if (($yesno eq "yes") || ($yesno eq "y"))
    {
        &installModule("CGI");
    }
    else
    {
        die "Required module is missing.";
    }
}
else
{
    print "ok\n";
}

print "\n\nChecking for optional modules:\n\n";

print "  * DBI ........ ";
$dbiexists = 1;
if (!&moduleExists("DBI"))
{
    print "nope (databases and survey creator disabled)\n";
    $dbiexists = 0;
}
else
{
    print "ok\n";
}

print "\n\n\n";

if (!$dbiexists)
{
    print "One or more optional modules is missing. This could cause some features\n";
    print "to be switched of (you should have got some minimal info above). If you\n";
    print "want to install one of these modules now (via CPAN::Shell->install),\n";
    print "then type the name of module (listed below), otherwise just press enter.\n\n";

    if (!$dbiexists) { print "  DBI\n"; }

    print "\n";

    print "Install which module? [] ";

    $installit = &getAnswer("");
    if ($installit)
    {
        &installModule($installit);
    }
    print "\n\n\n";
}
$nowpart++;

# --- PLATFORM

&partTitle($nowpart, "Installing for which platform");
print "Mod_Survey is known to function on most unixoid platforms, on MacOS X\n";
print "and on Windows. However, these platforms differ slightly in layout,\n";
print "as an example on where to place configuration files. Thus we first\n";
print "need to determine which platform we are installing for. The script\n";
print "has guessed that we are installing for \"" . $distrib . "\".\n\n";
print "If this is correct, just press enter. Otherwise please choose one\n";
print "of the following:\n\n";

print "  linux/mandriva\n";
print "  linux/mandrake\n";
print "  linux/redhat\n";
print "  linux/suse\n";
print "  linux/debian\n";
print "  linux/gentoo\n";
print "  linux/fedora\n";
print "  unix/solaris\n";
print "  unix/bsd\n";
print "  unix/macosx\n";
print "  unix/generic\n";
print "  windows\n";

print "\nWhich platform are we installing for? [" . $distrib . "] ";
$distrib = &getAnswer($distrib);
print "Platform is: $distrib\n\n\n";
$nowpart++;

if ((!$windows) && ($distrib eq "windows"))
{
    $windows                  = 1;
    $cpcommand                = "copy";
    $distrib                  = "windows";
    $conf{"_SURVEY_SENSIBLE"} = "0";
    $destination              = "c:\\mod_survey\\";
}

# --- INSTALL TO

&partTitle($nowpart, "WHERE TO INSTALL");
print "We now need to determine where to install Mod_survey. It does\n";
print "usually not matter much where you want it, but for unix systems\n";
print "it is probably more efficient to install it on a partition with a\n";
print "low number of bytes per inode. The install directory will contain\n";
print "all source code files, the optional mod_survey web root, the data\n";
print "repository and the configuration files.\n\n";

print "Where do you want to install mod_survey? [$destination] ";
$destination = &getAnswer($destination);
print "Installing in $destination.\n\n\n";

$conf{"_SURVEY_ROOT"}           = $destination . "webroot/";
$conf{"_SURVEY_SYSBASE"}        = $destination . "data/";
$conf{"_SURVEY_LANG_DIRECTORY"} = $destination . "Lang/";

$nowpart++;

# --- Removed add http line

$addhttpd = 0;

# --- USER/GROUP

&partTitle($nowpart, "USER AND GROUP");
if ($windows)
{
    print "For unixoid systems, user and group security has to be specified for\n";
    print "all files. Things does not work that way in windows, so this step\n";
    print "is void. However, please note that that this does also mean that all\n";
    print "*local* users on the system can view all mod_survey related files,\n";
    print "unless you manually fix the file security in the installation folder.\n";

    print "Ok, I understand [ok] ";
    &getAnswer("ok");
}
else
{
    ($webuser, $webgroup) = &getUidGid();

    print "In order to correctly set file security for the files in the\n";
    print "Mod_Survey installation directory, we need to know determine under\n";
    print "which user (uid) and group (gid) the web process runs. The script\n";
    print "has guessed that these are \"$webuser\" and \"$webgroup\" respectively.\n";
    print "If these values are correct, just press enter twice. Otherwise\n";
    print "specify other uid/gid values.\n\n";

    print "Under which user (uid) does the web process run? [$webuser] ";
    $webuser = &getAnswer($webuser);
    print "Web user is $webuser\n";
    print "Under which group (gid) does the web process run? [$webgroup] ";
    $webgroup = &getAnswer($webgroup);
    print "Web group is $webgroup\n";
}
print "\n\n\n";
$nowpart++;

# --- CONFIGURE OPTIONAL MODULES

&partTitle($nowpart, "OPTIONAL EXPORT MODULES");

$casebrowser = 1;
$descriptive = 1;
$frequency   = 1;
$gfrequency  = 1;
$casereport  = 1;
$overview    = 1;
$aggregate   = 1;

print "Later versions of Mod_Survey support several optional data export\n";
print "modules. The difference between the plain (\"core\") data exports and\n";
print "the optional exports, is that the optional exports either analyses the\n";
print "data somehow, presents it formatted or requires extra external code\n";
print "libraries.\n\n";

print "Currently, all optional modules are enabled per default, since there\n";
print "are no external dependencies. There is no harm in keeping these\n";
print "enabled. However, if you want, you can choose to disable some or all\n";
print "of them. The optional export modules are:\n\n";

print "  Case browser (enabled)\n";
print "  Descriptive statistics (enabled)\n";
print "  Frequency tables (enabled)\n";
print "  Grouped frequency tables (enabled)\n";
print "  Case reports (enabled)\n";
print "  Overview reports (enabled)\n";
print "  Aggregates (enabled)\n\n";

print "Do you want to change the above? [no] ";
$yesno = lc(&getAnswer("no"));

if (($yesno ne "no") && ($yesno ne "n"))
{
    print "\n";

    print "Keep case browser enabled? [yes] ";
    $yesno = lc(&getAnswer("yes"));
    if (($yesno ne "no") && ($yesno ne "n")) { $casebrowser = 0; }

    print "Keep descriptive statistics enabled? [yes] ";
    $yesno = lc(&getAnswer("yes"));
    if (($yesno ne "no") && ($yesno ne "n")) { $descriptive = 0; }

    print "Keep frequency tables enabled? [yes] ";
    $yesno = lc(&getAnswer("yes"));
    if (($yesno ne "no") && ($yesno ne "n")) { $frequency = 0; }

    print "Keep grouped frequency tables enabled? [yes] ";
    $yesno = lc(&getAnswer("yes"));
    if (($yesno ne "no") && ($yesno ne "n")) { $gfrequency = 0; }

    print "Keep case reports enabled? [yes] ";
    $yesno = lc(&getAnswer("yes"));
    if (($yesno ne "no") && ($yesno ne "n")) { $casereport = 0; }

    print "Keep overview reports enabled? [yes] ";
    $yesno = lc(&getAnswer("yes"));
    if (($yesno ne "no") && ($yesno ne "n")) { $overview = 0; }

    print "Keep aggregate export enabled? [yes] ";
    $yesno = lc(&getAnswer("yes"));
    if (($yesno ne "no") && ($yesno ne "n")) { $aggregate = 0; }
}

print "\n\n\n";
$nowpart++;

# --- CONFIGURE LANGUAGE

$lang = "en";

&partTitle($nowpart, "CONFIGURING LANGUAGE");
print "Mod_Survey supports several languages, and more are continuously\n";
print "being added. The language support works rather well, but it is\n";
print "that not all translations are complete, and that those languages\n";
print "therefore will be mixed with english sentences. If you feel\n";
print "satisfied with running everything in english, just press enter,\n";
print "otherwise, specify one of the following language codes:\n\n";

print "  \"en\" - English\n";
print "  \"de\" - German (Deutsch)\n";
print "  \"fr\" - French (francais)\n";
print "  \"it\" - Italian (italiano)\n";
print "  \"sw\" - Swedish (svenska)\n\n";

print "Which language do you want to use? [$lang] ";
$lang = lc(&getAnswer($lang));
print "Language is $lang\n";
$nowpart++;
print "\n\n\n";

# --- PerlSendHeader

$isa2 = "Off";

&partTitle($nowpart, "PERLSENDHEADER");
print "There are sometimes problems with HTTP headers and Apache2 when using\n";
print "Mod_Survey. This is caused by an inconsistency in the backward-compat\n";
print "layer in mod_perl.\n\n";

print "If you are using Apache 2 (which, for example, is the case in most\n";
print "modern linux distributions) you will likely want to keep the config\n";
print "directive \"PerlSendHeader\" set to \"Off\". However, if you experience\n";
print "problems with this (internal server errors, strangely displayed surveys,\n";
print "no stylesheets, blank pages...) or are using Apache 1, then you might\n";
print "want to set it to \"On\".\n\n";

print "If you don't understand anything of the above, go for the default\n";
print "option.\n\n";

print "Do you want to set PerlSendHeader to \"On\"? [no] ";
$yesno = lc(&getAnswer("no"));
if (($yesno ne "no") && ($yesno ne "n"))
{
    print "PerlSendHeader On\n";
    $isa2 = "On";
}
else
{
    print "PerlSendHeader Off\n";
}

$nowpart++;
print "\n\n\n";

# --- ADVANCED CONFIG

$protest   = 1;
$rootalias = "/mod_survey/";
$ext       = ".survey";
$prext     = ".presentation";
$parser    = 1;
$display   = 1;

&partTitle($nowpart, "ADVANCED CONFIGURATION");
print "The configuration is now in principle finished. The script has set\n";
print "a couple of parameters to their default values. Advanced users can\n";
print "check this and choose to change them. It is, however, recommended\n";
print "to keep these values as unforeseen consequences can occur if they\n";
print "are changed. The settings are:\n\n";

print "  _SURVEY_ROOT_ALIAS          \"$rootalias\"\n";
print "  Normal survey extension:    \"$ext\"\n\n";

print "Do you want to change the above? [no] ";
$yesno = lc(&getAnswer("no"));
if (($yesno ne "no") && ($yesno ne "n"))
{
    print "Changing advanced settings.\n\n";

    print "Which root alias should Mod_Survey use? [$rootalias] ";
    $rootalias = &getAnswer($rootalias);
    print "Root alias is $rootalias";

    print "Which extension should be used for surveys? [$ext] ";
    $ext = &getAnswer($ext);
    print "Extension is $ext\n";
}
else
{
    print "Using default settings.\n";
}

$nowpart++;
print "\n\n\n";

# --- COPY FILES

&partTitle($nowpart, "Set up installation directory");
print "The script is now ready to copy files to their designed locations\n";
print "and create the necessary configuration files.\n";
print "When this is done, stop apache (to a full stop) and restart it.\n";
print "Then you should be able to use Mod_Survey. A good starting point\n";
print "would be by browsing to http://<your hostname>$rootalias\n\n";

print "Do you want to review your settings before continuing? [no] ";
$yesno = lc(&getAnswer("no"));
if (($yesno eq "yes") || ($yesno eq "y"))
{
    print "\n\nYour settings are as follows:\n\n";

    print "   _SURVEY_SYSBASE        \"" . $conf{"_SURVEY_SYSBASE"} . "\"\n";
    print "   _SURVEY_HOME           \"" . $destination . "\"\n";
    print "   _SURVEY_ROOT           \"" . $conf{"_SURVEY_ROOT"} . "\"\n";
    print "   _SURVEY_ROOT_ALIAS     \"$rootalias\"\n";
    if   ($windows) { print "   _SURVEY_SENSIBLE       0\n"; }
    else            { print "   _SURVEY_SENSIBLE       1\n"; }
    print "   _SURVEY_USEDBI         $dbiexists\n";
    print "   _SURVEY_ALLOWAUTO      1\n";
    print "   _SURVEY_LANG_DIRECTORY \"" . $conf{"_SURVEY_LANG_DIRECTORY"} . "\"\n";
    print "   _SURVEY_LANG           \"$lang\"\n";
    print "   _SURVEY_COOKIE_NAME    \"$cookie\"\n\n";

    print "   Extension for surveys: $ext\n";

    print "   Installation destination: $destination\n";

    print "\nDoes this look as expected? [yes] ";
    $yesno = lc(&getAnswer("yes"));
    if (($yesno ne "yes") && ($yesno ne "y"))
    {
        &askRestart();
    }
}

open(FIL, ">survey.conf") || die "Could not open survey.conf for writing";

print FIL "### Survey configuration\n\n";

print FIL "# -----------------------------------------\n";
print FIL "# Config new for 32x\n";
print FIL "# -----------------------------------------\n\n";
print FIL "PerlSetEnv   _SURVEY_ALLOWED_EXPORTS	\"html,spss,sql,delim,fixed,xml,r\"\n";

print FIL "PerlSetEnv   _SURVEY_OPTIONAL_EXPORTS \"";

$first = 1;
if ($casebrowser) { print FIL "browse"; $first = 0; }
if (!$first)      { print FIL ","; }
if ($descriptive) { print FIL "desc";   $first = 0; }
if (!$first)      { print FIL ","; }
if ($frequency)   { print FIL "freq";   $first = 0; }
if (!$first)      { print FIL ","; }
if ($gfrequency)  { print FIL "gfreq";  $first = 0; }
if (!$first)      { print FIL ","; }
if ($casereport)  { print FIL "case";  $first = 0; }
if (!$first)      { print FIL ","; }
if ($overview)  { print FIL "overview";  $first = 0; }
if (!$first)      { print FIL ","; }
if ($aggregate)  { print FIL "aggregate";  $first = 0; }

print FIL "\"\n\n";

print FIL "PerlSetEnv   _SURVEY_EXPORT_html	\"Survey::Export::HtmlTables\"\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_spss	\"Survey::Export::SPSS\"\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_xml		\"Survey::Export::XML\"\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_sql		\"Survey::Export::SQL\"\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_delim	\"Survey::Export::DelimitedFields\"\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_fixed	\"Survey::Export::FixedColumns\"\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_r	\"Survey::Export::R\"\n";

print FIL "PerlSetEnv   _SURVEY_EXPORT_browse		\"Survey::Export::CaseBrowser\"\n\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_desc		\"Survey::Export::Descriptive\"\n\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_freq		\"Survey::Export::Frequency\"\n\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_gfreq		\"Survey::Export::GroupedFrequency\"\n\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_case		\"Survey::Export::CaseReport\"\n\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_overview		\"Survey::Export::OverviewReport\"\n\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_aggregate	\"Survey::Export::Aggregate\"\n\n";

print FIL "# -----------------------------------------\n";
print FIL "# Mail settings\n";
print FIL "# -----------------------------------------\n";
print FIL "PerlSetEnv   _SURVEY_SMTP_HOST \"127.0.0.1\"\n";
print FIL "PerlSetEnv   _SURVEY_MAIL_ADMIN \"root\@localhost\"\n\n";

print FIL "# -----------------------------------------\n";
print FIL "# Encoding settings\n";
print FIL "# -----------------------------------------\n\n";

print FIL "# Use this encoding for all output and input. It is unlikely\n";
print FIL "# that you want to change this unless you have old data in\n";
print FIL "# another format.\n";
print FIL "PerlSetEnv   _SURVEY_ENCODING \"UTF-8\"\n\n";

print FIL "# Should the parser parry for differences in input encoding?\n";
print FIL "# If unset, the system will assume all survey files are in UTF8\n";
print FIL "# format, which is probably a bad idea in most cases.\n";
print FIL "PerlSetEnv   _SURVEY_PARRY_ENCODING 1\n\n";

print FIL "# Assume the perl scripts themselves is in this encoding\n";
print FIL "# (they are unless you changed them)\n";
print FIL "PerlSetEnv LANG en_US.UTF-8\n\n";

print FIL "# Gracefully handle source files in UTF-8 format\n";
print FIL "PerlSwitches -C\n\n";

print FIL "# !!! IF YOU CHANGE ANY OF THE ABOVE, ALSO CHANGE THE\n";
print FIL "# !!! SetEnv STATEMENT IN THE <Files> SECTION BELOW\n\n";

print FIL "# -----------------------------------------\n";
print FIL "# Set global survey configuration variables\n";
print FIL "# -----------------------------------------\n\n";

print FIL "# Directory where caches, autodata files and keys should be stored\n";
print FIL "PerlSetEnv   _SURVEY_SYSBASE        \"" . $conf{"_SURVEY_SYSBASE"} . "\"\n\n";

print FIL "# Whether parser is allowed to write and read cache at all (overrides\n";
print FIL "# ALLOWCACHE in survey docs if set to 0)\n";
print FIL "PerlSetEnv   _SURVEY_PARSERCACHE    $parser\n\n";

print FIL "# Whether display module is allowed to write and read html output cache \n";
print FIL "# (overrides ALLOWCACHE in survey docs if set to 0)\n";
print FIL "PerlSetEnv   _SURVEY_DISPLAYCACHE   $display\n\n";

print FIL "# Where mod_survey is installed\n";
print FIL "PerlSetEnv   _SURVEY_HOME           \"" . $destination . "\"\n\n";

print FIL "# Where mod_survey web root is\n";
print FIL "PerlSetEnv   _SURVEY_ROOT           \"" . $conf{"_SURVEY_ROOT"} . "\"\n\n";

print FIL "# Which root alias to use for doc links and similar\n";
print FIL "PerlSetEnv   _SURVEY_ROOT_ALIAS     \"$rootalias\"\n\n";

print FIL "# Whether we have installed on a sensible (unixoid) system\n";
if   ($windows) { print FIL "PerlSetEnv   _SURVEY_SENSIBLE       0\n"; }
else            { print FIL "PerlSetEnv   _SURVEY_SENSIBLE       1\n\n"; }

print FIL "# -------------------------\n";
print FIL "# Security related settings\n";
print FIL "# -------------------------\n\n";

print FIL "# If you plan to use DBI tables, you must install DBI 1.13 or later and\n";
print FIL "# set the below to 1. Even if you have DBI installed, you may want to\n";
print FIL "# choose to not enable it in Mod_Survey for security reasons.\n";
print FIL "PerlSetEnv   _SURVEY_USEDBI         $dbiexists\n\n";

print FIL "# Whether is is allowed to let system automatically handle data submission\n";
print FIL "# It would a stupid idea to change this unless you know what you are doing.\n";
print FIL "PerlSetEnv   _SURVEY_ALLOWAUTO      1\n\n";

print FIL "# Whether submit should protest at illegal characters and ask the user\n";
print FIL "# to go back and correct these. This will be ignored if _SURVEY_USENEWAUTo\n";
print FIL "PerlSetEnv   _SURVEY_PROTESTILLEGAL 1\n\n";

print FIL "# Whether to use safe delimiters in the AutoData file. This makes things\n";
print FIL "# safer, but also makes the AutoData file partially binary. Most users \n";
print FIL "# want to enable this.\n";
print FIL "PerlSetEnv   _SURVEY_USENEWAUTO     1\n\n";

print FIL "# -------------------------------------\n";
print FIL "# Internationalization related settings\n";
print FIL "# -------------------------------------\n\n";

print FIL "# Where to find language files\n";
print FIL "PerlSetEnv   _SURVEY_LANG_DIRECTORY \"" . $conf{"_SURVEY_LANG_DIRECTORY"} . "\"\n\n";

print FIL "# Which language to use\n";
print FIL "PerlSetEnv   _SURVEY_LANG           \"$lang\"\n\n";

print FIL "# ---------------------------------------------------\n";
print FIL "# Startup check and addition of survey folder to \@INC\n";
print FIL "# ---------------------------------------------------\n\n";

print FIL "Perlrequire \"$destination" . "startup.pl\"\n\n";

print FIL "# --------------------------------------------------------------------\n";
print FIL "# Add type for survey files. If you change this, please change pattern \n";
print FIL "# for the files tag below too\n";
print FIL "# --------------------------------------------------------------------\n\n";

print FIL "AddType text/html $ext\n\n";

print FIL "# ---------------------------------------------------------------\n";
print FIL "# Match all files containing \"$ext\" (tail \"*\" is necessary for \n";
print FIL "# admin part). Enclosure in Directory necessary to avoid clash with\n";
print FIL "# mod_proxy.\n";
print FIL "# ---------------------------------------------------------------\n\n";

print FIL "<Directory \"/\">\n";
print FIL "  <Files *$ext*>\n";
print FIL "    SetHandler perl-script\n";
print FIL "    PerlHandler Survey::Handler\n";
print FIL "    PerlSendHeader \"$isa2\"\n";
print FIL "    AuthType Basic\n";
print FIL "    AuthName Mod_Survey\n";
print FIL "    PerlAuthenHandler Apache::AuthAny\n";
print FIL "    SetEnv LANG en_US.UTF-8\n";
print FIL "  </Files>\n";
print FIL "</Directory>\n\n";

print FIL "# ---------------------------------------------------------------\n";
print FIL "# Settings for alias/directory containing docs, examples, creator\n";
print FIL "# scripts and similar\n";
print FIL "# ---------------------------------------------------------------\n\n";

print FIL "Alias $rootalias \"" . $conf{"_SURVEY_ROOT"} . "\"\n\n";

print FIL "<Directory " . $conf{"_SURVEY_ROOT"} . ">\n";
print FIL "  Options +All\n";
print FIL "  AllowOverride All\n";
print FIL "  Order allow,deny\n";
print FIL "  Allow from all\n";
print FIL "</Directory>\n\n";

close(FIL);

print "\n\n\n";

system "mkdir " . &osFix($destination);

&makeDir($destination);
&copyCoreFiles($source, $destination);

if (!$windows)
{
    print "Chmodding/chowning files and directories... ";
    system "chown 0:0 -R $destination";
    system "chmod 755 -R $destination";
    system "chown $webuser" . ":" . "$webgroup $destination" . "data";
    system "chmod 700 $destination" . "data";
    print "ok\n";
}

print "\n\nInstallation finished successfully.\n\n";
print "You need to perform some configuration steps manually to fully enable\n";
print "Mod_Survey. You can read about these steps in README.txt.\n\n";

1;

