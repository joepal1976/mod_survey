#!/usr/bin/perl -w

use strict;

use Encode::Guess;
use Encode;

my ($master)   = $ARGV[0];
my ($addition) = $ARGV[1];
my ($output)   = $ARGV[2];

sub makehash {
	my ($src) = shift;

	my ( %langhash, $id, $trans );

	if ( open( SRC, $src ) ) {
		my (@lines) = <SRC>;
		close(SRC);

		my ($line);
		my ($was) = "";
		$id    = "";
		$trans = "";

		foreach $line (@lines) {
			my ($skip) = 0;

			chop($line);

			if ( !$line ) { $skip = 1; }
			if ( $line =~ m/^\#/ ) { $skip = 1; }

			if ( !$skip ) {
				if ( $line =~ m/^msgid/ ) {
					if ( $was && ( $id ne "" ) ) {
						$langhash{$id} = $trans;
					}
					$was = "msgid";
					$line =~ s/^msgid//;
					$line =~ s/^[\ \x09]+\"//;
					$line =~ s/\"$//;

					$id = $line;
				}
				else {
					if ( $line =~ m/^msgstr/ ) {
						$was = "msgstr";
						$line =~ s/^msgstr//;
						$line =~ s/^[\ \x09]+\"//;
						$line =~ s/\"$//;

						$trans = $line;
					}
					else {
						$line =~ s/^[\ \x09]+\"//;
						$line =~ s/\"$//;

						if ( $was eq "msgid" ) {
							$id .= $line;
						}
						else {
							$trans .= $line;
						}
					}
				}
			}
		}
	}
	else {
		print "\nERROR: Could not open $src for reading\n";
		return;
	}
	return \%langhash;
}

if ( !$master || !$addition || !$output ) {
	print "USAGE: mergepos.pl <master> <addition> <output>\n\n";
	exit(0);
}

my (%masterhash) = %{ &makehash($master) };
my (%addhash)    = %{ &makehash($addition) };
my ($k);

open( OUT, ">$output" ) || die;

print OUT "\# Mod_survey internationalization module.\n";
print OUT "msgid\t\"\"\n";
print OUT "msgstr\t\"Project-Id-Version: 3.2.x\"\n";
print OUT "\t\"POT-Creation-Date: 2004-xx-xx 00:00+0200\"\n";
print OUT "\t\"PO-Revision-Date: 2004-xx-xx 00:00+0200\"\n";
print OUT "\t\"Last-Translator: Merge script <joel.palmius\@mh.se>\"\n";
print OUT "\t\"Language-Team: Merge script <joel.palmius\@mh.se>\"\n";
print OUT "\t\"MIME-Version: 1.0\"\n";
print OUT "\t\"Content-Type: text/plain; charset=iso-8859-1\"\n";
print OUT "\t\"Content-Transfer-Encoding: 8bit\"\n\n";

foreach $k ( keys(%masterhash) ) {
	print OUT "msgid \"$k\"\n";
	if ( $addhash{$k} ) {
		
		my($encname) = "?";
		
		my($enc) = Encode::Guess->guess($addhash{$k});
		
		if(ref($enc))
		{			
			$encname = $enc->name;
		}
		else
		{
			print "ENCODING ERROR: " . $enc . "\n\n";
		}
		
		if($encname eq "utf8" || $encname eq "ascii")
		{		
		print OUT "msgstr \"" . $addhash{$k} . "\"\n\n";
		print "ADDED: " . $addhash{$k} . "\n\n";
		}		
	}
	else {
		print OUT "msgstr \"" . $masterhash{$k} . "\"\n\n";
	}
}

foreach $k ( keys(%addhash) ) {
	if ( !defined( $masterhash{$k} ) ) {
		print "REJECTED: $k\n\n";
	}
}

close(OUT);

