#!/usr/bin/perl -w

use strict;

my ($master) = $ARGV[0];

sub makehash
{
    my ($src) = shift;

    my (%langhash, $id, $trans);

    if (open(SRC, $src))
    {
        my (@lines) = <SRC>;
        close(SRC);

        my ($line);
        my ($was) = "";
        $id    = "";
        $trans = "";

        foreach $line (@lines)
        {
            my ($skip) = 0;

            chop($line);

            if (!$line) { $skip = 1; }
            if ($line =~ m/^\#/) { $skip = 1; }

            if (!$skip)
            {
                if ($line =~ m/^msgid/)
                {
                    if ($was && ($id ne ""))
                    {
                        $langhash{$id} = $trans;
                    }
                    $was = "msgid";
                    $line =~ s/^msgid//;
                    $line =~ s/^[\ \x09]+\"//;
                    $line =~ s/\"$//;

                    $id = $line;
                }
                else
                {
                    if ($line =~ m/^msgstr/)
                    {
                        $was = "msgstr";
                        $line =~ s/^msgstr//;
                        $line =~ s/^[\ \x09]+\"//;
                        $line =~ s/\"$//;

                        $trans = $line;
                    }
                    else
                    {
                        $line =~ s/^[\ \x09]+\"//;
                        $line =~ s/\"$//;

                        if ($was eq "msgid")
                        {
                            $id .= $line;
                        }
                        else
                        {
                            $trans .= $line;
                        }
                    }
                }
            }
        }
    }
    else
    {
        print "\nERROR: Could not open $src for reading\n";
        return;
    }
    return \%langhash;
}

if (!$master)
{
    print "USAGE:translate.pl <master>\n\n";
    exit(0);
}

my (%masterhash) = %{ &makehash($master) };
my ($k, $transl);

my ($no) = 0;

foreach $k (keys(%masterhash))
{
    if (!$masterhash{$k})
    {
        $no++;
    }
}

my ($pos) = 0;

foreach $k (keys(%masterhash))
{
    if (!$masterhash{$k})
    {
        $pos++;
        print "\n\n";
        print "-------------------------------------------------------------\n\n";
        print "Translate this string, do not include the asterisks. To skip, \n";
        print "just press enter. To save and quit write _-_\n\n";
        print "***$k***\n\n";
        print "Translation $pos of $no: ";
        $transl = <STDIN>;
        chop($transl);
        if ($transl eq "_-_") { last; }
        else
        {

            if ($transl)
            {
                $masterhash{$k} = $transl;
            }
            else
            {
                print "\nSKIPPED\n";
            }
        }
    }
}

print "\n\n";

system "mv $master $master.old";

open(OUT, ">$master") || die;

print OUT "\# Mod_survey internationalization module.\n";
print OUT "msgid\t\"\"\n";
print OUT "msgstr\t\"Project-Id-Version: 3.2.x\"\n";
print OUT "\t\"POT-Creation-Date: 2004-xx-xx 00:00+0200\"\n";
print OUT "\t\"PO-Revision-Date: 2004-xx-xx 00:00+0200\"\n";
print OUT "\t\"Last-Translator: Merge script <joel.palmius\@mh.se>\"\n";
print OUT "\t\"Language-Team: Merge script <joel.palmius\@mh.se>\"\n";
print OUT "\t\"MIME-Version: 1.0\"\n";
print OUT "\t\"Content-Type: text/plain; charset=iso-8859-1\"\n";
print OUT "\t\"Content-Transfer-Encoding: 8bit\"\n\n";

foreach $k (keys(%masterhash))
{
    print OUT "msgid\t\"$k\"\n";
    print OUT "msgstr\t\"" . $masterhash{$k} . "\"\n\n";
}

close(OUT);

