#!/usr/bin/perl

use lib ("..");

use Survey::Language;

print "Simple PO to Survey::Language compiler\n";
print "--------------------------------------\n";

my ($lang) = $ARGV[0];

if ($lang)
{
    print "Language: $lang\n";
}
else
{
    die "USAGE: po2sl.pl <language code>\n";
}

Survey::Language->compile($lang);

print "\n\n";

