#!/usr/bin/perl

use Encode::Guess;
use Encode;

$cmd = "find ../Survey -name \"*.pm\" -print";
open( PIPE, "$cmd |" ) || die "Could not open pipe";

@files = ();

while ( $inlin = <PIPE> ) {
	chop($inlin);
	push( @files, $inlin );
}

close(PIPE);

print "\# Mod_survey internationalization module.\n";
print "msgid\t\"\"\n";
print "msgstr\t\"Project-Id-Version: 3.2.x\"\n";
print "\t\"POT-Creation-Date: 2004-xx-xx 00:00+0200\"\n";
print "\t\"PO-Revision-Date: 2004-xx-xx 00:00+0200\"\n";
print "\t\"Last-Translator: Automatic <joel.palmius\@datacollection.se>\"\n";
print "\t\"Language-Team: Unknown <joel.palmius\@datacollection.se>\"\n";
print "\t\"MIME-Version: 1.0\"\n";
print "\t\"Content-Type: text/plain; charset=utf-8\"\n";
print "\t\"Content-Transfer-Encoding: 8bit\"\n\n";

$tmp1 = "/tmp/translation" . $$;

$i = 1;

foreach $file (@files) {
	open( FILE, $file ) || die;
	$complete = join( '', <FILE> );
	close(FILE);

	$complete =~ s/[\x0d\x0a]//g;
	$complete =~ s/\x09/ /g;
	$complete =~ s/\ +/ /g;
	$complete =~ s/lprint/\x0alprint/g;
	$complete =~ s/lprint \(/lprint\(/g;
	$complete =~ s/\"\ +\)/\"\)/g;
	$complete =~ s/;/;\x0a/g;

	$tmp = $tmp1 . "-" . $i;

	$i++;

	open( FILE, ">$tmp" ) || die;
	print FILE $complete;
	close(FILE);

	open( PIPE, "cat $tmp | grep \"lprint\(\" |" )
	  || die "Could not open $file";

	while ( $inlin = <PIPE> ) {
		chop($inlin);
		my (@kompo) = split( /lprint\(/, $inlin );

		foreach $k (@kompo) {
			if ( $k =~ m/\"\)/ ) {
				$i = index( $k, "\")" );
				$s = substr( $k, 0, $i + 1 );

				if ( $s eq "" || $s eq "\"" ) {
					$i = index( $k, "\")", 2 );
					$s = substr( $k, 0, $i + 1 );
				}
				$s =~ s/\\//g;
				$s =~ s/^\s+//g;

				if ( !( $s =~ m/\$self/ ) && !( $s =~ m/\$doc/ ) ) {
					$enc = guess_encoding( $s, qw/iso-8859-1 utf-8 ascii/ );
					$encname = $enc->name;
					
					if ( $encname eq "iso-8859-1" ) {
						my($string) = decode("iso-8859-1", $s);
						$s = encode("utf-8",$string);
					}

					print "msgid $s\n";
					print "msgstr \"\"\n\n";
				}
			}
		}
	}
	close(PIPE);

	system "rm -f $tmp";

}

