"use strict";

var ALLOWSUBMIT = true;
var FIRSTERRORDIV = "";

function checkText(id)
{
	var name = "";
	var mustanswer = false;
	var numerical = false;
	var maxval = Number.MAX_VALUE;		
	var minval = Number.MIN_VALUE;		
	var value = "";
	
	$("#" + id).find("input").first().each( function() 
	{
		name = $(this).attr("name");		
		mustanswer = $(this).hasClass("mustanswer");
		
		numerical = $(this).attr("data-numerical");
		if(numerical == 1)
		{
			numerical = true;
			
			var attr = $(this).attr('data-maxval');
			
			if (typeof attr !== 'undefined' && attr !== false) 
			{
				maxval = attr;
			}
			
			attr = $(this).attr('data-minval');
			
			if (typeof attr !== 'undefined' && attr !== false) 
			{
				minval = attr;
			}
		}
		
		value = $(this).val();
	});
		
	if(mustanswer)
	{
		if(value == "")
		{
			$("#" + name + "_mustanswer").show();
			ALLOWSUBMIT = false;
			
			if(FIRSTERRORDIV == "")
			{
				FIRSTERRORDIV=name+"_mustanswer";
			}	
		}
		else
		{		
			if(numerical)
			{
				if(isNaN(value))
				{
					$("#" + name + "_numerical").show();
					ALLOWSUBMIT = false;
					
					if(FIRSTERRORDIV == "")
					{
						FIRSTERRORDIV=name+"_numerical";
					}	
				}
				else
				{
					value = parseInt(value);
					minval = parseInt(minval);
					maxval = parseInt(maxval);
					
					if(value < minval)
					{						
						$("#" + name + "_minval").show();
						ALLOWSUBMIT = false;
						
						if(FIRSTERRORDIV == "")
						{
							FIRSTERRORDIV=name+"_minval";
						}	
					}
					
					if(value > maxval)
					{
						$("#" + name + "_maxval").show();
						ALLOWSUBMIT = false;
						
						if(FIRSTERRORDIV == "")
						{
							FIRSTERRORDIV=name+"_maxval";
						}	
					}
				}
			}
		}
	}
		
}

function checkChoice(id)
{
	var name = "";
	var mustanswer = false;
	var hasanswer = false;
			
	$("#" + id).find("input").first().each( function()
	{
		name = $(this).attr("name");
		mustanswer = $(this).hasClass("mustanswer");		
	});
	
	if(mustanswer)
	{
		$("#" + id).find('input[type="radio"]').each( function()
		{
			//console.log("radio id=" + $(this).attr("id") + " ischecked=" + $(this).is(":checked"));
			if( $(this).is(':checked'))
			{
				hasanswer = true;
			}
		});	
		
		$("#" + id).find('input[type="checkbox"]').each( function()
		{
			//console.log("radio id=" + $(this).attr("id") + " ischecked=" + $(this).is(":checked"));
			if( $(this).is(':checked'))
			{
				hasanswer = true;
			}
		});
	}
	
	//console.log("CHOICE CHECK id=" + id + " name=" + name + " mustanswer=" + mustanswer + " hasanswer=" + hasanswer);
	
	if(mustanswer && !hasanswer)
	{		
		//$("#" + name + "_error").text("Den här frågan måste besvaras");
		$("#" + name + "_mustanswer").show();
		ALLOWSUBMIT = false;
				
		if(FIRSTERRORDIV == "")
		{
			FIRSTERRORDIV=name+"_mustanswer";
		}
	}
}

function checkLickert(id)
{
	var name = "";
	var mustanswer = false;
	var hasanswer = false;
			
	$("#" + id).find("input").first().each( function()
	{
		name = $(this).attr("name");
		mustanswer = $(this).hasClass("mustanswer");		
	});
	
	if(mustanswer)
	{
		$("#" + id).find('input[type="radio"]').each( function()
		{
			//console.log("radio id=" + $(this).attr("id") + " ischecked=" + $(this).is(":checked"));
			if( $(this).is(':checked'))
			{
				hasanswer = true;
			}
		});	
	}
	
	console.log("LICKERT CHECK id=" + id + " name=" + name + " mustanswer=" + mustanswer + " hasanswer=" + hasanswer);
	
	if(mustanswer && !hasanswer)
	{		
		$("#" + name + "_mustanswer").show();
		ALLOWSUBMIT = false;
				
		if(FIRSTERRORDIV == "")
		{
			FIRSTERRORDIV=name+"_mustanswer";
		}
	}
}

var onMdsSubmit = function(event)
{		
	ALLOWSUBMIT = true;
	FIRSTERRORDIV = "";
	
	event.preventDefault();
	$(".errormessage").hide();
	
	$(".component").each( function()
	{
		var id = $(this).attr("id");
		if(id !== undefined)
		{
			if( $(this).hasClass("TEXT") ) { checkText( id );}
			if( $(this).hasClass("CHOICE") ) { checkChoice( id ); }
			if( $(this).hasClass("LICKERT") ) { checkLickert( id ); }
		}		
	});

	if(!ALLOWSUBMIT)
	{
		//alert("Det finns fel i formuläret. Vänligen scrolla upp och fixa dessa.");
		event.preventDefault();
		console.log(FIRSTERRORDIV);
		//$('#'+FIRSTERRORDIV)[0].scrollIntoView(true);
		$(window).scrollTop($('#' + FIRSTERRORDIV).offset().top-50);
	}	
	
}


$( function() 
{
  $('input[type="submit"]').button();
  $('input[type="reset"]').button();
        
	$("form").submit(onMdsSubmit);	
});

