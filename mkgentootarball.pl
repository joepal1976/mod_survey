#!/usr/bin/perl

# This script will build a gentoo source tarball from a normal
# mod_survey distribution tarball. The produced tarball is what
# should be added to distfiles, *not* the mod_survey release
# tarballs.

use CPAN;

# --- SETTINGS ---

$version      = "3.2.5";
$extraversion = "";

$workroot = "/tmp";
$wdname   = "mod_survey";
$workdir  = "$workroot/$wdname";

$datadir  = "/var/lib/mod_survey";
$libdir   = "/usr/lib/mod_survey";
$sharedir = "/usr/share/mod_survey";
$webdir   = "/var/www/mod_survey";

# Add a symlink in /var/www/localhost/htdocs to webroot?
$makewebsymlink = 1;

$webuser  = "apache";
$webgroup = "apache";

# --- END OF SETTINGS ---

### DONT CHANGE STUFF BELOW ###

$tarballname = "mod_survey-gentoo-$version";
if ($extraversion) { $tarballname .= "_$extraversion"; }
$tarballname .= ".tar.gz";

$cpcommand = "cp -f";
$windows   = 0;
$distrib   = "unix/gentoo";
$source    = ".";
$creator   = 0;

$conf{"_SURVEY_ALLOWAUTO"}      = 1;
$conf{"_SURVEY_ROOT"}           = $webdir;
$conf{"_SURVEY_SYSBASE"}        = $datadir;
$conf{"_SURVEY_LANG_DIRECTORY"} = $libdir . "/Lang/";
$conf{"_SURVEY_SENSIBLE"}       = "1";

mkdir($workdir);

# --- FILES AND DIRECTORIES ----

@directories = ("$libdir",
                "$libdir/Lang",
                "$libdir/Lang/src",
                "$libdir/Survey",
                "$libdir/Survey/Auth",
                "$libdir/Survey/Component",
                "$libdir/Survey/Export",
                "$datadir",
                "$sharedir",
                "$sharedir/docs",
                "$webdir",
                "$webdir/docs",
                "$webdir/images",
                "$webdir/examples",
                "$webdir/examples/basic",
                "$webdir/examples/markup",
                "$webdir/examples/routing",
                "$webdir/system",
                "/var/www/localhost/htdocs",
                "/etc/apache2/modules.d",
                "$libdir/templates",
                "$libdir/templates/default");

@libfiles = ("Lang/en.sl",
             "Lang/de.sl",
             "Lang/fr.sl",
             "Lang/sw.sl",
             "Lang/it.sl",
             "Lang/src/en.po",
             "Lang/src/de.po",
             "Lang/src/fr.po",
             "Lang/src/sw.po",
             "Lang/src/it.po",
             "Lang/src/nl.po",
             "Lang/po2sl.pl",
             "Lang/makepotemplate.pl",
             "Lang/mergepos.pl",
             "Lang/translate.pl",
             "startup.pl",
             "docinterpreter.pl",
             "genauth.pl",
             "installer.pl",
             "mkgentootarball.pl",
             "setupdomain.pl",
             "Survey/Admin.pm",
             "Survey/Argument.pm",
             "Survey/Constants.pm",
             "Survey/Data.pm",
             "Survey/DataAggressiveCache.pm",
             "Survey/DataEntry.pm",
             "Survey/DBCommon.pm",
             "Survey/Display.pm",
             "Survey/Debug.pm",
             "Survey/Language.pm",
             "Survey/Document.pm",
             "Survey/Slask.pm",
             "Survey/Handler.pm",
             "Survey/Persistance.pm",
             "Survey/PersistanceArg.pm",
             "Survey/Session.pm",
             "Survey/SessionArg.pm",
             "Survey/Statistics.pm",
             "Survey/Submit.pm",
             "Survey/System.pm",
             "Survey/Template.pm",
             "Survey/Upload.pm",
             "Survey/Auth/Auth.pm",
             "Survey/Auth/DBIAuth.pm",
             "Survey/Auth/FileAuth.pm",
             "Survey/Auth/TokenAuth.pm",
             "Survey/Component/Cati.pm",
             "Survey/Component/Env.pm",
             "Survey/Component/Matrix.pm",
             "Survey/Component/Lickert.pm",
             "Survey/Component/Route.pm",
             "Survey/Component/Custom.pm",
             "Survey/Component/Boolean.pm",
             "Survey/Component/List.pm",
             "Survey/Component/Ifroute.pm",
             "Survey/Component/Comment.pm",
             "Survey/Component/Memo.pm",
             "Survey/Component/Component.pm",
             "Survey/Component/Newline.pm",
             "Survey/Component/Constant.pm",
             "Survey/Component/Text.pm",
             "Survey/Component/Choice.pm",
             "Survey/Component/Caseroute.pm",
             "Survey/Component/Randomroute.pm",
             "Survey/Component/Sequence.pm",
             "Survey/Component/Security.pm",
             "Survey/Component/Submit.pm",
             "Survey/Component/SubmitError.pm",
             "Survey/Component/Import.pm",
             "Survey/Component/MailCopy.pm",
             "Survey/Component/DateTime.pm",
             "Survey/Component/Timer.pm",
             "Survey/Export/DelimitedFields.pm",
             "Survey/Export/EventBasedExport.pm",
             "Survey/Export/GroupedFrequency.pm",
             "Survey/Export/Frequency.pm",
             "Survey/Export/Descriptive.pm",
             "Survey/Export/StatUtils.pm",
             "Survey/Export/CaseBrowser.pm",
             "Survey/Export/Export.pm",
             "Survey/Export/FixedColumns.pm",
             "Survey/Export/HtmlTables.pm",
             "Survey/Export/SPSS.pm",
             "Survey/Export/SQL.pm",
             "Survey/Export/Template.pm",
             "Survey/Export/XML.pm",
             "Survey/Export/R.pm",
             "templates/default/Lickert.tmpl",
             "templates/default/Matrix.tmpl",
             "templates/default/List.tmpl",
             "templates/default/Choice.tmpl",
             "templates/default/ChoiceMulti.tmpl",
             "templates/default/Boolean.tmpl",
             "templates/default/Text.tmpl",
             "templates/default/Memo.tmpl");

@webfiles = ("examples/example.survey",
             "examples/examples.html",
             "examples/basic/choice.survey",
             "examples/basic/lickert.survey",
             "examples/basic/list.survey",
             "examples/basic/matrix.survey",
             "examples/basic/memo.survey",
             "examples/basic/text.survey",
             "examples/markup/markup.survey",
             "examples/markup/custom.survey",
             "examples/markup/angry.png",
             "examples/markup/extatic.png",
             "examples/markup/happy.png",
             "examples/markup/indifferent.png",
             "examples/markup/logo1.png",
             "examples/markup/logo2.png",
             "examples/markup/logo3.png",
             "examples/markup/logo4.png",
             "examples/markup/mh.gif",
             "examples/markup/sour.png",
             "examples/routing/random1.survey",
             "examples/routing/alt1.survey",
             "examples/routing/alt2.survey",
             "examples/routing/alt3.survey",
             "examples/routing/alt4.survey",
             "examples/routing/alt5.survey",
             "examples/routing/rfinal.survey",
             "examples/routing/boss.survey",
             "examples/routing/case-final.survey",
             "examples/routing/case.survey",
             "examples/routing/route-part2.survey",
             "examples/routing/route.survey",
             "examples/routing/worker.survey",
             "examples/routing/if.survey",
             "examples/routing/female0-15.survey",
             "examples/routing/error.html",
             "examples/routing/male0-15.survey",
             "examples/routing/male16-25.survey",
             "examples/routing/female16-25.survey",
             "examples/routing/if-final.survey",
             "examples/invisible.survey",
             "examples/dynamic.survey",
             "examples/dynamic2.survey",
             "index.html",
             "survey-3.2.5.dtd",
             "docs/index.html",
             "main.css",
             "system/slate.css",
             "system/eveca.css",
             "system/invert.css",
             "system/rose.css",
             "system/null.css",
             "system/msu.css",
             "system/data.css",
             "system/cloud.css",
             "system/eveca.css",
             "system/formal.css",
             "images/windows.jpg",
             "images/windows.jpg");

@sharefiles = ("docs/CHANGELOG.txt",
               "docs/CHANGELOG.txt.old",
               "docs/INSTALL.txt",
               "docs/Todo.txt",
               "docs/Dynamic_content.txt",
               "docs/Pushed.txt",
               "docs/LICENSE.txt",
               "README.txt",
               "survey.conf.sample",
               "survey-v3.0.0.dtd");
my ($dir);

$casebrowser = 1;
$descriptive = 1;
$frequency   = 1;
$gfrequency  = 1;
$lang        = "en";
$isa2        = "Off";
$protest     = 1;
$rootalias   = "/mod_survey/";
$ext         = ".survey";
$prext       = ".presentation";
$parser      = 1;
$display     = 1;
$dbiexists   = 1;

$destination = $libdir;

foreach $dir (@directories)
{
    $dir = "$workdir/$dir";
    $dir =~ s/\/\//\//g;
    print "mkdir -p $dir\n";
    system "mkdir -p $dir";
}

open(FIL, ">$workdir/etc/apache2/modules.d/98_mod_survey.conf")
  || die "Could not open survey.conf for writing";

print FIL "### Survey configuration\n\n";

print FIL "# -----------------------------------------\n";
print FIL "# Config new for 32x\n";
print FIL "# -----------------------------------------\n\n";
print FIL "PerlSetEnv   _SURVEY_ALLOWED_EXPORTS	\"html,spss,sql,delim,fixed,xml,r\"\n";

print FIL "PerlSetEnv   _SURVEY_OPTIONAL_EXPORTS \"";

$first = 1;
if ($casebrowser) { print FIL "browse"; $first = 0; }
if (!$first)      { print FIL ","; }
if ($descriptive) { print FIL "desc";   $first = 0; }
if (!$first)      { print FIL ","; }
if ($frequency)   { print FIL "freq";   $first = 0; }
if (!$first)      { print FIL ","; }
if ($gfrequency)  { print FIL "gfreq";  $first = 0; }

print FIL "\"\n\n";

print FIL "PerlSetEnv   _SURVEY_EXPORT_html	\"Survey::Export::HtmlTables\"\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_spss	\"Survey::Export::SPSS\"\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_xml		\"Survey::Export::XML\"\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_sql		\"Survey::Export::SQL\"\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_delim	\"Survey::Export::DelimitedFields\"\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_fixed	\"Survey::Export::FixedColumns\"\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_r	\"Survey::Export::R\"\n";

print FIL "PerlSetEnv   _SURVEY_EXPORT_browse		\"Survey::Export::CaseBrowser\"\n\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_desc		\"Survey::Export::Descriptive\"\n\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_freq		\"Survey::Export::Frequency\"\n\n";
print FIL "PerlSetEnv   _SURVEY_EXPORT_gfreq		\"Survey::Export::GroupedFrequency\"\n\n";

print FIL "# -----------------------------------------\n";
print FIL "# Mail settings\n";
print FIL "# -----------------------------------------\n";
print FIL "PerlSetEnv   _SURVEY_SMTP_HOST \"127.0.0.1\"\n";
print FIL "PerlSetEnv   _SURVEY_MAIL_ADMIN \"root\@localhost\"\n\n";

print FIL "# -----------------------------------------\n";
print FIL "# Set global survey configuration variables\n";
print FIL "# -----------------------------------------\n\n";

print FIL "# Directory where caches, autodata files and keys should be stored\n";
print FIL "PerlSetEnv   _SURVEY_SYSBASE        \"" . $conf{"_SURVEY_SYSBASE"} . "/\"\n\n";

print FIL "# Whether parser is allowed to write and read cache at all (overrides\n";
print FIL "# ALLOWCACHE in survey docs if set to 0)\n";
print FIL "PerlSetEnv   _SURVEY_PARSERCACHE    $parser\n\n";

print FIL "# Whether display module is allowed to write and read html output cache \n";
print FIL "# (overrides ALLOWCACHE in survey docs if set to 0)\n";
print FIL "PerlSetEnv   _SURVEY_DISPLAYCACHE   $display\n\n";

print FIL "# Where mod_survey is installed\n";
print FIL "PerlSetEnv   _SURVEY_HOME           \"" . $destination . "/\"\n\n";

print FIL "# Where mod_survey web root is\n";
print FIL "PerlSetEnv   _SURVEY_ROOT           \"" . $conf{"_SURVEY_ROOT"} . "/\"\n\n";

print FIL "# Which root alias to use for doc links and similar\n";
print FIL "PerlSetEnv   _SURVEY_ROOT_ALIAS     \"$rootalias\"\n\n";

print FIL "# Whether we have installed on a sensible (unixoid) system\n";
print FIL "PerlSetEnv   _SURVEY_SENSIBLE       1\n\n";

print FIL "# -------------------------\n";
print FIL "# Security related settings\n";
print FIL "# -------------------------\n\n";

print FIL "# If you plan to use DBI tables, you must install DBI 1.13 or later and\n";
print FIL "# set the below to 1. Even if you have DBI installed, you may want to\n";
print FIL "# choose to not enable it in Mod_Survey for security reasons.\n";
print FIL "PerlSetEnv   _SURVEY_USEDBI         $dbiexists\n\n";

print FIL "# Whether is is allowed to let system automatically handle data submission\n";
print FIL "# It would a stupid idea to change this unless you know what you are doing.\n";
print FIL "PerlSetEnv   _SURVEY_ALLOWAUTO      1\n\n";

print FIL "# Whether submit should protest at illegal characters and ask the user\n";
print FIL "# to go back and correct these. This will be ignored if _SURVEY_USENEWAUTo\n";
print FIL "PerlSetEnv   _SURVEY_PROTESTILLEGAL 1\n\n";

print FIL "# Whether to use safe delimiters in the AutoData file. This makes things\n";
print FIL "# safer, but also makes the AutoData file partially binary. Most users \n";
print FIL "# want to enable this.\n";
print FIL "PerlSetEnv   _SURVEY_USENEWAUTO     1\n\n";

print FIL "# -------------------------------------\n";
print FIL "# Internationalization related settings\n";
print FIL "# -------------------------------------\n\n";

print FIL "# Where to find language files\n";
print FIL "PerlSetEnv   _SURVEY_LANG_DIRECTORY \"" . $conf{"_SURVEY_LANG_DIRECTORY"} . "\"\n\n";

print FIL "# Which language to use\n";
print FIL "PerlSetEnv   _SURVEY_LANG           \"$lang\"\n\n";

print FIL "# ---------------------------------------------------\n";
print FIL "# Startup check and addition of survey folder to \@INC\n";
print FIL "# ---------------------------------------------------\n\n";

print FIL "Perlrequire \"$destination/startup.pl\"\n\n";

print FIL "# --------------------------------------------------------------------\n";
print FIL "# Add type for survey files. If you change this, please change pattern \n";
print FIL "# for the files tag below too\n";
print FIL "# --------------------------------------------------------------------\n\n";

print FIL "AddType text/html $ext\n\n";

print FIL "# ---------------------------------------------------------------\n";
print FIL "# Match all files containing \"$ext\" (tail \"*\" is necessary for \n";
print FIL "# admin part). Enclosure in Directory necessary to avoid clash with\n";
print FIL "# mod_proxy.\n";
print FIL "# ---------------------------------------------------------------\n\n";

print FIL "<Directory \"/\">\n";
print FIL "  <Files *$ext*>\n";
print FIL "    SetHandler perl-script\n";
print FIL "    PerlHandler Survey::Handler\n";
print FIL "    PerlSendHeader \"$isa2\"\n";
print FIL "    AuthType Basic\n";
print FIL "    AuthName Mod_Survey\n";
print FIL "    PerlAuthenHandler Apache::AuthAny\n";
print FIL "  </Files>\n";
print FIL "</Directory>\n\n";

print FIL "# ---------------------------------------------------------------\n";
print FIL "# Settings for alias/directory containing docs, examples, creator\n";
print FIL "# scripts and similar\n";
print FIL "# ---------------------------------------------------------------\n\n";

print FIL "Alias $rootalias \"" . $conf{"_SURVEY_ROOT"} . "/\"\n\n";

print FIL "<Directory " . $conf{"_SURVEY_ROOT"} . ">\n";
print FIL "  Options +All\n";
print FIL "  AllowOverride All\n";
print FIL "  Order allow,deny\n";
print FIL "  Allow from all\n";
print FIL "</Directory>\n\n";

close(FIL);

print "\n\n\n";

foreach $lib (@libfiles)
{
    print "cp $source/$lib $workdir/$libdir/$lib\n";
    system "cp $source/$lib $workdir/$libdir/$lib";
}

foreach $sh (@sharefiles)
{
    print "cp $source/$sh $workdir/$sharedir/$sh\n";
    system "cp $source/$sh $workdir/$sharedir/$sh";
}

foreach $web (@webfiles)
{
    print "cp $source/webroot/$web $workdir/$webdir/$web\n";
    system "cp $source/webroot/$web $workdir/$webdir/$web";
}

print "touch $workdir/$datadir/.keep\n";
system "touch $workdir/$datadir/.keep";

print "ln -s ../../mod_survey $workdir/var/www/localhost/htdocs/mod_survey\n";
system "ln -s ../../mod_survey $workdir/var/www/localhost/htdocs/mod_survey";

print "ln -s sw.sl $workdir/$libdir/Lang/sv.sl\n";
system "ln -s sw.sl $workdir/$libdir/Lang/sv.sl";

print "tar -C $workdir -cvzf $workroot/$tarballname ets usr var\n";
system "tar -C $workdir  -cvzf $workroot/$tarballname etc usr var";

1;

