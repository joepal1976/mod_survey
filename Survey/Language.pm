#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Language;

use Exporter;
use strict;

use vars qw($crap %alternates $language_file);

BEGIN
{
    use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    @ISA = qw(Exporter);

    @EXPORT = qw(lprint);

    # use below line instead of one above if backward-compat with
    # old Locale::PGetText
    # @EXPORT = qw(lprint get1text);
}

return 1;

sub setupLanguage
{
    ($crap, $language_file) = @_;
    Survey::Language->buildHash($language_file);
}

# Backwards compatibility with Locale::PGetText, should be removed
# sooner or later
# sub get1text
# {
#  return lprint(shift);
# }

sub lprint
{
    my ($original) = shift;

    my (%strings) = %{ $alternates{$language_file} };

    if ($ENV{"_INTERNAL_LANGUAGE_OVERRIDE"})
    {
        %strings = %{ $alternates{ $ENV{"_INTERNAL_LANGUAGE_OVERRIDE"} } };
    }

    my ($text) = $strings{$original};
    return $text || $original;
}

sub buildHash
{
    my ($crap, $langfile) = @_;

    if ($alternates{$langfile})
    {
        return 0;
    }

    my (%strings);
    if (!-r $langfile)
    {
        return "$langfile does not exist or is not readable";
    }

    my ($num) = 0;

    if (open(FIL, $langfile))
    {
        my (@data) = <FIL>;
        my ($raw) = join('', @data);

        my (@lines) = split(/\x01/, $raw);
        my ($line);

        foreach $line (@lines)
        {
            if ($line)
            {
                my ($id, $trans) = split(/\x02/, $line, 2);
                $strings{$id} = $trans;
                $num++;
            }
        }
        close(FIL);
    }
    else
    {
        die "could not open language file";
    }

    $alternates{$langfile} = \%strings;

    return;
}

sub compile
{
    my ($crap, $lang) = @_;
    my ($src) = "src/$lang" . ".po";
    my ($out) = "$lang" . ".sl";
    print "Input file: $src\n";
    print "Output file: $out\n";

    if (open(SRC, $src))
    {
        if (open(OUT, ">$out"))
        {
            my (@lines) = <SRC>;
            close(SRC);

            my ($line);
            my ($was)   = "";
            my ($id)    = "";
            my ($trans) = "";

            print "\nCOMPILING: ";

            foreach $line (@lines)
            {
                my ($skip) = 0;

                chop($line);

                if (!$line) { $skip = 1; }
                if ($line =~ m/^\#/) { $skip = 1; }

                if (!$skip)
                {
                    if ($line =~ m/^msgid/)
                    {
                        if ($was && ($id ne "") && ($trans ne ""))
                        {
                            print ".";
                            print OUT $id . "\x02" . $trans . "\x01";
                        }
                        $was = "msgid";
                        $line =~ s/^msgid//;
                        $line =~ s/^[\ \x09]+\"//;
                        $line =~ s/\"$//;

                        $id = $line;
                    }
                    else
                    {
                        if ($line =~ m/^msgstr/)
                        {
                            $was = "msgstr";
                            $line =~ s/^msgstr//;
                            $line =~ s/^[\ \x09]+\"//;
                            $line =~ s/\"$//;

                            $trans = $line;
                        }
                        else
                        {
                            $line =~ s/^[\ \x09]+\"//;
                            $line =~ s/\"$//;

                            if ($was eq "msgid")
                            {
                                $id .= $line;
                            }
                            else
                            {
                                $trans .= $line;
                            }
                        }
                    }
                }
            }
            print "\n\nDone.\n";
            close(OUT);
        }
        else
        {
            print "\nERROR: Could not open $out for writing\n";
            return;
        }
    }
    else
    {
        print "\nERROR: Could not open $src for reading\n";
        return;
    }
}

1;

