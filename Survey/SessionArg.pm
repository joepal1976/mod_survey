#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::SessionArg;
use strict;

use Survey::Language;

sub new
{
    my $self = {};

    if ($ENV{"REQUEST_METHOD"} eq "POST")
    {
        read(STDIN, $ENV{"QUERY_STRING"}, $ENV{'CONTENT_LENGTH'});
        $ENV{"REQUEST_METHOD"} = "GET";
    }

    $self->{QUERY_STRING} = $ENV{"QUERY_STRING"} || "";

    #print $self->{QUERY_STRING};

    bless($self);

    $self->ParseQuery();

    return ($self);
}

sub ParseQuery
{
    my ($self) = shift;
    my (@args, $cell, $name, $value, $fullName);

    $self->{QUERY_STRING} =~ s/\+/\ /g;
    @args = split(/&/, $self->{QUERY_STRING});
    map s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg, @args;
    foreach $cell (@args)
    {
        ($name, $value) = split(/=/, $cell, 2);
        $fullName = "ARG_" . $name;
        $self->{$fullName} = $value;
    }

    1;
}

sub ArgByName
{
    my ($self, $name) = @_;
    return $self->{ "ARG_" . $name };
}

1;

