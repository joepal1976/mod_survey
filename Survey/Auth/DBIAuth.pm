#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

# This module permits you to manage authentication
# using DBI to connect to a db where you've stored
# your auth's stuff

package Survey::Auth::DBIAuth;
use strict;

@Survey::Auth::DBIAuth::ISA = qw(Survey::Auth::Auth);

use Survey::Language;

use CGI qw/:standard/;
use CGI::Cookie;

sub ScanDbForNameAndPassword
{
    my $self    = shift;
    my $Dsn     = shift;
    my $DbUser  = shift;
    my $DbPass  = shift;
    my $Table   = shift;
    my $NameCol = shift;
    my $PwdCol  = shift;
    my $dbiEnc  = shift;

    my $currentUser = $self->{USER};
    my $currentPass = $self->{PASS};

    my $doc = $self->{DOCUMENT};

    # connect to database
    my $dbh = DBI->connect($Dsn, $DbUser, $DbPass);

    # if connection to database was not successful
    if (!$dbh)
    {
        $doc->{ERROR} = "could not connect to database: $DBI::errstr";
        return 0;
    }

    # query for password in the database
    my $query = "select $PwdCol from $Table where $NameCol = \'$currentUser\';";

    # execute query
    my $result;
    if (!($result = $dbh->prepare($query)))
    {
        $doc->{ERROR} = $DBI::errstr;
        return 0;
    }
    $result->execute();

    # get findings
    my $resultHash = $result->fetchrow_hashref();

    # if nothing was found
    if (!$resultHash) { return 0; }

    # get the password in the database
    my $passwordInDb = $resultHash->{$PwdCol};

    # disconnect from database
    $result->finish();
    $dbh->disconnect();

    if ($dbiEnc ne "no")
    {

        # Password is encrypted, perform salt/crypt ops (JP/20041129)

        # get the salt
        my $salt = substr($passwordInDb, 0, 2);
        return ($passwordInDb eq crypt($currentPass, $salt));
    }
    else
    {

        # Password is stored plaintext, no salt/crypt ops (JP/20041129)
        return ($passwordInDb eq $currentPass);
    }

    return 0;
}

# scans the resps db for currently logged-in user
sub IsValidRespondent
{
    my $self = shift;

    my $doc = $self->{DOCUMENT};

    my $dbiDsn     = $doc->{"SECURITY_DBIAUTH_DBIDSN"};
    my $dbiUser    = $doc->{"SECURITY_DBIAUTH_DBIUSER"};
    my $dbiPass    = $doc->{"SECURITY_DBIAUTH_DBIPASS"};
    my $dbiTable   = $doc->{"SECURITY_DBIAUTH_RESP-TABLE"};
    my $dbiNameCol = $doc->{"SECURITY_DBIAUTH_RESP-NAMES-COL"};
    my $dbiPwdCol  = $doc->{"SECURITY_DBIAUTH_RESP-PWD-COL"};
    my $dbiEnc     = $doc->{"SECURITY_DBIAUTH_ENCRYPTED"};

    return $self->ScanDbForNameAndPassword($dbiDsn, $dbiUser, $dbiPass, $dbiTable, $dbiNameCol, $dbiPwdCol, $dbiEnc);
}

# scans the users db for currently logged-in user
sub IsValidUser
{
    my $self = shift;

    my $doc = $self->{DOCUMENT};

    my $dbiDsn     = $doc->{"SECURITY_DBIAUTH_DBIDSN"};
    my $dbiUser    = $doc->{"SECURITY_DBIAUTH_DBIUSER"};
    my $dbiPass    = $doc->{"SECURITY_DBIAUTH_DBIPASS"};
    my $dbiTable   = $doc->{"SECURITY_DBIAUTH_USER-TABLE"};
    my $dbiNameCol = $doc->{"SECURITY_DBIAUTH_USER-NAMES-COL"};
    my $dbiPwdCol  = $doc->{"SECURITY_DBIAUTH_USER-PWD-COL"};
    my $dbiEnc     = $doc->{"SECURITY_DBIAUTH_ENCRYPTED"};

    return $self->ScanDbForNameAndPassword($dbiDsn, $dbiUser, $dbiPass, $dbiTable, $dbiNameCol, $dbiPwdCol, $dbiEnc);
}

