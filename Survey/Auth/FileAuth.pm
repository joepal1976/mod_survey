#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

# This module permits you to manage authentication
# using an htaccess file to store your auth's stuff

package Survey::Auth::FileAuth;
use strict;

@Survey::Auth::FileAuth::ISA = qw(Survey::Auth::Auth);

use Survey::Language;

use CGI qw/:standard/;
use CGI::Cookie;

sub ScanFileForNameAndPassword
{
    my $self      = shift;
    my $File      = shift;
    my $User      = shift;
    my $ClearPass = shift;

    # if the file doesn't exist
    if (!-r $File)
    {
        $self->{ERROR}     = lprint("The RESPFILE") . " $File " . lprint("does not exist or is not readable");
        $self->{ERRORCODE} = 99;
        return;
    }

    # read the file
    open(FIN, $File) || die "Aaargh";

    # for every line in the file
    while (<FIN>)
    {

        # remove linebreak at the end
        chomp $_;

        # split line into username and password
        my ($userInFile, $passInFile) = split(/:/, $_, 2);

        # get the salt to encrypt the password given by the user
        my $salt = substr($passInFile, 0, 2);

        # if the username of current user is found in the file...
        if ($User eq $userInFile)
        {

            # ... and if the password is OK
            if (crypt($ClearPass, $salt) eq $passInFile)
            {
                close(FIN);
                return 1;
            }
        }

    }
    close(FIN);

    return 0;
}

# scans the resps db for currently logged-in user
sub IsValidRespondent
{
    my $self = shift;

    my $doc = $self->{DOCUMENT};

    my $reqAuth = $doc->GetOption("REQAUTH");

    #get respfile
    my $file = $doc->GetOption("RESPFILE");
    return $self->ScanFileForNameAndPassword($file, $self->{USER}, $self->{PASS});
}

# scans the users file for currently logged-in user
sub IsValidUser
{
    my $self = shift;

    my $doc = $self->{DOCUMENT};

    my $reqAuth = $doc->GetOption("REQAUTH");

    # get userfile
    my $file = $doc->GetOption("USERFILE");
    return $self->ScanFileForNameAndPassword($file, $self->{USER}, $self->{PASS});
}
