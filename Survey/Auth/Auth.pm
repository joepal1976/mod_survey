#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Auth::Auth;
use strict;

use Survey::Language;

use CGI qw/:standard/;
use CGI::Cookie;

sub new
{
    my ($crap, $doc, $arg, $ses, $user, $pass) = @_;

    my ($self) = {};
    my ($class) = ref($crap) || $crap;

    bless($self, $class);

    $self->{DOCUMENT} = $doc;
    $self->{ARGUMENT} = $arg;
    $self->{SESSION}  = $ses;

    $self->{USER} = $user;
    $self->{PASS} = $pass;

    $self->{AUTH_BAD_PASSWORD} = 0;
    $self->{AUTH_NOT_REQUIRED} = 1;
    $self->{AUTH_RESPONDENT}   = 2;
    $self->{AUTH_USER}         = 3;
    $self->{AUTH_CLOSED}       = 4;

    return $self;
}

# check USER parameter of DATA, ANSWER, FLUSH, DEBUG, SOURCE tag
sub CheckUserRestriction
{
    my ($self)   = shift;
    my ($doc)    = $self->{DOCUMENT};
    my ($action) = $self->{ARGUMENT}->ArgByName("action");
    my ($admin)  = $self->{ARGUMENT}->ArgByName("admin");

    my ($users) = "";

    # get the users which are allowed to perform current action

    # display data
    if ($action eq "data") { $users = $doc->{SECURITY_DATA_USERS}; }

    # answere surveys
    elsif ($action eq "display") { $users = $doc->{SECURITY_ANSWER_USERS}; }

    # do administration
    elsif ($action eq "admin")
    {

        # delete data
        if ($admin eq "flush") { $users = $doc->{SECURITY_FLUSH_USERS}; }

        # view debug information
        elsif ($admin eq "debug") { $users = $doc->{SECURITY_DEBUG_USERS}; }

        # view source code of survey file
        elsif ($admin eq "source") { $users = $doc->{SECURITY_SOURCE_USERS}; }
    }

    # if users were given
    if ($users)
    {

        # current user
        my ($user) = $self->{USER};

        # split users from USERS parameter
        my (@upart) = split(/,/, $users);
        my ($u);

        # if current user is given in USERS parameter
        foreach $u (@upart)
        {
            if ($user eq $u)
            {
                return 1;
            }
        }
        return 0;
    }

    return 1;    # Access is OK
}

# check HOST parameter of DATA, ANSWER, FLUSH, DEBUG, SOURCE tag
sub CheckHostRestriction
{
    my ($self)   = shift;
    my ($doc)    = $self->{DOCUMENT};
    my ($action) = $self->{ARGUMENT}->ArgByName("action");
    my ($admin)  = $self->{ARGUMENT}->ArgByName("admin");

    my ($hosts) = "";

    # get the hosts from current action is allowd

    # view data
    if ($action eq "data") { $hosts = $doc->{SECURITY_DATA_HOSTS}; }

    # answer surveys
    elsif ($action eq "display") { $hosts = $doc->{SECURITY_ANSWER_HOSTS}; }

    # do some administration
    elsif ($action eq "admin")
    {

        # delete data
        if ($admin eq "flush") { $hosts = $doc->{SECURITY_FLUSH_HOSTS}; }

        # view debug information
        elsif ($admin eq "debug") { $hosts = $doc->{SECURITY_DEBUG_HOSTS}; }

        # view source code of current survey file
        elsif ($admin eq "source") { $hosts = $doc->{SECURITY_SOURCE_HOSTS}; }
    }

    # if hosts where given in HOSTS-parameter
    if ($hosts)
    {

        # get current host
        my ($host) = $ENV{"REMOTE_ADDR"};

        # split hosts from HOSTS parameter
        my (@hpart) = split(/,/, $hosts);

        my ($h);

        # check if current host is given in HOSTS parameter
        foreach $h (@hpart)
        {
            if ($host eq $h)
            {
                return 1;
            }
        }

        $self->{ERROR}     = lprint("Access denied for host") . " " . $host;
        $self->{ERRORCODE} = 99;
        return 0;
    }

    return 1;    # Access is OK
}

# check PASSWORD parameter of DATA, ANSWER, FLUSH, DEBUG, SOURCE tag
sub CheckPasswordRestriction
{
    my ($self) = shift;

    my ($action) = $self->{ARGUMENT}->ArgByName("action");
    my ($admin)  = $self->{ARGUMENT}->ArgByName("admin");

    my ($doc)     = $self->{DOCUMENT};
    my ($session) = $doc->{SESSION};

    # get the password from the session
    my ($pw_sess) = $session->getValue("p_password");

    # get password from GET/POST data
    my ($pw_arg) = $self->{ARGUMENT}->ArgByName("password");

    my ($pw_req) = "";

    # if a password was given via GET/POST
    if ($pw_arg) { $pw_sess = $pw_arg; }

    # view data
    if ($action eq "data") { $pw_req = $doc->{SECURITY_DATA_PASSWORD}; }

    # answer surveys
    elsif ($action eq "display") { $pw_req = $doc->{SECURITY_ANSWER_PASSWORD}; }

    # do some administration
    elsif ($action eq "admin")
    {

        # delete data
        if ($admin eq "flush")
        {
            $pw_req = $doc->{SECURITY_FLUSH_PASSWORD};
        }

        # view debug information
        elsif ($admin eq "debug") { $pw_req = $doc->{SECURITY_DEBUG_PASSWORD}; }

        # view sourcecode of current survey file
        elsif ($admin eq "source") { $pw_req = $doc->{SECURITY_SOURCE_PASSWORD}; }
    }

    # if a password is given in PASSWORD parameter

    if ($pw_req)
    {
        if ($pw_req ne $pw_sess)
        {
            return 0;
        }
    }

    # save password to session
    $session->setValue("p_password", $pw_sess);

    return 1;    # Access is OK
}

# scans the resps repository for currently logged-in user
# this method has to be overridden on all auth descendants
sub IsValidRespondent
{
    my ($self) = shift;
    die "OO Exception: The IsValidRespondent() method must be overridden (by all descendants of Survey::Auth::Auth)";
    1;
}

# scans the users repository for currently logged-in user
# this method has to be overridden on all auth descendants
sub IsValidUser
{
    my ($self) = shift;
    die "OO Exception: The IsValidUser() method must be overridden (by all descendants of Survey::Authentication)";
    1;
}

# returns either "AUTH_NOT_REQUIRED" or "AUTH_BAD_PASSWORD"
# for a user which typed in username + password
sub GetUserLevel
{
    my ($self) = shift;

    my ($action) = $self->{ARGUMENT}->ArgByName("action");
    my ($admin)  = $self->{ARGUMENT}->ArgByName("admin");

    my $session = $self->{SESSION};

    # for data module
    if ($action eq "data") { return $self->CheckDataCredentials(); }

    # answer module
    elsif ($action eq "display")
    {
        my $isAuth = $self->CheckAnswerCredentials();
        return $isAuth;
    }

    elsif ($action eq "admin")
    {
        if ($admin eq "flush") { return $self->CheckFlushCredentials(); }

        elsif ($admin eq "debug") { return $self->CheckDebugCredentials(); }

        elsif ($admin eq "source") { return $self->CheckSourceCredentials(); }
    }

    return $self->{AUTH_NOT_REQUIRED};
}

# check if current user is allowed to answer the survey
sub CheckAnswerCredentials
{
    my ($self)  = shift;
    my ($doc)   = $self->{DOCUMENT};
    my ($level) = $doc->{SECURITY_ANSWER_LEVEL};
    return $self->CheckCredentials($level, "answer");
}

# check if current user is allowed to view data
sub CheckDataCredentials
{
    my ($self)  = shift;
    my ($doc)   = $self->{DOCUMENT};
    my ($level) = $doc->{SECURITY_DATA_LEVEL};

    return $self->CheckCredentials($level, "data");
}

# check if current user is allowed to delete data
sub CheckFlushCredentials
{
    my ($self)  = shift;
    my ($doc)   = $self->{DOCUMENT};
    my ($level) = $doc->{SECURITY_FLUSH_LEVEL};
    return $self->CheckCredentials($level, "flush");
}

# check if current user is allowed to view debug information
sub CheckDebugCredentials
{
    my ($self)  = shift;
    my ($doc)   = $self->{DOCUMENT};
    my ($level) = $doc->{SECURITY_DEBUG_LEVEL};
    return $self->CheckCredentials($level, "debug");
}

# check if current user is allowed to view sourcecode of current survey file
sub CheckSourceCredentials
{
    my ($self)  = shift;
    my ($doc)   = $self->{DOCUMENT};
    my ($level) = $doc->{SECURITY_SOURCE_LEVEL};
    return $self->CheckCredentials($level, "source");
}

# check if current users is allowed to perform a specific action
sub CheckCredentials
{
    my ($self, $level, $mod) = @_;

    # if nobody is allowed
    if ($level eq "closed")
    {
        $self->{ERROR}     = lprint("The ") . " $mod " . lprint("module is switched off");
        $self->{ERRORCODE} = 99;
        return $self->{AUTH_CLOSED};
    }

    # everybody is allowed
    elsif ($level eq "open") { return $self->{AUTH_NOT_REQUIRED}; }

    # only users listed in RESPFILE are allowed
    elsif ($level eq "respondent")
    {

        # check if username + password are correct
        if (!$self->IsValidRespondent()) { return $self->{AUTH_BAD_PASSWORD}; }
    }

    # only users listed in USERFILE are allowed
    elsif ($level eq "user")
    {

        # check if username + password are correct
        if (!$self->IsValidUser()) { return $self->{AUTH_BAD_PASSWORD}; }
    }

    # everybody listed in USERFILE or RESPFILE is allowed
    elsif ($level eq "any")
    {

        # check if username + password are correct
        if ((!$self->IsValidUser()) && (!$self->IsValidRespondent()))
        {
            return $self->{AUTH_BAD_PASSWORD};
        }
    }

    return $self->{AUTH_NOT_REQUIRED};
}

sub Error
{
    my ($self) = shift;
    return $self->{ERROR};
}

sub PrintErrorDescription
{
    my $self = shift;

    my ($e)     = $self->{ERRORCODE};
    my ($found) = 0;

    my ($lb) = "<b>\&lt\;";
    my ($rb) = "\&gt\;</b>";

    print "    <br /><br />\n    ";

    if ($e)
    {
        if ($e eq 1)
        {
            print "<b><i>[" . lprint("AUTHENTICATION ERROR 1, An authentication error occurred") . "]</i></b> ";
            print lprint("This is just a sample error. ");
            $found = 1;
        }

        if (!$found)
        {
            print lprint("This error is not in the knowledge base, so I guess the programmer has made a mistake.");
        }
    }
    else
    {
        print "<i>[" . lprint("AUTHENTICATION ERROR 0, NO ERROR") . "]</i> ";
        print lprint(
                "No error has occured, but the programmer has for some reason called PrintErrorDescription() anyway. ");
        print lprint("I guess the programmer has again made a mistake, something that is a very common event indeed.");
    }
    print "\n    <br /><br />\n";

    1;
}

1;
