#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2005  bugant (YaaCers - Demetra - www.opinioni.net)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

# This module permits you to manage authentication
# using an htaccess file to store your auth's stuff

package Survey::Auth::TokenAuth;

@Survey::Auth::TokenAuth::ISA = qw(Survey::Auth::Auth);

# Don't include apache stuff directly in modules
#use Apache2::Const qw(:common);
#use Apache::File;

use Survey::Language;

use CGI qw/:standard/;
use CGI::Cookie;

use strict;

sub ScanFileForToken
{
    my $self  = shift;
    my $File  = shift;
    my $Token = shift;

    if (!$Token || !$File) { return 0 }

    # if the file doesn't exist
    if (!-r $File)
    {
        $self->{ERROR} = lprint("The RESPFILE") . " $File " . lprint("does not exist or is not readable");

        $self->{ERRORCODE} = 99;
        return;
    }

    # read the file
    open(FIN, $File) || die "Aaargh";

    # for every line in the file
    while (<FIN>)
    {

        # remove linebreak at the end
        chomp $_;

        # read current token
        my ($tokenInFile) = $_;

        # if the username of current user is found in the file...
        if ($Token eq $tokenInFile)
        {
            close(FIN);
            return 1;
        }
    }
    close(FIN);

    return 0;
}

sub ScanDbForToken
{
    my ($self, $Dsn, $DbUser, $DbPass, $Table, $NameCol, $Token) = @_;

    if (!$Token) { return 0; }
    my $doc = $self->{DOCUMENT};

    # connect to database
    my $dbh = DBI->connect($Dsn, $DbUser, $DbPass);

    # if connection to database was not successful
    if (!$dbh)
    {
        $self->{ERROR}     = "could not connect to database: $DBI::errstr";
        $self->{ERRORCODE} = 99;
        return 0;
    }

    # query for token retrive
    my $query = "select $NameCol from $Table where $NameCol = \'$Token\';";

    # execute query
    my $result;
    if (!($result = $dbh->prepare($query)))
    {
        $self->{ERROR}     = $DBI::errstr;
        $self->{ERRORCODE} = 99;
        return 0;
    }
    $result->execute();

    # get findings
    my $resultHash = $result->fetchrow_hashref();

    # if nothing was found
    if (!$resultHash)
    {

        # disconnect from database
        $result->finish();
        $dbh->disconnect();
        return 0;
    }

    my $tokenInDb = $resultHash->{$NameCol};

    # disconnect from database
    $result->finish();
    $dbh->disconnect();

    return ($tokenInDb eq $Token);
}

# scans the resps db for currently logged-in user
sub IsValidRespondent
{
    my $self = shift;

    my $doc = $self->{DOCUMENT};

    my $reqAuth = $doc->GetOption("REQAUTH");

    my $tokenMedia;
    my $dbiDsn;
    my $dbiUser;
    my $dbiPass;
    my $dbiTable;
    my $dbiNameCol;
    my $file;

    #get respfile
    if ($doc->GetOption("RESPFILE"))
    {
        $file       = $doc->GetOption("RESPFILE");
        $tokenMedia = "file";
    }

    # there's a db?
    if ($doc->{"SECURITY_TOKENAUTH_DBIDSN"})
    {
        $dbiDsn     = $doc->{"SECURITY_TOKENAUTH_DBIDSN"};
        $dbiUser    = $doc->{"SECURITY_TOKENAUTH_DBIUSER"};
        $dbiPass    = $doc->{"SECURITY_TOKENAUTH_DBIPASS"};
        $dbiTable   = $doc->{"SECURITY_TOKENAUTH_RESP-TABLE"};
        $dbiNameCol = $doc->{"SECURITY_TOKENAUTH_RESPCOL"};

        # db takes precedence on file
        $tokenMedia = "db";
    }

    my $token = $self->{ARGUMENT}->ArgByName($doc->GetOption("TOKENARG"));

    if (!$token)
    {
        $token = $self->{SESSION}->getValue($doc->GetOption("TOKENARG"));
    }

    if ($token)
    {
        $self->{SESSION}->setValue($doc->GetOption("TOKENARG"), $token);
    }

    $doc->{SESSION}->setValue("TOKENAPPEND", $doc->GetOption("TOKENARG") . "=" . $token);

    if ($tokenMedia eq "file")
    {
        return $self->ScanFileForToken($file, $token);
    }
    elsif ($tokenMedia eq "db")
    {
        return $self->ScanDbForToken($dbiDsn, $dbiUser, $dbiPass, $dbiTable, $dbiNameCol, $token);
    }
    else
    {

        #no media matching
        #some error messages has to be put here
        # TODO
        return 0;
    }
}

# scans the users file for currently logged-in user
sub IsValidUser
{
    my $self = shift;

    my $doc = $self->{DOCUMENT};

    my $reqAuth = $doc->GetOption("REQAUTH");

    my $tokenMedia;
    my $dbiDsn;
    my $dbiUser;
    my $dbiPass;
    my $dbiTable;
    my $dbiNameCol;
    my $file;

    # get userfile
    if ($doc->GetOption("USERFILE"))
    {
        $file       = $doc->GetOption("USERFILE");
        $tokenMedia = "file";
    }

    # there's a db?
    if ($doc->{"SECURITY_TOKENAUTH_DBIDSN"})
    {
        $dbiDsn     = $doc->{"SECURITY_TOKENAUTH_DBIDSN"};
        $dbiUser    = $doc->{"SECURITY_TOKENAUTH_DBIUSER"};
        $dbiPass    = $doc->{"SECURITY_TOKENAUTH_DBIPASS"};
        $dbiTable   = $doc->{"SECURITY_TOKENAUTH_USER-TABLE"};
        $dbiNameCol = $doc->{"SECURITY_TOKENAUTH_USERCOL"};

        # db takes precedence on file
        $tokenMedia = "db";
    }

    my $token = $self->{ARGUMENT}->ArgByName($doc->GetOption("TOKENARG"));

    if (!$token)
    {
        $token = $self->{SESSION}->getValue($doc->GetOption("TOKENARG"));
    }

    if ($token)
    {
        $self->{SESSION}->setValue($doc->GetOption("TOKENARG"), $token);
    }

    $doc->{SESSION}->setValue("TOKENAPPEND", $doc->GetOption("TOKENARG") . "=" . $token);

    if ($tokenMedia eq "file")
    {
        return $self->ScanFileForToken($file, $token);
    }
    elsif ($tokenMedia eq "db")
    {
        return $self->ScanDbForToken($dbiDsn, $dbiUser, $dbiPass, $dbiTable, $dbiNameCol, $token);
    }
    else
    {

        #no media matching
        #some error messages has to be put here
        # TODO
        return 0;
    }
}

