#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Export::CaseBrowser;
use strict;

@Survey::Export::CaseBrowser::ISA = qw(Survey::Export::EventBasedExport);

use Survey::Language;
use Survey::Slask;
#use Survey::Export::EventBasedExport;
use Text::ParseWords;

use CGI;

sub new
{
    my ($crap, $doc, $arg, $data) = @_;

    my ($self) = {};
    bless $self;

    $self->DoCommonStuff($doc, $arg, $data);

    return $self;
}

sub GetTitle
{
    my ($self) = shift;
    return lprint("Case Browser");
}

sub GetCredits
{
    my ($self) = shift;
    return lprint("This is an optional demonstration module.");
}

sub printHead
{
    my ($self) = shift;
    my ($case) = $self->{THISCASE};
    Survey::Slask->HtmlHead();
    print "    <title>" . $self->{DOCUMENT}->GetOption("TITLE") . "</title>\n";
    print "    <style><!--\n";
    print "      body     { background-color: white; color: black; }\n";
    print "      .tblcap  { font-style: sans-serif; font-size: 16pt; font-weight: bold; }\n";
    print "      .varcap  { font-style: serif; font-size: 14pt; font-weight: bold; }\n";
    print "    --></style>\n";
    print "  </head>\n";
    print "  <body>\n";
    print "<html>\n";
    print "<head>\n";
    print "<title>" . lprint("Case Browser") . "</title>\n";
    print $self->GetStyleSheetLine();
    print "</head>\n";
    print "<body>\n";
    print "<span class=\"commonblk\"><center>\n";
    print "<span class=\"mainhead\">" . lprint("Case Browser") . "</span><br />\n";
    my ($url) = $self->{DOCUMENT}->GetOption("URI");
    print "<a href=\"$url?action=data\">" . lprint("Back to Data Menu") . "</a>\n";
    print "</center></span>\n";

    print "<span class=\"commonblk\"><br /><center>\n";
    print $self->GetContinueForm("browse");

    my ($arg)      = $self->{ARGUMENT};
    my ($onlytext) = $arg->ArgByName("onlytext");

    print "<input type=\"hidden\" name=\"onlytext\" value=\"$onlytext\" />\n";

    print "<input type=\"submit\" name=\"direction\" value=\"prev\" />\n";
    print "<input type=\"text\" name=\"caseno\" value=\"$case\" />\n";
    print "<input type=\"submit\" name=\"direction\" value=\"next\" />\n";
    print "</center></form></span>\n\n";

    print "<span class=\"commonblk\">\n";

    print "<table border=\"0\" cols=\"2\" width=\"100%\" cellpadding=\"2\">\n";

    1;
}

sub printFoot
{
    my ($self) = shift;

    my ($case) = $self->{THISCASE};

    print "\n</table>";
    print "\n</span>";

    print "<span class=\"commonblk\"><br /><center>\n";
    print $self->GetContinueForm("browse");
    print "<input type=\"submit\" name=\"direction\" value=\"prev\" />\n";
    print "<input type=\"text\" name=\"caseno\" value=\"$case\" />\n";
    print "<input type=\"submit\" name=\"direction\" value=\"next\" />\n";
    print "</center><br /></form></span>\n\n";
    print "</body>\n";
    print "</html>\n";
}

sub printCase
{
    my ($self, $casearr, $caseno, $numcases) = @_;
    my ($arg) = $self->{ARGUMENT};
    my ($entry);
    my (@entries) = @{$casearr};

    my ($onlytext) = $arg->ArgByName("onlytext");

    foreach $entry (@entries)
    {
        my ($value) = $self->SanityParse($entry->{VALUE});

        $value =~ s/¤¤/\<br \/\>/g;

        if ($onlytext)
        {
            if (!$entry->IsNumerical())
            {
                print "<tr><td width=\"100\" valign=\"top\"><b>" . $self->SanityParse($entry->{NAME}) . ":</b></td>";
                print "<td valign=\"top\">" . $value . "</td></tr>\n";
            }
        }
        else
        {
            print "<tr><td width=\"100\" valign=\"top\"><b>" . $self->SanityParse($entry->{NAME}) . ":</b></td>";
            print "<td valign=\"top\">" . $value . "</td></tr>\n";
        }

    }

}

sub GetID
{
    my ($self) = shift;
    return "browse";
}

sub PrintWelcomeBodyContents
{
    my ($self) = shift;

    print "<span class=\"subhead\">" . lprint("Info") . "</span><br /><br />\n";
    print lprint("The \"Case Browser\" export module displays submitted cases one by one.") . "\n";
    print "<br /><br />";

    print "<span class=\"subhead\">" . lprint("Display") . "</span><br /><br />\n";
    print $self->GetBaseForm("html");

    print "<input type=\"checkbox\" name=\"reverse\" value=\"1\" />";
    print lprint("Display cases with newest case first") . "<br />\n";

    print "<input type=\"checkbox\" name=\"onlytext\" value=\"1\" />";
    print lprint("Only display textual fields") . "<br />\n";

    print "<br />\n";
    print "<input type=\"submit\" value=\"Display case\" />\n";
    print "</form>\n";

    1;
}

sub wantsCase
{
    my ($self, $caseno, $numcases) = @_;
    if ($caseno == $self->{THISCASE})
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

sub init
{
    my ($self, $numcase) = @_;

    my ($arg) = $self->{ARGUMENT};

    my ($isrev)  = $arg->ArgByName("reverse");
    my ($caseno) = $arg->ArgByName("caseno");
    my ($dir)    = $arg->ArgByName("direction");

    if (!$caseno)
    {
        if   ($isrev) { $caseno = $numcase; }
        else          { $caseno = 1; }
    }

    if ($dir eq "next")
    {
        if ($caseno < $numcase)
        {
            $caseno++;
        }
    }

    if ($dir eq "prev")
    {
        if ($caseno > 1)
        {
            $caseno--;
        }
    }

    $self->{MAXCASES} = $numcase;
    $self->{THISCASE} = $caseno;

    1;
}

sub PrintDataContentType
{
    my ($self) = shift;
    my($enc) = $ENV{"_SURVEY_ENCODING"} || "UTF-8";
    $self->GoodContentType("text/html; charset=$enc");
}

sub SanityParse
{

    #This method does a simply sanity check
    #for the html output.

    my ($crap, $out) = @_;

    #we don't want \ but \\
    $out =~ s/\\/\\\\/g;

    #we don't want < but &lt;
    $out =~ s/</\&lt\;/g;

    #we don't want > but &gt;
    $out =~ s/>/\&gt\;/g;

    #we don't want " but \"
    $out =~ s/\"/\\\"/g;

    #we don't want ' but \'
    $out =~ s/\'/\\\'/g;

    return $out;
}

1;

