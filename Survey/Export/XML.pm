#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Export::XML;
use strict;

@Survey::Export::XML::ISA = qw(Survey::Export::Export);

use Survey::Language;

use Text::ParseWords;
use Survey::Debug;

use CGI;

sub new
{
    my ($crap, $doc, $arg, $data) = @_;

    my ($self) = {};
    bless $self;

    $self->DoCommonStuff($doc, $arg, $data);

    return $self;
}

sub GetTitle
{
    my ($self) = shift;
    return "XML";
}

sub GetCredits
{
    my ($self) = shift;
    return lprint("This is a standard module in Mod_Survey. Parts were written by Matthew Buckett.");
}

sub PrintDataContents
{
    my ($self) = shift;

    my ($cases) = ();

    if (defined($self->{DATA}) && defined($self->{DATA}->{CASES}))
    {
        $cases = $self->{DATA}->{CASES};
    }

    my ($entry, $entries, $case, $value);

    my($enc) = $ENV{"_SURVEY_ENCODING"} || "UTF-8";
    print "<?xml version=\"1.0\" encoding=\"$enc\" standalone=\"yes\"?>\n";

    print '<survey title="' . $self->GetTitle() . '">' . "\n";

    $case = $self->{DATA}->{META};

    print "  <form>\n";

    foreach $entry (@{$case})
    {
        print '    <question name="'
          . $self->SanityParse($entry->GetName()) . '">'
          . $entry->GetVariableCaption()
          . "</question>\n";
    }
    print "  </form>\n";

    foreach $case (@{$cases})
    {
        print '  <case>' . "\n";

        foreach $entry (@{$case})
        {
            print '    <variable type="'
              . $entry->GetType()
              . '" name="'
              . $self->SanityParse($entry->{NAME}) . '">' . "\n";

            # Doesn't seem to be a nice way to work out how many choice were made?
            if ($entry->GetType() eq "CHOICE")
            {
                foreach $value (split(/,/, $entry->GetValue()))
                {
                    print '      <value>' . $self->SanityParse($value) . "</value>\n";
                }
            }
            else
            {
                print '      <value>' . $self->SanityParse($entry->GetValue()) . "</value>\n";
            }
            print "    </variable>\n";
        }
        print '  </case>' . "\n";
    }
    print <<EOF;
</survey>
EOF

    1;
}

sub GetID
{
    my ($self) = shift;
    return "xml";
}

sub PrintWelcomeBodyContents
{
    my ($self) = shift;

    print "<span class=\"subhead\">" . lprint("Info") . "</span><br /><br />\n";
    print lprint("This is the standard XML export.") . "<br /><br />\n";
    print "<span class=\"subhead\">" . lprint("Download") . "</span><br /><br />\n";
    print $self->GetBaseForm("xml");

    print "<input type=\"checkbox\" name=\"incdata\" value=\"1\" checked=\"checked\" />";
    print lprint("Include data in file") . "<br />\n";

    print "<br />\n";
    print "<input type=\"submit\" value=\"Get data\" />\n";
    print "</form>\n";

    1;
}

sub PrintDataContentType
{
    my ($self) = shift;
    my($enc) = $ENV{"_SURVEY_ENCODING"} || "UTF-8";
    $self->GoodContentType("text/xml; charset=$enc");
}

sub SanityParse
{
    my ($crap, $out) = @_;

    #remove all \ to avoid double-escape script injection
    $out =~ s/\\//g;

    #we don't want < but &lt;
    $out =~ s/</\&lt\;/g;

    #we don't want > but &gt;
    $out =~ s/>/\&gt\;/g;

    #we don't want " but \"
    $out =~ s/\"/\\\"/g;

    #we don't want ' but \'
    $out =~ s/\'/\\\'/g;

    return $out;
}

sub OneLineDesc
{
    return lprint("Generic export for stand-alone XML. It outputs both a variable description and a data matrix.");
}

1;

