#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

# Change to name of your module
package Survey::Export::Template;
use strict;

# Change to name of your module
@Survey::Export::HtmlTables::ISA = qw(Survey::Export::Export);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

# Leave the "new" sub untouched unless there are very good
# reasons for changing it
sub new
{
    my ($crap, $doc, $arg, $data) = @_;

    my ($self) = {};
    bless $self;

    $self->DoCommonStuff($doc, $arg, $data);

    return $self;
}

sub GetTitle
{
    my ($self) = shift;

    # Set to title of your module
    return "Template";
}

sub GetCredits
{
    my ($self) = shift;

    # Change to line telling us that you made this module
    return "This is a standard module in Mod_Survey";
}

sub PrintDataContents
{
    my ($self) = shift;

    #
    #  This is where you write the actual export
    #
    1;
}

sub GetID
{
    my ($self) = shift;

    # Set to id of your export
    return "template";
}

# Change to whatever is a good welcome page for your export
sub PrintWelcomeBodyContents
{
    my ($self) = shift;

    print "<span class=\"subhead\">Info</span><br /><br />\n";
    print "Blah blah...<br /><br />\n";
    print "<span class=\"subhead\">Download</span><br /><br />\n";

    # Change to extension of the resulting file
    print $self->GetBaseForm("dat");

    # Change to options you want to include
    print "<input type=\"checkbox\" name=\"incdata\" value=\"1\" checked=\"checked\" />";
    print "Include data in file<br />\n";

    print "<br />\n";
    print "<input type=\"submit\" value=\"Get data\" />\n";
    print "</form>\n";

    1;
}

sub PrintDataContentType
{
    my ($self) = shift;

    # Change to content type of your wanted data format
    print "Content-type: text/html\n\n";
}

1;

