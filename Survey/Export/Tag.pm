#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Export::Tag;
use strict;

sub new
{
  my ($self, $contents) = @_;

  $self = {};
  bless($self);

  $self->{RAW} = $contents;

  if($contents =~ m/<([A-Z0-9]+):([A-Z0-9]+)/ || $contents =~ m/<([A-Z0-9]+)/)
  {
    $self->{NAMESPACE} = $1;
    $self->{TAGNAME} = $2 || $1;
  }

  my(@attributes);

  while($contents =~ /([A-Z]+)="([^"]+)"/g)
  {
    $self->{"ATTRIBUTE_$1"} = $2;
    push(@attributes, $1);
  }

  $self->{ATTRIBUTES} = \@attributes;

  return $self;
}

sub getAttributes
{
  my($self) = shift;
  return $self->{ATTRIBUTES};
}

sub getAttribute
{
  my($self) = shift;
  my($attribute) = shift;

  return $self->{"ATTRIBUTE_$attribute"};
}

sub getNamespace
{
  my($self) = shift;
  return $self->{NAMESPACE};
}

sub getTagname
{
  my($self) = shift;
  return $self->{TAGNAME};
}


1;

