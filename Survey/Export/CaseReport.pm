#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Export::CaseReport;
use strict;

@Survey::Export::CaseReport::ISA = qw(Survey::Export::Export);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use Survey::Export::Tag;

use CGI;

sub new
{
    my ($crap, $doc, $arg, $data) = @_;

    my ($self) = {};
    bless $self;

    my($filename) = $doc->{FILE};

    # Ugly hard hack... should use env for extension instead
    $filename =~ s/\.survey$/\.report/;
    if(!-e $filename)
    {
      $data->{ERROR} = "Could not find $filename";
      $data->{ERRORCODE} = 99;
    }

    $self->{FILE} = $filename;

    $self->DoCommonStuff($doc, $arg, $data);
    
    return $self;
}

sub GetTitle
{
    my ($self) = shift;
    return lprint("Case Report");
}

sub GetCredits
{
    my ($self) = shift;
    return lprint("This is an optional module for Mod_Survey");
}

sub parseValue
{
  my($self) = shift;
  my($tag) = shift;
  my($case) = shift;

  my($value) = "undef";
  my($variable) = $tag->getAttribute("VARIABLE");

  my($e);

  foreach $e (@{$case})
  {
    if($e->{NAME} eq $variable)
    {
      $value = $e->{VALUE};
    }
  }

  return $value;
}

sub parseLabel
{
  my($self) = shift;
  my($tag) = shift;
  my($case) = shift;

  my($value) = "undef";
  my($variable) = $tag->getAttribute("VARIABLE");

  my($e);

  foreach $e (@{$case})
  {
    if($e->{NAME} eq $variable)
    {
      $value = $e->{VALUECAPTION};
      if( (!defined($value)) || ($value eq "") )
      {
        $value = $e->{VALUE};
      }
    }
  }

  return $value;
}


sub parseQuestion
{
  my($self) = shift;
  my($tag) = shift;
  my($case) = shift;

  my($value) = "undef";
  my($variable) = $tag->getAttribute("VARIABLE");

  my($e);

  foreach $e (@{$case})
  {
    if($e->{NAME} eq $variable)
    {
      $value = $e->{VARIABLECAPTION};
      if( (!defined($value)) || ($value eq "") )
      {
        $value = $e->{VALUE};
      }
    }
  }

  return $value;
}

sub getVariableValue
{
  my($self) = shift;
  my($case) = shift;
  my($variable) = shift;

  my($value) = undef;

  my($e);

  foreach $e (@{$case})
  {
    if($e->{NAME} eq $variable)
    {
      $value = $e->{VALUE};
    }
  }


  return $value;

}
 
sub parseCalculation
{
  my($self) = shift;
  my($string) = shift;
  my($case) = shift;

  my ($maxiter) = 100;

  my($badstring) = 0;

  while( ($string =~ m/(\$[a-zA-Z0-9]+)/) && ($maxiter-- > 0))
  {
    my($found) = $1;
    my($vname) = $found;

    $vname =~ s/\$//g;

    my($val) = $self->getVariableValue($case,$vname);

    my($replace);

    if(!defined($val) || $val =~ m/[^0-9\.\-\+]+/ || $val eq "")
    {
      $badstring = "[variable '$vname' is not numerical or does not exist]";
      last;
    }
    else
    {
      # also trim initial zeros to avoid octalization
      $val =~ s/^0+//;
      if($val eq "" || $val =~ m/^\./)
      {
        $val = "0" . $val;
      }
      $replace = "\$string =~ s/\\$found/$val/g";
    }
    eval($replace);
    if($@) { $badstring = "[" . $@ . "]"; }
  }

  if($badstring) { return $badstring; }

  my($value);

  if($string =~ m/[^0-9\.\-\+\s\*\/\(\)]/)
  {
    $value = "[calculation did not resolve. String at evaluation was: '" . $string . "']";
  }
  else
  {
    my($calculation);
    my($calcstring) = "\$calculation = $string;";
    eval($calcstring);
    if($@) 
    { 
      $calculation = $@;
      chomp($calculation);
      $value = "[ calculation did not resolve. String at evaluation was: '" . $string . "', error was '" . $@ . "]"; 
    }
    else 
    {       
      $value = $calculation; 
    }
  }

  return $value;

}
 
sub parseCalculated
{
  my($self) = shift;
  my($tag) = shift;
  my($case) = shift;

  my($string) = $tag->getAttribute("CALCULATION");

  my($result) = $self->parseCalculation($string,$case);

  my($dec) = $tag->getAttribute("DECIMALS");
  if(defined($dec))
  {
    if($result =~ m/[0-9\.\+\-]+/)
    {
      my($mult) = 1;
      while($dec-- > 0)
      {
        $mult = $mult * 10;
      }

      $result = $result * $mult;
      $result = int($result);
      $result = $result / $mult;
    }
  }

  return $result;

}

sub parseInterval
{
  my($self) = shift;
  my($tag) = shift;
  my($case) = shift;

  my($string) = $tag->getAttribute("CALCULATION");

  my($value) = $self->parseCalculation($string,$case);

  if($value =~ m/[^0-9\.\-\+\s]/)
  {
    return $value;
  }
 
  my($intervals) = $tag->getAttribute("INTERVALS");

  my(@ints) = split(/,/, $intervals);
  my($i);

  my(@intdef);

  my($crap,$max,$min,$label,$def);

  foreach $i (@ints)
  {
    ($crap,$label) = split(/:/,$i,2);
    ($min,$max) = split(/-/,$crap,2);

    $def = {};
    $def->{LABEL} = $label;
    $def->{MIN} = $min;
    $def->{MAX} = $max;
    push(@intdef, $def);
  }

  my($target) = $value;
  my($label) = $target;
  foreach $i (@intdef)
  {
    if( ($target <= $i->{MAX}) && ($target >= $i->{MIN} ) ) { $label = $i->{LABEL}; }
  }
  
  $value = $label;

  return $value;
}

sub SelectionPreParse
{
  my($self) = shift;
  my($content) = shift;
  my($case) = shift;

  my($string) = $content;

  print "<!-- parse -->";
  $string =~ s/[\x0a\x0d]//g;

  $string =~ m/\<SURVEY\:CONDITIONAL +VARIABLE=\"([a-zA-Z0-9]+)\" *\>(.*?)\<\/SURVEY:CONDITIONAL\>/;

  my($matchstart) = $-[0];
  my($matchend) = $+[0];

  my($variable) = $1;
  my($tag) = $2;

  my($casevalue) = $self->getVariableValue($case,$variable);

  my($MAXITER) = 100;

  my($found) = 0;

  my($selected) = "";

  while($tag =~ m/\<SURVEY:IF +VALUE=\"([^"]*)\" *\>(.*?)\<\/SURVEY:IF\>/g && $MAXITER-- > 0 && !$found)
  {
    my($value) = $1;
    my($internal) = $2;

    if($value eq $casevalue)
    {
      $found = 1;
      $selected = $internal;
    }
  }

  if(!$found)
  {
    $tag =~ m/\<SURVEY:ELSE>(.*?)\<\/SURVEY:ELSE\>/;
    $selected = $1;
  }

  my($result) = substr($string,0,$matchstart) . $selected . substr($string,$matchend);

  return $result;
}


sub PrintDataContents
{
    my ($self) = shift;
    my ($arg)  = $self->{ARGUMENT};

    if(open(FIL, $self->{FILE}))
    {
      my($html) = join('', <FIL>);
      close(FIL);
    
      if(!($html =~ m/<CASE>/))
      {
        $self->{DATA}->{ERROR} = "The report file does not seem to contain an CASE section";
        $self->{DATA}->{ERRORCODE} = 99;
        return;
      }

      my($selectedCase) = undef;

      my($case);
      my(@cases) = @{ $self->{DATA}->{CASES} };
      foreach $case (@cases)
      {
        my(@entries) = @{$case};
        my($entry) = $entries[0];
        if(($entry->{VALUE} eq $arg->ArgByName("srvkey")) || (!$arg->ArgByName("srvkey")))
        {
          $selectedCase = $case;
        }
      }

      if(!defined($selectedCase))
      {
        $self->{DATA}->{ERROR} = "Could not find the selected case";
        $self->{DATA}->{ERRORCODE} = 99;
        return;
      }
 
      my($before,$mid,$after) = split(/<CASE>/,$html);
      ($mid,$after) = split(/<\/CASE>/,$mid,2);


      my($string) = $mid;

      my($MAXITER) = 1000;

      while($string =~ m/SURVEY:CONDITIONAL/ && $MAXITER-- > 0)
      {
        $string = $self->SelectionPreParse($string,$selectedCase);
      }

      $MAXITER = 1000;

      while(($string =~ m/(<SURVEY:[^<]+\/>)/) && ($MAXITER-- > 0))
      {
        my($found) = $1;

        my($tag) = Survey::Export::Tag->new($found);

        my($cmd) = $tag->getTagname();
        my($value) = "[unknown survey tag]";

        if($cmd eq "VALUE") 
        {
          $value = $self->parseValue($tag,$selectedCase);
        }

        if($cmd eq "LABEL") 
        {
          $value = $self->parseLabel($tag,$selectedCase);
        }

        if($cmd eq "QUESTION") 
        {
          $value = $self->parseQuestion($tag,$selectedCase);
        }

        if($cmd eq "CALCULATED") 
        {
          $value = $self->parseCalculated($tag,$selectedCase);
        }

        if($cmd eq "INTERVAL") 
        {
          $value = $self->parseInterval($tag,$selectedCase);
        }

        # forbid backslashes. Ugly, but I doubt anyone will ever
        # see it. Anyway, it'll void attempts at script injection
        # at the eval below.

        $value =~ s/\\//g;

        $found =~ s/([*\/\$\=\+\"\[\]\{\}\(\)])/\\$1/g;
        $value =~ s/([*\/\$\=\+\"\[\]\{\}\(\)\@\.])/\\$1/g;

        my($replace) = "\$string =~ s/$found/$value/g";
        print "<!-- $replace -->\n";
        eval($replace);

      }

      print $string;
    }
    else
    {
      $self->{DATA}->{ERROR} = "Could not open " . $self->{FILE} . " for reading";
      $self->{DATA}->{ERRORCODE} = 99;
    }

    1;
}

sub GetID
{
    my ($self) = shift;
    return "case";
}

sub PrintWelcomeBodyContents
{
    my ($self) = shift;

    print "<span class=\"subhead\">" . lprint("Info") . "</span><br /><br />\n";
    print lprint("The case report module exports the data for a single case") . " ";
    print lprint("formatted according to an external template.");
    print "<br /><br />\n";
    print "<span class=\"subhead\">" . lprint("Display") . "</span><br /><br />\n";
    print $self->GetBaseForm("html");

    my($selectorVariable) = "srvkey";

    if(open(FIL, $self->{FILE}))
    {
      my($html) = join('', <FIL>);
      close(FIL);
    
      if(!($html =~ m/<SELECTOR>/))
      {
        print lprint("As no SELECTOR clause was found in the report template") . " ";
        print lprint("the srvkey variable will be used for selecting case");
        print "<br />";
        print "<br />\n";
        # No selector statement, use srvkey for selecting variable
      }

      my($before,$mid,$after) = split(/<SELECTOR>/,$html);
      ($mid,$after) = split(/<\/SELECTOR>/,$mid,2);


      print "<b>" . lprint("Select case to show") . "</b><br />\n";

      my($case) = $self->{DATA}->{META};
      my($c,$p);
      my($entryPos) = 0;

      $p = 0;
      foreach $c (@{$case})
      {        
        if($mid eq $c->{NAME})
        {          
          $entryPos = $p;
        }
        $p++;
      }

      print "<select name=\"srvkey\" size=\"10\">\n";

      my(@cases) = @{ $self->{DATA}->{CASES} };
      foreach $case (@cases)
      {
        my(@entries) = @{$case};
        my($entry) = $entries[$entryPos];
        print "<option value=\"" . $entries[0]->{VALUE} . "\">" . $entry->{VALUE} . "</option>\n";
      }
      print "</select>\n<br /><br />";

    }
    else
    {
      $self->{DATA}->{ERROR} = "Could not open " . $self->{FILE} . " for reading";
      $self->{DATA}->{ERRORCODE} = 99;
    }


    print "<br />\n";
    print "<input type=\"submit\" value=\"Display\" />\n";
    print "</form>\n";

    1;
}

sub PrintDataContentType
{
    my ($self) = shift;
    my($enc) = $ENV{"_SURVEY_ENCODING"} || "UTF-8";
    $self->GoodContentType("text/html; charset=$enc");
}

sub OneLineDesc
{
    return lprint("Displays data formatted with an external template");
}

sub SanityParse
{
    my ($self, $string) = @_;

    # Replace delimiter if through data
    my ($d) = $self->{DELIMITER};

    # Escape "dangerous" delimiters
    $d =~ s/([\.\+\\\$\^\(\)\[\]\{\}\*\|])/\\$1/g;

    return $string;
}

1;

