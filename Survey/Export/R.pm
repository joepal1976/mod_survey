#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Export::R;
use strict;

@Survey::Export::R::ISA = qw(Survey::Export::Export);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub new
{
    my ($crap, $doc, $arg, $data) = @_;

    my ($self) = {};
    bless $self;

    $self->DoCommonStuff($doc, $arg, $data);

    return $self;
}

sub GetTitle
{
    my ($self) = shift;
    return "R";
}

sub GetCredits
{
    my ($self) = shift;
    return lprint("This is a standard module in Mod_Survey");
}

sub PrintDataContents
{
    my ($self) = shift;
    my ($arg)  = $self->{ARGUMENT};

    my ($case, $isfirst, $entry);

    $case = $self->{DATA}->{META};

    # Filter out string values if user so chose
    my ($dofilter) = $arg->ArgByName("nostring");

    my (@cases) = ();

    if (defined($self->{DATA}) && defined($self->{DATA}->{CASES}))
    {
        @cases = @{ $self->{DATA}->{CASES} };
    }

    my ($firstcase) = 1;

    if (scalar(@cases) > 0)
    {
        print "surveydata <- rbind(";

        foreach $case (@cases)
        {
            my (@entries) = @{$case};

            if (!$firstcase)
            {
                print ",\n";
            }
            else
            {
                $firstcase = 0;
                print "\n";
            }
            print "\t\t\tdata.frame(";    #srvkey="abcd",a=1,b=2)

            $isfirst = 1;
            foreach $entry (@entries)
            {
                if ($entry)
                {
                    my ($type) = $entry->GetType();
                    my ($name) = $entry->GetName();
                    my ($isno) = $entry->IsNumerical();

                    if ($type ne "MEMO" && (!$dofilter || $isno))
                    {
                        if (!$isfirst)
                        {
                            print ",";
                        }
                        else
                        {
                            $isfirst = 0;
                        }
                        my ($val) = $self->SanityParse($entry->GetValue());
                        if (!$entry->IsNumerical())
                        {
                            $val = "\"$val\"";
                        }
                        print "$name=$val";
                    }
                }
            }
            print ")";
        }
        print "\n)\n\n";
        print "attach(surveydata)\n";
    }

    1;
}

sub GetID
{
    my ($self) = shift;
    return "r";
    1;
}

sub PrintWelcomeBodyContents
{
    my ($self) = shift;

    print "<span class=\"subhead\">" . lprint("Info") . "</span><br /><br />\n";
    print lprint("The R export module outputs a script suitable for opening in the R statistics software.") . "\n";
    print "<br /><br />\n";
    print "<span class=\"subhead\">" . lprint("Download") . "</span><br /><br />\n";
    print $self->GetBaseForm("R");

    print "<input type=\"checkbox\" name=\"nostring\" value=\"1\" checked=\"checked\" />";
    print lprint("Only include numerical variables.") . "<br />\n";

    print "<br />\n";
    print "<input type=\"submit\" value=\"Get data\" />\n";
    print "</form>\n";

    1;
}

sub PrintDataContentType
{
    my ($self) = shift;
    $self->GoodContentType('text/plain');
}

sub OneLineDesc
{
    return lprint("R statistics software script.");
}

1;

