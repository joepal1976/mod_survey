#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Export::FixedColumns;
use strict;

@Survey::Export::FixedColumns::ISA = qw(Survey::Export::Export);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub new
{
    my ($crap, $doc, $arg, $data) = @_;

    my ($self) = {};
    bless $self;

    $self->DoCommonStuff($doc, $arg, $data);

    return $self;
}

sub GetTitle
{
    my ($self) = shift;
    return lprint("Fixed Columns");
}

sub Pad
{
    my ($self, $str, $len) = @_;

    if ($len > length($str))
    {
        $str .= " " x ($len - length($str));
    }

    return $str;
}

sub GetCredits
{
    my ($self) = shift;
    return lprint("This is a standard module in Mod_Survey");
}

sub PrintPosHeader
{
    my ($self, $adjust) = @_;

    my ($pos) = 1;

    my ($arg)  = $self->{ARGUMENT};
    my ($case) = $self->{DATA}->{META};
    my ($entry);

    foreach $entry (@{$case})
    {
        if ($entry)
        {
            my ($len) = $entry->GetFieldLength();
            print $self->SanityParse($entry->GetName()) . " " . $pos . "-";
            $pos += $len;
            print $pos . " ";
            $pos += $adjust;
            $pos++;
        }
    }

    1;
}

sub PrintWidthHeader
{
    my ($self, $adjust) = @_;

    my ($arg)  = $self->{ARGUMENT};
    my ($case) = $self->{DATA}->{META};
    my ($entry);

    foreach $entry (@{$case})
    {
        if ($entry)
        {
            my ($len) = $entry->GetFieldLength();
            print $self->SanityParse($entry->GetName()) . " " . $len . " ";
        }
    }

    1;
}

sub PrintFixedHeader
{
    my ($self, $adjust) = @_;
    my ($arg)  = $self->{ARGUMENT};
    my ($case) = $self->{DATA}->{META};
    my ($entry);

    foreach $entry (@{$case})
    {
        if ($entry)
        {
            my ($len) = $entry->GetFieldLength();

            print $self->Pad($self->SanityParse($entry->GetName()), $len + $adjust);
        }
    }

    1;
}

sub PrintDataContents
{
    my ($self) = shift;
    my ($arg)  = $self->{ARGUMENT};

    my ($case, $isfirst, $entry);

    my ($line) = "\n";

    my ($adjust) = 0;

    if ($arg->ArgByName("space"))
    {
        $adjust++;
    }

    if ($arg->ArgByName("win"))
    {
        $line = "\x0d\x0a";
    }

    $case = $self->{DATA}->{META};

    # Filter out string values if user so chose
    my ($dofilter) = $arg->ArgByName("nostring");
    if ($dofilter)
    {
        my ($i);
        for ($i = scalar(@{$case}) - 1 ; $i >= 0 ; $i--)
        {
            my ($entry) = @{$case}[$i];
            if (!$entry->{ISNUMERIC})
            {
                splice(@{$case}, $i, 1);
            }
        }
    }
    else
    {

        # Add two to field lengths if user wants cite marks
        if ($arg->ArgByName("cite"))
        {
            my ($i);
            for ($i = scalar(@{$case}) - 1 ; $i >= 0 ; $i--)
            {
                my ($entry) = @{$case}[$i];
                if (!$entry->{ISNUMERIC})
                {
                    $entry->{FIELDLENGTH} += 2;
                }
            }
        }
    }

    # Always filter out memo values
    for (my ($i) = scalar(@{$case}) - 1 ; $i >= 0 ; $i--)
    {
        my ($entry) = @{$case}[$i];
        my ($type)  = $entry->GetType();
        if ($type eq "MEMO")
        {
            splice(@{$case}, $i, 1);
        }
    }

    my ($head) = $arg->ArgByName("header");
    if ($head)
    {
        if ($head eq "fixed") { $self->PrintFixedHeader($adjust); }
        if ($head eq "width") { $self->PrintWidthHeader($adjust); }
        if ($head eq "pos")   { $self->PrintPosHeader($adjust); }
        print $line;
    }

    my (@cases) = ();

    if (defined($self->{DATA}) && defined($self->{DATA}->{CASES}))
    {
        @cases = @{ $self->{DATA}->{CASES} };
    }

    if (scalar(@cases) > 0)
    {
        foreach $case (@cases)
        {
            my (@entries) = @{$case};

            $isfirst = 1;
            foreach $entry (@entries)
            {
                if ($entry)
                {
                    my ($type) = $entry->GetType();
                    my ($isno) = $entry->IsNumerical();

                    if ($type ne "MEMO" && (!$dofilter || $isno))
                    {
                        my ($val)     = $self->SanityParse($entry->GetValue());
                        my ($citeadd) = 0;
                        if ($arg->ArgByName("cite"))
                        {
                            if (!$entry->IsNumerical())
                            {
                                $val = "\"$val\"";
                                $citeadd += 2;
                            }
                        }
                        my ($len) = $entry->GetFieldLength() + $citeadd;
                        print $self->Pad($val, $len + $adjust);
                    }
                }
            }
            print $line;
        }
    }

    1;
}

sub GetID
{
    my ($self) = shift;
    return "fixed";
    1;
}

sub PrintWelcomeBodyContents
{
    my ($self) = shift;

    print "<span class=\"subhead\">" . lprint("Info") . "</span><br /><br />\n";
    print lprint("The Fixed Columns module exports the data as a list of values printed at fixed") . " ";
    print lprint("horizontal positions in a text file.");
    print "<br /><br />\n";
    print "<span class=\"subhead\">" . lprint("Download") . "</span><br /><br />\n";
    print $self->GetBaseForm("txt");

    print "<input type=\"radio\" name=\"header\" value=\"0\" checked=\"checked\" />";
    print lprint("Do not include a header.") . "<br />\n";
    print "<input type=\"radio\" name=\"header\" value=\"fixed\" />";
    print lprint("Include a header with variable names in fixed columns.") . "<br />\n";
    print "<input type=\"radio\" name=\"header\" value=\"width\" />";
    print lprint("Include a header listing variable names and field widths.") . "<br />\n";
    print "<input type=\"radio\" name=\"header\" value=\"pos\" />";
    print lprint("Include a header listing variable names and field positions.") . "<br /><br />\n";

    print "<input type=\"checkbox\" name=\"nostring\" value=\"1\" />";
    print lprint("Only include numerical variables.") . "<br />\n";
    print "<input type=\"checkbox\" name=\"space\" value=\"1\" />";
    print lprint("Include an extra unit in field widths for readability.") . "<br />\n";
    print "<input type=\"checkbox\" name=\"cite\" value=\"1\" />";
    print lprint("Put citation marks around non-numerical values.") . "<br />\n";
    print "<input type=\"checkbox\" name=\"win\" value=\"1\" />";
    print lprint("Use DOS/windows-style linefeeds") . " (\\x0d\\x0a " . lprint("instead of") . " \\n)<br />\n";

    print "<br />\n";
    print "<input type=\"submit\" value=\"Get data\" />\n";
    print "</form>\n";

    1;
}

sub PrintDataContentType
{
    my ($self) = shift;
    my($enc) = $ENV{"_SURVEY_ENCODING"} || "UTF-8";
    $self->GoodContentType("text/plain; charset=$enc");
}

sub OneLineDesc
{
    return lprint(
"Generic fixed-column export format. Suitable for import in many spreadsheets and statistics programs, for example SPSS and Excel."
    );
}

1;

