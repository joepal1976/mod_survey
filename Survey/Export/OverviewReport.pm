#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Export::OverviewReport;
use strict;

@Survey::Export::OverviewReport::ISA = qw(Survey::Export::Export);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub new
{
    my ($crap, $doc, $arg, $data) = @_;

    my ($self) = {};
    bless $self;

    my($filename) = $doc->{FILE};

    # Ugly hard hack... should use env for extension instead
    $filename =~ s/\.survey$/\.report/;
    if(!-e $filename)
    {
      $data->{ERROR} = "Could not find $filename";
      $data->{ERRORCODE} = 99;
    }

    $self->{FILE} = $filename;

    $self->DoCommonStuff($doc, $arg, $data);
    
    return $self;
}

sub GetTitle
{
    my ($self) = shift;
    return lprint("Overview Report");
}

sub GetCredits
{
    my ($self) = shift;
    return lprint("This is an optional module for Mod_Survey");
}

sub parseMean
{
  my($self) = shift;
  my($tag) = shift;

  my($variable) = $tag->getAttribute("VARIABLE");

  return $variable;
}

sub PrintDataContents
{
    my ($self) = shift;
    my ($arg)  = $self->{ARGUMENT};


    if(open(FIL, $self->{FILE}))
    {
      my($html) = join('', <FIL>);
      close(FIL);
    
      if(!($html =~ m/<OVERVIEW>/))
      {
        $self->{DATA}->{ERROR} = "The report file does not seem to contain an OVERVIEW section";
        $self->{DATA}->{ERRORCODE} = 99;
        return;
      }

      $self->{STAT} = Survey::Export::StatUtils->new($self->{DOCUMENT}, $self->{DATA});

      my($before,$mid,$after) = split(/<OVERVIEW>/,$html);
      ($mid,$after) = split(/<\/OVERVIEW>/,$mid,2);

      my($string) = $mid;

      my($MAXITER) = 1000;

      while(($string =~ m/(<SURVEY:[^<]+\/>)/) && ($MAXITER-- > 0))
      {
        my($found) = $1;

        my($tag) = Survey::Export::Tag->new($found);

        my($cmd) = $tag->getTagname();
        my($value) = "[unknown survey tag]";

        
        my($stat) = $self->{STAT};
        my($variable) = $tag->getAttribute("VARIABLE");

        if(!$variable) 
        {
          $cmd = "xyz";
          $value = "[VARIABLE is a required parameter]";
        }

        if ($cmd eq "COUNT")
        {
          $value = $stat->Count($variable);
        }

        if ($cmd eq "LEGAL")
        {
          $value = $stat->LegalCount($variable);
        }

        if ($cmd eq "ILLEGAL")
        {
          $value = $stat->IllegalCount($variable);
        }

        if ($cmd eq "MIN")
        {
          $value = $stat->Min($variable);
        }

        if ($cmd eq "Q1")
        {
          $value = $stat->Q1($variable);
        }

        if ($cmd eq "MEDIAN")
        {
          $value = $stat->Median($variable);
        }

        if ($cmd eq "MEAN")
        {
          $value = $stat->Mean($variable);
        }

        if ($cmd eq "Q3")
        {
          $value = $stat->Q3($variable);
        }

        if ($cmd eq "MAX")
        {
          $value = $stat->Max($variable);
        }

        if ($cmd eq "SUM")
        {
          $value = $stat->Sum($variable);
        }

        if ($cmd eq "MODE")
        {
          $value = $stat->Mode($variable);
        }

        if ($cmd eq "RANGE")
        {
          $value = $stat->Range($variable);
        }

        if ($cmd eq "IQR")
        {
          $value = $stat->InterQuartileRange($variable);
        }

        if ($cmd eq "VARIANCE")
        {
          $value = $stat->SampleVariance($variable);
        }

        if ($cmd eq "STDDEV")
        {
          $value = $stat->StandardDeviation($variable);
        }

        # forbid backslashes. Ugly, but I doubt anyone will ever
        # see it. Anyway, it'll void attempts at script injection
        # at the eval below.

        $value =~ s/\\//g;

        $found =~ s/([\/\$\=\+\"\[\]\{\}\(\)])/\\$1/g;
        $value =~ s/([\/\$\=\+\"\[\]\{\}\(\)])/\\$1/g;

        my($replace) = "\$string =~ s/$found/$value/g";
        #print "<!-- $replace -->\n";
        eval($replace);
      }

      print $string;
    }
    else
    {
      $self->{DATA}->{ERROR} = "Could not open " . $self->{FILE} . " for reading";
      $self->{DATA}->{ERRORCODE} = 99;
    }

    1;
}

sub GetID
{
    my ($self) = shift;
    return "overview";
}

sub PrintWelcomeBodyContents
{
    my ($self) = shift;

    print "<span class=\"subhead\">" . lprint("Info") . "</span><br /><br />\n";
    print lprint("The overview report module exports the data") . " ";
    print lprint("formatted according to an external template.");
    print "<br /><br />\n";
    print "<span class=\"subhead\">" . lprint("Display") . "</span><br /><br />\n";
    print $self->GetBaseForm("html");

    print "<br />\n";
    print "<input type=\"submit\" value=\"Display\" />\n";
    print "</form>\n";

    1;
}

sub PrintDataContentType
{
    my ($self) = shift;
    my($enc) = $ENV{"_SURVEY_ENCODING"} || "UTF-8";
    $self->GoodContentType("text/html; charset=$enc");
}

sub OneLineDesc
{
    return lprint("Displays data formatted with an external template");
}

sub SanityParse
{
    my ($self, $string) = @_;

    # Replace delimiter if through data
    my ($d) = $self->{DELIMITER};

    # Escape "dangerous" delimiters
    $d =~ s/([\.\+\\\$\^\(\)\[\]\{\}\*\|])/\\$1/g;

    return $string;
}

1;

