#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Export::PHP;
use strict;

@Survey::Export::PHP::ISA = qw(Survey::Export::Export);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub new
{
    my ($crap, $doc, $arg, $data) = @_;

    my ($self) = {};
    bless $self;

    $self->DoCommonStuff($doc, $arg, $data);

    return $self;
}

sub GetTitle
{
    my ($self) = shift;
    return "PHP";
}

sub GetCredits
{
    my ($self) = shift;
    return "This is a standard module in Mod_Survey";
}

sub PrintDataContents
{
    my ($self) = shift;

    print "Blah blah...";
    1;
}

sub GetID
{
    my ($self) = shift;
    return "php";
}

sub PrintWelcomeBodyContents
{
    my ($self) = shift;

    print "<span class=\"subhead\">Info</span><br /><br />\n";
    print "Blah blah...<br /><br />\n";
    print "<span class=\"subhead\">Download</span><br /><br />\n";
    print $self->GetBaseForm("php");

    print "<input type=\"checkbox\" name=\"incdata\" value=\"1\" checked=\"checked\" />";
    print "Include data in file<br />\n";

    print "<br />\n";
    print "<input type=\"submit\" value=\"Get data\" />\n";
    print "</form>\n";

    1;
}

sub PrintDataContentType
{
    my ($self) = shift;
    print "Content-type: text/html\n\n";
}

1;

