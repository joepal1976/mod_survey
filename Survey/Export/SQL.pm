#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Export::SQL;
use strict;

@Survey::Export::SQL::ISA = qw(Survey::Export::Export);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub new
{
    my ($crap, $doc, $arg, $data) = @_;

    my ($self) = {};
    bless $self;

    $self->DoCommonStuff($doc, $arg, $data);

    return $self;
}

sub GetTitle
{
    my ($self) = shift;
    return "SQL";
}

sub GetCredits
{
    my ($self) = shift;
    return lprint("This is a standard module in Mod_Survey. Parts were written by Matthew Buckett.");
}

sub PrintDataContents
{
    my ($self) = shift;
    my ($arg)  = $self->{ARGUMENT};
    my ($tblname, $case, $uid);

    if ($arg->ArgByName("usetblname"))
    {
        my ($tb) = $arg->ArgByName("tblname");
        if ($tb)
        {
            $tblname = $self->SanityParse($tb);
        }
    }
    else
    {
        my ($crap);
        my ($uri)  = $self->{DOCUMENT}->GetOption("URI");
        my (@path) = split(/\//, $uri);
        my ($fn)   = @path[@path - 1];
        ($fn, $crap) = split(/\./, $fn, 2);

        $tblname = $self->SanityParse($fn);
    }

    if ($arg->ArgByName("create"))
    {
        if ($arg->ArgByName("drop"))
        {
            print "DROP TABLE $tblname;\n\n";
        }

        print "CREATE TABLE $tblname (\n  ";

        if ($arg->ArgByName("serial"))
        {
            print $tblname . "_id SERIAL NOT NULL PRIMARY KEY, srvkey VARCHAR(40)";
        }
        else
        {
            print "srvkey VARCHAR(40) NOT NULL PRIMARY KEY";
        }

        $case = $self->{DATA}->{META};
        my ($entry);

        foreach $entry (@{$case})
        {
            if ($entry && ($entry->GetType() ne "KEY"))
            {
                print ",\n  ";
                print $self->SanityParse($entry->GetName()) . " ";
                if ($entry->IsNumerical())
                {
                    print "INTEGER";
                }
                elsif (!$entry->GetFieldLength())
                {
                    print "TEXT";
                }
                else
                {
                    print "VARCHAR(";
                    print $entry->GetFieldLength();
                    print ")";
                }
            }
        }
        print "\n);\n\n";
        if ($arg->ArgByName("grant"))
        {
            $uid = undef;

            # Rejected, not mp2 compatible
            #$uid = $self->{DOCUMENT}->{HANDLER}->server()->uid();
            $uid = $< unless ($uid);
            print "GRANT SELECT,INSERT ON " . $tblname . " TO \"" . scalar(getpwuid($uid)) . "\";\n";
        }
    }

    my ($nomemo) = $arg->ArgByName("nomemo");

    if ($arg->ArgByName("insert") && defined($self->{DATA}) && defined($self->{DATA}->{CASES}))
    {
        $case = $self->{DATA}->{META};
        my ($entry);

        my ($base) = "INSERT INTO $tblname(srvkey";

        foreach $entry (@{$case})
        {
            my ($type) = $entry->GetType();
            if ($entry && ($type ne "KEY"))
            {
                if (!$nomemo || ($type ne "MEMO"))
                {
                    $base .= ",";
                    $base .= $self->SanityParse($entry->GetName());
                }
            }
        }
        $base .= ") VALUES(";

        my (@cases) = @{ $self->{DATA}->{CASES} };
        my ($isfirst);

        if (scalar(@cases) > 0)
        {
            foreach $case (@cases)
            {
                print $base;
                my ($entry);
                my (@entries) = @{$case};

                print "'";
                print $entries[0]->GetValue();
                print "'";

                $isfirst = 1;
                foreach $entry (@entries)
                {
                    my ($type) = $entry->GetType();
                    if ($entry && ($entry->GetType() ne "KEY"))
                    {
                        print ",";
                        if ($entry->IsNumerical())
                        {
                            print $self->SanityParse($entry->GetValue());
                        }
                        else
                        {
                            if (!$nomemo || ($type ne "MEMO"))
                            {
                                print "'";
                                print $self->SanityParse($entry->GetValue());
                                print "'";
                            }
                        }

                    }
                }
                print ");\n";
            }
        }
    }

    1;
}

sub GetID
{
    my ($self) = shift;
    return "sql";
}

sub PrintWelcomeBodyContents
{
    my ($self) = shift;

    print "<span class=\"subhead\">" . lprint("Info") . "</span><br /><br />\n";
    print lprint("The SQL module export both definition statements (create) and data statements") . " ";
    print lprint("(insert). The definition statement can then be used as a base for setting up") . " ";
    print lprint("a table for the DBI save method.") . "\n";
    print "<br /><br />\n";
    print "<span class=\"subhead\">Download</span><br /><br />\n";
    print $self->GetBaseForm("sql");

    print "<input type=\"checkbox\" name=\"usetblname\" value=\"1\" />";
    print lprint("Use") . " <input type=\"text\" name=\"tblname\" value=\"\" /> ";
    print lprint("as table name (rather than filename base).") . "<br />\n";
    print "<input type=\"checkbox\" name=\"serial\" value=\"1\" />";
    print lprint("Use a SERIAL primary key field rather than \"srvkey\" (does not work in all DBMSs).") . "<br />\n";
    print "<br />\n";
    print "<input type=\"checkbox\" name=\"create\" value=\"1\" checked=\"checked\" />";
    print lprint("Include CREATE statements in script") . "<br />\n";
    print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"checkbox\" name=\"nomemo\" value=\"1\" />";
    print lprint("Do not include MEMO fields in inserts.") . "<br />\n";
    print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"checkbox\" name=\"drop\" value=\"1\" checked=\"checked\" />";
    print lprint("DROP table before creating it.") . "<br />\n";
    print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"checkbox\" name=\"grant\" value=\"1\" />";
    print lprint("GRANT INSERT,SELECT on table to user (useful for a PostgreSQL setup).") . "<br />\n";
    print "<br />\n";
    print "<input type=\"checkbox\" name=\"insert\" value=\"1\" checked=\"checked\" />";
    print lprint("Include INSERT statements in script") . "<br />\n";

    print "<br />\n";
    print "<input type=\"submit\" value=\"Get data\" />\n";
    print "</form>\n";

    1;
}

sub PrintDataContentType
{
    my ($self) = shift;
    $self->GoodContentType('application/sql');
}

sub SanityParse
{
    my ($crap, $out) = @_;

    #remove all \ to avoid double-escape SQL injection
    $out =~ s/\\//g;

    #we don't want ; but \;
    $out =~ s/\;/\\\;/g;

    #we don't want ' but \'
    $out =~ s/\'/\\\'/g;

    return $out;
}

sub OneLineDesc
{
    return lprint(
"Exports SQL script with optional support for both definition and data. Suitable for import in most modern RDBMSs (not MS Access)."
    );
}

1;
