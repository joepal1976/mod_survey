#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Export::StatUtils;
use strict;

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub new
{
    my ($crap, $doc, $data) = @_;

    my ($self) = {};
    $self->{DATA}     = $data;
    $self->{DOCUMENT} = $doc;

    # there's no group by now
    $self->{GROUPS} = {};

    bless $self;

    $self->SetupBasics();

    return $self;
}

sub SetupBasics
{
    my ($self) = shift;

    my (@meta) = @{ $self->{DATA}->{META} };
    my ($len)  = scalar(@meta);
    my ($i);
    my (@numericals) = ();

    for ($i = 0 ; $i < $len ; $i++)
    {
        my ($entry) = $meta[$i];
        $self->{ "VAR_" . $entry->{NAME} } = $i;
        if ($entry->IsNumerical())
        {
            push(@numericals, $entry->{NAME});
        }
    }

    $self->{NUMERICALS} = \@numericals;
}

sub GetGroup
{
    my ($self, $group) = @_;
    if (!defined($group))
    {
        return $self->{DATA}->{DATA};
    }
    else
    {
        if (!defined($self->{GROUPS}->{$group}))
        {
            return $self->{DATA}->{DATA};
        }

        return $self->{GROUPS}->{$group};
    }
}

sub DefineGroups
{
    my ($self, $varname) = @_;

    # gets all var values
    my (@values) = @{ $self->DistinctValueArray($varname, undef) };

    # gets data
    my (@data) = @{ $self->{DATA}->{DATA} };

    my ($group);
    my ($v, $vs);

    $self->{GROUPS} = {};

    # create an array for this group
    # we'll use each of it to group data.
    for ($vs = 0 ; $vs < scalar(@values) ; $vs++)
    {
        $v = $values[$vs];

        # create the group adding it to self->{GROUPS}
        $group = $varname . "_" . $v;
        my (@temp) = ();

        # match cases that fit this group
        my ($len) = scalar(@data);
        my ($var) = $self->{ "VAR_" . $varname };
        my ($i);

        my ($groupElements) = 0;

        for ($i = 0 ; $i < $len ; $i++)
        {
            my (@case) = @{ $data[$i] };
            my ($val)  = $case[$var];

            if (defined($val) && ($val == $v))
            {
                $temp[$groupElements] = [@case];
                $groupElements++;
            }
        }
        $self->{GROUPS}->{$group} = \@temp;
    }
}

sub ValueArray
{
    my ($self, $varname, $group) = @_;

    my (@data);

    @data = @{ $self->GetGroup($group) };

    my (@values);
    my ($len) = scalar(@data);

    my ($varno) = $self->{ "VAR_" . $varname };
    if (defined($varno))
    {
        my ($i);

        for ($i = 0 ; $i < $len ; $i++)
        {
            my ($value) = @{ $data[$i] }[$varno];
            if (defined($value))
            {
                push(@values, $value);
            }
        }
    }
    return \@values;
}

sub LegalValueArray
{
    my ($self, $varname, $group) = @_;
    my (@values) = @{ $self->ValueArray($varname, $group) };

    my ($illegal) = -1;
    my ($notdisp) = 999;

    my ($len) = scalar(@values);
    my ($i);
    for ($i = $len - 1 ; $i >= 0 ; $i--)
    {
        if (($values[$i] eq $illegal) || ($values[$i] eq $notdisp))
        {
            splice(@values, $i, 1);
        }
    }

    return \@values;
}

sub Distinct
{
    my (%data) = map { ($_, 1) } @_;
    return keys %data;
}

sub DistinctValueArray
{
    my ($self, $varname, $group) = @_;
    my (@values) = @{ $self->ValueArray($varname, $group) };
    my (@distvalues) = &Distinct(@values);
    return \@distvalues;
}

sub LegalDistinctValueArray
{
    my ($self, $varname, $group) = @_;
    my (@values) = @{ $self->LegalValueArray($varname, $group) };
    my (@distvalues) = &Distinct(@values);
    return \@distvalues;
}

sub VariableSampleSize
{
    my ($self, $varname, $group) = @_;
    my (@values) = @{ $self->ValueArray($varname, $group) };
    return scalar(@values);
}

sub VariableLegalSampleSize
{
    my ($self, $varname, $group) = @_;
    my (@values) = @{ $self->LegalValueArray($varname, $group) };
    return scalar(@values);
}

sub Count
{
    my ($self, $varname, $group) = @_;
    return $self->VariableSampleSize($varname, $group);
}

sub LegalCount
{
    my ($self, $varname, $group) = @_;
    return $self->VariableLegalSampleSize($varname, $group);
}

sub IllegalCount
{
    my ($self, $varname, $group) = @_;
    my (@allvalues) = @{ $self->ValueArray($varname,      $group) };
    my (@legvalues) = @{ $self->LegalValueArray($varname, $group) };
    return scalar(@allvalues) - scalar(@legvalues);
}

sub AbsoluteFrequencyTable
{
    my ($self, $varname, $group) = @_;
    my (@values) = @{ $self->ValueArray($varname,         $group) };
    my (@dvals)  = @{ $self->DistinctValueArray($varname, $group) };

    my (%valmap, $val);

    foreach $val (@dvals)
    {
        $valmap{$val} = 0;
    }

    foreach $val (@values)
    {
        $valmap{$val}++;
    }

    return %valmap;
}

sub AllGroupsAbsoluteFrequencyTable
{
    my ($self, $varname) = @_;

    my (%valgroupped);
    my ($group);

    # calculate absolute freq for all groups
    # and return it in an hash where each group
    # has its own key (element).
    foreach $group (sort (keys %{ $self->{GROUPS} }))
    {
        $valgroupped{$group} = { $self->AbsoluteFrequencyTable($varname, $group) };
    }

    return %valgroupped;
}

sub RelativeFrequencyTable
{
    my ($self, $varname, $group) = @_;
    my (%valmap) = $self->AbsoluteFrequencyTable($varname, $group);

    my (@vals) = keys %valmap;

    my ($val);

    my ($numval) = $self->VariableSampleSize($varname, $group);

    foreach $val (@vals)
    {
        my ($absval) = $valmap{$val};
        $valmap{$val} = $absval / $numval;
    }

    return %valmap;
}

sub AllGroupsRelativeFrequencyTable
{
    my ($self, $varname) = @_;

    my (%valgroupped);
    my ($group);

    # calculate relative freq for all groups
    # and return it in an hash where each group
    # has its own key (element).
    foreach $group (sort (keys %{ $self->{GROUPS} }))
    {
        $valgroupped{$group} = { $self->RelativeFrequencyTable($varname, $group) };
    }

    return %valgroupped;
}

sub LegalAbsoluteFrequencyTable
{
    my ($self, $varname, $group) = @_;
    my (@values) = @{ $self->LegalValueArray($varname,         $group) };
    my (@dvals)  = @{ $self->LegalDistinctValueArray($varname, $group) };

    my (%valmap, $val);

    foreach $val (@dvals)
    {
        $valmap{$val} = 0;
    }

    foreach $val (@values)
    {
        $valmap{$val}++;
    }

    return %valmap;
}

sub AllGroupsLegalAbsoluteFrequencyTable
{
    my ($self, $varname) = @_;

    my (%valgroupped);
    my ($group);

    # calculate relative freq for all groups
    # and return it in an hash where each group
    # has its own key (element).
    foreach $group (sort (keys %{ $self->{GROUPS} }))
    {
        $valgroupped{$group} = { $self->LegalAbsoluteFrequencyTable($varname, $group) };
    }

    return %valgroupped;
}

sub LegalRelativeFrequencyTable
{
    my ($self, $varname, $group) = @_;
    my (%valmap) = $self->LegalAbsoluteFrequencyTable($varname, $group);

    my (@vals) = keys %valmap;

    my ($val);

    my ($numval) = $self->VariableLegalSampleSize($varname, $group);

    foreach $val (@vals)
    {
        my ($absval) = $valmap{$val};
        $valmap{$val} = $absval / $numval;
    }

    return %valmap;
}

sub AllGroupsLegalRelativeFrequencyTable
{
    my ($self, $varname) = @_;

    my (%valgroupped);
    my ($group);

    # calculate relative freq for all groups
    # and return it in an hash where each group
    # has its own key (element).
    foreach $group (sort (keys %{ $self->{GROUPS} }))
    {
        $valgroupped{$group} = { $self->LegalRelativeFrequencyTable($varname, $group) };
    }

    return %valgroupped;
}

sub Sum
{
    my ($self, $varname, $group) = @_;
    my (@values) = @{ $self->LegalValueArray($varname, $group) };
    my ($sum) = 0;
    $sum += $_ for @values;
    return $sum;
}

sub Mean
{
    my ($self, $varname, $group) = @_;
    my ($sum) = $self->Sum($varname, $group);
    my ($num) = $self->VariableLegalSampleSize($varname, $group);
    my ($mean) = $sum / $num;
    return $mean;
}

sub Range
{
    my ($self, $varname, $group) = @_;
    my ($min) = $self->Min($varname, $group);
    my ($max) = $self->Max($varname, $group);
    return $max - $min;
}

sub Min
{
    my ($self, $varname, $group) = @_;
    my (%valmap) = $self->LegalAbsoluteFrequencyTable($varname, $group);
    my (@vals) = sort keys %valmap;
    return $vals[0];
}

sub Max
{
    my ($self, $varname, $group) = @_;
    my (%valmap) = $self->LegalAbsoluteFrequencyTable($varname, $group);
    my (@vals) = sort keys %valmap;
    return $vals[scalar(@vals) - 1];
}

sub Median
{
    my ($self, $varname, $group) = @_;
    my (@values) = sort @{ $self->LegalValueArray($varname, $group) };
    my ($num)    = scalar(@values);
    my ($odd)    = $num % 2;

    my ($value) = 0;

    if ($odd)
    {
        my ($pick) = int($num / 2);
        $value = $values[$pick];
    }
    else
    {
        my ($pick1) = int($num / 2);
        my ($pick2) = int($num / 2) - 1;
        my ($val1)  = $values[$pick1];
        my ($val2)  = $values[$pick2];
        $value = int(($val1 + $val2) / 2);
    }

    return $value;
}

sub Mode
{
    my ($self, $varname, $group) = @_;
    my (%valmap) = $self->LegalAbsoluteFrequencyTable($varname, $group);
    my (@vals)   = sort keys %valmap;
    my ($most)   = $vals[0];
    my ($i);
    for ($i = 1 ; $i < scalar(@vals) ; $i++)
    {
        if ($valmap{ $vals[$i] } > $valmap{$most})
        {
            $most = $vals[$i];
        }
    }
    return $most;
}

# Note that for Q1 and Q3 no interpolation is done. This is a conscious
# choice. Further in the case of not exact hits, the closest exact hit
# *upwards* is chosen. This follows since the theory is that we want to
# say that we've included "at least 25% of the sample" or "at least 75%
# of the sample". Thus, rounding upwards is to go for the safe option.
sub Q1
{
    my ($self, $varname, $group) = @_;
    my (@values) = sort @{ $self->LegalValueArray($varname, $group) };
    my ($num)    = scalar(@values);
    my ($value)  = 0;

    my ($pos) = int(($num + 1) / 4);
    if ((($num + 1) % 4)) { $pos++; }

    if (($pos < 0) || ($pos >= $num))
    {
        return 0;
    }

    return $values[$pos];
}

sub Q3
{
    my ($self, $varname, $group) = @_;
    my (@values) = sort @{ $self->LegalValueArray($varname, $group) };
    my ($num)    = scalar(@values);
    my ($value)  = 0;

    my ($pos) = int((($num + 1) / 4) * 3);
    if ((($num + 1) % 4)) { $pos++; }

    if (($pos < 0) || ($pos >= $num))
    {
        return 0;
    }

    return $values[$pos];
}

sub InterQuartileRange
{
    my ($self, $varname, $group) = @_;
    my ($q1) = $self->Q1($varname, $group);
    my ($q3) = $self->Q3($varname, $group);
    return $q3 - $q1;
}

sub StandardDeviation
{
    my ($self, $varname, $group) = @_;
    my ($variance) = $self->SampleVariance($varname, $group);

    if ($variance <= 0)
    {
        return 0;
    }

    return sqrt($variance);
}

sub SampleVariance
{
    my ($self, $varname, $group) = @_;
    my ($mean) = $self->Mean($varname, $group);
    my (@values) = @{ $self->LegalValueArray($varname, $group) };

    if (scalar(@values) < 2)
    {
        return 0;
    }

    my ($dev2sum) = 0;
    my ($val, $dev);

    foreach $val (@values)
    {
        $dev = $val - $mean;
        $dev2sum += $dev * $dev;
    }

    # "-1" follows since this is a *sample* variance
    my ($variance) = $dev2sum / (scalar(@values) - 1);
    return $variance;
}

1;

