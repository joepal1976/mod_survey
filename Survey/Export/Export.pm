#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Export::Export;
use strict;

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub DoCommonStuff
{

    # Don't override this method
    my ($self, $doc, $arg, $data) = @_;

    $self->{DOCUMENT} = $doc;
    $self->{ARGUMENT} = $arg;
    $self->{DATA}     = $data;

    if ($arg->ArgByName("exportaction") ne "download")
    {
        $self->PrintWelcomePage();
    }
    else
    {
        $self->PrintDataContentType();
        $self->PrintDataContents();
    }

    1;
}

sub GetBaseFile
{

    # Don't override this method
    my ($self, $extension) = @_;
    my ($crap);
    my ($uri)  = $self->{DOCUMENT}->GetOption("URI");
    my (@path) = split(/\//, $uri);
    my ($fn)   = @path[@path - 1];
    ($fn, $crap) = split(/\./, $fn, 2);
    $fn = $uri . "/" . $fn . ".$extension";

    return $fn;
}

sub GetBaseURL
{

    # Don't override this method
    my ($self, $extension) = @_;
    my ($url) = $self->GetBaseFile($extension);

    $url .= "?action=data\&export=" . $self->GetID() . "\&exportaction=download";

    return $url;
}

sub GetBaseForm
{

    # Don't override this method
    my ($self, $extension) = @_;
    my ($url) = $self->GetBaseFile($extension);

    my ($out) = "      <form method=\"get\" action=\"$url\">\n";
    $out .= "        <input type=\"hidden\" name=\"action\" value=\"data\" />\n";
    $out .= "        <input type=\"hidden\" name=\"export\" value=\"" . $self->GetID() . "\" />\n";
    $out .= "        <input type=\"hidden\" name=\"exportaction\" value=\"download\" />\n";

    return $out;
}

sub GetContinueForm
{

    # Don't override this method
    my ($self, $extension) = @_;
    my ($url) = $self->{DOCUMENT}->GetOption("URI");

    my ($out) = "      <form method=\"get\" action=\"$url\">\n";
    $out .= "        <input type=\"hidden\" name=\"action\" value=\"data\" />\n";
    $out .= "        <input type=\"hidden\" name=\"export\" value=\"" . $self->GetID() . "\" />\n";
    $out .= "        <input type=\"hidden\" name=\"exportaction\" value=\"download\" />\n";

    return $out;
}

sub GetStyleSheetLine
{
    return "    <link rel=\"stylesheet\" type=\"text/css\" href=\""
      . $ENV{_SURVEY_ROOT_ALIAS}
      . "system/data.css\" />\n";
}

sub PrintWelcomePage
{

    # Don't override this method
    my ($self) = shift;

    my ($title) = $self->GetTitle();

    $self->GoodContentType('text/html');

    Survey::Slask->HtmlHead();

    print "    <title>$title</title>\n";
    print $self->GetStyleSheetLine();
    print "  </head>\n";

    #  print "  <!-- DEBUG\n\n";
    #
    #  print "Time: " . $self->{DATA}->{TIME} . "\n";
    my ($url) = $self->{DOCUMENT}->GetOption("URI");

    #
    #  print "\n\n  -->\n";
    print "  <body>\n";
    print "    <span class=\"commonblk\"><center>\n";
    print "      <span class=\"mainhead\">$title</span><br />\n";
    print "<a href=\"$url?action=data\">" . lprint("Back to Data Menu") . "</a>\n";
    print "    </center></span>\n";

    print "    <span class=\"commonblk\">\n";
    $self->PrintWelcomeBodyContents();
    print "\n";
    print "    </span>\n";

    print "    <span class=\"commonblk\"><br /><center>\n";
    print "      <span class=\"subhead\">" . lprint("Credits") . "</span><br /><br />\n";
    print $self->GetCredits();
    print "\n";
    print "    </center><br /></span>\n";
    print "  </body>\n";
    print "</html>\n";

    1;
}

sub GetID
{
    my ($self) = shift;

    # This method should be overridden by the individual exports
    return "anon";
}

sub GetTitle
{
    my ($self) = shift;
    return "Anonymous export";
}

sub OneLineDesc
{
    return "General export";
}

sub GetCredits
{
    my ($self) = shift;
    return "The GetCredits() method should be overridden by the individual exports";
}

sub PrintDataContents
{
    my ($self) = shift;
    print "The PrintDataContents() method should be overridden by the individual exports";
}

sub PrintWelcomeBodyContents
{
    my ($self) = shift;
    print "The PrintWelcomeBodyContents() method should be overridden by the individual exports";
}

sub PrintDataContentType
{
    my ($self) = shift;

    # This method should likely be overridden by the individual exports
    $self->GoodContentType('text/html');
}

sub GoodContentType
{
    my ($self, $ct) = @_;
    $self->{DOCUMENT}->{HANDLER}->content_type($ct);
    if ($ENV{PERL_SEND_HEADER})
    {
        print "\n";
    }
}

sub IsEventBased
{

    # Override this if you want event-based API
    return 0;
}

sub SanityParse
{
    my ($self, $string) = @_;

    # Override this if you want local security checking
    return $string;
}

1;

