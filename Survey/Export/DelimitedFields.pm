#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Export::DelimitedFields;
use strict;

@Survey::Export::DelimitedFields::ISA = qw(Survey::Export::Export);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub new
{
    my ($crap, $doc, $arg, $data) = @_;

    my ($self) = {};
    bless $self;

    $self->DoCommonStuff($doc, $arg, $data);

    return $self;
}

sub GetTitle
{
    my ($self) = shift;
    return lprint("Delimited Fields");
}

sub GetCredits
{
    my ($self) = shift;
    return lprint("This is a standard module in Mod_Survey");
}

sub PrintDataContents
{
    my ($self) = shift;
    my ($arg)  = $self->{ARGUMENT};

    my ($case, $isfirst, $entry);
    my ($delim) = ";";
    my ($line)  = "\n";

    if ($arg->ArgByName("usesep") eq "2")
    {
        $delim = "\t";
    }
    if ($arg->ArgByName("usesep") eq "3")
    {
        $delim = $arg->ArgByName("sep");
    }

    $self->{DELIMITER} = $delim;

    if ($arg->ArgByName("win"))
    {
        $line = "\x0d\x0a";
    }

    if ($arg->ArgByName("incvar"))
    {
        $case = $self->{DATA}->{META};

        $isfirst = 1;
        foreach $entry (@{$case})
        {
            if ($entry)
            {
                if ($isfirst)
                {
                    $isfirst = 0;
                }
                else
                {
                    print $delim;
                }
                print $self->SanityParse($entry->GetName());
            }
        }
        print $line;
    }

    my (@cases) = ();

    if (defined($self->{DATA}) && defined($self->{DATA}->{CASES}))
    {
        @cases = @{ $self->{DATA}->{CASES} };
    }

    if (scalar(@cases) > 0)
    {
        foreach $case (@cases)
        {
            my (@entries) = @{$case};

            $isfirst = 1;
            foreach $entry (@entries)
            {
                if ($entry)
                {
                    if ($isfirst)
                    {
                        $isfirst = 0;
                    }
                    else
                    {
                        print $delim;
                    }
                    if ($arg->ArgByName("cite"))
                    {
                        if ($entry->IsNumerical())
                        {
                            print $self->SanityParse($entry->GetValue());
                        }
                        else
                        {
                            print "\"";
                            print $self->SanityParse($entry->GetValue());
                            print "\"";
                        }
                    }
                    else
                    {
                        print $self->SanityParse($entry->GetValue());
                    }
                }
            }
            print $line;
        }
    }

    1;
}

sub GetID
{
    my ($self) = shift;
    return "delim";
}

sub PrintWelcomeBodyContents
{
    my ($self) = shift;

    print "<span class=\"subhead\">" . lprint("Info") . "</span><br /><br />\n";
    print lprint("The delimited fields module exports the data as a list of values separated by a") . " ";
    print lprint("choosable character, usually a semi-colon.");
    print "<br /><br />\n";
    print "<span class=\"subhead\">" . lprint("Download") . "</span><br /><br />\n";
    print $self->GetBaseForm("txt");

    print "<input type=\"radio\" name=\"usesep\" value=\"1\" checked=\"checked\" />";
    print lprint("Use semi-colon as separator (suitable for MS Access).") . "<br />\n";
    print "<input type=\"radio\" name=\"usesep\" value=\"2\" />";
    print lprint("Use TAB as separator (suitable for MiniTab).") . "<br />\n";
    print "<input type=\"radio\" name=\"usesep\" value=\"3\" />";
    print lprint("Use")
      . " <input type=\"text\" name=\"sep\" value=\"\" /> "
      . lprint("as separator.")
      . "<br /><br />\n";

    print "<input type=\"checkbox\" name=\"incvar\" value=\"1\" checked=\"checked\" />";
    print lprint("Include a variable list as the first line.") . "<br />\n";
    print "<input type=\"checkbox\" name=\"cite\" value=\"1\" checked=\"checked\" />";
    print lprint("Put citation marks around non-numerical values.") . "<br />\n";
    print "<input type=\"checkbox\" name=\"win\" value=\"1\" checked=\"checked\" />";
    print lprint("Use DOS/windows-style linefeeds. Disable this if you are using unixoid software.") . "<br />\n";

    print "<br />\n";
    print "<input type=\"submit\" value=\"Get data\" />\n";
    print "</form>\n";

    1;
}

sub PrintDataContentType
{
    my ($self) = shift;
    my($enc) = $ENV{"_SURVEY_ENCODING"} || "UTF-8";
    $self->GoodContentType("text/plain; charset=$enc");
}

sub OneLineDesc
{
    return lprint(
"Generic delimited-fields export. Exports data and metadata as lists delimited with selectable character. Suitable for import in many statistics programs and databases, such as MS Access and MiniTab."
    );
}

sub SanityParse
{
    my ($self, $string) = @_;

    # Replace delimiter if through data
    my ($d) = $self->{DELIMITER};

    # Escape "dangerous" delimiters
    $d =~ s/([\.\+\\\$\^\(\)\[\]\{\}\*\|])/\\$1/g;

    if ($d eq " ")
    {
        $string =~ s/$d/\_/g;
    }
    else
    {
        $string =~ s/$d/\ /g;
    }

    return $string;
}

1;

