#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2005  Joel Palmius - bugant
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Export::GroupedFrequency;
use strict;

@Survey::Export::GroupedFrequency::ISA = qw(Survey::Export::Export);

use Survey::Language;
use Survey::Slask;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub new
{
    my ($crap, $doc, $arg, $data) = @_;

    my ($self) = {};
    bless $self;

    $self->DoCommonStuff($doc, $arg, $data);

    return $self;
}

sub GetTitle
{
    my ($self) = shift;
    return "Grouped Frequency Table";
}

sub GetCredits
{
    my ($self) = shift;
    return lprint("This is an optional add-on module in Mod_Survey. Developed by bugant with many hints from joel.");
}

sub printHead
{
    my ($self) = shift;
    Survey::Slask->HtmlHead();
    print "    <title>" . $self->{DOCUMENT}->GetOption("TITLE") . "</title>\n";
    print "    <style><!--\n";
    print "      body     { background-color: white; color: black; }\n";
    print "      .tblcap  { font-style: sans-serif; font-size: 16pt; font-weight: bold; }\n";
    print "      .varcap  { font-style: serif; font-size: 14pt; font-weight: bold; }\n";
    print "    --></style>\n";
    print "  </head>\n";
    print "  <body>\n";

    1;
}

sub printFoot
{
    print "\n  </body>\n";
    print "</html>\n";
}

sub PrintDataContents
{
    my ($self) = shift;
    my ($arg)  = $self->{ARGUMENT};

    my ($stat) = Survey::Export::StatUtils->new($self->{DOCUMENT}, $self->{DATA});

    my ($variable) = $arg->ArgByName("variable");
    my ($groupVar) = $arg->ArgByName("groupby");

    my ($i) = 0;

    my ($rel)    = $arg->ArgByName("rel");
    my ($abs)    = $arg->ArgByName("abs");
    my ($val)    = $arg->ArgByName("val");
    my ($cap)    = $arg->ArgByName("cap");
    my ($varcap) = $arg->ArgByName("varcap");

    #  my($graph) = $arg->ArgByName("graph");
    my ($total)   = $arg->ArgByName("total");
    my ($illegal) = $arg->ArgByName("illegal");

    my ($numcol) = 1 + scalar(@{ $stat->DistinctValueArray($groupVar) });

    #  if($graph) { $numcol++; }

    if ($numcol < 1)
    {
        print "no columns, giving up.";
        return;
    }

    my (@meta) = @{ $self->{DATA}->{META} };
    my ($ent);
    my ($metaentry);
    my ($metagroup);
    foreach $ent (@meta)
    {
        if ($ent->{NAME} eq $variable)
        {
            $metaentry = $ent;
        }

        if ($ent->{NAME} eq $groupVar)
        {
            $metagroup = $ent;
        }
    }
    my ($doc) = $self->{DOCUMENT};

    # define the new group
    $stat->DefineGroups($groupVar);

    $self->printHead();

    if ($abs)
    {
        $self->printTab("abs",
                        $val,
                        $cap,
                        $varcap,
                        $illegal,
                        $variable,
                        $groupVar,
                        $numcol,
                        $ent,
                        $metaentry,
                        $metagroup,
                        $stat,
                        $doc,
                        $total);
        print "<br /><br />\n";
    }

    if ($rel)
    {
        $self->printTab("rel",
                        $val,
                        $cap,
                        $varcap,
                        $illegal,
                        $variable,
                        $groupVar,
                        $numcol,
                        $ent,
                        $metaentry,
                        $metagroup,
                        $stat,
                        $doc,
                        $total);
    }

    $self->printFoot();

    1;
}

sub printTab
{

    my ($self,
        $freqType,
        $val,
        $cap,
        $varcap,
        $illegal,
        $variable,
        $groupVar,
        $numcol,
        $ent,
        $metaentry,
        $metagroup,
        $stat,
        $doc,
        $total) = @_;

    my ($tagno)      = $metaentry->{TAGNO};
    my ($type)       = $metaentry->{TYPE};
    my ($tagnoGroup) = $metagroup->{TAGNO};
    my ($typeGroup)  = $metagroup->{TYPE};
    my ($translate)  = $doc->Translate($type);
    my ($translate1) = $doc->Translate($typeGroup);

    my ($ttlbg)     = "bgcolor=\"#FFFFBB\"";
    my ($captbg)    = "bgcolor=\"#BBBBFF\"";
    my ($captclass) = "class=\"tblcap\"";
    my ($varbg)     = "bgcolor=\"#DDDDDD\"";
    my ($varclass)  = "class=\"varcap\"";
    my ($padspace)  = "cellspacing=\"0\" cellpadding=\"3\"";
    my ($align)     = "valign=\"top\"";
    my ($optwidth)  = "width=\"100\"";
    my ($border)    = "border=\"1\"";

    my ($ind1) = "    ";
    my ($ind2) = $ind1 . "  ";
    my ($ind3) = $ind2 . "  ";
    my ($ind4) = $ind2 . "  ";

    my ($str) = "";

    if ($freqType eq "abs")
    {
        $str = "Absolute Frequencies";
    }
    if ($freqType eq "rel")
    {
        $str = "Relative Frequencies";
    }

    #    print $ind1 . "<p>" . $str . "</p>\n";
    print $ind1 . "<table $border cols=\"$numcol\" $padspace>\n";
    print $ind2 . "<tr><td width=\"100\" colspan=\"$numcol\" $ttlbg>$str</td></tr>\n";
    print $ind2 . "<tr>\n";
    print $ind3 . "<td $captbg valign=\"top\" align=\"center\" height=\"30\">\n";
    if ($varcap)
    {
        my ($name) = $metaentry->{VARIABLECAPTION} || "[unknown variable]";
        print $ind4 . "<span $captclass>" . $self->SanityParse($name) . "</span>\n";
    }
    else
    {
        print $ind4 . "<span $captclass>" . $self->SanityParse($variable) . "</span>\n";
    }
    print $ind3 . "</td>\n";

    my (%res) = {};
    my (@distinct);
    my ($count);
    my ($value);

    #  print group elements
    if ($illegal)
    {
        @distinct = sort @{ $stat->DistinctValueArray($groupVar) };

        if ($freqType eq "abs")
        {
            %res = $stat->AllGroupsAbsoluteFrequencyTable($variable);
        }
        if ($freqType eq "rel")
        {
            %res = $stat->AllGroupsRelativeFrequencyTable($variable);
        }

        #$count    = $stat->AllGroupsCount($groupVar);
    }
    else
    {
        @distinct = sort @{ $stat->LegalDistinctValueArray($groupVar) };
        if ($freqType eq "abs")
        {
            %res = $stat->AllGroupsLegalAbsoluteFrequencyTable($variable);
        }
        if ($freqType eq "rel")
        {
            %res = $stat->AllGroupsLegalRelativeFrequencyTable($variable);
        }

        #$count    = $stat->AllGroupsLegalCount($groupVar);
    }

    foreach $value (@distinct)
    {
        if (!$val)
        {
            print $ind3 . "<td width=\"50\">$value</td>\n";
        }
        else
        {
            my ($captext) = "[ill]";
            my ($eval)    = "\$captext = $translate1->GetValueCaption(\$doc,undef,undef,undef,$tagnoGroup,$value);";
            eval $eval;
            print $ind3 . "<td $varbg>$captext</td>\n";
        }
    }

    print $ind2 . "</tr>\n";

    my ($rows);
    my ($i);

    if ($illegal)
    {
        $rows     = scalar @{ $stat->DistinctValueArray($variable) };
        @distinct = sort @{ $stat->DistinctValueArray($variable) };
    }
    else
    {
        $rows     = scalar @{ $stat->LegalDistinctValueArray($variable) };
        @distinct = sort @{ $stat->LegalDistinctValueArray($variable) };
    }

    my ($k);

    for ($i = 0 ; $i < $rows ; $i++)
    {
        $value = $distinct[$i];

        print $ind2 . "<tr>\n";

        if (!$val)
        {
            print $ind3 . "<td width=\"50\">$value</td>\n";
        }
        else
        {
            my ($captext) = "[ill]";
            my ($eval)    = "\$captext = $translate->GetValueCaption(\$doc,undef,undef,undef,$tagno,$value);";
            eval $eval;
            print $ind3 . "<td $varbg>$captext</td>\n";
        }

        foreach $k (sort (keys %res))
        {
            if ($res{$k}{$value})
            {
                if ($freqType eq "abs")
                {
                    print $ind3 . "<td>" . $res{$k}{$value} . "</td>\n";
                }
                if ($freqType eq "rel")
                {
                    print $ind3 . "<td>" . (int(10000 * $res{$k}{$value})) / 100 . "%</td>\n";
                }
            }
            else
            {
                print $ind3 . "<td> 0 </td>\n";
            }
        }

        print $ind2 . "</tr>\n";
    }

    if ($total && ($freqType eq "abs"))
    {
        print $ind2 . "<tr>\n";
        print $ind3 . "<td $varbg>" . lprint("Total") . "</td>\n";

        my ($group);

        foreach $group (sort (keys %res))
        {
            my (%laft) = $stat->LegalAbsoluteFrequencyTable($variable, $group);
            my ($l);

            my ($grptot) = 0;

            foreach $l (keys %laft)
            {
                $grptot += $laft{$l};
            }

            print $ind3 . "<td>" . $grptot . "</td>\n";

        }

        print $ind2 . "</tr>\n";
    }

    print $ind1 . "</table>\n";
}

sub GetID
{
    my ($self) = shift;
    return "gfreq";
}

sub PrintWelcomeBodyContents
{
    my ($self) = shift;

    print "<span class=\"subhead\">" . lprint("Info") . "</span><br /><br />\n";
    print lprint("The Frequency Table export calculates in which frequency a value occurs within") . " ";
    print lprint("a specified variable. Frequencies can be absolute or relative. As a limitation,") . " ";
    print lprint("only numerical variables are available for study through this module.");
    print "<br /><br />\n";
    print "<span class=\"subhead\">" . lprint("Display") . "</span><br /><br />\n";
    print $self->GetBaseForm("html");

    print lprint("Variable to use as group (columns entry):") . " ";
    print "<select size=\"1\" name=\"groupby\">\n";

    my ($case) = $self->{DATA}->{META};

    my ($i);
    for ($i = scalar(@{$case}) - 1 ; $i >= 0 ; $i--)
    {
        my ($entry) = @{$case}[$i];
        if (!$entry->{ISNUMERIC})
        {
            splice(@{$case}, $i, 1);
        }
    }

    my ($entry);

    foreach $entry (@{$case})
    {
        my ($name) = $entry->{NAME};
        print "  <option value=\"$name\">$name</option>\n";
    }
    print "</select><br /><br />\n";

    print lprint("Variable for which to display descriptive statistics:") . " ";
    print "<select size=\"1\" name=\"variable\">\n";

    $case = $self->{DATA}->{META};

    for ($i = scalar(@{$case}) - 1 ; $i >= 0 ; $i--)
    {
        $entry = @{$case}[$i];
        if (!$entry->{ISNUMERIC})
        {
            splice(@{$case}, $i, 1);
        }
    }

    foreach $entry (@{$case})
    {
        my ($name) = $entry->{NAME};
        print "  <option value=\"$name\">$name</option>\n";
    }
    print "</select><br /><br />\n";

    print "Settings for frequency table: <br />\n";
    print "<input type=\"checkbox\" name=\"val\" value=\"1\" checked=\"checked\" /> ";
    print lprint("Use value labels rather than values") . "<br />\n";
    print "<input type=\"checkbox\" name=\"rel\" value=\"1\" checked=\"checked\" /> ";
    print lprint("Include relative frequencies") . "<br />\n";
    print "<input type=\"checkbox\" name=\"abs\" value=\"1\" checked=\"checked\" /> ";
    print lprint("Include absolute frequencies") . "<br />\n";
    print "<input type=\"checkbox\" name=\"varcap\" value=\"1\" checked=\"checked\" /> ";
    print lprint("Use variable caption rather than variable name") . "<br />\n";
    print "<input type=\"checkbox\" name=\"illegal\" value=\"1\" /> ";
    print lprint("Include invalid cases") . "<br />\n";
    print "<input type=\"checkbox\" name=\"total\" value=\"1\" checked=\"checked\" /> ";
    print lprint("Print totals") . "<br />\n";

    print "<br />\n";
    print "<input type=\"submit\" value=\"Get data\" />\n";
    print "</form>\n";

    1;
}

sub PrintDataContentType
{
    my ($self) = shift;
    my($enc) = $ENV{"_SURVEY_ENCODING"} || "UTF-8";
    $self->GoodContentType("text/html; charset=$enc");
}

sub OneLineDesc
{
    return lprint("Grouped Frequency tables.");
}

1;

