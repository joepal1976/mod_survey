#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Export::HtmlTables;
use strict;

@Survey::Export::HtmlTables::ISA = qw(Survey::Export::Export);

use Survey::Language;
use Survey::Slask;
use Text::ParseWords;

use CGI;

sub new
{
    my ($crap, $doc, $arg, $data) = @_;

    my ($self) = {};
    bless $self;

    $self->DoCommonStuff($doc, $arg, $data);

    return $self;
}

sub GetTitle
{
    my ($self) = shift;
    return lprint("Html Tables");
}

sub GetCredits
{
    my ($self) = shift;
    return lprint("This is a standard module in Mod_Survey. A security update was added by BugAnt.");
}

sub printHead
{
    my ($self) = shift;
    Survey::Slask->HtmlHead();

    print "    <title>" . $self->{DOCUMENT}->GetOption("TITLE") . "</title>\n";
    print "    <style><!--\n";
    print "      body     { background-color: white; color: black; }\n";
    print "      .tblcap  { font-style: sans-serif; font-size: 16pt; font-weight: bold; }\n";
    print "      .varcap  { font-style: serif; font-size: 14pt; font-weight: bold; }\n";
    print "    --></style>\n";
    print "  </head>\n";
    print "  <body>\n";

    1;
}

sub printFoot
{
    print "\n  </body>\n";
    print "</html>\n";
}

sub printData
{
    my ($self) = shift;
    my ($arg)  = $self->{ARGUMENT};

    my ($border) = "border=\"";
    if ($arg->ArgByName("usegrid"))
    {
        $border .= "1";
    }
    else
    {
        $border .= "0";
    }
    $border .= "\"";

    my ($usetblcap) = 0;
    if ($arg->ArgByName("usecapt")) { $usetblcap = 1; }

    my ($multisplit) = 0;
    if ($arg->ArgByName("multi")) { $multisplit = 1; }

    my ($captbg)    = "";
    my ($captclass) = "";
    my ($varbg)     = "";
    my ($varclass)  = "";
    my ($padspace)  = "";
    my ($align)     = "";
    my ($optwidth)  = "";

    if ($arg->ArgByName("usestyle"))
    {
        $captbg    = "bgcolor=\"#BBBBFF\"";
        $captclass = "class=\"tblcap\"";
        $varbg     = "bgcolor=\"#DDDDDD\"";
        $varclass  = "class=\"varcap\"";
        $padspace  = "cellspacing=\"0\" cellpadding=\"3\"";
        $align     = "valign=\"top\"";
        $optwidth  = "width=\"100\"";
    }

    my ($ind1) = "    ";
    my ($ind2) = $ind1 . "  ";
    my ($ind3) = $ind2 . "  ";
    my ($ind4) = $ind2 . "  ";

    my ($numvar) = $self->{DATA}->{NUMVAR};

    my ($numcol) = $numvar;

    if ($multisplit)
    {

    }

    if ($arg->ArgByName("incdata"))
    {
        print $ind1 . "<table $border cols=\"$numcol\" $padspace>\n";
        if ($usetblcap)
        {
            print $ind2 . "<tr>\n";
            print $ind3 . "<td $captbg colspan=\"$numcol\" valign=\"top\" align=\"center\" height=\"30\">\n";
            print $ind4 . "<span $captclass>Data</span>\n";
            print $ind3 . "</td>\n";
            print $ind2 . "</tr>\n";
        }

        my ($case);
        $case = $self->{DATA}->{META};

        print $ind2 . "<tr>\n";
        for (my ($i) = 0 ; $i < $numvar ; $i++)
        {
            my ($entry) = @{$case}[$i];
            if ($entry)
            {
                if (!$arg->ArgByName("usevarcap"))
                {
                    my ($name) = $entry->GetName();
                    print $ind3 . "<td $varbg><span $varclass>$name</span></td>\n";
                }
                else
                {
                    my ($capt) = $self->SanityParse($entry->GetVariableCaption());
                    print $ind3 . "<td $varbg><span $varclass>$capt</span></td>\n";
                }
            }
        }
        print $ind2 . "</tr>\n";

        my (@cases) = ();

        if (defined($self->{DATA}) && defined($self->{DATA}->{CASES}))
        {
            @cases = @{ $self->{DATA}->{CASES} };
        }

        if (scalar(@cases) > 0)
        {
            foreach $case (@cases)
            {
                print $ind2 . "<tr>\n";
                for (my ($i) = 0 ; $i < $numvar ; $i++)
                {
                    my ($entry) = @{$case}[$i];
                    if ($entry)
                    {
                      my ($value);
                      my ($etype) = $entry->GetType();
                      if($etype ne "GEO")
                      {

                        if (!$arg->ArgByName("usevalcap"))
                        {
                          $value = $self->SanityParse($entry->GetValue());
                        }
                        else
                        {
                          $value = $self->SanityParse($entry->GetValueCaption());
                          if ($value eq "" || !defined($value))
                          {
                            $value = $self->SanityParse($entry->GetValue());
                          }
                        }

                        if ($arg->ArgByName("reinsert"))
                        {
                          $value =~ s/��/\<br \/\>/g;
                        }
                      }
                      else
                      {
                        $value = $entry->GetValue();
                        if($value && $arg->ArgByName("usegmap"))
                        {
                          my($link) = $value;
                          $link =~ s/ /+/g;
                          $value = "<a href=\"http://maps.google.se/maps?f=q&source=s_q&hl=sv&geocode=&q=$link\">$value</a>";

                        }
                      }

                        if ($arg->ArgByName("datnoempty") && (($value eq "") || ($value eq undef)))
                        {
                            $value = "&nbsp;";
                        }
                        print $ind3 . "<td $align>$value</td>\n";
                    }
                }
                print $ind2 . "</tr>\n";
            }
        }

        print $ind1 . "</table>\n";
        print $ind1 . "<br /><br />\n\n\n\n";
    }

    if ($arg->ArgByName("incmeta"))
    {
        if ($arg->ArgByName("metavarlist"))
        {
            print $ind1 . "<table $border cols=\"3\" $padspace>\n";
            if ($usetblcap)
            {
                print $ind2 . "<tr>\n";
                print $ind3 . "<td $captbg colspan=\"$numvar\" valign=\"top\" align=\"center\" height=\"30\">\n";
                print $ind4 . "<span $captclass>" . lprint("Variables") . "</span>\n";
                print $ind3 . "</td>\n";
                print $ind2 . "</tr>\n";
            }

            my ($case);

            print $ind2 . "<tr>\n";
            print $ind3 . "<td $optwidth $varbg $align><span $varclass>&nbsp;</span></td>\n";
            print $ind3 . "<td $optwidth $varbg $align><span $varclass>" . lprint("Type") . "</span></td>\n";
            print $ind3 . "<td $varbg $align><span $varclass>" . lprint("Caption") . "</span></td>\n";
            print $ind2 . "</tr>\n";

            $case = $self->{DATA}->{META};
            my ($entry);

            foreach $entry (@{$case})
            {
                if ($entry)
                {
                    print $ind2 . "<tr>\n";

                    print $ind3 . "<td $optwidth $varbg $align><span $varclass>" . $entry->GetName() . "</span></td>\n";
                    print $ind3 . "<td $optwidth $align>" . $entry->GetType() . "</td>\n";
                    print $ind3 . "<td $align>" . $self->SanityParse($entry->GetVariableCaption()) . "</td>\n";

                    print $ind2 . "</tr>\n";
                }
            }

            print $ind1 . "</table>\n";
            print $ind1 . "<br /><br />\n\n\n\n";
        }

        if ($arg->ArgByName("metacaplist"))
        {
            print $ind1 . "<table $border cols=\"3\" $padspace>\n";
            if ($usetblcap)
            {
                print $ind2 . "<tr>\n";
                print $ind3 . "<td $captbg colspan=\"$numvar\" valign=\"top\" align=\"center\" height=\"30\">\n";
                print $ind4 . "<span $captclass>" . lprint("Captions") . "</span>\n";
                print $ind3 . "</td>\n";
                print $ind2 . "</tr>\n";
            }

            my ($case);

            print $ind2 . "<tr>\n";
            print $ind3 . "<td $optwidth $varbg $align><span $varclass>&nbsp;</span></td>\n";
            print $ind3 . "<td $optwidth $varbg $align><span $varclass>" . lprint("Value") . "</span></td>\n";
            print $ind3 . "<td $varbg $align><span $varclass>" . lprint("Caption") . "</span></td>\n";
            print $ind2 . "</tr>\n";

            $case = $self->{DATA}->{META};
            my ($entry);

            foreach $entry (@{$case})
            {
                if ($entry)
                {
                    my ($pv)     = $entry->GetPossibleValues();
                    my (@posval) = @{$pv};
                    if (scalar(@posval) > 0)
                    {
                        my ($n) = 1;
                        my ($val);

                        foreach $val (@posval)
                        {
                            print $ind2 . "<tr>\n";
                            if ($n)
                            {
                                print $ind3
                                  . "<td $optwidth $varbg $align rowspan=\""
                                  . $self->SanityParse(scalar(@posval)) . "\">";
                                print "<span $varclass>" . $entry->GetName() . "</span></td>\n";
                                $n = 0;
                            }
                            print $ind3 . "<td $optwidth $align>" . $self->SanityParse($val->{VALUE}) . "</td>\n";
                            print $ind3 . "<td $align>" . $self->SanityParse($val->{CAPTION}) . "</td>\n";
                            print $ind2 . "</tr>\n";
                        }
                    }
                }
            }

            print $ind1 . "</table>\n";
            print $ind1 . "<br /><br />\n";

        }
    }

    1;
}

sub PrintDataContents
{
    my ($self) = shift;
    my ($arg)  = $self->{ARGUMENT};

    $self->printHead();
    $self->printData();
    $self->printFoot();

    1;
}

sub GetID
{
    my ($self) = shift;
    return "html";
}

sub PrintWelcomeBodyContents
{
    my ($self) = shift;

    print "<span class=\"subhead\">" . lprint("Info") . "</span><br /><br />\n";
    print lprint("The Html Tables export module handles a set of data exports formatted as HTML tables.") . " ";
    print lprint("Currently, this set consists of two major categories: The data export and the meta-data exports.")
      . " ";
    print "<br /><br />";
    print lprint("The <i>data</i> export is a table containing all the raw data, or in other words, the data that")
      . " ";
    print lprint("the respondents have submitted so far.") . "<br /><br />\n";
    print lprint("The <i>meta-data</i> exports is a number of the meta-data tables describing the data, or in other")
      . " ";
    print lprint("words things like a list of variable names, what captions they have and so on.") . "<br /><br />\n";
    print "<span class=\"subhead\">" . lprint("Download") . "</span><br /><br />\n";
    print $self->GetBaseForm("html");

    print "<input type=\"checkbox\" name=\"usestyle\" value=\"1\" checked=\"checked\" />";
    print lprint("Use font sizes, background colors and boldfacing to improve readability.") . "<br />\n";
    print "<input type=\"checkbox\" name=\"usegrid\" value=\"1\" />";
    print lprint("Use visible table grid.") . "<br />\n";
    print "<input type=\"checkbox\" name=\"usecapt\" value=\"1\" checked=\"checked\" />";
    print lprint("Insert table name captions.") . "<br />\n";

    print "<br />\n";

    print "<input type=\"checkbox\" name=\"incdata\" value=\"1\" checked=\"checked\" />";
    print lprint("Include data in file.") . "<br />\n";
    print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"checkbox\" name=\"usegmap\" value=\"1\" checked=\"checked\" />";
    print lprint("Link GEO tags to google maps.") . "<br />\n";
    print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"checkbox\" name=\"usevalcap\" value=\"1\" />";
    print lprint("Use caption rather than value whenever possible.") . "<br />\n";
    print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"checkbox\" name=\"usevarcap\" value=\"1\" />";
    print lprint("Use caption rather than variable name whenever possible.") . "<br />\n";
    print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"checkbox\" name=\"datnoempty\" value=\"1\" />";
    print lprint("Don\'t collapse empty table cells.") . "<br />\n";
    print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"checkbox\" name=\"reinsert\" value=\"1\" />";
    print lprint("Re-insert coded linebreaks.") . " "
      . lprint("(looks better, but breaks some functionality in Excel)")
      . "<br />\n";

    print "<br />\n";

    print "<input type=\"checkbox\" name=\"incmeta\" value=\"1\" />";
    print lprint("Include meta-data in file.") . "<br />\n";
    print
      "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"checkbox\" name=\"metavarlist\" value=\"1\" checked=\"checked\" />";
    print lprint("Include a list of variables and their respective captions.") . "<br />\n";
    print
      "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type=\"checkbox\" name=\"metacaplist\" value=\"1\" checked=\"checked\" />";
    print lprint("Include a conversion table between values and value captions.") . "<br />\n";

    #  print "<br />\n";
    #
    #  print "<input type=\"checkbox\" name=\"multi\" value=\"1\" checked=\"checked\" />";
    #  print "Split CHOICE into several variables when MULTI=\"no\" [<b>BETA CODE, use at own risk</b>]<br />\n";

    print "<br />\n";
    print "<input type=\"submit\" value=\"Get data\" />\n";
    print "</form>\n";

    1;
}

sub PrintDataContentType
{
    my ($self) = shift;
    my($enc) = $ENV{"_SURVEY_ENCODING"} || "UTF-8";
    $self->GoodContentType("text/html; charset=$enc");    
}

sub SanityParse
{

    #This method does a simply sanity check
    #for the html output.

    my ($crap, $out) = @_;

    #we don't want \ but \\
    $out =~ s/\\/\\\\/g;

    #we don't want < but &lt;
    $out =~ s/</\&lt\;/g;

    #we don't want > but &gt;
    $out =~ s/>/\&gt\;/g;

    #we don't want " but \"
    $out =~ s/\"/\\\"/g;

    #we don't want ' but \'
    $out =~ s/\'/\\\'/g;

    return $out;
}

sub OneLineDesc
{
    return lprint(
"Export data as HTML tables, with optional tables for meta-data. Suitable for presentations and import into spreadsheets like MS Excel."
    );
}

1;

