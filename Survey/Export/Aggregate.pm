#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Export::Aggregate;
use strict;

@Survey::Export::Aggregate::ISA = qw(Survey::Export::Export);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub new
{
    my ($crap, $doc, $arg, $data) = @_;

    my ($self) = {};
    bless $self;

    $self->DoCommonStuff($doc, $arg, $data);

    return $self;
}

sub GetTitle
{
    my ($self) = shift;
    return lprint("Aggregate");
}

sub GetCredits
{
    my ($self) = shift;
    return lprint("This is a standard module in Mod_Survey");
}

sub PrintDataContents
{
  Survey::Slask->HtmlHead();
  print "</head><body>\n";

    my ($self) = shift;
    my ($arg)  = $self->{ARGUMENT};

    my ($case, $isfirst, $entry,$name);
    my ($delim) = ";";
    my ($line)  = "\n";

    my($casevar) = $arg->ArgByName("casevar");
    my($timeline) = $arg->ArgByName("timeline");
    my($observations) = $arg->ArgByName("observations");

    if($observations eq "-1") { $observations = undef; }

    my(@variablenames) = ();
    my(@tmp) = ();

    $case = $self->{DATA}->{META};

    foreach $entry (@{$case})
    {
      $name = $entry->{NAME};

      if ($entry)
      {
        @tmp = grep(/$name/,("srvkey",$casevar,$timeline,$observations));
        if(scalar(@tmp) == 0)
        {
          push(@variablenames,$name);
        }
      }
    }

    @variablenames = sort @variablenames;

    my (@cases) = ();

    if (defined($self->{DATA}) && defined($self->{DATA}->{CASES}))
    {
        @cases = @{ $self->{DATA}->{CASES} };
    }

    my(%casevals,%timevals,%obsvals);

    if (scalar(@cases) > 0)
    {
        foreach $case (@cases)
        {
            my (@entries) = @{$case};

            foreach $entry (@entries)
            {
                if ($entry)
                {
                  if($entry->{NAME} eq $casevar)
                  {
                    $casevals{$entry->{VALUE}} = 1;
                  }

                  if($entry->{NAME} eq $timeline)
                  {
                    $timevals{$entry->{VALUE}} = 1;
                  }

                  if($observations && $entry->{NAME} eq $observations)
                  {
                    $obsvals{$entry->{VALUE}} = 1;
                  }
                }
            }
        }
    }

    @cases = keys(%casevals);
    my(@times,@obs,$ob,$time,$hashname,%turnedhash,%entryhash);

    if($observations)
    {
      @obs = sort keys(%obsvals);
    }

    foreach $case (@cases)
    {
      @times = keys(%timevals);

      foreach $time (@times)
      {
        if($observations)
        {
          foreach $ob (@obs)
          {
            foreach $name (@variablenames)
            {
              $hashname = $case . "_" . $time . "_" . $ob . "_" . $name;
              $turnedhash{$hashname} = "";
            }
          }
        }
        else
        {
          foreach $name (@variablenames)
          {
            $hashname = $case . "_" . $time . "_" . $name;
            $turnedhash{$hashname} = "";
          }
        }
      }
    }

    @cases = @{ $self->{DATA}->{CASES} };

    if (scalar(@cases) > 0)
    {
        foreach $case (@cases)
        {
            my (@entries) = @{$case};

            foreach $entry (@entries)
            {
              $entryhash{$entry->{NAME}} = $entry->{VALUE};
            }

            $line = $entryhash{$casevar};
            $time = $entryhash{$timeline};
            if($observations)
            {
              $ob = $entryhash{$observations};
            }

            foreach $name (@variablenames)
            {
              if($observations)
              {
                $hashname = $line . "_" . $time . "_" . $ob . "_" . $name;
              }
              else
              {
                $hashname = $line . "_" . $time . "_" . $name;
              }

              $turnedhash{$hashname} = $entryhash{$name};
            }
        }
    }

    @cases = sort keys(%casevals);
    @times = sort keys(%timevals);

    my($numnames) = scalar(@variablenames);
    my($numtimes) = scalar(@times);

    my($numcols,$numobs,$on,$tn);

    if($observations)
    {
      $numobs = scalar(@obs);
      $numcols = $numnames * $numtimes * $numobs + 1; 
    }
    else
    {
      $numcols = $numnames * $numtimes + 1; 
    }
    
    print "<table cols='$numcols' border='1'>\n";

    if($observations)
    {
      my($timewidth) = $numnames * $numobs;

      if($arg->ArgByName("inchead"))
      {
        print "<tr><td>&nbsp;</td>";
        foreach $time (@times)
        {
          print "<td colspan='$timewidth'>$time</td>";
        }
        print "</tr>\n";

        print "<tr><td>&nbsp;</td>";
        foreach $time (@times)
        {
          foreach $ob (@obs)
          {
            print "<td colspan='$numnames'>$ob</td>";
          }
        }
        print "</tr>\n";
      }
      if($arg->ArgByName("incvars"))
      {
        print "<tr><td>$casevar</td>";

        $tn = 0;

        foreach $time (@times)
        {
          $on = 0;
          $tn++;

          foreach $ob (@obs)
          {
            $on++;
            foreach $name (@variablenames)
            {
              if($arg->ArgByName("varrename"))
              {
                print "<td>t" . $tn . "o" . $on . $name . "</td>";
              }
              else
              {
                print "<td>$name</td>";
              }
            }
          }
        }
        print "</tr>\n";
      }
    }
    else
    {
      if($arg->ArgByName("inchead"))
      {
        print "<tr><td>&nbsp;</td>";
        foreach $time (@times)
        {
          print "<td colspan='$numnames'>$time</td>";
        }
        print "</tr>\n";
      }
      if($arg->ArgByName("incvars"))
      {
        print "<tr><td>$casevar</td>";

        $tn = 0;
        foreach $time (@times)
        {
          $tn++;
          foreach $name (@variablenames)
          {
            if($arg->ArgByName("varrename"))
            {              
              print "<td>t" . $tn . $name . "</td>";
            }
            else
            {              
              print "<td>$name</td>";
            }
          }
        }
        print "</tr>\n";
      }
    }

    foreach $case (@cases)
    {
      print "<tr><td>$case</td>";

      foreach $time (@times)
      {
        if($observations)
        {
          foreach $ob (@obs)
          {
            foreach $name (@variablenames)
            {
              $hashname = $case . "_" . $time . "_" . $ob . "_" . $name;
              print "<td>";
              print $turnedhash{$hashname} || "&nbsp;";
              print "</td>";
            }
          }
        }
        else
        {
          foreach $name (@variablenames)
          {
            $hashname = $case . "_" . $time . "_" . $name;
            print "<td>";
            print $turnedhash{$hashname} || "&nbsp;";
            print "</td>";
          }
        }
      }
      print "</tr>\n";
    }
    print "</table></body></html>";

    1;
}

sub GetID
{
    my ($self) = shift;
    return "aggregate";
}

sub PrintWelcomeBodyContents
{
    my ($self) = shift;

    print "<span class=\"subhead\">" . lprint("Info") . "</span><br /><br />\n";
    print lprint("The Aggregate module exports data reformatted for time series. As an illustration") . " ";
    print lprint("a repeated measurement in the form of a diary could be formatted so that each") . " ";
    print lprint("person appear as one row in the output, despite having several entries in the data.") . " ";

    print "<br /><br />\n";

    print lprint("To follow the diary example, the Case Variable below would be the variable") . " ";
    print lprint("defining the rows, in this case the test person. The Timeline Variable would") . " ";
    print lprint("be the day of the diary entry, and the Observation Variable would be if there") . " ";
    print lprint("were repeated measurements during the day.") . " ";

    print "<br /><br />\n";

    print lprint("The Observation Variable can be left empty if the Timeline Variable is sufficient for grouping.") . " ";

    print "<br /><br />\n";

    print "<span class=\"subhead\">" . lprint("Download") . "</span><br /><br />\n";
    print $self->GetBaseForm("txt");


    my ($case) = $self->{DATA}->{META};

    print "<!--\n";

    my ($i);
    for ($i = scalar(@{$case}) - 1 ; $i >= 0 ; $i--)
    {
        my ($entry) = @{$case}[$i];
        print $entry->{NAME} . "\n";
        if ($entry->{NAME} eq "srvkey")
        {
            splice(@{$case}, $i, 1);
        }
    }
    print "-->";

    print lprint("Case variable:") . " ";
    print "<select size=\"1\" name=\"casevar\">\n";

    my ($entry);

    foreach $entry (@{$case})
    {
        my ($name) = $entry->{NAME};
        print "  <option value=\"$name\">$name</option>\n";
    }

    print "</select><br /><br />\n";

    print lprint("Timeline variable:") . " ";
    print "<select size=\"1\" name=\"timeline\">\n";

    foreach $entry (@{$case})
    {
        my ($name) = $entry->{NAME};
        print "  <option value=\"$name\">$name</option>\n";
    }

    print "</select><br /><br />\n";

    print lprint("Observations variable:") . " ";

    print "<select size=\"1\" name=\"observations\">\n";
    print "  <option value=\"-1\"></option>\n";
    foreach $entry (@{$case})
    {
        my ($name) = $entry->{NAME};
        print "  <option value=\"$name\">$name</option>\n";
    }
    print "</select><br /><br />\n";

    print "<input type=\"checkbox\" name=\"inchead\" value=\"1\" checked=\"checked\" />";
    print lprint("Include captions for timeline and observations.") . "<br />\n";
    print "<input type=\"checkbox\" name=\"incvars\" value=\"1\" checked=\"checked\" />";
    print lprint("Include variable names.") . "<br />\n";
    print "<input type=\"checkbox\" name=\"varrename\" value=\"1\" checked=\"checked\" />";
    print lprint("Rename variables by adding timeline and observations.") . "<br />\n";

    print "<br />\n";
    print "<input type=\"submit\" value=\"Get data\" />\n";
    print "</form>\n";

    1;
}

sub PrintDataContentType
{
    my ($self) = shift;
    my($enc) = $ENV{"_SURVEY_ENCODING"} || "UTF-8";
    $self->GoodContentType("text/html; charset=$enc");
}

sub OneLineDesc
{
    return lprint("Generic aggregate export. Exports time series data.");
}

sub SanityParse
{
    my ($self, $string) = @_;

    # Replace delimiter if through data
    my ($d) = $self->{DELIMITER};

    # Escape "dangerous" delimiters
    $d =~ s/([\.\+\\\$\^\(\)\[\]\{\}\*\|])/\\$1/g;

    if ($d eq " ")
    {
        $string =~ s/$d/\_/g;
    }
    else
    {
        $string =~ s/$d/\ /g;
    }

    return $string;
}

1;

