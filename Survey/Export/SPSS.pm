#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Export::SPSS;

@Survey::Export::SPSS::ISA = qw(Survey::Export::Export);

# Don't include apache stuff directly in modules
#use Apache2::Const qw(:common);
#use Apache::File;

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;
use strict;

use CGI;

sub new
{
    my ($crap, $doc, $arg, $data) = @_;

    my ($self) = {};
    bless $self;

    $self->DoCommonStuff($doc, $arg, $data);

    return $self;
}

sub GetTitle
{
    my ($self) = shift;
    return "SPSS";
}

sub GetCredits
{
    my ($self) = shift;
    return lprint("This is a standard module in Mod_Survey. Citing and multisplitting was contributed by BugAnt.");
}

sub PrintScript
{
    my ($self) = shift;
    my ($arg)  = $self->{ARGUMENT};

    print "INPUT PROGRAM.\n";
    print "DATA LIST ";
    if (!$arg->ArgByName("inline"))
    {
        my ($crap);
        my ($uri)  = $self->{DOCUMENT}->GetOption("URI");
        my (@path) = split(/\//, $uri);
        my ($fn)   = @path[@path - 1];
        ($fn, $crap) = split(/\./, $fn, 2);
        print "FILE=\"$fn.dat\" ";
    }
    print "LIST(\"\;\") /";

    my ($case);

    $case = $self->{DATA}->{META};
    if ($arg->ArgByName("nostring"))
    {
        my ($i);
        for ($i = scalar(@{$case}) - 1 ; $i >= 0 ; $i--)
        {
            my ($entry) = @{$case}[$i];
            if (!$entry->{ISNUMERIC})
            {
                splice(@{$case}, $i, 1);
            }
        }
    }

    my ($entry);

    foreach $entry (@{$case})
    {

        # Support filtering out MEMO fields
        my ($type) = $entry->GetType();
        if ($arg->ArgByName("nomemo") && ($type eq "MEMO"))
        {
            $entry = undef;
        }

        if ($entry)
        {
            my ($isamultic) =
              ($entry->GetType() eq "CHOICE") && ($self->{DOCUMENT}->GetTagParam($entry->GetTagNo(), "MULTI") eq "yes");

            if (($isamultic) && $arg->ArgByName("split") && ($ENV{"_SURVEY_CHOICE_SPLIT"} ne "yes"))
            {
                my ($howmuch) = $self->{DOCUMENT}->GetTagParam($entry->GetTagNo(), "ELEMENTS");
                my ($i)       = 0;
                my ($str_i)   = "";
                for ($i = 0 ; $i < $howmuch ; $i++)
                {
                    print " ";
                    if ($i < 10)
                    {
                        $str_i = "0" . $i;
                    }
                    else
                    {
                        $str_i = $i;
                    }
                    print $self->SanityParse($entry->GetName()) . $str_i . "(F8.0)";
                }

                if (($self->{DOCUMENT}->GetTagParam($entry->GetTagNo(), "OTHERFIELD")) ne "-1")
                {
                    print " " . $entry->GetName() . "_o(A80)";
                }
            }

            else
            {
                print " ";
                print $self->SanityParse($entry->GetName()) . "(";
                if ($entry->IsNumerical())
                {
                    print "F8.0";
                }
                else
                {
                    print "A";
                    my ($flen) = $entry->GetFieldLength();
                    if (!$flen) { $flen = 250; }    # Assume this is memo, break at 250 chars
                    print $flen;
                }
                print ")";
            }
        }
    }
    print ".\n";
    print "END INPUT PROGRAM.\n\n";

    my (@cases) = ();

    if (defined($self->{DATA}) && defined($self->{DATA}->{CASES}))
    {
        @cases = @{ $self->{DATA}->{CASES} };
    }

    if (scalar(@cases) > 0)
    {
        if ($arg->ArgByName("inline"))
        {
            print "BEGIN DATA\n";
            $self->PrintDataFile();
            print "END DATA.\n";
        }

        print "\nLIST.\n\n";

        if ($arg->ArgByName("vallab"))
        {
            foreach $entry (@{$case})
            {

                # Support filtering out MEMO fields
                if ($entry)
                {
                    my ($type) = $entry->GetType();
                    if ($arg->ArgByName("nomemo") && ($type eq "MEMO"))
                    {
                        $entry = undef;
                    }
                }
                if ($entry)
                {
                    my ($pv)     = $entry->GetPossibleValues();
                    my (@posval) = @{$pv};

                    my ($isamultic) =
                         ($entry->GetType() eq "CHOICE")
                      && ($self->{DOCUMENT}->GetTagParam($entry->GetTagNo(), "MULTI") eq "yes");

                    if (   (scalar(@posval) > 0)
                        && ($isamultic)
                        && $arg->ArgByName("split")
                        && ($ENV{"_SURVEY_CHOICE_SPLIT"} ne "yes"))
                    {
                        print "VALUE LABELS ";
                        my ($howmuch) = $self->{DOCUMENT}->GetTagParam($entry->GetTagNo(), "ELEMENTS");
                        my ($i)       = 0;
                        my ($str_i)   = "";
                        my ($val);
                        for ($i = 0 ; $i < $howmuch ; $i++)
                        {
                            if ($i < 10)
                            {
                                $str_i = "0" . $i;
                            }
                            else
                            {
                                $str_i = $i;
                            }
                            print $self->SanityParse($entry->GetName()) . $str_i . " ";
                        }

                        foreach $val (@posval)
                        {
                            my ($value) = $val->{VALUE};
                            my ($capt)  = $val->{CAPTION};
                            $capt =~ s/'/"/g;
                            print " $value '$capt'";
                        }

                        print ".\n";

                    }
                    else
                    {
                        if ((scalar(@posval) > 0) && ($entry->IsNumerical()))
                        {
                            my ($n) = 1;
                            my ($val);

                            print "VALUE LABELS " . $entry->GetName();

                            foreach $val (@posval)
                            {
                                my ($value) = $val->{VALUE};
                                my ($capt)  = $val->{CAPTION};
                                $capt =~ s/'/"/g;

                                print " $value '$capt'";
                            }
                            print ".\n";

                        }
                    }
                }
            }

            print "\n";
        }

        if ($arg->ArgByName("varlab"))
        {
            foreach $entry (@{$case})
            {
                if ($entry)
                {

                    # Support filtering out MEMO fields
                    my ($type) = $entry->GetType();
                    if ($arg->ArgByName("nomemo") && ($type eq "MEMO"))
                    {
                        $entry = undef;
                    }
                }
                if ($entry)
                {
                    my ($caption) = $entry->GetVariableCaption();
                    $caption =~ s/'/"/g;
                    my ($name) = $entry->GetName();

                    my ($isamultic) =
                         ($entry->GetType() eq "CHOICE")
                      && ($self->{DOCUMENT}->GetTagParam($entry->GetTagNo(), "MULTI") eq "yes");

                    if ($caption)
                    {
                        if (   ($isamultic)
                            && $arg->ArgByName("split")
                            && ($caption)
                            && ($ENV{"_SURVEY_CHOICE_SPLIT"} ne "yes"))
                        {
                            my ($howmuch) = $self->{DOCUMENT}->GetTagParam($entry->GetTagNo(), "ELEMENTS");
                            my ($i)       = 0;
                            my ($str_i)   = "";
                            for ($i = 0 ; $i < $howmuch ; $i++)
                            {
                                if ($i < 10)
                                {
                                    $str_i = "0" . $i;
                                }
                                else
                                {
                                    $str_i = $i;
                                }
                                print "VARIABLE LABELS $name$str_i '$caption'.\n";
                            }
                            if (($self->{DOCUMENT}->GetTagParam($entry->GetTagNo(), "OTHERFIELD")) ne "-1")
                            {
                                print "VARIABLE LABELS " . $name . "_o '$caption'.\n";
                            }
                        }
                        else
                        {
                            print "VARIABLE LABELS $name '$caption'.\n";
                        }
                    }
                }
            }
            print "\n";
        }

        if ($arg->ArgByName("missing"))
        {
            my ($systmiss) = $self->{DOCUMENT}->GetOption("NOTDISPLAYEDVAL");

            foreach $entry (@{$case})
            {

                # Support filtering out MEMO fields
                if ($entry)
                {
                    my ($type) = $entry->GetType();
                    if ($arg->ArgByName("nomemo") && ($type eq "MEMO"))
                    {
                        $entry = undef;
                    }
                }
                if ($entry)
                {
                    my ($caption) = $entry->GetVariableCaption();
                    $caption =~ s/'/"/g;

                    my ($isamultic) =
                         ($entry->GetType() eq "CHOICE")
                      && ($self->{DOCUMENT}->GetTagParam($entry->GetTagNo(), "MULTI") eq "yes");

                    my ($name) = $entry->GetName();
                    if ($caption && $entry->IsNumerical())
                    {
                        if (   ($isamultic)
                            && $arg->ArgByName("split")
                            && ($caption)
                            && ($ENV{"_SURVEY_CHOICE_SPLIT"} ne "yes"))
                        {
                            my ($howmuch) = $self->{DOCUMENT}->GetTagParam($entry->GetTagNo(), "ELEMENTS");
                            my ($i)       = 0;
                            my ($str_i)   = "";
                            for ($i = 0 ; $i < $howmuch ; $i++)
                            {
                                if ($i < 10)
                                {
                                    $str_i = "0" . $i;
                                }
                                else
                                {
                                    $str_i = $i;
                                }
                                print "MISSING VALUES $name$str_i (";
                                my ($usermiss) = $self->{DOCUMENT}->GetTagParam($entry->GetTagNo(), "ILLEGALVAL");
                                print "$systmiss,$usermiss";
                                print ").\n";
                            }
                            if (($self->{DOCUMENT}->GetTagParam($entry->GetTagNo(), "OTHERFIELD")) ne "-1")
                            {
                                print "MISSING VALUES " . $name . "_o (";
                                my ($usermiss) = $self->{DOCUMENT}->GetTagParam($entry->GetTagNo(), "ILLEGALVAL");
                                print "$systmiss,$usermiss";
                                print ").\n";
                            }
                        }
                        else
                        {
                            print "MISSING VALUES $name (";
                            my ($usermiss) = $self->{DOCUMENT}->GetTagParam($entry->{TAGNO}, "ILLEGALVAL");
                            print "$systmiss,$usermiss";
                            print ").\n";
                        }
                    }
                }
            }
            print "\n";
        }

    }

    1;
}

sub PrintDataFile
{
    my ($self) = shift;
    my ($arg)  = $self->{ARGUMENT};

    my (@cases) = @{ $self->{DATA}->{CASES} };
    my ($case, $isfirst);

    my ($ns) = $arg->ArgByName("nostring");

    if (scalar(@cases) > 0)
    {
        foreach $case (@cases)
        {
            my ($entry);
            my (@entries) = @{$case};

            $isfirst = 1;
            foreach $entry (@entries)
            {
                if ($entry)
                {

                    # Support filtering out MEMO fields
                    my ($type) = $entry->GetType();
                    if ($arg->ArgByName("nomemo") && ($type eq "MEMO"))
                    {
                        $entry = undef;
                    }
                }
                if ($entry && (!$ns || $entry->{ISNUMERIC}))
                {

                    if ($isfirst)
                    {
                        $isfirst = 0;
                    }
                    else
                    {
                        print ";";
                    }

                    my ($isamultic) =
                         ($entry->GetType() eq "CHOICE")
                      && ($self->{DOCUMENT}->GetTagParam($entry->GetTagNo(), "MULTI") eq "yes");

                    if (($isamultic) && $arg->ArgByName("split") && ($ENV{"_SURVEY_CHOICE_SPLIT"} ne "yes"))
                    {
                        my ($howmuch) = $self->{DOCUMENT}->GetTagParam($entry->GetTagNo(), "ELEMENTS");
                        my ($i)       = 0;
                        my ($vals)    = $self->SanityParse($entry->GetValue());
                        my (@valus)   = split(/,/, $vals);
                        my ($otherf)  = "";

                        if ($self->{DOCUMENT}->GetTagParam($entry->GetTagNo(), "OTHERFIELD") ne "-1")
                        {
                            my ($vlen) = scalar(@valus);

                            # This question *has* an otherfield
                            if ($vlen > 0)
                            {

                                # data was submitted in this field
                                if ($valus[$vlen - 1] =~ m/^!/)
                                {

                                    # First char in last value was "!", thus this is the otherfield
                                    $otherf = $valus[$vlen - 1];
                                }
                            }
                        }

                        my ($ill) = $self->{DOCUMENT}->GetTagParam($entry->GetTagNo(), "ILLEGALVAL");
                        my ($v, %vmap);

                        # map existing values in a has so we know if they are set
                        foreach $v (@valus)
                        {
                            $vmap{$v} = $v;
                        }

                        # Why not do as we did when defining value labels?
                        my ($pv)     = $entry->GetPossibleValues();
                        my (@posval) = @{$pv};

                        my ($misfirst) = 1;

                        foreach $v (@posval)
                        {
                            if ($v->{VALUE} ne $ill)
                            {
                                if ($misfirst)
                                {
                                    $misfirst = 0;
                                }
                                else
                                {
                                    print ";";
                                }
                                if (defined($vmap{ $v->{VALUE} }))
                                {
                                    print $v->{VALUE};
                                }
                                else
                                {
                                    print "-1";
                                }
                            }
                        }

                        if ($self->{DOCUMENT}->GetTagParam($entry->GetTagNo(), "OTHERFIELD") ne "-1")
                        {
                            print ";";
                            if ($otherf)
                            {
                                $otherf =~ s/^\!//;
                                if ($arg->ArgByName("around"))
                                {
                                    print "\"$otherf\"";
                                }
                                else
                                {
                                    print $otherf;
                                }
                            }
                            else
                            {
                                $otherf = $ill;
                                if ($arg->ArgByName("around"))
                                {
                                    print "\"$otherf\"";
                                }
                                else
                                {
                                    print $otherf;
                                }
                            }
                        }

                    }
                    else
                    {
                        if ((!($entry->{ISNUMERIC})) && $arg->ArgByName("around"))
                        {
                            print "\"";
                        }

                        my ($temp) = $self->SanityParse($entry->GetValue());

                        if ($arg->ArgByName("around"))
                        {
                            $temp =~ s/\"/\'/g;
                        }
                        print $temp;
                        if ((!($entry->{ISNUMERIC})) && $arg->ArgByName("around"))
                        {
                            print "\"";
                        }

                    }
                }
            }
            print "\n";
        }
    }
}

sub PrintDataContents
{
    my ($self) = shift;
    my ($arg)  = $self->{ARGUMENT};

    if ($arg->ArgByName("sepdata"))
    {
        $self->PrintDataFile();
    }
    else
    {
        $self->PrintScript();
    }

    1;
}

sub enc
{
  my($self) = shift;
  my($out) = shift;

  my ($arg)  = $self->{ARGUMENT};
  if ($arg->ArgByName("forceenc"))
  {
    $out = Survey::Slask->explicitEncoding("windows-1252",$out);
  }

  return $out;
}

sub GetID
{
    my ($self) = shift;
    return "spss";
}

sub PrintWelcomeBodyContents
{
    my ($self) = shift;

    my (%scriptOpts) = (inline   => "checked",
                        vallab   => "checked",
                        varlab   => "checked",
                        forceenc => "checked",
                        missing  => "checked",
                        nostring => "",
                        split    => "checked",
                        around   => "",
                        nomemo   => "",);

    my (%downOpts) = (nostring => "", split => "checked", araound => "", nomemo => "",);

    my ($checkedStr) = " checked=\"checked\"";

    print "<span class=\"subhead\">" . lprint("Info") . "</span><br /><br />\n";
    print lprint("The SPSS export exports an <i>SPSS Syntax Script</i>, or in other words a") . " ";
    print lprint("a kind of program which builds a data file. This script is run through") . " ";
    print lprint("SPSS's special script editor. In windows, the easiest way to run the script") . " ";
    print lprint("is to download and save it somewhere, and then double-click on it.") . "";
    print "<br /><br />\n";
    print lprint("SPSS scripts can include the actual data, or keep it in an external file.") . " ";
    print lprint("The reason for the latter is that old versions of SPSS are not able to parse command lines") . " ";
    print lprint("longer than 255 characters. Thus, for a small survey, you will probably") . " ";
    print lprint("want to keep the checkbox for \"inline data\" checked. For a large survey") . " ";
    print lprint("uncheck this box and download both the script file and the data file, and") . " ";
    print lprint("save them in the same directory on your harddrive.");
    print "<br /><br />\n";

    print "<span class=\"subhead\">" . lprint("Download script file") . "</span><br /><br />\n";
    print $self->GetBaseForm("sps");
    print "<input type=\"hidden\" name=\"sepdata\" value=\"0\" />";

    print "<input type=\"checkbox\" name=\"inline\" value=\"1\"";
    if ($scriptOpts{inline} ne "") { print $checkedStr; }
    print "/>";
    print lprint("Include inline data") . "<br />\n";

    print "<input type=\"checkbox\" name=\"vallab\" value=\"1\"";
    if ($scriptOpts{vallab} ne "") { print $checkedStr; }
    print "/>";
    print lprint("Include value labels") . "<br />\n";

    print "<input type=\"checkbox\" name=\"varlab\" value=\"1\"";
    if ($scriptOpts{varlab} ne "") { print $checkedStr; }
    print "/>";
    print lprint("Include variable labels") . "<br />\n";

    print "<input type=\"checkbox\" name=\"missing\" value=\"1\"";
    if ($scriptOpts{missing} ne "") { print $checkedStr; }
    print "/>";
    print lprint("Include \"missing values\"") . "<br />\n";

    print "<input type=\"checkbox\" name=\"nostring\" value=\"1\"";
    if ($scriptOpts{nostring} ne "") { print $checkedStr; }
    print "/>";
    print lprint("Only include numerical variables (will also exclude multiple-answer CHOICEs)") . "<br />\n";

    if ($ENV{"_SURVEY_CHOICE_SPLIT"} ne "yes")
    {
        print "<input type=\"checkbox\" name=\"split\" value=\"1\"";
        if ($scriptOpts{split} ne "") { print $checkedStr; }
        print "/>";
        print lprint("Split multiple-answer CHOICEs into one variable per possible answer.") . "<br/>\n";
    }

    print "<input type=\"checkbox\" name=\"around\" value=\"1\"";
    if ($scriptOpts{around} ne "") { print $checkedStr; }
    print "/>";
    print lprint("Cite string values") . " (\" " . lprint("in values will be substituted with") . " \\\').<br/>\n";

    print "<input type=\"checkbox\" name=\"nomemo\" value=\"1\"";
    if ($scriptOpts{nomemo} ne "") { print $checkedStr; }
    print "/>";
    print lprint(
          "Exclude all MEMO fields. If not filtered out, MEMO fields will be included but truncated at 250 characters.")
      . "<br />\n";

    print "<input type=\"checkbox\" name=\"forceenc\" value=\"1\"";
    if ($scriptOpts{forceenc} ne "") { print $checkedStr; }
    print "/>";
    print lprint("Force output encoding to Windows-1252 (if unchecked, output will be UTF-8)") . "<br />\n";


    print "<br />\n";
    print "<input type=\"submit\" value=\"Get script\" />\n";
    print "</form>\n";

    print "<br />\n";

    print "<span class=\"subhead\">" . lprint("Download separate data file") . "</span><br /><br />\n";
    print $self->GetBaseForm("dat");

    print "<input type=\"checkbox\" name=\"nostring\" value=\"1\"";
    if ($downOpts{nostring} ne "") { print $checkedStr; }
    print "/>";
    print lprint("Only include numerical variables (will also exclude multiple-answer CHOICEs)") . "<br />\n";

    if ($ENV{"_SURVEY_CHOICE_SPLIT"} ne "yes")
    {
        print "<input type=\"checkbox\" name=\"split\" value=\"1\"";
        if ($downOpts{split} ne "") { print $checkedStr; }
        print "/>";
        print lprint("Split multiple-answer CHOICEs into one variable per possible answer.") . "<br/>\n";
    }

    print "<input type=\"checkbox\" name=\"around\" value=\"1\"";
    if ($downOpts{around} ne "") { print $checkedStr; }
    print "/>";
    print lprint("Cite string values") . " (\" " . lprint("in values will be substituted with") . " \\\').<br/>\n";

    print "<input type=\"checkbox\" name=\"nomemo\" value=\"1\"";
    if ($downOpts{nomemo}) { print $checkedStr; }
    print "/>";
    print lprint(
          "Exclude all MEMO fields. If not filtered out, MEMO fields will be included but truncated at 250 characters.")
      . "<br />\n";

    print "<input type=\"hidden\" name=\"sepdata\" value=\"1\" />";
    print "<input type=\"submit\" value=\"Get data file\" />\n";
    print "</form>\n";

    1;
}

sub PrintDataContentType
{
    my ($self) = shift;
    my ($arg)  = $self->{ARGUMENT};

    if ($arg->ArgByName("sepdata"))
    {
        $self->GoodContentType('application/spss-data');
    }
    else
    {
        $self->GoodContentType('application/spss-syntax');
    }
}

sub OneLineDesc
{
    return lprint(
"Native syntax export for the SPSS statistical toolkit. Data and meta-data are exported as syntax scripts possible to run directly via SPSS."
    );
}

sub SanityParse
{
    my ($self, $string) = @_;

    # hard-coded delimiter is ; in spss output, replace it if in data
    $string =~ s/\;/\:/g;

    return $string;
}

1;

