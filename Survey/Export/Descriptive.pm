#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Export::Descriptive;
use strict;

@Survey::Export::Descriptive::ISA = qw(Survey::Export::Export);

use Survey::Slask;
use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub new
{
    my ($crap, $doc, $arg, $data) = @_;

    my ($self) = {};
    bless $self;

    $self->DoCommonStuff($doc, $arg, $data);

    return $self;
}

sub GetTitle
{
    my ($self) = shift;
    return lprint("Descriptive Statistics");
}

sub GetCredits
{
    my ($self) = shift;
    return lprint("This is an optional add-on module in Mod_Survey");
}

sub printHead
{
    my ($self) = shift;
    Survey::Slask->HtmlHead();
    print "    <title>" . $self->{DOCUMENT}->GetOption("TITLE") . "</title>\n";
    print "    <style><!--\n";
    print "      body     { background-color: white; color: black; }\n";
    print "      .tblcap  { font-style: sans-serif; font-size: 16pt; font-weight: bold; }\n";
    print "      .varcap  { font-style: serif; font-size: 14pt; font-weight: bold; }\n";
    print "    --></style>\n";
    print "  </head>\n";
    print "  <body>\n";

    1;
}

sub printFoot
{
    print "\n  </body>\n";
    print "</html>\n";
}

sub PrintDataContents
{
    my ($self) = shift;
    my ($arg)  = $self->{ARGUMENT};

    my ($stat) = Survey::Export::StatUtils->new($self->{DOCUMENT}, $self->{DATA});

    my ($variable) = $arg->ArgByName("variable");

    my ($total)    = 0;
    my ($legal)    = 0;
    my ($illegal)  = 0;
    my ($min)      = 0;
    my ($q1)       = 0;
    my ($median)   = 0;
    my ($mean)     = 0;
    my ($q3)       = 0;
    my ($max)      = 0;
    my ($sum)      = 0;
    my ($mode)     = 0;
    my ($range)    = 0;
    my ($iqrange)  = 0;
    my ($variance) = 0;
    my ($stddev)   = 0;

    my (@titlerow) = ();
    my (@datarow)  = ();

    if ($arg->ArgByName("total"))
    {
        $total = $stat->Count($variable);
        push(@titlerow, "n");
        push(@datarow,  $total);
    }

    if ($arg->ArgByName("legal"))
    {
        $legal = $stat->LegalCount($variable);
        push(@titlerow, "legal");
        push(@datarow,  $legal);
    }

    if ($arg->ArgByName("illegal"))
    {
        $illegal = $stat->IllegalCount($variable);
        push(@titlerow, "illegal");
        push(@datarow,  $illegal);
    }

    if ($arg->ArgByName("min"))
    {
        $min = $stat->Min($variable);
        push(@titlerow, "min");
        push(@datarow,  $min);
    }

    if ($arg->ArgByName("q1"))
    {
        $q1 = $stat->Q1($variable);
        push(@titlerow, "Q1");
        push(@datarow,  $q1);
    }

    if ($arg->ArgByName("median"))
    {
        $median = $stat->Median($variable);
        push(@titlerow, "med");
        push(@datarow,  $median);
    }

    if ($arg->ArgByName("mean"))
    {
        $mean = $stat->Mean($variable);
        push(@titlerow, "mean");
        push(@datarow,  $mean);
    }

    if ($arg->ArgByName("q3"))
    {
        $q3 = $stat->Q3($variable);
        push(@titlerow, "Q3");
        push(@datarow,  $q3);
    }

    if ($arg->ArgByName("max"))
    {
        $max = $stat->Max($variable);
        push(@titlerow, "max");
        push(@datarow,  $max);
    }

    if ($arg->ArgByName("sum"))
    {
        $sum = $stat->Sum($variable);
        push(@titlerow, "sum");
        push(@datarow,  $sum);
    }

    if ($arg->ArgByName("mode"))
    {
        $mode = $stat->Mode($variable);
        push(@titlerow, "mode");
        push(@datarow,  $mode);
    }

    if ($arg->ArgByName("range"))
    {
        $range = $stat->Range($variable);
        push(@titlerow, "range");
        push(@datarow,  $range);
    }

    if ($arg->ArgByName("iqrange"))
    {
        $iqrange = $stat->InterQuartileRange($variable);
        push(@titlerow, "IQR");
        push(@datarow,  $iqrange);
    }

    if ($arg->ArgByName("variance"))
    {
        $variance = $stat->SampleVariance($variable);
        push(@titlerow, "var");
        push(@datarow,  $variance);
    }

    if ($arg->ArgByName("stddev"))
    {
        $stddev = $stat->StandardDeviation($variable);
        push(@titlerow, "stddev");
        push(@datarow,  $stddev);
    }

    my ($i) = 0;

    my ($captbg)    = "bgcolor=\"#BBBBFF\"";
    my ($captclass) = "class=\"tblcap\"";
    my ($varbg)     = "bgcolor=\"#DDDDDD\"";
    my ($varclass)  = "class=\"varcap\"";
    my ($padspace)  = "cellspacing=\"0\" cellpadding=\"3\"";
    my ($align)     = "valign=\"top\"";
    my ($optwidth)  = "width=\"100\"";
    my ($border)    = "border=\"1\"";

    my ($ind1) = "    ";
    my ($ind2) = $ind1 . "  ";
    my ($ind3) = $ind2 . "  ";
    my ($ind4) = $ind2 . "  ";

    my ($numcol) = scalar(@titlerow);

    $self->printHead();

    print $ind1 . "<table $border cols=\"$numcol\" $padspace>\n";
    print $ind2 . "<tr>\n";
    print $ind3 . "<td $captbg colspan=\"$numcol\" valign=\"top\" align=\"center\" height=\"30\">\n";
    print $ind4 . "<span $captclass>" . $self->SanityParse($variable) . "</span>\n";
    print $ind3 . "</td>\n";
    print $ind2 . "</tr>\n";

    print $ind2 . "<tr>\n";
    for ($i = 0 ; $i < scalar(@titlerow) ; $i++)
    {
        print $ind3 . "<td $varbg><span $varclass>" . $titlerow[$i] . "</span></td>\n";
    }
    print $ind2 . "</tr>\n";

    print $ind2 . "<tr>\n";
    for ($i = 0 ; $i < scalar(@datarow) ; $i++)
    {
        print $ind3 . "<td>" . $datarow[$i] . "</td>\n";
    }
    print $ind2 . "</tr>\n";

    print $ind1 . "</table>\n";

    $self->printFoot();

    1;
}

sub GetID
{
    my ($self) = shift;
    return "desc";
}

sub PrintWelcomeBodyContents
{
    my ($self) = shift;

    print "<span class=\"subhead\">" . lprint("Info") . "</span><br /><br />\n";
    print lprint("The Descriptive module calculates some basic descriptive statistics about a") . " ";
    print lprint("numerical variable. Note that for small samples, some of these descriptives") . " ";
    print lprint("are pointless, but that they will be calculated anyway if you choose to") . " ";
    print lprint("include them. The result is presented as a HTML table.") . "\n";
    print "<br /><br />\n";
    print "<span class=\"subhead\">" . lprint("Display") . "</span><br /><br />\n";
    print $self->GetBaseForm("html");

    print lprint("Variable for which to display descriptive statistics:") . " ";
    print "<select size=\"1\" name=\"variable\">\n";

    my ($case) = $self->{DATA}->{META};

    my ($i);
    for ($i = scalar(@{$case}) - 1 ; $i >= 0 ; $i--)
    {
        my ($entry) = @{$case}[$i];
        if (!$entry->{ISNUMERIC})
        {
            splice(@{$case}, $i, 1);
        }
    }

    my ($entry);

    foreach $entry (@{$case})
    {
        my ($name) = $entry->{NAME};
        print "  <option value=\"$name\">$name</option>\n";
    }

    #  print "  <option value=\"age\">age</option>\n";
    #  print "  <option value=\"likedat\">likedat</option>\n";
    print "</select><br /><br />\n";

    print lprint("Include which fields in the output:") . "<br />\n";
    print "<input type=\"checkbox\" name=\"total\" value=\"1\" checked=\"checked\" /> ";
    print lprint("Total number of cases") . "<br />\n";
    print "<input type=\"checkbox\" name=\"legal\" value=\"1\" checked=\"checked\" /> ";
    print lprint("Number of valid cases") . "<br />\n";
    print "<input type=\"checkbox\" name=\"illegal\" value=\"1\" checked=\"checked\" /> ";
    print lprint("Number of invalid cases") . "<br />\n";
    print "<input type=\"checkbox\" name=\"min\" value=\"1\" /> Min<br />\n";
    print "<input type=\"checkbox\" name=\"q1\" value=\"1\" /> Q1<br />\n";
    print "<input type=\"checkbox\" name=\"median\" value=\"1\" /> Median<br />\n";
    print "<input type=\"checkbox\" name=\"mean\" value=\"1\" checked=\"checked\" /> Mean<br />\n";
    print "<input type=\"checkbox\" name=\"q3\" value=\"1\" /> Q3<br />\n";
    print "<input type=\"checkbox\" name=\"max\" value=\"1\" /> Max<br />\n";
    print "<input type=\"checkbox\" name=\"sum\" value=\"1\" /> Sum<br />\n";
    print "<input type=\"checkbox\" name=\"mode\" value=\"1\" /> Mode<br />\n";
    print "<input type=\"checkbox\" name=\"range\" value=\"1\" /> Range<br />\n";
    print "<input type=\"checkbox\" name=\"iqrange\" value=\"1\" /> Inter-Quartile Range<br />\n";
    print "<input type=\"checkbox\" name=\"variance\" value=\"1\" /> Sample Variance<br />\n";
    print "<input type=\"checkbox\" name=\"stddev\" value=\"1\" checked=\"checked\" /> Standard Deviation<br />\n";

    print "<br />\n";
    print "<input type=\"submit\" value=\"Get data\" />\n";
    print "</form>\n";

    1;
}

sub PrintDataContentType
{
    my ($self) = shift;
    my($enc) = $ENV{"_SURVEY_ENCODING"} || "UTF-8";
    $self->GoodContentType("text/html; charset=$enc");
}

sub OneLineDesc
{
    return lprint("Generated basic descriptive statistics, such as means and deviations.");
}

1;

