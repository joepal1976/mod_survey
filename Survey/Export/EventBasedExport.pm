#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Export::EventBasedExport;
use strict;

@Survey::Export::EventBasedExport::ISA = qw(Survey::Export::Export);

use Survey::Language;
use Text::ParseWords;

use CGI;

sub startDataEvents
{
    my ($self) = @_;

    my ($data) = $self->{DATA};

    my ($caseno) = 1;
    my ($case);
    my (@cases) = @{ $data->{DATA} };

    foreach $case (@cases)
    {
        if ($self->wantsCase($caseno, scalar(@cases)))
        {
            my ($casearr) = $data->MakeCase($caseno);
            $self->printCase($casearr, $caseno, scalar(@cases));
        }
        $caseno++;
    }
}

sub printHead
{
    my ($self) = shift;

    # If a header should be printed, override this method

    1;
}

sub printFoot
{
    my ($self) = shift;

    # If a footer should be printed, override this method

    1;
}

sub printCase
{
    my ($self, $casearr, $caseno, $numcase) = @_;

    # This method must be overridden
    die "Must override this method";
}

sub wantsCase
{
    my ($self, $caseno, $numcase) = @_;

    # accept all cases by default, override if not wanted
    return 1;
}

sub init
{
    my ($self, $numcase) = @_;

    # do nothing, override if something should be done
    1;
}

sub PrintDataContents
{
    my ($self) = shift;

    my ($data) = $self->{DATA};
    $data->ReadRawData();
    my (@cases) = @{ $data->{DATA} };

    $self->init(scalar(@cases));
    $self->printHead();
    $self->startDataEvents();
    $self->printFoot();

    1;
}

sub IsEventBased
{
    return 1;
}

1;

