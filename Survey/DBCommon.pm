#    This source code file is part of the "mod_survey" package.
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::DBCommon;
use strict;

use Survey::Language;

use CGI qw/:standard/;
use CGI::Cookie;
use DBI;

sub new
{
    my ($crap, $dsn, $usr, $pwd) = @_;
    my $self = {};

    bless($self);

    $self->{ERROR}     = 0;
    $self->{ERRORCODE} = 0;

    $self->{DBHANDLER} = 0;
    $self->{STATEMENT} = "";

    if ($dsn eq "")
    {
        $self->{ERROR}     = 1;
        $self->{ERRORCODE} = 401;

        return ($self);
    }

    if ($usr eq "")
    {
        $self->{ERROR}     = 1;
        $self->{ERRORCODE} = 402;

        return ($self);
    }

    $self->{dsn} = $dsn;
    $self->{usr} = $usr;
    $self->{pwd} = $pwd;

    return ($self);
}

sub openDB
{
    my ($self) = @_;

    if (!( $self->{DBHANDLER} = DBI->connect(
                                            $self->{dsn}, $self->{usr}, $self->{pwd},
                                            { PrintError => 0, AutoCommit => 1, RaiseError => 1 })))
    {
        $self->{ERROR}     = 1;
        $self->{ERRORCODE} = 403;
        $self->{ERRORMSG}  = $DBI::errstr;
    }
}

sub closeDB
{
    my ($self) = @_;

    if ($self->{EXECUTEME})
    {
        $self->{EXECUTEME}->finish();
    }

    if (!($self->{DBHANDLER}->disconnect()))
    {
        $self->{ERROR}     = 1;
        $self->{ERRORCODE} = 404;
        $self->{ERRORMSG}  = $DBI::errstr;
    }
}

sub setStatement
{
    my ($self, $stm) = @_;

    $self->{STATEMENT} = $stm;
}

sub getStatement
{
    my ($self) = @_;

    return ($self->{STATEMENT});
}

sub prepareStm
{
    my ($self) = @_;

    if ($self->{STATEMENT} eq "")
    {
        $self->{ERROR}     = 1;
        $self->{ERRORCODE} = 405;
        return 0;
    }

    my ($ref_st) = ($self->{DBHANDLER}->prepare($self->{STATEMENT}));

    if (!$ref_st)
    {
        $self->{ERROR}     = 1;
        $self->{ERRORCODE} = 410;
        $self->{ERRORMSG}  = $DBI::errstr;
    }

    $self->{EXECUTEME} = $ref_st;

    return 1;
}

sub executeStm
{
    my ($self, $ukey) = @_;

    $self->{EXECUTEME}->execute();

    if ($ukey eq "")
    {
        $ukey = 1;
    }

    #return ($self->{EXECUTEME}->fetchall_hashref($ukey));
    return $self->{EXECUTEME}->fetchall_arrayref;
}

sub execStm
{
    my ($self, $stm, $ukey) = @_;

    $self->setStatement($stm);
    if ($self->prepareStm())
    {
        return $self->executeStm($ukey);
    }
    return "";
}

sub execSimple
{
    my ($self, $sql) = @_;

    my ($err) = undef;

    if (!$self->{DBHANDLER}->do($sql))
    {
        $self->{ERROR}     = 1;
        $self->{ERRORCODE} = 410;
        $self->{ERRORMSG}  = $DBI::errstr;
    }
}

sub doIT
{
    my ($self, $dsn, $usr, $pwd, $stm, $ukey) = @_;

    my ($db) = new DBCommon($dsn, $usr, $pwd);
    $db->openDB();
    return ($db->execStm($stm, $ukey));
    $db->closeDB();
}

sub Error()
{
    my ($self) = shift;
    return $self->{ERROR};
}

1;
