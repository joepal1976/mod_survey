#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Debug;

use Survey::Language;
use strict;

sub new
{
    my $crap = shift;
    my $self = {};

    bless($self);

    return ($self);
}

sub SetDebugParam
{
    my ($self, $param, $value) = @_;
    $self->{ "DEBUG_" . $param } = $value;
    1;
}

sub GetDebugParam
{
    my ($self, $param) = @_;
    return $self->{ "DEBUG_" . $param };
}

sub AddDebugMsg
{
    my ($self, $module, $msg) = @_;
    $msg = $module . ": " . $msg;
    if ($self->{"MSGS"}) { $self->{"MSGS"} .= "\n" . $msg; }
    else                 { $self->{"MSGS"} = $msg; }
    1;
}

sub PrintDebugMsgs
{
    my ($self) = shift;
    my ($msg, $mod, $cell);
    my (@msgs) = split("\n", $self->{"MSGS"});

    foreach $cell (@msgs)
    {
        ($mod, $msg) = split(": ", $cell, 2);
        print "<b>" . $mod . ": </b>" . $msg . "<br />\n";
    }

    1;
}

1;

