#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::DataEntry;
use strict;

use Survey::Language;

sub new
{
    my ($crap, $data, $type, $name, $isnumeric, $value, $varcap, $valcap, $tagno, $elno, $posval, $fieldlen) = @_;
    my ($self) = {};

    $self->{DATA}            = $data;
    $self->{TYPE}            = $type;
    $self->{NAME}            = $name;
    $self->{ISNUMERIC}       = $isnumeric;
    $self->{VALUE}           = $value;
    $self->{VARIABLECAPTION} = $varcap;
    $self->{VALUECAPTION}    = $valcap;
    $self->{TAGNO}           = $tagno;
    $self->{ELEMENT}         = $elno;
    $self->{POSSIBLEVALUES}  = $posval;
    $self->{FIELDLENGTH}     = $fieldlen;

    bless($self);

    return ($self);
}

sub GetType
{
    my ($self) = shift;
    return $self->{TYPE};
}

sub GetName
{
    my ($self) = shift;
    return $self->{NAME};
}

sub GetValue
{
    my ($self) = shift;
    return $self->{VALUE};
}

sub IsNumerical
{
    my ($self) = shift;
    return $self->{ISNUMERIC};
}

sub GetTagNo
{
    my ($self) = shift;
    return $self->{TAGNO};
}

sub GetElementNo
{
    my ($self) = shift;
    return $self->{ELEMENT};
}

sub GetVariableCaption
{
    my ($self) = shift;
    return $self->{VARIABLECAPTION};
}

sub GetValueCaption
{
    my ($self) = shift;
    return $self->{VALUECAPTION};
}

sub GetPossibleValues
{
    my ($self) = shift;
    return $self->{POSSIBLEVALUES};
}

sub GetFieldLength
{
    my ($self) = shift;
    return $self->{FIELDLENGTH};
}

1;

