#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::Memo;
use strict;

@Survey::Component::Memo::ISA = qw(Survey::Component::Component);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub FillParams
{
    my ($crap, $self) = @_;

    @{ $self->{"ALLOWED_MEMO"} } = ("NAME", "COLS", "ROWS", "CAPTION", "CAPTSTYLE", "STYLE");
    @{ $self->{"ALL_MEMO"} } = (@{ $self->{"ALLOWED_MEMO"} }, "TYPE", "FILE");

    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;

    $self->{"MEMO_TYPE"}      = "MEMO";
    $self->{"MEMO_NAME"}      = "";
    $self->{"MEMO_COLS"}      = "50";
    $self->{"MEMO_ROWS"}      = "15";
    $self->{"MEMO_CAPTION"}   = "";
    $self->{"MEMO_CAPTSTYLE"} = "";       #"MEMOcap";
    $self->{"MEMO_STYLE"}     = "";       #"MEMOelm";

    1;
}

sub PlaceComponent
{
    my ($crap, $self, $paramstr) = @_;

    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    my (%params, $cell, $name, $value, @pararr);

    %params = $self->GetDefaults("MEMO");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "MEMO");
    }

    #bugant: fix for session loading values
    $params{FILE} = $self->{FILE};

    if ((!$self->{ERROR}) && (!$params{NAME}))
    {
        $self->{ERROR}     = lprint("A MEMO tag must have a name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 12;
    }

    if ((!$self->{ERROR}) && (length($params{NAME}) > 8))
    {
        $self->{ERROR}     = lprint("A MEMO tag name can only be 8 characters wide (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 21;
    }

    if (!$self->{ERROR}) { $self->CheckName($params{NAME}, "MEMO"); }

    $name = "TAGNAME_" . $params{"NAME"};
    $name = "\U$name";

    if (defined($self->{$name}))
    {
        $self->{ERROR}     = lprint("A MEMO tag must have unique name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 20;
    }
    else
    {
        $self->{$name} = $self->{NUMTAGS};
    }

    $self->PlaceParams("MEMO", $tn, %params);
    $self->{NUMTAGS}++;

    $self->SetOption("HASMEMO", 1);

    1;
}

sub PrintComponent
{
    my ($crap, $self, $tagno) = @_;
    my ($doc) = $self->{DOCUMENT};

    $doc->SetVisited($doc->GetTagParam($tagno, "NAME"));

    my $tmpl = new Survey::Template($ENV{_SURVEY_HOME} . "/templates/default/Memo.tmpl");

    $tmpl->setVar("captStyle", $doc->GetTagParam($tagno, "CAPTSTYLE"));
    $tmpl->setVar("caption",   $doc->GetTagParam($tagno, "CAPTION"));

    $tmpl->setVar("style", $doc->GetTagParam($tagno, "STYLE"));
    $tmpl->setVar("name",  $doc->GetTagParam($tagno, "NAME"));
    $tmpl->setVar("rows",  $doc->GetTagParam($tagno, "ROWS"));
    $tmpl->setVar("cols",  $doc->GetTagParam($tagno, "COLS"));

    #bugant persist ;)
    my ($thistagname) = $doc->GetTagParam($tagno, "NAME");
    my ($chkpersist) = ($doc->{SESSION}->getValue("SUBMITTED_$thistagname"));

    if (defined($chkpersist)) { $tmpl->setVar("value", $chkpersist); }

    #bugant persisted ;)

    return $tmpl->get();
}

sub NumberOfValues
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 1;
}

sub GetValueNumerical
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 0;
}

sub GetCheckValue
{
    my ($self, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;

    my ($value) = {};
    my ($name) = $doc->GetTagParam($tagno, "NAME");

    my ($visited) = $doc->CheckVisited($name);

    if ($visited)
    {
        $value->{VALUE} = $arg->ArgByName($name) || "";
    }
    else
    {
        $value->{VALUE} = $doc->GetOption("NOTDISPLAYEDVAL");
    }

    $value->{VALUE} =~ s/\x0d\x0a/\�\�/g;
    $value->{VALUE} =~ s/\x0a/\�\�/g;
    $value->{VALUE} =~ s/\x0d/\�\�/g;

    $value->{NUMERICAL} = 0;
    $value->{NAME}      = $name;
    $value->{ISBLOB}    = 1;
    $value->{CAPTION}   = $doc->GetTagParam($tagno, "CAPTION");

    return $value;
}

1;

