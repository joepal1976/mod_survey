#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::Choice;
use strict;

@Survey::Component::Choice::ISA = qw(Survey::Component::Component);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;
use Survey::Component::Component;
use Survey::Template;

use CGI;

sub FillParams
{
    my ($crap, $self) = @_;

    @{ $self->{"ALLOWED_CHOICE"} } = ("NAME",
                                      "MUSTANSWER",
                                      "CAPTION",
                                      "CAPTSTYLE",
                                      "LABELCAPTSTYLE",
                                      "STYLE",
                                      "OTHERFIELD",
                                      "ILLEGALVAL",
                                      "RANDOM",
                                      "MULTI");
    @{ $self->{"ALL_CHOICE"} } = (@{ $self->{"ALLOWED_CHOICE"} }, "RAW", "ELEMENTS", "TYPE", "MAXLEN", "FILE");

    @{ $self->{"ALLOWED_CHOICEELEMENT"} } = ("VALUE", "CHECKED", "CAPTION", "ACCESS", "STAYHERE");
    @{ $self->{"ALL_CHOICEELEMENT"} } = (@{ $self->{"ALLOWED_CHOICEELEMENT"} });

    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;

    $self->{"CHOICE_TYPE"}           = "CHOICE";
    $self->{"CHOICE_NAME"}           = "";
    $self->{"CHOICE_MUSTANSWER"}     = "no";
    $self->{"CHOICE_MAXLEN"}         = "80";
    $self->{"CHOICE_CAPTION"}        = "";
    $self->{"CHOICE_CAPTSTYLE"}      = "";
    $self->{"CHOICE_LABELCAPTSTYLE"} = "";
    $self->{"CHOICE_STYLE"}          = "";
    $self->{"CHOICE_OTHERFIELD"}     = "-1";
    $self->{"CHOICE_ILLEGALVAL"}     = "-1";
    $self->{"CHOICE_RANDOM"}         = "no";
    $self->{"CHOICE_MULTI"}          = "no";

    1;
}

sub PlaceComponent
{
    my ($crap, $self, $paramstr) = @_;

    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    my (%params, %epara, $cell, $name, $value, @pararr, $work, $tag, $s, $e, $elems);

    %params = $self->GetDefaults("CHOICE");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "CHOICE");
    }

    if ($params{empty} eq "yes")
    {
        $self->{ERROR}     = lprint("A CHOICE tag cannot be immediately terminated (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 10;
    }

    if ((!$self->{ERROR}) && (!$params{NAME}))
    {
        $self->{ERROR}     = lprint("A CHOICE tag must have a name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 12;
    }

    if (!$self->{ERROR}) { $self->CheckName($params{NAME}, "CHOICE"); }

    if ((!$self->{ERROR}) && (length($params{NAME}) > 8))
    {
        $self->{ERROR}     = lprint("A CHOICE tag name can only be 8 characters wide (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 21;
    }

    if (!$self->{ERROR})
    {
        my ($endtag) = index($self->{WORK}, "</CHOICE>");
        if ($endtag eq 0)
        {
            $self->{ERROR} = lprint("A CHOICE tag must contain at least one CHOICEELEMENT (in ") . $self->{FILE} . ")";
            $self->{ERRORCODE} = 13;
        }
        else
        {
            if ($endtag > 0)
            {
                $params{"RAW"} = substr($self->{WORK}, 0, $endtag - 1);
                $self->{WORK} = substr($self->{WORK}, 0 - length($self->{WORK}) + length($params{"RAW"}) + 10);
            }
            else
            {
                $self->{ERROR}     = lprint("A CHOICE tag must have an end tag (in ") . $self->{FILE} . ")";
                $self->{ERRORCODE} = 14;
            }
        }
    }

    if (!$self->{ERROR})
    {
        $elems = 0;
        $work  = $params{"RAW"};
        $s     = index($work, "<");
        $e     = index($work, ">");

        while (($s ne -1) && ($e ne -1) && (!$self->{ERROR}))
        {
            if ($s) { $e = $e - $s }
            $tag = substr($work, $s, $e + 1);
            if ($s)
            {
                $work = substr($work, 0 - length($work) + $s);
            }
            $work = substr($work, 0 - length($work) + length($tag));
            ($cell, $tag) = $self->CleanUpTag($tag);

            if (($cell ne "CHOICEELEMENT") && ($cell ne "DBCHOICEELEMENT"))
            {
                $self->{ERROR} = lprint("A CHOICE block can only contain CHOICEELEMENTs (in ") . $self->{FILE} . ")";
                $self->{ERRORCODE} = 15;
            }

            if ($cell eq "CHOICEELEMENT")
            {
                $epara{"VALUE"}    = "!";
                $epara{"CHECKED"}  = "no";
                $epara{"CAPTION"}  = "";
                $epara{"ACCESS"}   = "";
                $epara{"STAYHERE"} = "";

                @pararr = @{$tag};
                foreach $cell (@pararr)
                {
                    ($name, $value) = split(/=/, $cell);
                    $epara{$name} = $value;
                }

                if ($epara{"empty"} eq "no")
                {
                    $self->{ERROR} =
                      lprint("A CHOICEELEMENT tag must be terminated immediately (in ") . $self->{FILE} . ")";
                    $self->{ERRORCODE} = 11;
                }
                $_ = $epara{"VALUE"};
                if (!/[0-9]+/)
                {
                    $self->{ERROR} =
                      lprint("A CHOICEELEMENT must be given a numerial value (in ") . $self->{FILE} . ")";
                    $self->{ERRORCODE} = 16;
                }

                if (!$self->{ERROR})
                {
                    $s = $tn . "CE" . $elems . "_";

                    $self->{ $s . "VALUE" }    = $epara{"VALUE"};
                    $self->{ $s . "CAPTION" }  = $epara{"CAPTION"};
                    $self->{ $s . "CHECKED" }  = $epara{"CHECKED"};
                    $self->{ $s . "ACCESS" }   = $epara{"ACCESS"};
                    $self->{ $s . "STAYHERE" } = $epara{"STAYHERE"};

                    $elems++;
                }
            }

            if ($cell eq "DBCHOICEELEMENT")
            {
                $epara{"VALUEARRAY"}    = "";
                $epara{"CAPTIONARRAY"}  = "";
                $epara{"ACCESSARRAY"}   = "";
                $epara{"STAYHEREARRAY"} = "";

                @pararr = @{$tag};
                foreach $cell (@pararr)
                {
                    ($name, $value) = split(/=/, $cell);
                    $epara{$name} = $value;
                }

                if ($epara{"empty"} eq "no")
                {
                    $self->{ERROR}     = lprint("A DBCHOICEELEMENT tag must be terminated immediately.");
                    $self->{ERRORCODE} = 11;
                }

                if (!$self->{ERROR})
                {
                    my ($ses) = $self->{SESSION};

                    if (!$epara{"CAPTIONARRAY"})
                    {
                        $self->{ERROR}     = lprint("CAPTIONARRAY is a required parameter in DBCHOICEELEMENT.");
                        $self->{ERRORCODE} = 99;
                    }

                    my ($cpt) = $ses->getArrayValue($epara{"CAPTIONARRAY"});

                    if (!$cpt)
                    {
                        $self->{ERROR}     = lprint("No such array: ") . $epara{"CAPTIONARRAY"};
                        $self->{ERRORCODE} = 50;
                    }

                    my ($vals, @vala);

                    if ($epara{"VALUEARRAY"})
                    {
                        my ($vals) = $ses->getArrayValue($epara{"VALUEARRAY"});

                        if (!$vals)
                        {
                            $self->{ERROR}     = lprint("No such array: ") . $epara{"VALUEARRAY"};
                            $self->{ERRORCODE} = 50;
                        }
                        else
                        {
                            @vala = @{$vals};
                        }
                    }

                    my ($accs, @acca);

                    if ($epara{"ACCESSARRAY"})
                    {
                        my ($accs) = $ses->getArrayValue($epara{"ACCESSARRAY"});

                        if (!$accs)
                        {
                            $self->{ERROR}     = lprint("No such array: ") . $epara{"ACCESSARRAY"};
                            $self->{ERRORCODE} = 51;
                        }
                        else
                        {
                            @acca = @{$accs};
                        }
                    }

                    my ($stas, @staa);

                    if ($epara{"STAYHEREARRAY"})
                    {
                        my ($stas) = $ses->getArrayValue($epara{"STAYHEREARRAY"});

                        if (!$stas)
                        {
                            $self->{ERROR}     = lprint("No such array: ") . $epara{"STAYHEREARRAY"};
                            $self->{ERRORCODE} = 52;
                        }
                        else
                        {
                            @staa = @{$stas};
                        }
                    }

                    # iterate over contents of array
                    if ($cpt && !$self->{ERROR})
                    {
                        my (@cpta) = @{$cpt};
                        if ((scalar(@cpta) < 1) && defined($cpt))
                        {
                            $self->{ERROR}     = lprint("Array has zero length: ") . $epara{"CAPTIONARRAY"};
                            $self->{ERRORCODE} = 99;
                        }

                        if (@vala)
                        {
                            if ((scalar(@vala) < 1) && defined($vals))
                            {
                                $self->{ERROR}     = lprint("Array has zero length: ") . $epara{"VALUEARRAY"};
                                $self->{ERRORCODE} = 99;
                            }
                            else
                            {
                                if (scalar(@vala) != scalar(@cpta))
                                {
                                    $self->{ERROR} =
                                        lprint("Lengths of arrays differ: ")
                                      . $epara{"VALUEARRAY"} . " != "
                                      . $epara{"CAPTIONARRAY"};
                                    $self->{ERRORCODE} = 99;
                                }
                                if (defined($accs) && (scalar(@vala) != scalar(@acca)))
                                {
                                    $self->{ERROR} =
                                        lprint("Lengths of arrays differ: ")
                                      . $epara{"VALUEARRAY"} . " == "
                                      . $epara{"CAPTIONARRAY"} . " != "
                                      . $epara{"ACCESSARRAY"};
                                    $self->{ERRORCODE} = 99;
                                }
                                if (defined($stas) && (scalar(@vala) != scalar(@staa)))
                                {
                                    $self->{ERROR} =
                                        lprint("Lengths of arrays differ: ")
                                      . $epara{"VALUEARRAY"} . " == "
                                      . $epara{"CAPTIONARRAY"} . " != "
                                      . $epara{"STAYHEREARRAY"};
                                    $self->{ERRORCODE} = 99;
                                }
                            }
                        }

                        for (my ($i) = 0 ; $i < scalar(@cpta) ; $i++)
                        {
                            $epara{"CAPTION"} = $cpta[$i];

                            if (!@vala)
                            {

                                # If no explicit array, number elements from 1..N
                                $epara{"VALUE"} = $i + 1;
                            }
                            else
                            {
                                $epara{"VALUE"} = $vala[$i];
                            }

                            if (!@acca)
                            {

                                # If no explicit array, no access functionaliti
                                $epara{"ACCESS"} = "";
                            }
                            else
                            {
                                $epara{"ACCESS"} = $acca[$i];
                            }

                            if (!@staa)
                            {

                                # If no explicit array, no stayhere functionaliti
                                $epara{"STAYHERE"} = "";
                            }
                            else
                            {
                                $epara{"STAYHERE"} = $staa[$i];
                            }

                            $s = $tn . "CE" . $elems . "_";

                            $self->{ $s . "CAPTION" } = $epara{"CAPTION"};
                            $self->{ $s . "CHECKED" } = "no";
                            $self->{ $s . "VALUE" }   = $epara{"VALUE"};
                            if ($epara{"ACCESS"} ne "")
                            {
                                $self->{ $s . "ACCESS" } = $epara{"ACCESS"};
                            }
                            if ($epara{"STAYHERE"} ne "")
                            {
                                $self->{ $s . "STAYHERE" } = $epara{"STAYHERE"};
                            }

                            $elems++;
                        }
                    }
                }
            }

            $s = index($work, "<");
            $e = index($work, ">");
        }
        $params{"ELEMENTS"} = $elems;
    }

    if (($elems eq 0) && (!$self->{ERROR}))
    {
        $self->{ERROR}     = lprint("A CHOICE tag must contain at least one CHOICEELEMENT (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 13;
    }

    $name = "TAGNAME_" . $params{"NAME"};
    $name = "\U$name";

    if (defined($self->{$name}))
    {
        $self->{ERROR}     = lprint("A CHOICE tag must have unique name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 20;
    }
    else
    {
        $self->{$name} = $self->{NUMTAGS};
    }

    $self->PlaceParams("CHOICE", $tn, %params);

    $self->{NUMTAGS}++;

    1;
}

sub PrintComponent
{
    my ($crap, $self, $tagno) = @_;
    my ($doc) = $self->{DOCUMENT};

    $doc->SetVisited($doc->GetTagParam($tagno, "NAME"));

    my $tmpl;

    if ($doc->GetTagParam($tagno, "MULTI") eq "no")
    {
        $tmpl = Survey::Component::Choice->PrintChoiceNoMulti($self, $tagno);
    }
    else { $tmpl = Survey::Component::Choice->PrintMultiChoice($self, $tagno); }

    return $tmpl->get();
}

sub PrintChoiceNoMulti
{
    my ($crap, $self, $tagno) = @_;
    my ($doc) = $self->{DOCUMENT};
    my $ses = $doc->{SESSION};
    my $numberOfSubtags = $doc->GetTagParam($tagno, "ELEMENTS");
    my ($i);

    # create a new template object
    my $tmpl = new Survey::Template($ENV{_SURVEY_HOME} . "/templates/default/Choice.tmpl");

    my $thistagname = $doc->GetTagParam($tagno, "NAME");
    my $valueErrorDescription = $ses->getValue("VALUE_ERROR_DESCRIPTION_$thistagname");

    if ($valueErrorDescription)
    {
        my $value = $ses->getValue("VALUE_ERROR_$thistagname");

        $tmpl->setVar("errorDescription", $valueErrorDescription);
        $ses->setValue("VALUE_ERROR_$thistagname",             "");
        $ses->setValue("VALUE_ERROR_DESCRIPTION_$thistagname", "");
    }

    $tmpl->setVar("captStyle", $doc->GetTagParam($tagno, "CAPTSTYLE"));
    $tmpl->setVar("caption",   $doc->GetTagParam($tagno, "CAPTION"));
    $tmpl->setVar("style",     $doc->GetTagParam($tagno, "STYLE"));
    $tmpl->setVar("name",     $thistagname);

	my($mustanswer) = lprint("The question with caption") 
              . " \""
              . $doc->GetTagParam($tagno, "CAPTION")
              . "\" "
              . lprint("must be answered.");

	if($doc->GetTagParam($tagno, "MUSTANSWER") eq "yes")
	{
		$tmpl->setVar("mustanswer"," mustanswer");
		$tmpl->setVar("mustanswermsg",$mustanswer);		
	}
	else
	{
		$tmpl->setVar("mustanswer","");
		$tmpl->setVar("mustanswermsg","");
	}
			
    # added in CRU patch (MJ/20020802)
    my ($numSelected, $found);

    if ($self->{RETRIEVE})
    {
        my $tablenum = $tagno + 1;
        my $sql = "SELECT " . $doc->GetTagParam($tagno, "NAME") . " FROM " . $doc->GetOption("DBITABLE");
        $sql .= " WHERE mail=\'";

        if ($doc->GetOption("REQAUTH") eq "soap")
        {
            $sql .= $doc->{EMAIL};
        }
        else
        {
            $sql .= $doc->{REMOTE_USER};
        }

        $sql .= "\' AND save_partial=\'1\'";
        my $sth = $self->{DBH}->prepare($sql);
        $sth->execute;
        ($numSelected) = $sth->fetchrow_array();
        $found = 0;
    }

    # added in CRU patch (MJ/20020802)

    if ($doc->GetTagParam($tagno, "RANDOM") eq "yes") { $tmpl->enterLoop("LOOP", "random"); }
    else                                              { $tmpl->enterLoop("LOOP"); }

    for (my $i = 0 ; $i < $numberOfSubtags ; $i++)
    {

        # do not randomize position of this looprun (only affects random loops)
        if ($doc->GetChoiceElementParam($tagno, $i, "STAYHERE") eq "yes") { $tmpl->fixLoopRun(); }

		if($doc->GetTagParam($tagno, "MUSTANSWER") eq "yes")
		{
			$tmpl->setVar("mustanswer"," mustanswer");		
		}
		else
		{
			$tmpl->setVar("mustanswer","");			
		}
	
		$tmpl->setVar("tagno", $i + 1);
        $tmpl->setVar("name", $doc->GetTagParam($tagno, "NAME"));
        $tmpl->setVar("value",     $doc->GetChoiceElementParam($tagno, $i, "VALUE"));
        $tmpl->setVar("accessKey", $doc->GetChoiceElementParam($tagno, $i, "ACCESS"));
        $tmpl->setVar("labelCaptStyle", $doc->GetTagParam($tagno, "LABELCAPTSTYLE"));
        $tmpl->setVar("labelCaption", $doc->GetChoiceElementParam($tagno, $i, "CAPTION"));

        #bugant persist ;)
        # the name of current item
        my $persistItemName = $doc->GetTagParam($tagno, "NAME");

        # the sumitted value
        my $chkpersist = $ses->getValue("SUBMITTED_$persistItemName");

        if (defined($chkpersist)    # if a value is available
            && ($chkpersist ne $doc->GetOption("NOTDISPLAYEDVAL"))         # if respondent saw the question
            && ($chkpersist ne $doc->GetTagParam($tagno, "ILLEGALVAL"))    # if respondent submitted a value (?)
            && ($chkpersist eq $doc->GetChoiceElementParam($tagno, $i, "VALUE")
            )    # if submitted value is equal to value of current element
          )
        {
            $tmpl->setVar("checked", 1);
        }

        elsif ($doc->GetChoiceElementParam($tagno, $i, "CHECKED") eq "yes")
        {
            $tmpl->setVar("checked", 1);
        }

        #bugant persist -- end --

        $tmpl->nextLoop();    # LOOP
    }
    $tmpl->exitLoop();        # LOOP

    # if an otherfield should be displayed
    if ($doc->GetTagParam($tagno, "OTHERFIELD") ne "-1")
    {
        $tmpl->setVar("otherField",          1);
        $tmpl->setVar("otherRadioName",      $doc->GetTagParam($tagno, "NAME"));
        $tmpl->setVar("otherRadioAccessKey", $doc->GetChoiceElementParam($tagno, $i, "ACCESS"));
        $tmpl->setVar("otherText", $doc->GetTagParam($tagno, "OTHERFIELD"));
        $tmpl->setVar("otherName", $doc->GetTagParam($tagno, "NAME") . "-otherfield");

        # added in CRU patch (MJ/20020807)
        if ($self->{RETRIEVE} and (int($numSelected) ne $numSelected))
        {
            $tmpl->setVar("otherChecked", 1);
        }

        #bugant persist ;)
        # fill textfield
        my $persistItemName = $doc->GetTagParam($tagno, "NAME") . "-otherfield";
        my $chkpersist = $ses->getValue("SUBMITTED_" . $persistItemName);

        if (   defined($chkpersist)
            && ($chkpersist ne $doc->GetOption("NOTDISPLAYEDVAL"))
            && ($chkpersist ne $doc->GetTagParam($tagno, "ILLEGALVAL")))
        {
            $tmpl->setVar("otherValue", $chkpersist);
        }

        # check radiobutton in front of the otherfield
        $persistItemName = $doc->GetTagParam($tagno, "NAME");
        $chkpersist = $ses->getValue("SUBMITTED_" . $persistItemName);

        if ($chkpersist == 777) { $tmpl->setVar("otherChecked", 1); }

        #bugant persist -- end --
    }

    return $tmpl;
}

sub PrintMultiChoice
{
    my ($crap, $self, $tagno) = @_;
    my ($doc) = $self->{DOCUMENT};
    my ($ses) = $doc->{SESSION};
    my $numberOfChoiceElements = $doc->GetTagParam($tagno, "ELEMENTS");
    my ($sp) = "      ";
    my ($i, $j);

    my ($other);

    # create a new template object
    my $tmpl = new Survey::Template($ENV{_SURVEY_HOME} . "/templates/default/ChoiceMulti.tmpl");

    my $thistagname = $doc->GetTagParam($tagno, "NAME");
    my $valueErrorDescription = $ses->getValue("VALUE_ERROR_DESCRIPTION_$thistagname");

    if ($valueErrorDescription)
    {
        my $value = $ses->getValue("VALUE_ERROR_$thistagname");

        $tmpl->setVar("errorDescription", $valueErrorDescription);
        $ses->setValue("VALUE_ERROR_$thistagname",             "");
        $ses->setValue("VALUE_ERROR_DESCRIPTION_$thistagname", "");
    }

    $tmpl->setVar("captStyle", $doc->GetTagParam($tagno, "CAPTSTYLE"));
    $tmpl->setVar("caption",   $doc->GetTagParam($tagno, "CAPTION"));
    $tmpl->setVar("style",     $doc->GetTagParam($tagno, "STYLE"));
    
    $tmpl->setVar("name",     $thistagname);

	my($mustanswer) = lprint("The question with caption") 
              . " \""
              . $doc->GetTagParam($tagno, "CAPTION")
              . "\" "
              . lprint("must be answered.");

	if($doc->GetTagParam($tagno, "MUSTANSWER") eq "yes")
	{
		$tmpl->setVar("mustanswer"," mustanswer");
		$tmpl->setVar("mustanswermsg",$mustanswer);		
	}
	else
	{
		$tmpl->setVar("mustanswer","");
		$tmpl->setVar("mustanswermsg","");
	}
    

    #bugant persist ;)

    if ($doc->GetTagParam($tagno, "RANDOM") eq "yes") { $tmpl->enterLoop("LOOP", "random"); }
    else                                              { $tmpl->enterLoop("LOOP"); }

    for (my $i = 0 ; $i < $numberOfChoiceElements ; $i++)
    {
        my $valueNumber = $i + 1;

        if ($doc->GetChoiceElementParam($tagno, $i, "STAYHERE") eq "yes") { $tmpl->fixLoopRun(); }

		if($doc->GetTagParam($tagno, "MUSTANSWER") eq "yes")
		{
			$tmpl->setVar("mustanswer"," mustanswer");		
		}
		else
		{
			$tmpl->setVar("mustanswer","");			
		}
	
		$tmpl->setVar("tagno", $i + 1);
        $tmpl->setVar("name", $doc->GetTagParam($tagno, "NAME"));
        $tmpl->setVar("value",     $doc->GetChoiceElementParam($tagno, $i, "VALUE"));
        $tmpl->setVar("accessKey", $doc->GetChoiceElementParam($tagno, $i, "ACCESS"));

        # bugant4persistance
        my $thistagname = $doc->GetTagParam($tagno, "NAME");
        my $chkpersist = ($doc->{SESSION}->getValue("SUBMITTED_" . $thistagname));

        if (   defined($chkpersist)
            && ($chkpersist ne $doc->GetOption("NOTDISPLAYEDVAL"))
            && ($chkpersist ne $doc->GetTagParam($tagno, "ILLEGALVAL")))
        {
            $tmpl->setVar("checked", 1);
        }
        else
        {
            if ($doc->GetChoiceElementParam($tagno, $i, "CHECKED") eq "yes")
            {
                $tmpl->setVar("checked", 1);
            }
        }

        $tmpl->setVar("labelCaption", $doc->GetChoiceElementParam($tagno, $i, "CAPTION"));

        $tmpl->nextLoop();    # LOOP
    }
    $tmpl->exitLoop();        # LOOP

    if ($doc->GetTagParam($tagno, "OTHERFIELD") ne "-1")
    {
        my $valueNumberOfCheckBoxInFrontOfOtherfield = $numberOfChoiceElements + 1;
        my $valueNumberOfOtherField                  = $numberOfChoiceElements + 2;

        $tmpl->setVar("otherField",        1);
        $tmpl->setVar("otherCheckBoxName", $doc->GetTagParam($tagno, "NAME"));
        $tmpl->setVar("accessKey",         $doc->GetChoiceElementParam($tagno, $i, "ACCESS"));
        $tmpl->setVar("otherText", $doc->GetTagParam($tagno, "OTHERFIELD"));
        $tmpl->setVar("otherName", $doc->GetTagParam($tagno, "NAME") . "-otherfield");

        # set checkbox in front of otherfield checked if necessary
        my $otherCheckBoxName = $doc->GetTagParam($tagno, "NAME") . "_$valueNumberOfCheckBoxInFrontOfOtherfield";
        my $chkpersist = ($doc->{SESSION}->getValue("SUBMITTED_" . $otherCheckBoxName));

        if (   defined($chkpersist)
            && ($chkpersist ne $doc->GetOption("NOTDISPLAYEDVAL"))
            && ($chkpersist ne $doc->GetTagParam($tagno, "ILLEGALVAL"))
            && ($chkpersist == 777))
        {
            $tmpl->setVar("otherChecked", 1);
        }

        my $otherFieldName = $doc->GetTagParam($tagno, "NAME") . "-otherfield";
        $chkpersist = ($doc->{SESSION}->getValue("SUBMITTED_" . $otherFieldName));

        if (   defined($chkpersist)
            && ($chkpersist ne $doc->GetOption("NOTDISPLAYEDVAL"))
            && ($chkpersist ne $doc->GetTagParam($tagno, "ILLEGALVAL")))
        {
            $tmpl->setVar("otherChecked", 1);
            $tmpl->setVar("otherValue",   $chkpersist);
        }
    }

    return $tmpl;
}

sub NumberOfValues
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 1;
}

sub GetValueNumerical
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;

    my ($numerical) = 1;

    if ($doc->GetTagParam($tagno, "MULTI") eq "yes") { $numerical = 0; }
    if ($doc->GetTagParam($tagno, "OTHERFIELD") ne "-1") { $numerical = 0; }

    return $numerical;
}

sub GetCaptionByValue
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $value) = @_;

    my ($cap) = "";
    my ($i);

    for ($i = 0 ; $i < $doc->GetTagParam($tagno, "ELEMENTS") ; $i++)
    {
        my ($val) = $doc->GetChoiceElementParam($tagno, $i, "VALUE");
        if ($val eq $value)
        {
            $cap = $doc->GetChoiceElementParam($tagno, $i, "CAPTION");
        }
    }

    return $cap;
}

sub GetCheckValue
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;

    my ($value) = {};
    my ($name) = $doc->GetTagParam($tagno, "NAME");
    $value->{NUMERICAL} = Survey::Component::Choice->GetValueNumerical($doc, $sub, $arg, $ses, $tagno);
    $value->{NAME} = $name;

    my ($visited) = $doc->CheckVisited($name);

    if ($visited)
    {
        if (defined($arg->ArgByName($name)))
        {
            $value->{VALUE} = $arg->ArgByName($name);
        }
        else
        {
            $value->{VALUE} = "";
        }
    }
    else
    {
        $value->{VALUE} = $doc->GetOption("NOTDISPLAYEDVAL");
    }

    if (($value->{VALUE} eq "!") || ($arg->ArgByName($name . "-otherfield") ne ""))
    {
        if ($doc->GetTagParam($tagno, "MULTI") eq "no")
        {
            $value->{VALUE} = $arg->ArgByName($name . "-otherfield");
        }
        else
        {
            if ($value->{VALUE} ne "")
            {
                $value->{VALUE} .= ",";
            }
            $value->{VALUE} .= "!" . $arg->ArgByName($name . "-otherfield");
        }
    }

    if ($value->{VALUE} eq "")
    {
        if (($doc->GetTagParam($tagno, "MUSTANSWER") eq "yes") && (!$sub->{SAVE}))
        {
            $sub->{ERROR} =
                lprint("The question with caption") 
              . " <i>\""
              . $doc->GetTagParam($tagno, "CAPTION")
              . "\"</i> "
              . lprint("must be answered.");
            $sub->{ERRORCODE} = 2;
        }
        else
        {
            if ($doc->GetTagParam($tagno, "MULTI") ne "yes")
            {
                $value->{VALUE} = $doc->GetTagParam($tagno, "ILLEGALVAL");
            }
        }
    }

    $value->{VALUE} =~ s/\!\,//g;
    $value->{CAPTION} = Survey::Component::Choice->GetCaptionByValue($doc, $sub, $arg, $ses, $tagno, $value->{VALUE});

    return $value;
}

sub GetValueCaption
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $value) = @_;

    my ($caption) = $value;

    if ($doc->GetTagParam($tagno, "MULTI") ne "yes")
    {
        $caption = "[illegal]";
        if ($value ne int($value)) { $caption = $value; }

        my ($elems) = $doc->GetTagParam($tagno, "ELEMENTS");
        for (my ($i) = 0 ; $i < $elems ; $i++)
        {
            my ($evalue) = $doc->GetChoiceElementParam($tagno, $i, "VALUE");
            if ($evalue eq $value)
            {
                $caption = $doc->GetChoiceElementParam($tagno, $i, "CAPTION");
            }
        }
    }

    return $caption;
}

sub GetPossibleValues
{
    my ($s, $doc, $sub, $arg, $ses, $tagno) = @_;
    my (@arr) = ();

    my ($elems) = $doc->GetTagParam($tagno, "ELEMENTS");
    for (my ($i) = 0 ; $i < $elems ; $i++)
    {
        my ($evalue) = $doc->GetChoiceElementParam($tagno, $i, "VALUE");
        if (defined($evalue) && ($evalue ne ""))
        {
            my ($entha) = {};
            $entha->{VALUE} = $evalue;
            $entha->{CAPTION} = $doc->GetChoiceElementParam($tagno, $i, "CAPTION");

            push(@arr, $entha);
        }
    }

    return \@arr;
}

sub MakeMultiSplit
{
    my ($s, $doc, $sub, $arg, $ses, $tagno) = @_;
    my (@arr) = ();

    if ($doc->GetTagParam($tagno, "MULTI") ne "yes")
    {
        return @arr;
    }

    my (@val) = @{ Survey::Component::Choice->GetPossibleValues($doc, $sub, $arg, $ses, $tagno) };
    my ($v);

    my ($name) = $doc->GetTagParam($tagno, "NAME");

    my ($c) = 0;

    foreach $v (@val)
    {
        my ($mv) = {};
        $mv->{VALUE} = $v;
        $mv->{NAME}  = $name . chr(97 + $c);
        $c++;
        push(@arr, $mv);
    }

    return \@arr;
}

sub MakeXML
{
    my ($s, $doc, $tagno) = @_;

    my ($out) = $s->MakeTagOpener($doc, $tagno);

    my ($elems) = $doc->GetTagParam($tagno, "ELEMENTS");
    for (my ($i) = 0 ; $i < $elems ; $i++)
    {
        my ($evalue) = $doc->GetChoiceElementParam($tagno, $i, "VALUE");
        my ($ecapt)  = $doc->GetChoiceElementParam($tagno, $i, "CAPTION");
        my ($echeck) = $doc->GetChoiceElementParam($tagno, $i, "CHECKED");

        my ($val, $cap, $check);
        $val = "VALUE=\"$evalue\"";
        $cap = "CAPTION=\"$ecapt\"";
        if ($echeck ne "no")
        {
            $check = "CHECKED=\"yes\"";
        }
        else
        {
            $check = "";
        }
        $out .= "  <CHOICEELEMENT $cap $val $check />\n";
    }

    $out .= $s->MakeTagCloser($doc, $tagno);

    return $out;
}

1;

