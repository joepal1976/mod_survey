#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::Timer;
use strict;

@Survey::Component::Timer::ISA = qw(Survey::Component::Component);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub FillParams
{
    my ($crap, $self) = @_;

    @{ $self->{"ALLOWED_TIMER"} } = ("NAME");
    @{ $self->{"ALL_TIMER"} } = (@{ $self->{"ALLOWED_TIMER"} }, "TYPE");

    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;

    $self->{"TIMER_TYPE"} = "TIMER";
    $self->{"TIMER_NAME"} = "";

    1;
}

sub PlaceComponent
{
    my ($crap, $self, $paramstr) = @_;

    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    my (%params, $cell, $name, $value, @pararr);

    %params = $self->GetDefaults("TIMER");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "TIMER");
    }

    if ($params{empty} eq "no")
    {
        $self->{ERROR}     = lprint("A TIMER tag must be terminated immediately (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 11;
    }

    if ((!$self->{ERROR}) && (!$params{NAME}))
    {
        $self->{ERROR}     = lprint("A TIMER tag must have a name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 12;
    }

    if ((!$self->{ERROR}) && (length($params{NAME}) > 8))
    {
        $self->{ERROR}     = lprint("A TIMER tag name can only be 8 characters wide (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 21;
    }

    if (!$self->{ERROR}) { $self->CheckName($params{NAME}, "TIMER"); }

    $name = "TAGNAME_" . $params{"NAME"};
    $name = "\U$name";

    if ((!$self->{ERROR}) && (defined($self->{$name})))
    {
        $self->{ERROR}     = lprint("A TIMER tag must have unique name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 20;
    }
    else
    {
        $self->{$name} = $self->{NUMTAGS};
    }

    $self->PlaceParams("TIMER", $tn, %params);

    $self->{NUMTAGS}++;

    1;
}

sub PrintComponent
{
    my ($crap, $self, $tagno) = @_;
    my ($doc) = $self->{DOCUMENT};

    my ($name) = $doc->GetTagParam($tagno, "NAME");
    $doc->SetVisited($name);

    my ($time) = time;

    $doc->{SESSION}->setValue($name . "_original", $time);

    return "\n<input type=\"hidden\" value=\"" . $time . "\" name=\"" . $doc->GetTagParam($tagno, "NAME") . "\" />\n";
}

sub NumberOfValues
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 1;
}

sub GetValueNumerical
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 1;
}

sub GetCheckValue
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;

    my ($value) = {};
    my ($name) = $doc->GetTagParam($tagno, "NAME");

    my ($visited) = $doc->CheckVisited($name);

    if ($visited)
    {
        my ($val) = $doc->{SESSION}->getValue($name . "_original");
        $val = time - int($val);
        $value->{VALUE} = $val;
    }
    else
    {
        $value->{VALUE} = $doc->GetOption("NOTDISPLAYEDVAL");
    }

    $value->{NAME}      = $name;
    $value->{NUMERICAL} = 1;

    return $value;
}

1;

