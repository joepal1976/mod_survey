#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::Calculated;
use strict;

@Survey::Component::Calculated::ISA = qw(Survey::Component::Component);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub FillParams
{
    my ($crap, $self) = @_;

    @{ $self->{"ALLOWED_CALCULATED"} } = ("NAME", "CALCULATION");
    @{ $self->{"ALL_CALCULATED"} } = (@{ $self->{"ALLOWED_CALCULATED"} }, "TYPE");

    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;

    $self->{"CALCULATED_TYPE"}   = "CALCULATED";
    $self->{"CALCULATED_NAME"}   = "";
    $self->{"CALCULATED_CALCULATION"}  = "";
    1;
}

sub PlaceComponent
{
    my ($crap, $self, $paramstr) = @_;

    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    my (%params, $cell, $name, $value, @pararr);

    %params = $self->GetDefaults("CALCULATED");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "CALCULATED");
    }

    # bugant persist
    my ($consn) = $params{NAME};

    if ($self->{SESSION}->getValue("SUBMITTED_" . $consn))
    {
        $params{VALUE} = ($self->{SESSION}->getValue("SUBMITTED_" . $consn));
    }

    # bugant persisted

    if ($params{empty} eq "no")
    {
        $self->{ERROR}     = lprint("A CALCULATED tag must be terminated immediately (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 11;
    }

    if ((!$self->{ERROR}) && (!$params{NAME}))
    {
        $self->{ERROR}     = lprint("A CALCULATED tag must have a name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 12;
    }

    if ((!$self->{ERROR}) && (!$params{CALCULATION}))
    {
        $self->{ERROR}     = lprint("A CALCULATED tag must have a CALCULATION field (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 35;
    }

    if ((!$self->{ERROR}) && (length($params{NAME}) > 8))
    {
        $self->{ERROR}     = lprint("A CALCULATED tag name can only be 8 characters wide (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 21;
    }

    if (!$self->{ERROR}) { $self->CheckName($params{NAME}, "CALCULATED"); }

    $name = "TAGNAME_" . $params{"NAME"};
    $name = "\U$name";

    if ((!$self->{ERROR}) && (defined($self->{$name})))
    {
        $self->{ERROR}     = lprint("A CALCULATED tag must have unique name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 20;
    }
    else
    {
        $self->{$name} = $self->{NUMTAGS};
    }

    if($params{"CALCULATION"} =~ m/[^\$a-zA-Z0-9\+\-\.\s\(\)\*\/]/)
    {
        $self->{ERROR}     = lprint("A CALCULATION field contains illegal characters");
        $self->{ERRORCODE} = 20;
    }

    $self->PlaceParams("CALCULATED", $tn, %params);

    $self->{NUMTAGS}++;

    1;
}

sub PrintComponent
{
    my ($crap, $self, $tagno) = @_;
    my ($doc) = $self->{DOCUMENT};

    $doc->SetVisited($doc->GetTagParam($tagno, "NAME"));

    
    return "<!-- calculated -->";
}

sub NumberOfValues
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 1;
}

sub GetValueNumerical
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 1;
}

sub GetCheckValue
{
  my ($s, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;

  my ($value) = {};
  my ($name) = $doc->GetTagParam($tagno, "NAME");

  my ($visited) = $doc->CheckVisited($name);

  my ($string) = $doc->GetTagParam($tagno, "CALCULATION");
  my ($maxiter) = 100;

  while( ($string =~ m/(\$[a-zA-Z0-9]+)/) && ($maxiter-- > 0))
  {
    my($found) = $1;
    my($vname) = $found;

    $vname =~ s/\$//g;

    my($val) = $ses->getValue("SUBMITTED_$vname");

    if($val =~ m/[^0-9\.\+\-\s]/)
    {
      $val = "0";
    }
    my($replace) = "\$string =~ s/\\$found/$val/g";
    eval($replace);
  }

  if($string =~ m/[^0-9\.\+\-\s]/)
  {
    $string = "-1";
  }

  my($calculation);
  my($calcstring) = "\$calculation = $string;";
  eval($calcstring);
  if($@) { $calculation = -1; }

  $value->{VALUE} = $calculation;

    $value->{NAME}      = $name;
    $value->{NUMERICAL} = 1;

    return $value;
}

1;

