#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::Text;
use strict;

@Survey::Component::Text::ISA = qw(Survey::Component::Component);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub FillParams
{
    my ($crap, $self) = @_;

    @{ $self->{"ALLOWED_TEXT"} } = ("NAME",
                                    "MAXLEN",
                                    "DEFAULT",
                                    "MUSTANSWER",
                                    "CAPTION",
                                    "CAPTSTYLE",
                                    "NUMERICAL",
                                    "MAXVAL",
                                    "MINVAL",
                                    "ILLEGALVAL",
                                    "STYLE",
                                    "UNIT");
    @{ $self->{"ALL_TEXT"} } = (@{ $self->{"ALLOWED_TEXT"} }, "TYPE", "FILE");

    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;

    $self->{"TEXT_TYPE"}       = "TEXT";
    $self->{"TEXT_NAME"}       = "";
    $self->{"TEXT_MAXLEN"}     = "80";
    $self->{"TEXT_DEFAULT"}    = "";
    $self->{"TEXT_MUSTANSWER"} = "no";
    $self->{"TEXT_CAPTION"}    = "";
    $self->{"TEXT_CAPTSTYLE"}  = "";       #"TEXTcap";
    $self->{"TEXT_NUMERICAL"}  = "no";
    $self->{"TEXT_MINVAL"}     = "";
    $self->{"TEXT_MAXVAL"}     = "";
    $self->{"TEXT_ILLEGALVAL"} = "-1";
    $self->{"TEXT_STYLE"}      = "";       #"TEXTelm";
    $self->{"TEXT_UNIT"}       = "";

    1;
}

sub PlaceComponent
{
    my ($crap, $self, $paramstr) = @_;

    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    my (%params, $cell, $name, $value, @pararr);

    %params = $self->GetDefaults("TEXT");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "TEXT");
    }

    #bugant: fix for session loading values
    $params{FILE} = $self->{FILE};

    if ($params{empty} eq "no")
    {
        $self->{ERROR}     = lprint("A TEXT tag must be terminated immediately (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 11;
    }

    if ((!$self->{ERROR}) && (!$params{NAME}))
    {
        $self->{ERROR}     = lprint("A TEXT tag must have a name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 12;
    }

    if ((!$self->{ERROR}) && (length($params{NAME}) > 8))
    {
        $self->{ERROR}     = lprint("A TEXT tag name can only be 8 characters wide (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 21;
    }

    if (!$self->{ERROR}) { $self->CheckName($params{NAME}, "TEXT"); }

    $name = "TAGNAME_" . $params{"NAME"};
    $name = "\U$name";

    if (defined($self->{$name}))
    {
        $self->{ERROR}     = lprint("A TEXT tag must have unique name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 20;
    }
    else
    {
        $self->{$name} = $self->{NUMTAGS};
    }

    if ((!$self->{ERROR}) && ($params{MAXVAL}) && ($params{MINVAL}) && ($params{MAXVAL} < $params{MINVAL}))
    {
        $self->{ERROR} =
          lprint("It seems that you have put a MAXVAL value < than the MINVAL value (in ") . $self->{FILE} . ")";

        #    $self->{ERRORCODE} = ; it's necessary to put a code here!!!
    }

    $self->PlaceParams("TEXT", $tn, %params);
    $self->{NUMTAGS}++;

    1;
}

sub PrintComponent
{
    my ($crap, $self, $tagno) = @_;    # $self points to an Survey::Submit object, so $sub would be a better name !!
    my ($doc) = $self->{DOCUMENT};
    my $ses = $self->{SESSION};
    my ($i);

    $doc->SetVisited($doc->GetTagParam($tagno, "NAME"));

    my $tmpl = new Survey::Template($ENV{_SURVEY_HOME} . "/templates/default/Text.tmpl");

    my $thistagname = $doc->GetTagParam($tagno, "NAME");

    my $valueErrorDescription = $ses->getValue("VALUE_ERROR_DESCRIPTION_$thistagname");

    if ($valueErrorDescription)
    {
        my $value = $ses->getValue("VALUE_ERROR_$thistagname");

        $tmpl->setVar("errorDescription", $valueErrorDescription);
        $tmpl->setVar("erroneusValue",    $value);

        $ses->setValue("VALUE_ERROR_$thistagname",             "");
        $ses->setValue("VALUE_ERROR_DESCRIPTION_$thistagname", "");
    }

    $tmpl->setVar("captStyle", $doc->GetTagParam($tagno, "CAPTSTYLE"));
    $tmpl->setVar("caption",   $doc->GetTagParam($tagno, "CAPTION"));
    $tmpl->setVar("style",     $doc->GetTagParam($tagno, "STYLE"));
    $tmpl->setVar("name",      $doc->GetTagParam($tagno, "NAME"));
    $tmpl->setVar("maxLength", $doc->GetTagParam($tagno, "MAXLEN"));

    $tmpl->setVar("name",     $thistagname);

	my($mustanswer) = lprint("The question with caption") 
              . " \""
              . $doc->GetTagParam($tagno, "CAPTION")
              . "\" "
              . lprint("must be answered.");

	if($doc->GetTagParam($tagno, "MUSTANSWER") eq "yes")
	{
		$tmpl->setVar("mustanswer"," mustanswer");
		$tmpl->setVar("mustanswermsg",$mustanswer);		
	}
	else
	{
		$tmpl->setVar("mustanswer","");
		$tmpl->setVar("mustanswermsg","");
	}

    my($numerical) = lprint("Answer must be numerical (to question with caption") . " \""
                  . $doc->GetTagParam($tagno, "CAPTION") . "\")";

	if($doc->GetTagParam($tagno, "NUMERICAL") eq "yes")
	{
		$tmpl->setVar("numerical"," data-numerical=\"1\"");
		$tmpl->setVar("numericalmsg",$numerical);		
	}
	else
	{
		$tmpl->setVar("numerical"," data-numerical=\"0\"");
		$tmpl->setVar("numericalmsg","");
	}

	my($maxval) = lprint("Answer cannot be higher than ")
		. $doc->GetTagParam($tagno, "MAXVAL")
		. lprint(" (in question with caption") . " "
		. $doc->GetTagParam($tagno, "CAPTION") . "\")";
             
	if ($doc->GetTagParam($tagno, "MAXVAL") ne "")
	{
		$tmpl->setVar("maxval"," data-maxval=\"" . $doc->GetTagParam($tagno, "MAXVAL") . "\"");
		$tmpl->setVar("maxvalmsg",$maxval);		
	}
	else
	{
		$tmpl->setVar("maxval","");
		$tmpl->setVar("maxvalmsg","");
	}

	my($minval) = lprint("Answer cannot be lower than ")
		. $doc->GetTagParam($tagno, "MINVAL")
		. lprint(" (in question with caption") . " "
		. $doc->GetTagParam($tagno, "CAPTION") . "\")";
             
	if ($doc->GetTagParam($tagno, "MINVAL") ne "")
	{
		$tmpl->setVar("minval"," data-minval=\"" . $doc->GetTagParam($tagno, "MINVAL") . "\"");
		$tmpl->setVar("minvalmsg",$minval);		
	}
	else
	{
		$tmpl->setVar("minval","");
		$tmpl->setVar("minvalmsg","");
	}
		     
    my $size = ($doc->GetTagParam($tagno, "MAXLEN") < 40) ? $doc->GetTagParam($tagno, "MAXLEN") : 40;

    if(Survey::Slask->isMobileOverride())
    {
      if($size > 16)
      {
        $size = 16;
      }
    }

    $tmpl->setVar("size", $size);

    $tmpl->setVar("unit", $doc->GetTagParam($tagno, "UNIT"));

    my $chkpersist = $doc->{SESSION}->getValue("SUBMITTED_" . $thistagname);

    #bugant persist
    if (   defined($chkpersist)
        && ($chkpersist ne $doc->GetOption("NOTDISPLAYEDVAL"))
        && ($chkpersist ne $doc->GetTagParam($tagno, "ILLEGALVAL")))
    {
        $tmpl->setVar("value", $chkpersist);
    }
    elsif ($doc->GetTagParam($tagno, "DEFAULT") ne "")
    {
        $tmpl->setVar("value", $doc->GetTagParam($tagno, "DEFAULT"));
    }

    #bugant persist -- end --

    return $tmpl->get();
}

sub NumberOfValues
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 1;
}

sub GetValueNumerical
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;

    if ($doc->GetTagParam($tagno, "NUMERICAL") eq "no")
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

sub GetCheckValue
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;

    my ($val);

    my ($value) = {};
    my ($name) = $doc->GetTagParam($tagno, "NAME");

    $value->{NAME} = $name;
    $value->{NUMERICAL} = Survey::Component::Text->GetValueNumerical($doc, $sub, $arg, $ses, $tagno);

    my ($visited) = $doc->CheckVisited($name);

    if ($visited)
    {
        $value->{VALUE} = $arg->ArgByName($name);
    }
    else
    {
        $value->{VALUE} = $doc->GetOption("NOTDISPLAYEDVAL");
    }

    my ($wasillegal) = 0;

    # Check if question was answered at all
    if (($doc->GetTagParam($tagno, "MUSTANSWER") eq "yes") && (!$sub->{SAVE}))
    {
        if ($value->{VALUE} eq "")
        {
            $sub->{ERROR} =
                lprint("The question with caption") 
              . " <i>\""
              . $doc->GetTagParam($tagno, "CAPTION")
              . "\"</i> "
              . lprint("must be answered.");
            $sub->{ERRORCODE} = 2;
        }
    }
    else
    {
        if ($value->{VALUE} eq "")
        {
            $value->{VALUE} = $doc->GetTagParam($tagno, "ILLEGALVAL");
            $wasillegal = 1;
        }
    }

    # Check if answer was a correctly formatted numerical
    my ($numerical) = $doc->GetTagParam($tagno, "NUMERICAL");
    if (grep(/^$numerical$/, ("yes", "float")) && (!$wasillegal))
    {
        my ($crap) = "-13412343dd";

        # check format of float (adapted from patch by MH)
        if ($numerical eq "float")
        {
            $val = $value->{VALUE};

            $val = '0' . $val if $val =~ /^[.,]/;
            if ($val =~ /^0*(\d+?(?:[,.]\d*)?)$/)
            {
                $val = $1;
                $val =~ s/[,.]/\./;
                $crap = $val;
            }
            else
            {
                $val = "";
            }
            $value->{VALUE} = $val;
        }
        else    # remove leading zeros (adapted from patch by MH)
        {

            #int removes the leading zeros itself (BugAnt)
            $val = int($value->{VALUE});

            # final check if answer was numerical at all
            # if there is no error and the value isn't  int (otherwise int() return 0=false)
            # or the value is not a zero then the value isn't numerical (BugAnt)
            if ((!$sub->{ERROR}) && (!((int($value->{VALUE})) || ($value->{VALUE} eq "0"))))
            {
                $sub->{"ERROR"} =
                  lprint("Answer must be numerical (to question with caption") . " \""
                  . $doc->GetTagParam($tagno, "CAPTION") . "\")";
                $sub->{"ERRORCODE"} = 7;
            }
            else
            {
                $value->{VALUE} = $val;
            }
        }

        # Check that value is between max and min limits
        if (!$sub->{ERROR})
        {

            #Check if there is a MAXVAL (BugAnt)
            if ($doc->GetTagParam($tagno, "MAXVAL") ne "")
            {

                if ($doc->GetTagParam($tagno, "MAXVAL") < $val)
                {
                    $sub->{"ERROR"} =
                        lprint("Answer cannot be higher than ")
                      . $doc->GetTagParam($tagno, "MAXVAL")
                      . lprint(" (in question with caption")
                      . $doc->GetTagParam($tagno, "CAPTION") . "\")";
                    $sub->{"ERRORCODE"} = 8;
                }
            }

            #Check if there is a MINVAL (BugAnt)
            if ($doc->GetTagParam($tagno, "MINVAL") ne "")
            {
                if ($doc->GetTagParam($tagno, "MINVAL") > $val)
                {
                    $sub->{"ERROR"} =
                        lprint("Answer cannot be lower than ")
                      . $doc->GetTagParam($tagno, "MINVAL")
                      . lprint(" (in question with caption")
                      . $doc->GetTagParam($tagno, "CAPTION") . "\")";
                    $sub->{"ERRORCODE"} = 9;
                }
            }
        }
    }

    $value->{CAPTION} = $doc->GetTagParam($tagno, "CAPTION");

    return $value;
}

1;
