#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

# FORMAT RULES:
#
#
# Code		Description		Example
# ----		-----------		-------
# "#Y" 		four-digit year		2003
# "#M"		two-digit month		04
# "#D"		two-digit day of month	05
# "#d"		three-digit day of year	290
#
# "#a"		abbrev month name	"feb"
# "#A"		full month name		"february"
# "#w"		abbrev weekday name	"tue"
# "#W"		full weekday name	"tuesday"
#
# "#h"		two-digit hour (24)	23
# "#m"		two-digit minute	15
# "#s"		two-digit second	20
#
# "#e"		seconds since epoch	1066464551
#
#!/usr/bin/perl

package Survey::Component::DateTime;
use strict;

@Survey::Component::DateTime::ISA = qw(Survey::Component::Component);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub FillParams
{
    my ($crap, $self) = @_;

    @{ $self->{"ALLOWED_DATETIME"} } = ("NAME", "FORMAT");
    @{ $self->{"ALL_DATETIME"} } = (@{ $self->{"ALLOWED_DATETIME"} }, "MAXLEN", "TYPE");

    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;

    $self->{"DATETIME_TYPE"}   = "DATETIME";
    $self->{"DATETIME_NAME"}   = "";
    $self->{"DATETIME_FORMAT"} = "#Y-#M-#D #h:#m:#s";
    $self->{"DATETIME_MAXLEN"} = "80";

    1;
}

sub PlaceComponent
{
    my ($crap, $self, $paramstr) = @_;

    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    my (%params, $cell, $name, $value, @pararr);

    %params = $self->GetDefaults("DATETIME");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "DATETIME");
    }

    if ($params{empty} eq "no")
    {
        $self->{ERROR}     = lprint("A DATETIME tag must be terminated immediately (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 11;
    }

    if ((!$self->{ERROR}) && (!$params{NAME}))
    {
        $self->{ERROR}     = lprint("A DATETIME tag must have a name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 12;
    }

    if ((!$self->{ERROR}) && (length($params{NAME}) > 8))
    {
        $self->{ERROR}     = lprint("A DATETIME tag name can only be 8 characters wide (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 21;
    }

    if (!$self->{ERROR}) { $self->CheckName($params{NAME}, "DATETIME"); }

    $name = "TAGNAME_" . $params{"NAME"};
    $name = "\U$name";

    if ((!$self->{ERROR}) && (defined($self->{$name})))
    {
        $self->{ERROR}     = lprint("A DATETIME tag must have unique name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 20;
    }
    else
    {
        $self->{$name} = $self->{NUMTAGS};
    }

    $self->PlaceParams("DATETIME", $tn, %params);

    $self->{NUMTAGS}++;

    1;
}

sub PrintComponent
{
    my ($crap, $self, $tagno) = @_;
    my ($doc) = $self->{DOCUMENT};

    $doc->SetVisited($doc->GetTagParam($tagno, "NAME"));

    return "";
}

sub NumberOfValues
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 1;
}

sub GetValueNumerical
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 0;
}

sub GetCheckValue
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;

    my ($value) = {};
    my ($name) = $doc->GetTagParam($tagno, "NAME");

    my ($visited) = $doc->CheckVisited($name);

    if ($visited)
    {
        my (@weekabbr) =
          ("", lprint("mon"), lprint("tue"), lprint("wed"), lprint("thu"), lprint("fri"), lprint("sat"), lprint("sun"));

        my (@weekfull) = (
                    "", lprint("moniday"), lprint("tuesday"), lprint("wednesday"), lprint("thursdat"), lprint("friday"),
                    lprint("saturday"), lprint("sunday"));

        my (@monabbr) = ("",
                         lprint("jan"),
                         lprint("feb"),
                         lprint("mar"),
                         lprint("apr"),
                         lprint("may"),
                         lprint("jun"),
                         lprint("jul"),
                         lprint("aug"),
                         lprint("sep"),
                         lprint("oct"),
                         lprint("nov"),
                         lprint("dec"));

        my (@monfull) = ("",
                         lprint("january"),
                         lprint("february"),
                         lprint("mars"),
                         lprint("april"),
                         lprint("may"),
                         lprint("june"),
                         lprint("july"),
                         lprint("august"),
                         lprint("september"),
                         lprint("october"),
                         lprint("november"),
                         lprint("december"));

        my ($val) = $doc->GetTagParam($tagno, "FORMAT");
        my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime(time);

        my ($epoch) = time;

        $year += 1900;
        $mon++;

        my ($mabbr) = $monabbr[$mon];
        my ($mfull) = $monfull[$mon];

        my ($wabbr) = $weekabbr[$wday];
        my ($wfull) = $weekfull[$wday];

        if ($sec < 10)   { $sec  = "0" . $sec; }
        if ($min < 10)   { $min  = "0" . $min; }
        if ($hour < 10)  { $hour = "0" . $hour; }
        if ($mday < 10)  { $mday = "0" . $mday; }
        if ($mon < 10)   { $mon  = "0" . $mon; }
        if ($yday < 100) { $yday = "0" . $yday; }
        if ($yday < 10)  { $yday = "0" . $yday; }

        $val =~ s/\#Y/$year/g;
        $val =~ s/\#M/$mon/g;
        $val =~ s/\#D/$mday/g;
        $val =~ s/\#d/$yday/g;
        $val =~ s/\#a/$mabbr/g;
        $val =~ s/\#A/$mfull/g;
        $val =~ s/\#w/$wabbr/g;
        $val =~ s/\#W/$wfull/g;
        $val =~ s/\#h/$hour/g;
        $val =~ s/\#m/$min/g;
        $val =~ s/\#s/$sec/g;
        $val =~ s/\#e/$epoch/g;

        $value->{VALUE} = $val;
    }
    else
    {
        $value->{VALUE} = $doc->GetOption("NOTDISPLAYEDVAL");
    }

    $value->{NAME}      = $name;
    $value->{NUMERICAL} = 0;

    return $value;
}

1;

