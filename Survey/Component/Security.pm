#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::Security;
use strict;

@Survey::Component::Security::ISA = qw(Survey::Component::Component);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub FillParams
{
    my ($crap, $self) = @_;

    @{ $self->{"ALLOWED_SECURITY"} } = ("REQAUTH");

    @{ $self->{"ALL_SECURITY"} } = (@{ $self->{"ALLOWED_SECURITY"} }, "TYPE", "AUTH_TYPE");

    @{ $self->{"SECURITY_SUBTAGS"} } =
      ("ANSWER", "DATA", "SOURCE", "FLUSH", "DEBUG", "DBIAUTH", "FILEAUTH", "TOKENAUTH");

    @{ $self->{"ALLOWED_ANSWER"} } = ("LEVEL", "HOSTS", "USERS", "UNIQUE", "PASSWORD");
    @{ $self->{"ALL_ANSWER"} } = (@{ $self->{"ALLOWED_ANSWER"} }, "TYPE");

    @{ $self->{"ALLOWED_DATA"} } = ("LEVEL", "HOSTS", "USERS", "PASSWORD");
    @{ $self->{"ALL_DATA"} } = (@{ $self->{"ALLOWED_DATA"} }, "TYPE");

    @{ $self->{"ALLOWED_SOURCE"} } = ("LEVEL", "HOSTS", "USERS", "PASSWORD");
    @{ $self->{"ALL_SOURCE"} } = (@{ $self->{"ALLOWED_SOURCE"} }, "TYPE");

    @{ $self->{"ALLOWED_FLUSH"} } = ("LEVEL", "HOSTS", "USERS", "PASSWORD");
    @{ $self->{"ALL_FLUSH"} } = (@{ $self->{"ALLOWED_FLUSH"} }, "TYPE");

    @{ $self->{"ALLOWED_DEBUG"} } = ("LEVEL", "HOSTS", "USERS", "PASSWORD");
    @{ $self->{"ALL_DEBUG"} } = (@{ $self->{"ALLOWED_DEBUG"} }, "TYPE");

    @{ $self->{"ALLOWED_DBIAUTH"} } = ("DBIDSN",
                                       "DBIUSER",
                                       "DBIPASS",
                                       "ENCRYPTED",
                                       "RESP-TABLE",
                                       "RESP-NAMES-COL",
                                       "RESP-PWD-COL",
                                       "USER-TABLE",
                                       "USER-NAMES-COL",
                                       "USER-PWD-COL");
    @{ $self->{"ALL_DBIAUTH"} } = (@{ $self->{"ALLOWED_DBIAUTH"} }, "TYPE");

    @{ $self->{"ALLOWED_FILEAUTH"} } = ("USERFILE", "RESPFILE");
    @{ $self->{"ALL_FILEAUTH"} } = (@{ $self->{"ALLOWED_FILEAUTH"} }, "TYPE");

    @{ $self->{"ALLOWED_TOKENAUTH"} } = ("USERFILE",
                                         "RESPFILE",
                                         "ARG",
                                         "APPEND",
                                         "DBIDSN",
                                         "DBIUSER",
                                         "DBIPASS",
                                         "USER-TABLE",
                                         "USERCOL",
                                         "RESP-TABLE",
                                         "RESPCOL");
    @{ $self->{"ALL_TOKENAUTH"} } = (@{ $self->{"ALLOWED_TOKENAUTH"} }, "TYPE");

    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;

    $self->{"SECURITY_TYPE"}      = "SECURITY";
    $self->{"SECURITY_REQAUTH"}   = "no";
    $self->{"SECURITY_AUTH_TYPE"} = "none";

    $self->{"ANSWER_TYPE"}     = "ANSWER";
    $self->{"ANSWER_LEVEL"}    = "open";
    $self->{"ANSWER_HOSTS"}    = "";
    $self->{"ANSWER_USERS"}    = "";
    $self->{"ANSWER_UNIQUE"}   = "";
    $self->{"ANSWER_PASSWORD"} = "";

    $self->{"DATA_TYPE"}     = "DATA";
    $self->{"DATA_LEVEL"}    = "closed";
    $self->{"DATA_HOSTS"}    = "";
    $self->{"DATA_USERS"}    = "";
    $self->{"DATA_PASSWORD"} = "";

    $self->{"SOURCE_TYPE"}     = "SOURCE";
    $self->{"SOURCE_LEVEL"}    = "closed";
    $self->{"SOURCE_HOSTS"}    = "";
    $self->{"SOURCE_USERS"}    = "";
    $self->{"SOURCE_PASSWORD"} = "";

    $self->{"FLUSH_TYPE"}     = "FLUSH";
    $self->{"FLUSH_LEVEL"}    = "closed";
    $self->{"FLUSH_HOSTS"}    = "";
    $self->{"FLUSH_USERS"}    = "";
    $self->{"FLUSH_PASSWORD"} = "";

    $self->{"DEBUG_TYPE"}     = "DEBUG";
    $self->{"DEBUG_LEVEL"}    = "closed";
    $self->{"DEBUG_HOSTS"}    = "";
    $self->{"DEBUG_USERS"}    = "";
    $self->{"DEBUG_PASSWORD"} = "";

    $self->{"DBIAUTH_TYPE"}           = "DBIAUTH";
    $self->{"DBIAUTH_DBIDSN"}         = "";
    $self->{"DBIAUTH_DBIPASS"}        = "";
    $self->{"DBIAUTH_ENCRYPTED"}      = "yes";
    $self->{"DBIAUTH_RESP-TABLE"}     = "";
    $self->{"DBIAUTH_RESP-NAME-COL"}  = "";
    $self->{"DBIAUTH_RESP-PWD-COL"}   = "";
    $self->{"DBIAUTH_USER-TABLE"}     = "";
    $self->{"DBIAUTH_USER-NAMES-COL"} = "";
    $self->{"DBIAUTH_USER-PWD-COL"}   = "";

    $self->{"FILEAUTH_TYPE"}     = "FILEAUTH";
    $self->{"FILEAUTH_USERFILE"} = "";
    $self->{"FILEAUTH_RESPFILE"} = "";

    $self->{"TOKENAUTH_TYPE"}       = "TOKENAUTH";
    $self->{"TOKENAUTH_USERFILE"}   = "";
    $self->{"TOKENAUTH_RESPFILE"}   = "";
    $self->{"TOKENAUTH_ARG"}        = "token";
    $self->{"TOKENAUTH_APPEND"}     = "";
    $self->{"TOKENAUTH_DBIDSN"}     = "";
    $self->{"TOKENAUTH_DBIUSER"}    = "";
    $self->{"TOKENAUTH_DBIPASS"}    = "";
    $self->{"TOKENAUTH_RESP-TABLE"} = "";
    $self->{"TOKENAUTH_RESPCOL"}    = "";
    $self->{"TOKENAUTH_USER-TABLE"} = "";
    $self->{"TOKENAUTH_USERCOL"}    = "";

    1;
}

sub PlaceComponent
{

    # Note: $self is a Document object !!
    my ($crap, $self, $paramstr) = @_;

    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    # get all parameters of SECURITY tag (with default values)
    my %params = $self->GetDefaults("SECURITY");

    # the params (and information about, if it was an inline tag (<A />): $pararr{empty} )
    # which were given in the survey file:
    my @pararr = @{$paramstr};

    # for every parameter in the file
    foreach my $cell (@pararr)
    {

        # split into pairs
        my ($name, $value) = split(/=/, $cell, 2);

        # overwrite default values
        $params{$name} = $value;

        # check if given parameters are allowed
        $self->CheckParam($name, "SECURITY");
    }

    $params{FILE} = $self->{FILE};
    $params{URI}  = $self->{OPTION_URI};

    # if it was an inline tag (<SECURITY ... />)
    if ($params{empty} eq "yes")
    {
        $self->{ERROR}     = lprint("A SECURITY tag cannot be immediately terminated (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 10;
    }

    # if no errors occurred up to now
    if (!$self->{ERROR})
    {

        # find the ending SECURITY tag
        my ($endtag) = index($self->{WORK}, "</SECURITY>");

        # if ending SECURITY tag was  found
        if ($endtag > -1)
        {

            # get everything between <SECURITY ... > and </SECURITY>
            $params{"RAW"} = substr($self->{WORK}, 0, $endtag - 1);

            # remove <SECURITY ... > ... </SECURITY> from $self->{WORK}
            $self->{WORK} = substr($self->{WORK}, 0 - length($self->{WORK}) + length($params{"RAW"}) + 13);
        }
        else
        {
            $self->{ERROR}     = lprint("A SECURITY tag must have an end tag (in ") . $self->{FILE} . ")";
            $self->{ERRORCODE} = 14;
        }
    }

    # make a copy of $params{RAW}
    $params{WORK} = $params{"RAW"};

    # set default values for parameters of ANSWER, DATA, DEBUG, SOURCE, FLUSH tag (?)
    $self->{SECURITY_ANSWER_LEVEL} = "closed";
    $self->{SECURITY_DATA_LEVEL}   = "closed";
    $self->{SECURITY_DEBUG_LEVEL}  = "closed";
    $self->{SECURITY_SOURCE_LEVEL} = "closed";
    $self->{SECURITY_FLUSH_LEVEL}  = "closed";

    $self->{SECURITY_ANSWER_PASSWORD} = "";
    $self->{SECURITY_DATA_PASSWORD}   = "";
    $self->{SECURITY_DEBUG_PASSWORD}  = "";
    $self->{SECURITY_SOURCE_PASSWORD} = "";
    $self->{SECURITY_FLUSH_PASSWORD}  = "";

    $self->{SECURITY_ANSWER_USERS} = "";
    $self->{SECURITY_DATA_USERS}   = "";
    $self->{SECURITY_DEBUG_USERS}  = "";
    $self->{SECURITY_SOURCE_USERS} = "";
    $self->{SECURITY_FLUSH_USERS}  = "";

    $self->{SECURITY_ANSWER_HOSTS} = "";
    $self->{SECURITY_DATA_HOSTS}   = "";
    $self->{SECURITY_DEBUG_HOSTS}  = "";
    $self->{SECURITY_SOURCE_HOSTS} = "";
    $self->{SECURITY_FLUSH_HOSTS}  = "";

    my ($e) = 1;
    my ($s) = 1;

    my (@subtags);
    my ($currsub)   = 0;
    my ($auth_type) = "none";
    $self->{"AUTH_TYPE"} = $auth_type;

    while (($e ne -1) && (!$self->{ERROR}))
    {

        # get beginning and end of next subtag
        $s = index($params{WORK}, "<");
        $e = index($params{WORK}, ">");

        # if no subtag was found
        if ($s == -1 || $e == -1) { last; }

        # if the opening tag bracket is not at offset 0
        # calculate width of subtag (number of characters)
        if ($s) { $e = $e - $s; }

        # get the subtag
        my $ret = substr($params{WORK}, $s, $e + 1);

        # remove everything before the opening tagbracket
        if ($s) { $params{WORK} = substr($params{WORK}, 0 - length($params{WORK}) + $s); }

        # remove current subtag from $params{WORK}
        $params{WORK} = substr($params{WORK}, 0 - length($params{WORK}) + length($ret));

        # get type (ANSWER, DATA, ...) and params
        my ($type, $rest) = $self->CleanUpTag($ret);

        # if current subtag is not a subtag of SECURITY
        if (!grep(/$type/, @{ $self->{"SECURITY_SUBTAGS"} }))
        {
            $self->{ERROR}     = lprint("$type is not a SECURITY subtag");
            $self->{ERRORCODE} = 99;
        }
        else
        {

            # parse the subtag
            if ($type eq "ANSWER")
            {
                $currsub++;
                Survey::Component::Security->PlaceAnswerTag($self, $rest, $tn, $currsub);
            }
            elsif ($type eq "DATA")
            {
                $currsub++;
                Survey::Component::Security->PlaceDataTag($self, $rest, $tn, $currsub);
            }
            elsif ($type eq "DEBUG")
            {
                $currsub++;
                Survey::Component::Security->PlaceDebugTag($self, $rest, $tn, $currsub);
            }
            elsif ($type eq "FLUSH")
            {
                $currsub++;
                Survey::Component::Security->PlaceFlushTag($self, $rest, $tn, $currsub);
            }
            elsif ($type eq "SOURCE")
            {
                $currsub++;
                Survey::Component::Security->PlaceSourceTag($self, $rest, $tn, $currsub);
            }
            elsif ($type eq "DBIAUTH")
            {
                $currsub++;
                $auth_type = "dbi_auth";
                $self->{"AUTH_TYPE"} = $auth_type;
                Survey::Component::Security->PlaceDBIAuthTag($self, $rest, $tn, $currsub);
            }
            elsif ($type eq "FILEAUTH")
            {
                $currsub++;
                $auth_type = "file_auth";
                $self->{"AUTH_TYPE"} = $auth_type;
                Survey::Component::Security->PlaceFileAuthTag($self, $rest, $tn, $currsub);
            }
            elsif ($type eq "TOKENAUTH")
            {
                $currsub++;
                $auth_type = "token_auth";
                $self->{"AUTH_TYPE"} = $auth_type;
                Survey::Component::Security->PlaceTokenAuthTag($self, $rest, $tn, $currsub);
            }
        }
    }

    # number of subtags
    $params{"SUBTAGS"} = $currsub;

    # if there were no (valid) subtags
    if (!$currsub && !$self->{ERROR})
    {
        $self->{ERROR}     = lprint("A SECURITY tag must contain subtags.");
        $self->{ERRORCODE} = 99;
    }

    $self->SetOption("REQAUTH",   $params{"REQAUTH"});
    $self->SetOption("AUTH_TYPE", $auth_type);

    my ($ra) = $params{REQAUTH};
    if (!grep(/^$ra$/, ("no", "yes")))
    {
        $self->{ERROR}     = "'$ra' is not a valid value for REQAUTH";
        $self->{ERRORCODE} = 99;
        return;
    }

    # save the params (?)
    $self->PlaceParams("SECURITY", $tn, %params);
    $self->{NUMTAGS}++;

    1;
}

sub PrintComponent
{
    my ($self) = shift;
    return "";
}

sub CheckLevel
{
    my ($crap, $self, $level) = @_;

    my ($out) = 0;

    if ($level eq "closed")     { $out = 1; }
    if ($level eq "open")       { $out = 2; }
    if ($level eq "respondent") { $out = 3; }
    if ($level eq "user")       { $out = 4; }
    if ($level eq "any")        { $out = 5; }

    if (!$out)
    {
        $self->{ERROR}     = lprint("\'$level\' is not a security level.");
        $self->{ERRORCODE} = 99;
    }

    return $out;
}

sub PlaceDBIAuthTag
{
    my ($crap, $self, $paramstr, $tn, $nr) = @_;

    my ($thisparam) = $tn . "DBIAUTH" . $nr . "_";

    my (%params, $cell, $name, $value, @pararr, $e);

    %params = $self->GetDefaults("DBIAUTH");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell, 2);
        $params{$name} = $value;
        $self->CheckParam($name, "DBIAUTH");
    }

    if ($params{empty} eq "no")
    {
        $self->{ERROR} = lprint("A DBIAUTH tag must be immediately terminated");
        $self->{ERRORCODE} = 21;    #to be updated... I've putted 21 randomly
    }

    foreach $e (@{ $self->{"ALL_DBIAUTH"} })
    {
        $self->{ $thisparam . $e } = $params{$e};
    }

    if (!$self->{ERROR})
    {
        $self->{"SECURITY_DBIAUTH_DBIDSN"}         = $params{"DBIDSN"};
        $self->{"SECURITY_DBIAUTH_DBIUSER"}        = $params{"DBIUSER"};
        $self->{"SECURITY_DBIAUTH_DBIPASS"}        = $params{"DBIPASS"};
        $self->{"SECURITY_DBIAUTH_ENCRYPTED"}      = $params{"ENCRYPTED"};
        $self->{"SECURITY_DBIAUTH_RESP-TABLE"}     = $params{"RESP-TABLE"};
        $self->{"SECURITY_DBIAUTH_RESP-NAMES-COL"} = $params{"RESP-NAMES-COL"};
        $self->{"SECURITY_DBIAUTH_RESP-PWD-COL"}   = $params{"RESP-PWD-COL"};
        $self->{"SECURITY_DBIAUTH_USER-TABLE"}     = $params{"USER-TABLE"};
        $self->{"SECURITY_DBIAUTH_USER-NAMES-COL"} = $params{"USER-NAMES-COL"};
        $self->{"SECURITY_DBIAUTH_USER-PWD-COL"}   = $params{"USER-PWD-COL"};
    }

    my @missingParams;

    #checking for general required parameters

    if (!$params{"DBIDSN"}) { push(@missingParams, "DBIDSN"); }

    if (!$params{"DBIUSER"}) { push(@missingParams, "DBIUSER"); }

    #  if (!$params{"DBIPASS"})
    #  {push(@missingParams,"DBIPASS");}

    # if neither DBI-RESP-TABLE nor DIB-USER-TABLE are available
    if (!$params{"RESP-TABLE"} && !$params{"USER-TABLE"})
    {
        push(@missingParams, "RESP-TABLE and/or USER-TABLE");
    }

    # if RESP-TABLE is given, also RESP-NAMES-COL
    # and RESP-PWD-COL are required
    if ($params{"RESP-TABLE"})
    {
        if (!$params{"RESP-NAMES-COL"}) { push(@missingParams, "RESP-NAMES-COL"); }

        if (!$params{"RESP-PWD-COL"}) { push(@missingParams, "RESP-PWD-COL"); }
    }

    # if USER-TABLE is given, also USER-NAMES-COL
    # and USER-PWD-COL are required
    if ($params{"USER-TABLE"})
    {
        if (!$params{"USER-NAMES-COL"}) { push(@missingParams, "USER-NAMES-COL"); }

        if (!$params{"USER-PWD-COL"}) { push(@missingParams, "USER-PWD-COL"); }
    }

    if (scalar(@missingParams))
    {
        $self->{ERROR} = "following parameter(s) of DBIAUTH tag is (are) missing:<br>\n-&gt; "
          . join("<br />\n-&gt; ", @missingParams);
        $self->{ERRCODE} = 99;
    }

}

sub PlaceFileAuthTag
{
    my ($crap, $self, $paramstr, $tn, $nr) = @_;

    my ($thisparam) = $tn . "FILEAUTH" . $nr . "_";

    my (%params, $cell, $name, $value, @pararr, $e);

    %params = $self->GetDefaults("FILEAUTH");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "FILEAUTH");
    }

    if ($params{empty} eq "no")
    {
        $self->{ERROR} = lprint("A FILEAUTH tag must be immediately terminated");
        $self->{ERRORCODE} = 21;    #to be updated... I've putted 21 randomly
    }

    foreach $e (@{ $self->{"ALL_FILEAUTH"} })
    {
        $self->{ $thisparam . $e } = $params{$e};
    }

    if (!$self->{ERROR})
    {
        $self->{SECURITY_FILEAUTH_RESPFILE} = $params{"RESPFILE"};
        $self->{SECURITY_FILEAUTH_USERFILE} = $params{"USERFILE"};
    }

    if (!$params{RESPFILE} && !$params{USERFILE})
    {
        $self->{ERROR}     = "neither RESPFILE nor USERFILE were given in a FILEAUTH tag";
        $self->{ERRORCODE} = 99;
    }

    $self->SetOption("USERFILE", $params{"USERFILE"});
    $self->SetOption("RESPFILE", $params{"RESPFILE"});

    # check if RESPFILE is existing
    if ($params{"RESPFILE"} && (!-r $params{"RESPFILE"}) && !$self->{ERROR})
    {
        $self->{ERROR}     = lprint("RESPFILE") . " " . $params{"RESPFILE"} . " " . lprint("does not exist");
        $self->{ERRORCODE} = 99;
    }

    # check if USERFILE is existing
    if ($params{"USERFILE"} && (!-r $params{"USERFILE"}) && !$self->{ERROR})
    {
        $self->{ERROR}     = lprint("USERFILE") . " " . $params{"USERFILE"} . " " . lprint("does not exist");
        $self->{ERRORCODE} = 99;
    }

}

sub PlaceTokenAuthTag
{
    my ($crap, $self, $paramstr, $tn, $nr) = @_;

    my ($thisparam) = $tn . "TOKENAUTH" . $nr . "_";

    my (%params, $cell, $name, $value, @pararr, $e);

    %params = $self->GetDefaults("TOKENAUTH");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell, 2);
        $params{$name} = $value;
        $self->CheckParam($name, "TOKENAUTH");
    }

    if ($params{empty} eq "no")
    {
        $self->{ERROR} = lprint("A TOKENAUTH tag must be immediately terminated");
        $self->{ERRORCODE} = 99;    #to be updated... I've putted 21 randomly
    }

    foreach $e (@{ $self->{"ALL_TOKENAUTH"} })
    {
        $self->{ $thisparam . $e } = $params{$e};
    }

    if (!$self->{ERROR})
    {
        $self->{SECURITY_TOKENAUTH_RESPFILE} = $params{"RESPFILE"};
        $self->{SECURITY_TOKENAUTH_USERFILE} = $params{"USERFILE"};
    }

    if (!$params{"ARG"})
    {
        $self->{ERROR}   = "no TOKENARG name has been specified for a TOKENAUTH tag.";
        $self->{ERRCODE} = 99;
    }

    if (!$params{RESPFILE} && !$params{USERFILE} && !$params{USERCOL} && !$params{RESPCOL})
    {
        $self->{ERROR}     = "neither RESP(FILE|COL) nor USER(FILE|COL) were given in a TOKENAUTH tag";
        $self->{ERRORCODE} = 99;
    }

    $self->SetOption("USERFILE", $params{"USERFILE"});
    $self->SetOption("RESPFILE", $params{"RESPFILE"});
    $self->SetOption("TOKENARG", $params{"ARG"});

    # check if RESPFILE is existing
    if ($params{"RESPFILE"} && (!-r $params{"RESPFILE"}) && !$self->{ERROR})
    {
        $self->{ERROR}     = lprint("RESPFILE") . " " . $params{"RESPFILE"} . " " . lprint("does not exist");
        $self->{ERRORCODE} = 99;
    }

    # check if USERFILE is existing
    if ($params{"USERFILE"} && (!-r $params{"USERFILE"}) && !$self->{ERROR})
    {
        $self->{ERROR}     = lprint("USERFILE") . " " . $params{"USERFILE"} . " " . lprint("does not exist");
        $self->{ERRORCODE} = 99;
    }

    if (!$self->{ERROR})
    {
        $self->{"SECURITY_TOKENAUTH_DBIDSN"}     = $params{"DBIDSN"};
        $self->{"SECURITY_TOKENAUTH_DBIUSER"}    = $params{"DBIUSER"};
        $self->{"SECURITY_TOKENAUTH_DBIPASS"}    = $params{"DBIPASS"};
        $self->{"SECURITY_TOKENAUTH_RESP-TABLE"} = $params{"RESP-TABLE"};
        $self->{"SECURITY_TOKENAUTH_RESPCOL"}    = $params{"RESPCOL"};
        $self->{"SECURITY_TOKENAUTH_USER-TABLE"} = $params{"USER-TABLE"};
        $self->{"SECURITY_TOKENAUTH_USERCOL"}    = $params{"USERCOL"};
    }
}

sub PlaceAnswerTag
{
    my ($crap, $self, $paramstr, $tn, $nr) = @_;

    my ($thisparam) = $tn . "ANSWER" . $nr . "_";

    my (%params, $cell, $name, $value, @pararr, $e);

    %params = $self->GetDefaults("ANSWER");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;

        # print "SECURITY / ANSWER : $name $value\n";
        $self->CheckParam($name, "ANSWER");
    }

    if ($params{empty} eq "no")
    {
        $self->{ERROR}     = lprint("An ANSWER tag must be immediately terminated");
        $self->{ERRORCODE} = 11;
    }

    Survey::Component::Security->CheckLevel($self, $params{"LEVEL"});

    foreach $e (@{ $self->{"ALL_ANSWER"} })
    {
        $self->{ $thisparam . $e } = $params{$e};
    }

    if (!$self->{ERROR})
    {
        $self->{SECURITY_ANSWER_LEVEL}    = $params{"LEVEL"};
        $self->{SECURITY_ANSWER_HOSTS}    = $params{"HOSTS"};
        $self->{SECURITY_ANSWER_USERS}    = $params{"USERS"};
        $self->{SECURITY_ANSWER_UNIQUE}   = $params{"UNIQUE"};
        $self->{SECURITY_ANSWER_PASSWORD} = $params{"PASSWORD"};
    }
    $self->SetOption("UNIQUE", $params{"UNIQUE"});
}

sub PlaceDataTag
{
    my ($crap, $self, $paramstr, $tn, $nr) = @_;

    my ($thisparam) = $tn . "DATA" . $nr . "_";

    my (%params, $cell, $name, $value, @pararr, $e);

    %params = $self->GetDefaults("DATA");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;

        # print "SECURITY / DATA : $name $value\n";
        $self->CheckParam($name, "DATA");
    }

    if ($params{empty} eq "no")
    {
        $self->{ERROR}     = lprint("A DATA tag must be immediately terminated");
        $self->{ERRORCODE} = 11;
    }

    Survey::Component::Security->CheckLevel($self, $params{"LEVEL"});

    foreach $e (@{ $self->{"ALL_DATA"} })
    {
        $self->{ $thisparam . $e } = $params{$e};
    }

    if (!$self->{ERROR})
    {
        $self->{SECURITY_DATA_LEVEL}    = $params{"LEVEL"};
        $self->{SECURITY_DATA_HOSTS}    = $params{"HOSTS"};
        $self->{SECURITY_DATA_USERS}    = $params{"USERS"};
        $self->{SECURITY_DATA_PASSWORD} = $params{"PASSWORD"};
    }
}

sub PlaceSourceTag
{
    my ($crap, $self, $paramstr, $tn, $nr) = @_;

    my ($thisparam) = $tn . "SOURCE" . $nr . "_";

    my (%params, $cell, $name, $value, @pararr, $e);

    %params = $self->GetDefaults("SOURCE");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;

        # print "SECURITY / SOURCE : $name $value\n";
        $self->CheckParam($name, "SOURCE");
    }

    if ($params{empty} eq "no")
    {
        $self->{ERROR}     = lprint("A SOURCE tag must be immediately terminated");
        $self->{ERRORCODE} = 11;
    }

    Survey::Component::Security->CheckLevel($self, $params{"LEVEL"});

    foreach $e (@{ $self->{"ALL_SOURCE"} })
    {
        $self->{ $thisparam . $e } = $params{$e};
    }

    if (!$self->{ERROR})
    {
        $self->{SECURITY_SOURCE_LEVEL}    = $params{"LEVEL"};
        $self->{SECURITY_SOURCE_HOSTS}    = $params{"HOSTS"};
        $self->{SECURITY_SOURCE_USERS}    = $params{"USERS"};
        $self->{SECURITY_SOURCE_PASSWORD} = $params{"PASSWORD"};
    }
}

sub PlaceDebugTag
{
    my ($crap, $self, $paramstr, $tn, $nr) = @_;

    my ($thisparam) = $tn . "DEBUG" . $nr . "_";

    my (%params, $cell, $name, $value, @pararr, $e);

    %params = $self->GetDefaults("DEBUG");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;

        # print "SECURITY / DEBUG : $name $value\n";
        $self->CheckParam($name, "DEBUG");
    }

    if ($params{empty} eq "no")
    {
        $self->{ERROR}     = lprint("A DEBUG tag must be immediately terminated");
        $self->{ERRORCODE} = 11;
    }

    Survey::Component::Security->CheckLevel($self, $params{"LEVEL"});

    foreach $e (@{ $self->{"ALL_DEBUG"} })
    {
        $self->{ $thisparam . $e } = $params{$e};
    }

    if (!$self->{ERROR})
    {
        $self->{SECURITY_DEBUG_LEVEL}    = $params{"LEVEL"};
        $self->{SECURITY_DEBUG_HOSTS}    = $params{"HOSTS"};
        $self->{SECURITY_DEBUG_USERS}    = $params{"USERS"};
        $self->{SECURITY_DEBUG_PASSWORD} = $params{"PASSWORD"};
    }
}

sub PlaceFlushTag
{
    my ($crap, $self, $paramstr, $tn, $nr) = @_;

    my ($thisparam) = $tn . "FLUSH" . $nr . "_";

    my (%params, $cell, $name, $value, @pararr, $e);

    %params = $self->GetDefaults("FLUSH");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;

        # print "SECURITY / FLUSH : $name $value\n";
        $self->CheckParam($name, "FLUSH");
    }

    if ($params{empty} eq "no")
    {
        $self->{ERROR}     = lprint("A FLUSH tag must be immediately terminated");
        $self->{ERRORCODE} = 11;
    }

    Survey::Component::Security->CheckLevel($self, $params{"LEVEL"});

    foreach $e (@{ $self->{"ALL_FLUSH"} })
    {
        $self->{ $thisparam . $e } = $params{$e};
    }

    if (!$self->{ERROR})
    {
        $self->{SECURITY_FLUSH_LEVEL}    = $params{"LEVEL"};
        $self->{SECURITY_FLUSH_HOSTS}    = $params{"HOSTS"};
        $self->{SECURITY_FLUSH_USERS}    = $params{"USERS"};
        $self->{SECURITY_FLUSH_PASSWORD} = $params{"PASSWORD"};
    }
}

sub MakeXML
{
    my ($s, $doc, $tagno) = @_;

    my ($out) = $s->MakeTagOpener($doc, $tagno);

    my (@elems) = @{ $doc->{"SECURITY_SUBTAGS"} };
    my ($elm, $t);

    foreach $elm (@elems)
    {
        $t = "  <$elm ";

        my (@params) = @{ $doc->{"ALLOWED_$elm"} };
        my ($p);

        foreach $p (@params)
        {
            my ($v) = $doc->{ "SECURITY_" . $elm . "_" . $p };
            if (($v ne $doc->{ $elm . "_" . $p }) || ($p eq "LEVEL"))
            {
                $t .= "$p=\"$v\" ";
            }
        }

        $t   .= "/>\n";
        $out .= $t;
    }

    $out .= $s->MakeTagCloser($doc, $tagno);

    return $out;
}

1;
