#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::SubmitError;
use strict;

@Survey::Component::SubmitError::ISA = qw(Survey::Component::Component);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub FillParams
{
    my ($crap, $self) = @_;

    @{ $self->{"ALLOWED_SUBMITERROR"} } = ("CODE", "ESCAPED");
    @{ $self->{"ALL_SUBMITERROR"} } = (@{ $self->{"ALLOWED_SUBMITERROR"} }, "RAW", "TYPE");

    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;

    $self->{"SUBMITERROR_TYPE"}    = "SUBMITERROR";
    $self->{"SUBMITERROR_CODE"}    = "";
    $self->{"SUBMITERROR_ESCAPED"} = "yes";

    1;
}

sub PlaceComponent
{
    my ($crap, $self, $paramstr) = @_;

    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    my (%params, $cell, $name, $value, @pararr);

    %params = $self->GetDefaults("SUBMITERROR");

    @pararr = @{$paramstr};

    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "SUBMITERROR");
    }

    if ($params{empty} eq "yes")
    {
        $self->{ERROR}     = lprint("A SUBMITERROR tag cannot be immediately terminated (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 10;
    }

    if (!$self->{ERROR})
    {
        my ($endtag) = index($self->{WORK}, "</SUBMITERROR>");
        if ($endtag > -1)
        {
            $params{"RAW"} = substr($self->{WORK}, 0, $endtag - 1);
            $self->{WORK} = substr($self->{WORK}, 0 - length($self->{WORK}) + length($params{"RAW"}) + 15);
        }
        else
        {
            $self->{ERROR}     = lprint("A SUBMITERROR tag must have an end tag");
            $self->{ERRORCODE} = 14;
        }
    }

    $self->{ "SUBMITERROR_" . $params{CODE} } = $self->{NUMTAGS};

    $self->PlaceParams("SUBMITERROR", $tn, %params);
    $self->{NUMTAGS}++;

    1;
}

sub PostParsePerl
{
    my ($crap, $submit, $tagno) = @_;

    my ($self) = $submit->{DOCUMENT};

    my ($start) = -1;
    my ($end)   = 0;

    my ($ml) = $self->{MAXLOOP};

    my ($raw) = $self->GetTagParam($tagno, "RAW");

    $submit->{COOKIES} = {};

    while (!$self->{ERROR} && ($start = index($raw, "{\|")) >= 0 && ($ml > 0))
    {
        $ml--;
        my ($contents) = "";
        $end = index($raw, "\|}");
        if ($end > $start)
        {
            my ($substr) = substr($raw, $start + 2, $end - $start - 2);
            $substr =~ s/\x0d//g;
            my (@ops) = split(/\x0a/, $substr);
            my ($cell, $reval);
            $reval = "\$output = \"\";\n";

            foreach $cell (@ops)
            {

                #$cell =~ s/^[\x09\ ]+//g;
                $cell =~ s/^print\ /\$output\ \.\=\ /g;
                if ($cell)
                {
                    $reval .= $cell . "\n";
                }
            }

            my ($output) = "";

            $reval .= "\$output;\n";

            my ($compartment) = new Safe("Tempo");

            $Tempo::self     = $submit;
            $Tempo::document = $self;
            $Tempo::argument = $self->{ARGUMENT};
            $Tempo::session  = $self->{SESSION};

            $compartment->permit(qw(:browse));
            $output = $compartment->reval($reval);
            if ($@)
            {
                $submit->{ERROR}     = lprint("Security exception: " . $@);
                $submit->{ERRORCODE} = 99;
                $self->{ERROR}       = lprint("Security exception: " . $@);
                $self->{ERRORCODE}   = 99;
            }

            my ($contents) = $output;
            $contents = substr($raw, 0, $start) . $contents;
            $contents .= substr($raw, $end + 2);
            $raw = $contents;

        }
        else
        {
            $submit->{ERROR}     = lprint("Found \"{\|\" without \"\|}\"");
            $submit->{ERRORCODE} = 99;
            $self->{ERROR}       = lprint("Found \"{\|\" without \"\|}\"");
            $self->{ERRORCODE}   = 99;
        }
    }

    my (@c) = keys(%{ $submit->{COOKIES} });
    my ($co);
    foreach $co (@c)
    {
        my ($cookie) =
          new CGI::Cookie(-name => $co, -value => $submit->{COOKIES}->{$co}, -path => '/', -expires => '+60M');
        $self->{HANDLER}->headers_out->{'Set-Cookie'} = $cookie;
    }
    $self->SetTagParam($tagno, "RAW", $raw);

    1;
}

sub PrintComponent
{
    my ($crap, $self, $tagno) = @_;
    my ($doc) = $self->{DOCUMENT};
    my ($out) = "";

    # During display phase, this shouldn't be visible at all. See
    # PostParsePerl and SubmitMessage instead

    #$out .=  "<!--\n\n";
    #$out .=  "SUBMIT tag:\n\n";
    #$out .=  $doc->GetTagParam($tagno,"RAW") . "\n";
    #$out .=  "-->\n\n";

    return $out;
}

sub MakeXML
{
    my ($s, $doc, $tagno) = @_;

    my ($out) = $s->MakeTagOpener($doc, $tagno);

    $out .= $doc->GetTagParam($tagno, "RAW");
    $out .= "\n";
    $out .= $s->MakeTagCloser($doc, $tagno);

    return $out;
}

1;

