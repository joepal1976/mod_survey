#!/usr/bin/perl

open(PIPE, "ls *.pm |") || die;

while ($f = <PIPE>)
{
    chop($f);
    open(FILE, $f) || die;
    $before = 1;
    $during = 0;

    while ($inlin = <FILE>)
    {
        if ($before)
        {
            if ($inlin =~ m/^sub FillDefaults/)
            {
                $inlin  = <FILE>;
                $before = 0;
                $during = 1;
            }
        }

        if ($during)
        {
            if ($inlin =~ m/self-/)
            {
                $ut = $inlin;
                $ut =~ s/^ +\$self-\>\{\"//;
                $ut =~ s/\"\}[\ \t]+\=/\t/;
                print $ut;
            }
            if ($inlin =~ m/^\}/)
            {
                $inlin  = <FILE>;
                $before = 0;
                $during = 0;
            }
        }
    }
    close(FILE);
}

