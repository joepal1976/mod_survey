#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::Route;
use strict;

@Survey::Component::Route::ISA = qw(Survey::Component::Component);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub FillParams
{
    my ($crap, $self) = @_;

    @{ $self->{"ALLOWED_ROUTE"} } = ("CONTINUE");
    @{ $self->{"ALL_ROUTE"} } = (@{ $self->{"ALLOWED_ROUTE"} }, "TYPE");

    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;

    $self->{"ROUTE_TYPE"}     = "ROUTE";
    $self->{"ROUTE_CONTINUE"} = "";

    1;
}

sub PlaceComponent
{
    my ($crap, $self, $paramstr) = @_;

    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    my (%params, $cell, $name, $value, @pararr);

    %params = $self->GetDefaults("ROUTE");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "ROUTE");
    }

    if ($params{empty} eq "no")
    {
        $self->{ERROR}     = lprint("A ROUTE tag must be terminated immediately (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 11;
    }

    if (!$params{"CONTINUE"})
    {
        $self->{ERROR}     = lprint("CONTINUE is a required parameter in a ROUTE tag.");
        $self->{ERRORCODE} = 99;
    }

    $self->SetOption("ROUTERTAG",  $self->{NUMTAGS});
    $self->SetOption("ROUTERTYPE", "PLAIN");

    $self->PlaceParams("ROUTE", $tn, %params);
    $self->{NUMTAGS}++;

    $self->SetOption("MULTIPAGE", 1);

    1;
}

sub PrintComponent
{
    my ($self) = shift;
    return "";
}

sub DoRouting
{
    my ($crap, $sub, $doc, $arg, $ses, $tagno) = @_;

    $sub->{IMPLICITCONTINUE} = 1;

    $doc->SetOption("REDIRECT", "yes");
    my ($continue) = $doc->GetTagParam($tagno, "CONTINUE");
    $doc->SetOption("CONTINUE", $continue);

    1;
}

1;

