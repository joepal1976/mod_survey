#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::Lickert;
use strict;

@Survey::Component::Lickert::ISA = qw(Survey::Component::Component);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;
use Survey::Template;

use CGI;

sub FillParams
{
    my ($crap, $self) = @_;

    @{ $self->{"ALLOWED_LICKERT"} } = ("NAME",
                                       "STEPS",
                                       "LEFTTAG",
                                       "RIGHTTAG",
                                       "REVERSED",
                                       "MUSTANSWER",
                                       "CAPTION",
                                       "LEFTCAPTSTYLE",
                                       "RIGHTCAPTSTYLE",
                                       "CAPTSTYLE",
                                       "STYLE",
                                       "ILLEGALVAL",
                                       "ACCESS");
    @{ $self->{"ALL_LICKERT"} } = (@{ $self->{"ALLOWED_LICKERT"} }, "TYPE", "FILE");

    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;

    $self->{"LICKERT_TYPE"}           = "LICKERT";
    $self->{"LICKERT_NAME"}           = "";
    $self->{"LICKERT_STEPS"}          = "5";
    $self->{"LICKERT_LEFTTAG"}        = "True";
    $self->{"LICKERT_RIGHTTAG"}       = "False";
    $self->{"LICKERT_REVERSED"}       = "no";
    $self->{"LICKERT_MUSTANSWER"}     = "no";
    $self->{"LICKERT_CAPTION"}        = "";
    $self->{"LICKERT_CAPTSTYLE"}      = "";          #"LICKERTcap";
    $self->{"LICKERT_LEFTCAPTSTYLE"}  = "";          #"LICKERTleftcap";
    $self->{"LICKERT_RIGHTCAPTSTYLE"} = "";          #"LICKERTrightcap";
    $self->{"LICKERT_STYLE"}          = "";          #"LICKERTelm";
    $self->{"LICKERT_ILLEGALVAL"}     = "-1";
    $self->{"LICKERT_ACCESS"}         = "no";
    1;
}

sub PlaceComponent
{
    my ($crap, $self, $paramstr) = @_;

    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    my (%params, $cell, $name, $value, @pararr);

    %params = $self->GetDefaults("LICKERT");

    @pararr = @{$paramstr};

    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "LICKERT");
    }

    #bugant: fix for session loading values
    $params{FILE} = $self->{FILE};

    if ($params{empty} eq "no")
    {
        $self->{ERROR}     = lprint("A LICKERT tag must be terminated immediately (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 11;
    }

    if ((!$self->{ERROR}) && (!$params{NAME}))
    {
        $self->{ERROR}     = lprint("A LICKERT tag must have a name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 12;
    }

    if ((!$self->{ERROR}) && (length($params{NAME}) > 8))
    {
        $self->{ERROR}     = lprint("A LICKERT tag name can only be 8 characters wide (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 21;
    }

    if ((!$self->{ERROR}) && (($params{ACCESS}) && (($params{ACCESS} != "no") && ($params{ACCESS} != "yes"))))
    {
        $self->{ERROR} = lprint("A LICKERT tag ACCESS property could be yes|no (in ") . $self->{FILE} . ")";
        $self->{ERROR} = 778;
    }

    if (!$self->{ERROR}) { $self->CheckName($params{NAME}, "LICKERT"); }

    $name = "TAGNAME_" . $params{"NAME"};
    $name = "\U$name";

    if ((!$self->{ERROR}) && (defined($self->{$name})))
    {
        $self->{ERROR}     = lprint("A LICKERT tag must have unique name (") . $name . ")";
        $self->{ERRORCODE} = 20;
    }
    else
    {
        $self->{$name} = $self->{NUMTAGS};
    }

    $self->PlaceParams("LICKERT", $tn, %params);

    $self->{NUMTAGS}++;

    1;
}

sub PrintComponent
{
    my ($crap, $self, $tagno) = @_;
    my ($doc) = $self->{DOCUMENT};
    my $ses = $doc->{SESSION};

    $doc->SetVisited($doc->GetTagParam($tagno, "NAME"));

    my $tmpl = new Survey::Template($ENV{_SURVEY_HOME} . "/templates/default/Lickert.tmpl");

    my $thistagname = $doc->GetTagParam($tagno, "NAME");

    my $valueErrorDescription = $ses->getValue("VALUE_ERROR_DESCRIPTION_$thistagname");

    if ($valueErrorDescription)
    {
        $tmpl->setVar("errorDescription", $valueErrorDescription);

        $ses->setValue("VALUE_ERROR_$thistagname",             "");
        $ses->setValue("VALUE_ERROR_DESCRIPTION_$thistagname", "");
    }

    $tmpl->setVar("captStyle", $doc->GetTagParam($tagno, "CAPTSTYLE"));
    $tmpl->setVar("caption",   $doc->GetTagParam($tagno, "CAPTION"));

    $tmpl->setVar("style", $doc->GetTagParam($tagno, "STYLE"));

    $tmpl->setVar("leftCaptStyle", $doc->GetTagParam($tagno, "LEFTCAPTSTYLE"));
    $tmpl->setVar("leftTag",       $doc->GetTagParam($tagno, "LEFTTAG"));

    $tmpl->setVar("rightCaptStyle", $doc->GetTagParam($tagno, "RIGHTCAPTSTYLE"));
    $tmpl->setVar("rightTag",       $doc->GetTagParam($tagno, "RIGHTTAG"));

    $tmpl->setVar("name",     $thistagname);

	my($mustanswer) = lprint("The question with caption") 
              . " \""
              . $doc->GetTagParam($tagno, "CAPTION")
              . "\" "
              . lprint("must be answered.");

	if($doc->GetTagParam($tagno, "MUSTANSWER") eq "yes")
	{
		$tmpl->setVar("mustanswer"," mustanswer");
		$tmpl->setVar("mustanswermsg",$mustanswer);		
	}
	else
	{
		$tmpl->setVar("mustanswer","");
		$tmpl->setVar("mustanswermsg","");
	}

    #bugant persist ;)
    my ($chkpersist) = ($doc->{SESSION}->getValue("SUBMITTED_$thistagname"));

    #bugant persisted ;)

    my $numSelected;

    if ($self->{RETRIEVE})
    {
        my $tablenum = $tagno + 1;
        my $sql = "SELECT " . $doc->GetTagParam($tagno, "NAME") . " FROM " . $doc->GetOption("DBITABLE");
        $sql .= " WHERE mail=\'";

        if ($doc->GetOption("REQAUTH") eq "soap")
        {
            $sql .= $doc->{EMAIL};
        }
        else
        {
            $sql .= $doc->{REMOTE_USER};
        }

        $sql .= "\' AND save_partial=\'1\'";
        my $sth = $self->{DBH}->prepare($sql);
        $sth->execute;
        ($numSelected) = $sth->fetchrow_array();
    }

    $tmpl->enterLoop("LOOP");
    for (my $i = 1 ; $i <= $doc->GetTagParam($tagno, "STEPS") ; $i++)
    {
		$tmpl->setVar("tagno", "" . $i);
        $tmpl->setVar("name", $doc->GetTagParam($tagno, "NAME"));

		if($doc->GetTagParam($tagno, "MUSTANSWER") eq "yes")
		{
			$tmpl->setVar("mustanswer"," mustanswer");		
		}
		else
		{
			$tmpl->setVar("mustanswer","");			
		}
		
        if ($doc->GetTagParam($tagno, "REVERSED") eq "yes")
        {
            my $count = int($doc->GetTagParam($tagno, "STEPS")) + 1 - $i;
            $tmpl->setVar("value", $count);

		
	

		
            if ($doc->GetTagParam($tagno, "ACCESS") eq "yes")
            {

                #$tmpl->setVar("accessKeyAvailable", 1);
                $tmpl->setVar("accessKey", $count);
            }

            #bugant persist ;)
            if ($count == $chkpersist) { $tmpl->setVar("checked", 1); }

            #bugant persisted ;)
        }
        else
        {
            $tmpl->setVar("value", $i);

            if ($doc->GetTagParam($tagno, "ACCESS") eq "yes")
            {

                # $tmpl->setVar("accessKeyAvailable", 1);
                $tmpl->setVar("accessKey", $i);
            }

            #bugant persist ;)
            if ($i == $chkpersist)
            {
                $tmpl->setVar("checked", 1);
            }

            #bugant persisted ;)
        }

        $tmpl->nextLoop();
    }
    $tmpl->exitLoop();

    return $tmpl->get();
}

sub NumberOfValues
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 1;
}

sub GetValueNumerical
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 1;
}

sub GetCheckValue
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;

    my ($value) = {};
    my ($name) = $doc->GetTagParam($tagno, "NAME");

    my ($visited) = $doc->CheckVisited($name);

    if ($visited)
    {
        $value->{VALUE} = $arg->ArgByName($name) || "";
    }
    else
    {
        $value->{VALUE} = $doc->GetOption("NOTDISPLAYEDVAL");
    }

    if ($value->{VALUE} eq "")
    {
        if (($doc->GetTagParam($tagno, "MUSTANSWER") eq "yes") && (!$sub->{SAVE}))
        {
            $sub->{ERROR} =
                lprint("The question with caption") 
              . " <i>\""
              . $doc->GetTagParam($tagno, "CAPTION")
              . "\"</i> "
              . lprint("must be answered.");
            $sub->{ERRORCODE} = 2;
        }
        else
        {
            $value->{VALUE} = $doc->GetTagParam($tagno, "ILLEGALVAL");
        }
    }

    $value->{NUMERICAL} = 1;
    $value->{NAME}      = $name;
    $value->{CAPTION}   = $doc->GetTagParam($tagno, "CAPTION");

    return $value;
}

1;

