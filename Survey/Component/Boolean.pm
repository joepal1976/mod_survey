#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::Boolean;
use strict;

@Survey::Component::Boolean::ISA = qw(Survey::Component::Component);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;
use Survey::Component::Component;

use CGI;

sub FillParams
{
    my ($crap, $self) = @_;

    @{ $self->{"ALLOWED_BOOLEAN"} } = ("NAME", "CHECKED", "CAPTION", "CAPTSTYLE", "STYLE");
    @{ $self->{"ALL_BOOLEAN"} } = (@{ $self->{"ALLOWED_BOOLEAN"} }, "TYPE", "FILE");

    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;
    $self->{"BOOLEAN_TYPE"}      = "BOOLEAN";
    $self->{"BOOLEAN_NAME"}      = "";
    $self->{"BOOLEAN_CHECKED"}   = "no";
    $self->{"BOOLEAN_CAPTION"}   = "";
    $self->{"BOOLEAN_CAPTSTYLE"} = "";          #"caption captionBOOLEAN"
    $self->{"BOOLEAN_STYLE"}     = "";          #"element elementBOOLEAN"
    1;
}

sub PlaceComponent
{
    my ($crap, $self, $paramstr) = @_;

    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    my (%params, $cell, $name, $value, @pararr);

    %params = $self->GetDefaults("BOOLEAN");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "BOOLEAN");
    }

    #bugant: fix for session loading values
    $params{FILE} = $self->{FILE};

    if ($params{empty} eq "no")
    {
        $self->{ERROR}     = lprint("A BOOLEAN tag must be terminated immediately (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 11;
    }

    if ((!$self->{ERROR}) && (!$params{NAME}))
    {
        $self->{ERROR}     = lprint("A BOOLEAN tag must have a name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 12;
    }

    if ((!$self->{ERROR}) && (length($params{NAME}) > 8))
    {
        $self->{ERROR}     = lprint("A BOOLEAN tag name can only be 8 characters wide (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 21;
    }

    if (!$self->{ERROR}) { $self->CheckName($params{NAME}, "BOOLEAN"); }

    $name = "TAGNAME_" . $params{"NAME"};
    $name = "\U$name";

    if (defined($self->{$name}))
    {
        $self->{ERROR}     = lprint("A BOOLEAN tag must have unique name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 20;
    }
    else
    {
        $self->{$name} = $self->{NUMTAGS};
    }

    $self->PlaceParams("BOOLEAN", $tn, %params);

    $self->{NUMTAGS}++;

    1;
}

sub PrintComponent
{
    my ($crap, $self, $tagno) = @_;
    my $doc = $self->{DOCUMENT};

    $doc->SetVisited($doc->GetTagParam($tagno, "NAME"));

    my $tmpl = new Survey::Template($ENV{_SURVEY_HOME} . "/templates/default/Boolean.tmpl");

    $tmpl->setVar("captStyle", $doc->GetTagParam($tagno, "CAPTSTYLE"));
    $tmpl->setVar("caption",   $doc->GetTagParam($tagno, "CAPTION"));
    $tmpl->setVar("style",     $doc->GetTagParam($tagno, "STYLE"));
    $tmpl->setVar("name",      $doc->GetTagParam($tagno, "NAME"));

    #bugant persist ;)
    my ($thistagname) = $doc->GetTagParam($tagno, "NAME");
    my ($chkpersist) = ($doc->{SESSION}->getValue("SUBMITTED_$thistagname"));

    if (defined($chkpersist))
    {
        $tmpl->setVar("checked", 1);
    }
    elsif ($doc->GetTagParam($tagno, "CHECKED") eq "yes") { $tmpl->setVar("checked", 1); }

    #bugant persisted

    return $tmpl->get();
}

sub NumberOfValues
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 1;
}

sub GetValueNumerical
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 1;
}

sub GetCheckValue
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;

    my ($value) = {};
    my ($name) = $doc->GetTagParam($tagno, "NAME");

    my ($visited) = $doc->CheckVisited($name);

    if ($visited)
    {
        $value->{VALUE} = $arg->ArgByName($name) || "0";
    }
    else
    {
        $value->{VALUE} = $doc->GetOption("NOTDISPLAYEDVAL");
    }

    $value->{NAME}      = $name;
    $value->{NUMERICAL} = 1;
    $value->{CAPTION}   = $doc->GetTagParam($tagno, "CAPTION");

    return $value;
}

1;

