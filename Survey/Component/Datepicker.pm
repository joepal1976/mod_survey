#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::Datepicker;
use strict;

@Survey::Component::Datepicker::ISA = qw(Survey::Component::Component);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub FillParams
{
    my ($crap, $self) = @_;

    @{ $self->{"ALLOWED_DATEPICKER"} } = ("NAME",
                                    "DEFAULT",
                                    "FORMAT",
                                    "MUSTANSWER",
                                    "CAPTION",
                                    "CAPTSTYLE",
                                    "ILLEGALVAL",
                                    "STYLE");
    @{ $self->{"ALL_DATEPICKER"} } = (@{ $self->{"ALLOWED_DATEPICKER"} }, "TYPE", "FILE");

    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;

    $self->{"DATEPICKER_TYPE"}       = "DATEPICKER";
    $self->{"DATEPICKER_NAME"}       = "";
    $self->{"DATEPICKER_FORMAT"}     = "yy-mm-dd";    
    $self->{"DATEPICKER_DEFAULT"}    = "";
    $self->{"DATEPICKER_MUSTANSWER"} = "no";
    $self->{"DATEPICKER_CAPTION"}    = "";
    $self->{"DATEPICKER_CAPTSTYLE"}  = "";       #"DATEPICKERcap";        
    $self->{"DATEPICKER_ILLEGALVAL"} = "-1";
    $self->{"DATEPICKER_STYLE"}      = "";       #"DATEPICKERelm";

    1;
}

sub PlaceComponent
{
    my ($crap, $self, $paramstr) = @_;

    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    my (%params, $cell, $name, $value, @pararr);

    %params = $self->GetDefaults("DATEPICKER");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "DATEPICKER");
    }

    #bugant: fix for session loading values
    $params{FILE} = $self->{FILE};

    if ($params{empty} eq "no")
    {
        $self->{ERROR}     = lprint("A DATEPICKER tag must be terminated immediately (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 11;
    }

    if ((!$self->{ERROR}) && (!$params{NAME}))
    {
        $self->{ERROR}     = lprint("A DATEPICKER tag must have a name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 12;
    }

    if ((!$self->{ERROR}) && (length($params{NAME}) > 8))
    {
        $self->{ERROR}     = lprint("A DATEPICKER tag name can only be 8 characters wide (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 21;
    }

    if (!$self->{ERROR}) { $self->CheckName($params{NAME}, "DATEPICKER"); }

    $name = "TAGNAME_" . $params{"NAME"};
    $name = "\U$name";

    if (defined($self->{$name}))
    {
        $self->{ERROR}     = lprint("A DATEPICKER tag must have unique name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 20;
    }
    else
    {
        $self->{$name} = $self->{NUMTAGS};
    }

    $self->PlaceParams("DATEPICKER", $tn, %params);
    $self->{NUMTAGS}++;

    1;
}

sub PrintComponent
{
    my ($crap, $self, $tagno) = @_;    # $self points to an Survey::Submit object, so $sub would be a better name !!
    my ($doc) = $self->{DOCUMENT};
    my $ses = $self->{SESSION};
    my ($i);

    $doc->SetVisited($doc->GetTagParam($tagno, "NAME"));
	
    my $tmpl = new Survey::Template($ENV{_SURVEY_HOME} . "/templates/default/Datepicker.tmpl");

    my $thistagname = $doc->GetTagParam($tagno, "NAME");

    my $valueErrorDescription = $ses->getValue("VALUE_ERROR_DESCRIPTION_$thistagname");

    if ($valueErrorDescription)
    {
        my $value = $ses->getValue("VALUE_ERROR_$thistagname");

        $tmpl->setVar("errorDescription", $valueErrorDescription);
        $tmpl->setVar("erroneusValue",    $value);

        $ses->setValue("VALUE_ERROR_$thistagname",             "");
        $ses->setValue("VALUE_ERROR_DESCRIPTION_$thistagname", "");
    }

    $tmpl->setVar("captStyle", $doc->GetTagParam($tagno, "CAPTSTYLE"));
    $tmpl->setVar("caption",   $doc->GetTagParam($tagno, "CAPTION"));
    $tmpl->setVar("style",     $doc->GetTagParam($tagno, "STYLE"));
    $tmpl->setVar("name",      $doc->GetTagParam($tagno, "NAME"));    
    $tmpl->setVar("format",    $doc->GetTagParam($tagno, "FORMAT"));

	my($lang) = Survey::Slask->GuessLang($doc);
	if($lang == "sw") 
	{
		$lang = "sv";
	}
	$tmpl->setVar("lang",     $lang);
	
    $tmpl->setVar("name",     $thistagname);

	my($mustanswer) = lprint("The question with caption") 
              . " \""
              . $doc->GetTagParam($tagno, "CAPTION")
              . "\" "
              . lprint("must be answered.");

	if($doc->GetTagParam($tagno, "MUSTANSWER") eq "yes")
	{
		$tmpl->setVar("mustanswer"," mustanswer");
		$tmpl->setVar("mustanswermsg",$mustanswer);		
	}
	else
	{
		$tmpl->setVar("mustanswer","");
		$tmpl->setVar("mustanswermsg","");
	}

    my $chkpersist = $doc->{SESSION}->getValue("SUBMITTED_" . $thistagname);

    #bugant persist
    if (   defined($chkpersist)
        && ($chkpersist ne $doc->GetOption("NOTDISPLAYEDVAL"))
        && ($chkpersist ne $doc->GetTagParam($tagno, "ILLEGALVAL")))
    {
        $tmpl->setVar("value", $chkpersist);
    }
    elsif ($doc->GetTagParam($tagno, "DEFAULT") ne "")
    {
        $tmpl->setVar("value", $doc->GetTagParam($tagno, "DEFAULT"));
    }

    #bugant persist -- end --

    return $tmpl->get();
}

sub NumberOfValues
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 1;
}

sub GetValueNumerical
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;

    if ($doc->GetTagParam($tagno, "NUMERICAL") eq "no")
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

sub GetCheckValue
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;

    my ($val);

    my ($value) = {};
    my ($name) = $doc->GetTagParam($tagno, "NAME");

    $value->{NAME} = $name;    

    my ($visited) = $doc->CheckVisited($name);

    if ($visited)
    {
        $value->{VALUE} = $arg->ArgByName($name);
    }
    else
    {
        $value->{VALUE} = $doc->GetOption("NOTDISPLAYEDVAL");
    }

    my ($wasillegal) = 0;

    # Check if question was answered at all
    if (($doc->GetTagParam($tagno, "MUSTANSWER") eq "yes") && (!$sub->{SAVE}))
    {
        if ($value->{VALUE} eq "")
        {
            $sub->{ERROR} =
                lprint("The question with caption") 
              . " <i>\""
              . $doc->GetTagParam($tagno, "CAPTION")
              . "\"</i> "
              . lprint("must be answered.");
            $sub->{ERRORCODE} = 2;
        }
    }
    else
    {
        if ($value->{VALUE} eq "")
        {
            $value->{VALUE} = $doc->GetTagParam($tagno, "ILLEGALVAL");
            $wasillegal = 1;
        }
    }

    $value->{CAPTION} = $doc->GetTagParam($tagno, "CAPTION");

    return $value;
}

1;
