#!/usr/bin/perl

sub Pad
{
    my ($str, $len) = @_;

    if ($len > length($str))
    {
        $str .= " " x ($len - length($str));
    }

    return $str;
}

print
"<!--\nPUBLIC \"-//Joel Palmius//DTD Survey markup definition//EN\"\nSYSTEM \"http://www.modsurvey.org/mod_survey/survey-3.2.5.dtd\"\n-->\n\n";

open(PIPE, "ls *.pm |") || die;

@withsubtag = ("IFROUTE", "CASEROUTE", "CHOICE", "MATRIX", "LIST", "SEQUENCE", "SECURITY");

$subtags{"CHOICE"}    = "CHOICEELEMENT";
$subtags{"MATRIX"}    = "MATRIXCOLUMN,MATRIXROW";
$subtags{"LIST"}      = "LISTELEMENT";
$subtags{"SEQUENCE"}  = "FILE";
$subtags{"SECURITY"}  = "ANSWER,DATA,SOURCE,FLUSH,DEBUG,DBIAUTH,FILEAUTH,TOKENAUTH";
$subtags{"IFROUTE"}   = "EQUALS,NOTEQUALS,LESSTHAN,MORETHAN,LESSEQUALS,MOREEQUALS";
$subtags{"CASEROUTE"} = "CASE";

@alltags = ();

while ($f = <PIPE>)
{
    chop($f);
    open(FILE, $f) || die;
    $before = 1;
    $during = 0;

    ($tag, $junk) = split(/\./, $f);
    $tag = uc($tag);
    print "<!ELEMENT $tag ";
    if (grep(/$tag/, @withsubtag))
    {
        print "(" . $subtags{$tag} . ")+";
    }
    else
    {
        print "EMPTY";
    }
    print ">\n";
    print "<!ATTLIST $tag\n";

    push(@alltags, $tag);

    while ($inlin = <FILE>)
    {
        if ($before)
        {
            if ($inlin =~ m/^sub FillDefaults/)
            {
                $inlin  = <FILE>;
                $before = 0;
                $during = 1;
            }
        }

        if ($during)
        {
            if ($inlin =~ m/self-/)
            {
                $ut = $inlin;
                $ut =~ s/^ +\$self-\>\{\"//;
                $ut =~ s/\"\}[\ \t]+\=/\t/;

                chomp($ut);
                $ut =~ s/\;//g;

                ($name, $rest) = split(/_/, $ut);
                if ($name eq $tag)
                {
                    ($name, $default) = split(/\ +/, $rest);
                    $name    =~ s/\ +//g;
                    $name    =~ s/\t//g;
                    $default =~ s/\ +//g;
                    $default =~ s/\t//g;
                    if ($default eq "\"\"") { $default = "#IMPLIED"; }

                    if (($name ne "TYPE") && ($name ne "RAW"))
                    {
                        print &Pad("  $name", 20) . &Pad("CDATA", 10) . "$default\n";
                    }
                }
                else
                {
                    ($aname, $default) = split(/\ +/, $rest);
                    $aname   =~ s/\ +//g;
                    $aname   =~ s/\t//g;
                    $default =~ s/\ +//g;
                    $default =~ s/\t//g;
                    if ($default eq "\"\"") { $default = "#IMPLIED"; }
                    if ($aname ne "TYPE")
                    {
                        $line = &Pad("  $aname", 20) . &Pad("CDATA", 10) . "$default\n";

                        if (undef($etag{$aname})) { $etag{$aname} = () }

                        if (!grep(/$name/, @extras))
                        {
                            push(@extras, $name);
                        }
                        push(@{ $etag{$name} }, $line);
                    }

                }
            }
            if ($inlin =~ m/^\}/)
            {
                $inlin  = <FILE>;
                $before = 0;
                $during = 0;
            }
        }
    }
    print ">\n\n";

    close(FILE);
}

foreach $ex (@extras)
{
    print "<!ELEMENT $ex EMPTY>\n";
    print "<!ATTLIST $ex\n";
    foreach $e (@{ $etag{$ex} })
    {
        print $e;
    }
    print ">\n\n";
}

$notfirst = 0;
print "<!ELEMENT SURVEY (";
foreach $t (@alltags)
{
    if   ($notfirst) { print ","; }
    else             { $notfirst = 1; }
    print $t;
}
print ")+>\n";
print "<!ATTLIST SURVEY\n";

open(NEWPIPE, "grep self ../Document.pm | grep \"{\\\"SURVEY_\" |");
while ($inlin = <NEWPIPE>)
{
    $ut = $inlin;
    $ut =~ s/^ +\$self-\>\{\"//;
    $ut =~ s/\"\}[\ \t]+\=/\t/;

    chomp($ut);
    $ut =~ s/\;//g;

    ($name, $rest)    = split(/_/,   $ut);
    ($name, $default) = split(/\ +/, $rest);
    $name    =~ s/\ +//g;
    $name    =~ s/\t//g;
    $default =~ s/\ +//g;
    $default =~ s/\t//g;
    if ($default eq "\"\"") { $default = "#IMPLIED"; }

    if (($name ne "TYPE") && ($name ne "RAW"))
    {
        print &Pad("  $name", 20) . &Pad("CDATA", 10) . "$default\n";
    }
}
close(NEWPIPE);

print "\n>\n\n";

