#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::Ifroute;
use strict;

@Survey::Component::Ifroute::ISA = qw(Survey::Component::Component);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub FillParams
{
    my ($crap, $self) = @_;

    @{ $self->{"ALLOWED_IFROUTE"} } = ("DEFAULT");
    @{ $self->{"ALL_IFROUTE"} } = (@{ $self->{"ALLOWED_IFROUTE"} }, "TYPE", "RAW", "SUBTAGS");

    @{ $self->{"ALLOWED_IF"} } = ("THEN", "ELSE", "BOOLOP");
    @{ $self->{"ALL_IF"} } = (@{ $self->{"ALLOWED_IF"} }, "TYPE", "RAW", "SUBTAGS");

    @{ $self->{"BOOLTYPES"} } = ("EQUALS", "NOTEQUALS", "LESSTHAN", "MORETHAN", "LESSEQUALS", "MOREEQUALS");

    @{ $self->{"ALLOWED_EQUALS"} } = ("VARIABLE", "VALUE");
    @{ $self->{"ALL_EQUALS"} } = (@{ $self->{"ALLOWED_EQUALS"} }, "TYPE");

    @{ $self->{"ALLOWED_NOTEQUALS"} } = ("VARIABLE", "VALUE");
    @{ $self->{"ALL_NOTEQUALS"} } = (@{ $self->{"ALLOWED_NOTEQUALS"} }, "TYPE");

    @{ $self->{"ALLOWED_LESSTHAN"} } = ("VARIABLE", "VALUE");
    @{ $self->{"ALL_LESSTHAN"} } = (@{ $self->{"ALLOWED_LESSTHAN"} }, "TYPE");

    @{ $self->{"ALLOWED_MORETHAN"} } = ("VARIABLE", "VALUE");
    @{ $self->{"ALL_MORETHAN"} } = (@{ $self->{"ALLOWED_MORETHAN"} }, "TYPE");

    @{ $self->{"ALLOWED_LESSEQUALS"} } = ("VARIABLE", "VALUE");
    @{ $self->{"ALL_LESSEQUALS"} } = (@{ $self->{"ALLOWED_LESSEQUALS"} }, "TYPE");

    @{ $self->{"ALLOWED_MOREEQUALS"} } = ("VARIABLE", "VALUE");
    @{ $self->{"ALL_MOREEQUALS"} } = (@{ $self->{"ALLOWED_MOREEQUALS"} }, "TYPE");

    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;

    $self->{"IFROUTE_TYPE"}    = "IFROUTE";
    $self->{"IFROUTE_DEFAULT"} = "";
    $self->{"IFROUTE_RAW"}     = "";

    $self->{"IF_TYPE"}   = "IF";
    $self->{"IF_THEN"}   = "";
    $self->{"IF_ELSE"}   = "";
    $self->{"IF_BOOLOP"} = "and";

    $self->{"EQUALS_TYPE"}     = "EQUALS";
    $self->{"EQUALS_VARIABLE"} = "";
    $self->{"EQUALS_VALUE"}    = "";

    $self->{"NOTEQUALS_TYPE"}     = "NOTEQUALS";
    $self->{"NOTEQUALS_VARIABLE"} = "";
    $self->{"NOTEQUALS_VALUE"}    = "";

    $self->{"LESSTHAN_TYPE"}     = "LESSTHAN";
    $self->{"LESSTHAN_VARIABLE"} = "";
    $self->{"LESSTHAN_VALUE"}    = "";

    $self->{"MORETHAN_TYPE"}     = "MORETHAN";
    $self->{"MORETHAN_VARIABLE"} = "";
    $self->{"MORETHAN_VALUE"}    = "";

    $self->{"LESSEQUALS_TYPE"}     = "LESSEQUALS";
    $self->{"LESSEQUALS_VARIABLE"} = "";
    $self->{"LESSEQUALS_VALUE"}    = "";

    $self->{"MOREEQUALS_TYPE"}     = "MOREEQUALS";
    $self->{"MOREEQUALS_VARIABLE"} = "";
    $self->{"MOREEQUALS_VALUE"}    = "";

    1;
}

sub PlaceComponent
{
    my ($crap, $self, $paramstr) = @_;

    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    my (%params, $cell, $name, $value, @pararr);

    %params = $self->GetDefaults("IFROUTE");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "IFROUTE");
    }

    if ($params{empty} eq "yes")
    {
        $self->{ERROR}     = lprint("An IFROUTE tag cannot be immediately terminated (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 10;
    }

    if (!$self->{ERROR})
    {
        my ($endtag) = index($self->{WORK}, "</IFROUTE>");
        if ($endtag > -1)
        {
            $params{"RAW"} = substr($self->{WORK}, 0, $endtag - 1);
            $self->{WORK} = substr($self->{WORK}, 0 - length($self->{WORK}) + length($params{"RAW"}) + 11);
        }
        else
        {
            $self->{ERROR}     = lprint("An IFROUTE tag must have an end tag (in ") . $self->{FILE} . ")";
            $self->{ERRORCODE} = 14;
        }
    }

    $params{WORK} = $params{"RAW"};

    my ($e) = 1;
    my ($s) = 1;

    my (@subtags);
    my ($currsub) = 0;

    while (($e ne -1) && (!$self->{ERROR}))
    {
        $s = index($params{WORK}, "<");
        $e = index($params{WORK}, ">");

        my $ret;

        if ($e ne -1)
        {
            if ($s) { $e = $e - $s; }
            $ret = substr($params{WORK}, $s, $e + 1);
            if ($s) { $params{WORK} = substr($params{WORK}, 0 - length($params{WORK}) + $s); }
            $params{WORK} = substr($params{WORK}, 0 - length($params{WORK}) + length($ret));

            my ($subtag, $p, $type, $rest);

            if (!$self->{ERROR})
            {
                ($type, $rest) = $self->CleanUpTag($ret);

                if ($type eq "IF")
                {
                    $currsub++;
                    my ($endtag) = index($params{WORK}, "</IF>");
                    if ($endtag > -1)
                    {
                        my ($raw) = substr($params{WORK}, 0, $endtag - 1);
                        $params{WORK} = substr($params{WORK}, 0 - length($params{WORK}) + length($raw) + 6);
                        Survey::Component::Ifroute->PlaceIfTag($self, $rest, $tn, $currsub, $raw);
                    }
                    else
                    {
                        $self->{ERROR}     = lprint("An IF tag must have an end tag (in ") . $self->{FILE} . ")";
                        $self->{ERRORCODE} = 14;
                    }
                }
                else
                {
                    $self->{ERROR}     = lprint("Only IF tags are allowed in an IFROUTE tag");
                    $self->{ERRORCODE} = 99;
                }
            }
        }
    }
    $params{"SUBTAGS"} = $currsub;

    if (!$currsub)
    {
        $self->{ERROR}     = lprint("An IFROUTE tag must contain IF statements.");
        $self->{ERRORCODE} = 99;
    }

    $self->SetOption("ROUTERTAG",  $self->{NUMTAGS});
    $self->SetOption("ROUTERTYPE", "IF");

    $self->PlaceParams("IFROUTE", $tn, %params);
    $self->{NUMTAGS}++;

    $self->SetOption("MULTIPAGE", 1);

    1;
}

sub PrintComponent
{
    my ($self) = shift;
    return "";
}

sub PlaceIfTag
{
    my ($crap, $self, $paramstr, $tn, $nr, $raw) = @_;

    #print $paramstr . "<br />\n";

    my ($thisparam) = $tn . "IF" . $nr . "_";
    my ($subparam)  = $thisparam . "SUB";

    my (%params, $cell, $name, $value, @pararr);

    %params = $self->GetDefaults("IF");

    $params{"RAW"} = $raw;

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "IF");
    }

    if ($params{empty} eq "yes")
    {
        $self->{ERROR}     = lprint("An IF tag cannot be immediately terminated (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 10;
    }

    $params{WORK} = $raw;

    my ($e) = 1;
    my ($s) = 1;

    my ($currsub) = 0;

    while (($e ne -1) && (!$self->{ERROR}))
    {
        $s = index($params{WORK}, "<");
        $e = index($params{WORK}, ">");

        my $ret;

        if ($e ne -1)
        {
            if ($s) { $e = $e - $s; }
            $ret = substr($params{WORK}, $s, $e + 1);
            if ($s) { $params{WORK} = substr($params{WORK}, 0 - length($params{WORK}) + $s); }
            $params{WORK} = substr($params{WORK}, 0 - length($params{WORK}) + length($ret));

            my ($subtag, $p, $type, $rest);

            if (!$self->{ERROR})
            {
                ($type, $rest) = $self->CleanUpTag($ret);

                if (grep(/^$type$/, @{ $self->{"BOOLTYPES"} }))
                {
                    $currsub++;
                    my ($subname) = $subparam . $currsub . "_";
                    my (%asubpar);
                    $asubpar{"TYPE"} = $type;

                    @pararr = @{$rest};
                    foreach $cell (@pararr)
                    {
                        ($name, $value) = split(/=/, $cell);
                        $asubpar{$name} = $value;
                        $self->CheckParam($name, $type);
                    }

                    if ($asubpar{empty} eq "no")
                    {
                        $self->{ERROR}     = lprint("A boolean operation tag must be immediately terminated");
                        $self->{ERRORCODE} = 99;
                    }

                    if ((!$self->{ERROR}) && (!$asubpar{"VARIABLE"}))
                    {
                        $self->{ERROR}     = lprint("VARIABLE is a required parameter in a boolean operation tag");
                        $self->{ERRORCODE} = 99;
                    }

                    if ((!$self->{ERROR}) && (($asubpar{"VALUE"} eq "") || (!defined($asubpar{"VALUE"}))))
                    {
                        $self->{ERROR}     = lprint("VALUE is a required parameter in a boolean operation tag.");
                        $self->{ERRORCODE} = 99;
                    }

                    if (!$self->{ERROR})
                    {
                        foreach $e (@{ $self->{ "ALL_" . $type } })
                        {
                            $self->{ $subname . $e } = $asubpar{$e};

                            #print " ++ $type " . $subname . $e . " = " . $asubpar{$e} . "<br />\n";
                        }
                    }

                    #print "$currsub -- $p<br />\n";
                }
                else
                {
                    $self->{ERROR}     = $type . " " . lprint("is not a recognized subtag of IF");
                    $self->{ERRORCODE} = 99;
                }

            }
        }
    }

    $params{SUBTAGS} = $currsub;

    if (!$currsub)
    {
        $self->{ERROR}     = lprint("An IF tag must contain boolean statements.");
        $self->{ERRORCODE} = 99;
    }

    if ((!$self->{ERROR}) && (!$params{"THEN"}))
    {
        $self->{ERROR}     = lprint("THEN is a required parameter in an IF tag. ");
        $self->{ERRORCODE} = 99;
    }

    foreach $e (@{ $self->{"ALL_IF"} })
    {
        $self->{ $thisparam . $e } = $params{$e};

        #print " IF-PARAM " . $thisparam . $e . " = " . $params{$e} . "<br />\n";
    }
}

sub DoRouting
{
    my ($crap, $sub, $doc, $arg, $ses, $tagno) = @_;

    $sub->{IMPLICITCONTINUE} = 1;

    $doc->SetOption("REDIRECT", "yes");
    my ($continue) = $doc->GetTagParam($tagno, "DEFAULT");
    my ($tags)     = $doc->GetTagParam($tagno, "SUBTAGS");

    my ($iftag);
    my ($search) = 1;
    for ($iftag = 1 ; ($iftag <= $tags) && ($search) ; $iftag++)
    {
        my ($then) = $doc->GetIfParam($tagno, $iftag, "THEN");
        my ($else) = $doc->GetIfParam($tagno, $iftag, "ELSE");
        my ($bool) = $doc->GetIfParam($tagno, $iftag, "BOOLOP");

        my ($optags) = $doc->GetIfParam($tagno, $iftag, "SUBTAGS");
        my ($optag);
        my (@booleans);
        for ($optag = 1 ; $optag <= $optags ; $optag++)
        {
            my ($opr) = $doc->GetIfOperatorParam($tagno, $iftag, $optag, "TYPE");
            my ($var) = $doc->GetIfOperatorParam($tagno, $iftag, $optag, "VARIABLE");

            my ($vval) = $ses->getValue("SUBMITTED_$var");
            my ($oval) = $doc->GetIfOperatorParam($tagno, $iftag, $optag, "VALUE");

            if ($opr eq 'LESSTHAN')
            {
                if   ($vval < $oval) { push(@booleans, 1); }
                else                 { push(@booleans, 0); }
            }
            if ($opr eq 'MORETHAN')
            {
                if   ($vval > $oval) { push(@booleans, 1); }
                else                 { push(@booleans, 0); }
            }
            if ($opr eq 'EQUALS')
            {
                if   ($vval eq $oval) { push(@booleans, 1); }
                else                  { push(@booleans, 0); }
            }
            if ($opr eq 'NOTEQUALS')
            {
                if   ($vval ne $oval) { push(@booleans, 1); }
                else                  { push(@booleans, 0); }
            }
            if ($opr eq 'LESSEQUALS')
            {
                if   ($vval <= $oval) { push(@booleans, 1); }
                else                  { push(@booleans, 0); }
            }
            if ($opr eq 'MOREEQUALS')
            {
                if   ($vval >= $oval) { push(@booleans, 1); }
                else                  { push(@booleans, 0); }
            }
        }

        my ($istrue);
        if ($bool eq "and")
        {
            $istrue = 1;
            my ($b);
            foreach $b (@booleans)
            {
                if (!$b) { $istrue = 0; }
            }
        }

        if ($bool eq "or")
        {
            $istrue = 0;
            my ($b);
            foreach $b (@booleans)
            {
                if ($b) { $istrue = 1; }
            }
        }

        if ($istrue)
        {
            $search   = 0;
            $continue = $then;
        }
        else
        {
            if ($else)
            {
                $search   = 0;
                $continue = $else;
            }
        }
    }

    $doc->SetOption("CONTINUE", $continue);

    1;
}

1;

