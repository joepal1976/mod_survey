#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::Sequence;
use strict;

@Survey::Component::Sequence::ISA = qw(Survey::Component::Component);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub FillParams
{
    my ($crap, $self) = @_;

    @{ $self->{"ALLOWED_SEQUENCE"} } = ("SELFINCLUDE");
    @{ $self->{"ALL_SEQUENCE"} } = (@{ $self->{"ALLOWED_SEQUENCE"} }, "TYPE", "RAW", "SUBTAGS");

    @{ $self->{"ALLOWED_FILE"} } = ("FILENAME");
    @{ $self->{"ALL_FILE"} } = (@{ $self->{"ALLOWED_FILE"} }, "TYPE");

    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;

    $self->{"SEQUENCE_TYPE"}        = "SEQUENCE";
    $self->{"SEQUENCE_SELFINCLUDE"} = "no";
    $self->{"SEQUENCE_RAW"}         = "";

    1;
}

sub PlaceComponent
{
    my ($crap, $self, $paramstr) = @_;

    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    my (%params, $cell, $name, $value, @pararr);

    %params = $self->GetDefaults("SEQUENCE");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "SEQUENCE");
    }

    if ($params{empty} eq "yes")
    {
        $self->{ERROR}     = lprint("A SEQUENCE tag cannot be immediately terminated (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 10;
    }

    if (!$self->{ERROR})
    {
        my ($endtag) = index($self->{WORK}, "</SEQUENCE>");
        if ($endtag > -1)
        {
            $params{"RAW"} = substr($self->{WORK}, 0, $endtag - 1);
            $self->{WORK} = substr($self->{WORK}, 0 - length($self->{WORK}) + length($params{"RAW"}) + 13);
        }
        else
        {
            $self->{ERROR}     = lprint("A SEQUENCE tag must have an end tag (in ") . $self->{FILE} . ")";
            $self->{ERRORCODE} = 14;
        }
    }

    $params{WORK} = $params{"RAW"};

    my ($e) = 1;
    my ($s) = 1;

    my (@subtags);
    my ($currsub) = 0;

    @{ $self->{OPTION_SEQUENCE} } = ();

    while (($e ne -1) && (!$self->{ERROR}))
    {
        $s = index($params{WORK}, "<");
        $e = index($params{WORK}, ">");

        my $ret;

        if ($e ne -1)
        {
            if ($s) { $e = $e - $s; }
            $ret = substr($params{WORK}, $s, $e + 1);
            if ($s) { $params{WORK} = substr($params{WORK}, 0 - length($params{WORK}) + $s); }
            $params{WORK} = substr($params{WORK}, 0 - length($params{WORK}) + length($ret));

            my ($subtag, $p, $type, $rest);

            if (!$self->{ERROR})
            {
                ($type, $rest) = $self->CleanUpTag($ret);

                if ($type eq "FILE")
                {
                    $currsub++;
                    my ($filename) = Survey::Component::Sequence->PlaceFileTag($self, $rest, $tn, $currsub);
                    push(@{ $self->{OPTION_SEQUENCE} }, $filename);
                }
                else
                {
                    $self->{ERROR}     = lprint("Only FILE tags are allowed in a SEQUENCE tag");
                    $self->{ERRORCODE} = 99;
                }
            }
        }
    }
    $params{"SUBTAGS"} = $currsub;

    if (!$currsub)
    {
        $self->{ERROR}     = lprint("A SEQUENCE tag must contain FILE statements.");
        $self->{ERRORCODE} = 99;
    }

    if ($self->GetOption("PERSIST") eq "yes")
    {
        $self->{ERROR}     = lprint("Cannot have both a SEQUENCE tag and PERSIST=\"yes\" in the SURVEY tag.");
        $self->{ERRORCODE} = 99;
    }

    $self->PlaceParams("SEQUENCE", $tn, %params);
    $self->{NUMTAGS}++;

    1;
}

sub PrintComponent
{
    my ($self) = shift;
    return "";
}

sub PlaceFileTag
{
    my ($crap, $self, $paramstr, $tn, $nr) = @_;

    my ($thisparam) = $tn . "FILE" . $nr . "_";

    my (%params, $cell, $name, $value, @pararr, $e);

    %params = $self->GetDefaults("FILE");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "FILE");
    }

    if ($params{empty} eq "no")
    {
        $self->{ERROR}     = lprint("A FILE tag must be immediately terminated (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 11;
    }

    if ((!$self->{ERROR}) && (!$params{"FILENAME"}))
    {
        $self->{ERROR}     = lprint("FILENAME is a required parameter in a FILE tag.");
        $self->{ERRORCODE} = 99;
    }

    foreach $e (@{ $self->{"ALL_FILE"} })
    {
        $self->{ $thisparam . $e } = $params{$e};
    }

    return $params{"FILENAME"};
}

1;
