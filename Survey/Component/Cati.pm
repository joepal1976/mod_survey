#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::Cati;
use strict;

@Survey::Component::Cati = qw(Survey::Component::Component);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub FillParams
{
    my ($crap, $self) = @_;

    @{ $self->{"ALLOWED_CATI"} } = (
        "DBIDSN", "DBITABLE", "DBIUSER", "DBIPASS",

        # Add parameters as needed
    );
    @{ $self->{"ALL_CATI"} } = (@{ $self->{"ALLOWED_CATI"} }, "TYPE", "VARIABLES");

    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;

    $self->{"CATI_TYPE"}      = "CATI";
    $self->{"CATI_DBIDSN"}    = "";
    $self->{"CATI_DBITABLE"}  = "";
    $self->{"CATI_DBIUSER"}   = "";
    $self->{"CATI_DBIPASS"}   = "";
    $self->{"CATI_VARIABLES"} = "";

    # Add/change defaults as needed

    1;
}

sub PlaceComponent
{
    my ($crap, $self, $paramstr) = @_;

    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    my (%params, $cell, $name, $value, @pararr);

    %params = $self->GetDefaults("CATI");

    @pararr = @{$paramstr};

    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "CATI");
    }

    if ($params{empty} eq "yes")
    {
        $self->{ERROR}     = lprint("A CATI tag cannot be immediately terminated (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 10;
    }

    # Check that required parameters are set, and that parameters look sane
    # ..

    if (!$self->{ERROR})
    {
        my ($endtag) = index($self->{WORK}, "</CATI>");
        if ($endtag > -1)
        {
            $params{"RAW"} = substr($self->{WORK}, 0, $endtag);
            $self->{WORK} = substr($self->{WORK}, 0 - length($self->{WORK}) + length($params{"RAW"}) + 8);

            # Do parsing of subtags here, look in IFROUTE and CASEROUTE for examples
        }
        else
        {
            $self->{ERROR}     = lprint("A CATI tag must have an end tag (in ") . $self->{FILE} . ")";
            $self->{ERRORCODE} = 14;
        }
    }

    # Should probably fill $params{"VARIABLES"} automatically with values from subtags

    if (!$self->{ERROR} && $params{"VARIABLES"})
    {
        my (@vars) = split(/,/, $params{"VARIABLES"});
        my ($var, $name);

        foreach $var (@vars)
        {
            if ((!$self->{ERROR}) && (length($var) > 8))
            {
                $self->{ERROR}     = lprint("An CATI name can only be 8 characters wide (in ") . $self->{FILE} . ")";
                $self->{ERRORCODE} = 21;
            }

            if (!$self->{ERROR}) { $self->CheckName($params{NAME}, "CATI"); }

            $name = "TAGNAME_" . $var;
            $name = "\U$name";

            if (defined($self->{$name}))
            {
                $self->{ERROR}     = lprint("An CATI name must be unique (in ") . $self->{FILE} . ")";
                $self->{ERRORCODE} = 20;
            }
            else
            {
                $self->{$name} = $self->{NUMTAGS};
            }
        }
    }

    $self->PlaceParams("CATI", $tn, %params);

    $self->{NUMTAGS}++;

    1;
}

sub PrintComponent
{
    my ($crap, $self, $tagno) = @_;
    my ($doc) = $self->{DOCUMENT};
    my ($ses) = $doc->{SESSION};

    # Do ses->setvisited, ses->submitted and ses->submittedsofar as
    # discussed in chat

    return "";
}

sub NumberOfValues
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;

    return 0;    # return actual number of variables here
}

sub GetValueNumerical
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;

    # This may have to be modified if both numerical and non-numerical
    # fields shall be supported

    if ($doc->GetTagParam($tagno, "NUMERICAL") eq "yes")
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

sub GetValueName
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;

    # Return the name of variable number $valuenumber

    return "";
}

sub GetCheckValue
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;

    my ($value) = {};
    my ($name) = Survey::Component::Cati->GetValueName($doc, $sub, $arg, $ses, $tagno, $valuenumber);

    my ($visited) = $doc->CheckVisited($name);

    # fetch value from DB here
    $value->{VALUE} = "";
    $value->{NAME}  = $name;

    $value->{NUMERICAL} = Survey::Component::Cati->GetValueNumerical($doc, $sub, $arg, $ses, $tagno);

    return $value;
}

1;

