#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::Comment;
use strict;

@Survey::Component::Comment::ISA = qw(Survey::Component::Component);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub FillParams
{
    my ($crap, $self) = @_;

    @{ $self->{"ALLOWED_COMMENT"} } = ("EMBED");
    @{ $self->{"ALL_COMMENT"} } = (@{ $self->{"ALLOWED_COMMENT"} }, "RAW", "TYPE");

    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;

    $self->{"COMMENT_TYPE"}  = "COMMENT";
    $self->{"COMMENT_EMBED"} = "no";

    1;
}

sub PlaceComponent
{
    my ($crap, $self, $paramstr) = @_;

    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    my (%params, $cell, $name, $value, @pararr);

    %params = $self->GetDefaults("COMMENT");

    @pararr = @{$paramstr};

    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "COMMENT");
    }

    if ($params{empty} eq "yes")
    {
        $self->{ERROR}     = lprint("A COMMENT tag cannot be immediately terminated (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 10;
    }

    if (!$self->{ERROR})
    {
        my ($endtag) = index($self->{WORK}, "</COMMENT>");
        if ($endtag > -1)
        {
            $params{"RAW"} = substr($self->{WORK}, 0, $endtag - 1);
            $self->{WORK} = substr($self->{WORK}, 0 - length($self->{WORK}) + length($params{"RAW"}) + 11);
            $params{"RAW"} =~ s/^[\x0a\ ]+//;
            $params{"RAW"} =~ s/^[\x0a\x0d]+//;
            $params{"RAW"} =~ s/^\x0a+//;
            $params{"RAW"} =~ s/^\ +//;
            $params{"RAW"} =~ s/[\x0a\ ]+$//;
            $params{"RAW"} =~ s/[\x0a\x0d]+$//;
            $params{"RAW"} =~ s/\x0a+$//;
            $params{"RAW"} =~ s/\ +$//;
        }
        else
        {
            $self->{ERROR}     = lprint("A COMMENT tag must have an end tag (in ") . $self->{FILE} . ")";
            $self->{ERRORCODE} = 14;
        }
    }

    $self->PlaceParams("COMMENT", $tn, %params);
    $self->{NUMTAGS}++;

    1;
}

sub PrintComponent
{
    my ($crap, $self, $tagno) = @_;
    my ($doc) = $self->{DOCUMENT};
    my ($out) = "";

    if ($doc->GetTagParam($tagno, "EMBED") eq "yes")
    {
        $out .= "<!--\n";
        $out .= $doc->GetTagParam($tagno, "RAW") . "\n";
        $out .= "-->\n\n";
    }

    return $out;
}

sub MakeXML
{
    my ($s, $doc, $tagno) = @_;

    my ($out) = $s->MakeTagOpener($doc, $tagno);

    $out .= $doc->GetTagParam($tagno, "RAW");
    $out .= "\n";
    $out .= $s->MakeTagCloser($doc, $tagno);

    return $out;
}

1;

