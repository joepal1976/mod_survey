#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::Geo;
use strict;

@Survey::Component::Geo::ISA = qw(Survey::Component::Component);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub FillParams
{
    my ($crap, $self) = @_;
    @{ $self->{"ALLOWED_GEO"} } = ("NAME", "MAXLEN");
    @{ $self->{"ALL_GEO"} } = (@{ $self->{"ALLOWED_GEO"} }, "TYPE");
    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;
    $self->{"GEO_TYPE"}   = "GEO";
    $self->{"GEO_NAME"}   = "";
    $self->{"GEO_MAXLEN"} = "80";
    1;
}

sub PlaceComponent
{
    my ($crap, $self, $paramstr) = @_;
    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    my (%params, $cell, $name, $value, @pararr);

    %params = $self->GetDefaults("GEO");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "GEO");
    }

    if ($params{empty} eq "no")
    {
        $self->{ERROR}     = lprint("A GEO tag must be terminated immediately (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 11;
    }

    if ((!$self->{ERROR}) && (!$params{NAME}))
    {
        $self->{ERROR}     = lprint("A GEO tag must have a name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 12;
    }

    if ((!$self->{ERROR}) && (length($params{NAME}) > 8))
    {
        $self->{ERROR}     = lprint("A GEO tag name can only be 8 characters wide (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 21;
    }

    if (!$self->{ERROR}) { $self->CheckName($params{NAME}, "GEO"); }

    $name = "TAGNAME_" . $params{"NAME"};
    $name = "\U$name";

    if ((!$self->{ERROR}) && (defined($self->{$name})))
    {
        $self->{ERROR}     = lprint("A GEO tag must have unique name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 20;
    }
    else
    {
        $self->{$name} = $self->{NUMTAGS};
    }

    $self->PlaceParams("GEO", $tn, %params);

    $self->{NUMTAGS}++;

    1;
}

sub PrintComponent
{
    my ($crap, $self, $tagno) = @_;
    my ($doc) = $self->{DOCUMENT};

    $doc->SetVisited($doc->GetTagParam($tagno, "NAME"));

    my($out) = "";

    my $name = $doc->GetTagParam($tagno, "NAME");

    $out .= "<input type=\"hidden\" name=\"$name\" value=\"-1\" />\n";

    $out .= "<script>\n";
    $out .= "if(navigator.geolocation) \n";
    $out .= "{\n";
    $out .= "  navigator.geolocation.getCurrentPosition(successFunction, errorFunction);\n";
    $out .= "}";

    $out .= "else\n";
    $out .= "{\n";
#    $out .= "  alert('error 1');\n";
    $out .= "  // do nothing \n";
    $out .= "}\n";

    $out .= "\n";
    $out .= "function successFunction(position) \n";
    $out .= "{\n";
    $out .= "  var lat = position.coords.latitude;\n";
    $out .= "  var long = position.coords.longitude;\n";
#    $out .= "  alert(lat);\n";
#    $out .= "  alert(long);\n";
    $out .= "  document.forms[0].$name.value = lat + ' ' + long;\n";
#    $out .= "  document.forms[0].test.value = lat + ' ' + long;\n";
    $out .= "}\n";
    $out .= "\n";
    $out .= "function errorFunction(position) \n";
    $out .= "{\n";
    $out .= "  // do nothing \n";
#    $out .= "  alert('error 2');\n";
    $out .= "}\n";
    $out .= "</script>\n";

    return $out;
}

sub NumberOfValues
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 1;
}

sub GetValueNumerical
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 0;
}

sub GetCheckValue
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;

    my ($value) = {};
    my ($name) = $doc->GetTagParam($tagno, "NAME");

    my ($visited) = $doc->CheckVisited($name);

    if ($visited)
    {
        if   (defined($arg->ArgByName($name))) { $value->{VALUE} = $arg->ArgByName($name); }
        else                                   { $value->{VALUE} = ""; }
    }

    if ($visited)
    {
        my ($envn) = $doc->GetTagParam($tagno, "NAME");

        if ($doc->{SESSION}->getValue("SUBMITTED_" . $envn))
        {
            $value->{VALUE} = ($doc->{SESSION}->getValue("SUBMITTED_" . $envn));
        }

        # Fix potential security hole with crafted GEO fields
        $value->{VALUE} =~ s/([\x00-\x1f\x22\x27\x2c\x3b\x7f-\xff])/("%".unpack("H*",$1))/eg;
    }
    else
    {
        $value->{VALUE} = $doc->GetOption("NOTDISPLAYEDVAL");
    }

    $value->{NAME}      = $name;
    $value->{NUMERICAL} = 0;

    return $value;
}

1;


