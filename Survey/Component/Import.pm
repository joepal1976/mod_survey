#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::Import;
use strict;

@Survey::Component::Import::ISA = qw(Survey::Component::Component);

use Text::ParseWords;
use CGI;
use DBI;

use Survey::Language;
use Survey::Debug;
use Survey::DBCommon;

sub FillParams
{
    my ($crap, $self) = @_;

    @{ $self->{"ALLOWED_IMPORT"} } = ("DBIDSN", "CAPTION", "DBITABLE", "DBIUSER", "DBIPASS", "VISIBLE", "IDFIELD");
    @{ $self->{"ALL_IMPORT"} } = (@{ $self->{"ALLOWED_IMPORT"} }, "TYPE", "VARIABLES", "FIELDS", "ARRAYS", "ARRFLD");

    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;

    $self->{"IMPORT_TYPE"}      = "IMPORT";
    $self->{"IMPORT_CAPTION"}   = "IMPORT";
    $self->{"IMPORT_DBIDSN"}    = "";
    $self->{"IMPORT_DBITABLE"}  = "";
    $self->{"IMPORT_DBIUSER"}   = "";
    $self->{"IMPORT_DBIPASS"}   = "";
    $self->{"IMPORT_VARIABLES"} = "";
    $self->{"IMPORT_IDFIELD"}   = "";
    $self->{"IMPORT_FIELDS"}    = "";
    $self->{"IMPORT_VISIBLE"}   = "no";
    $self->{"IMPORT_ARRAYS"}    = "";
    $self->{"IMPORT_ARRFLD"}    = "";

    1;
}

sub PlaceComponent
{
    my ($crap, $self, $paramstr) = @_;

    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    my (%params, %epara, $cell, $cell2, $name, $value, @pararr, $work, $s, $e, $tag, $cri, $gru, $gruc);

    %params = $self->GetDefaults("IMPORT");

    @pararr = @{$paramstr};

    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell, 2);
        $params{$name} = $value;
        $self->CheckParam($name, "IMPORT");
    }

    if ($params{empty} eq "yes")
    {
        $self->{ERROR}     = lprint("A IMPORT tag cannot be immediately terminated (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 10;
    }

    if ((!$self->{ERROR}) && (!$params{DBIDSN}))
    {
        $self->{ERROR} = lprint("An IMPORT tag must have the DBIDSN parameter specified (in ") . $self->{FILE} . ")";
    }

    if ((!$self->{ERROR}) && (!$params{DBITABLE}))
    {
        $self->{ERROR} = lprint("An IMPORT tag must have the DBITABLE parameter specified (in ") . $self->{FILE} . ")";
    }

    if ((!$self->{ERROR}) && (!(($params{VISIBLE} eq "no") || ($params{VISIBLE} eq "yes") || ($params{VISIBLE} eq ""))))
    {
        $self->{ERROR} = lprint("A VISIBLE parameter in an IMPORT tag should be set to yes or no");
    }

    if (!$self->{ERROR})
    {
        my ($endtag) = index($self->{WORK}, "</IMPORT>");
        if ($endtag > -1)
        {
            $params{"RAW"} = substr($self->{WORK}, 0, $endtag);
            $self->{WORK} = substr($self->{WORK}, 0 - length($self->{WORK}) + length($params{"RAW"}) + 10);

            my ($valid) = "";
            if ($self->{WORK} =~ m/<\s*VARIABLE/) { $valid .= "variable"; }
            if ($self->{WORK} =~ m/<\s*FILTER/)   { $valid .= "-filter"; }
            if ($valid eq "-filter")
            {
                $self->{ERROR} = lprint("An IMPORT tag must contain at least one VARIABLE (in ") . $self->{FILE} . ")";
                $self->{ERRORCODE} = 24;    # to be updated!!!!!
            }
            elsif ($valid ne "")
            {
                $params{"RAW"} = substr($self->{WORK}, 0, $endtag - 1);
                $self->{WORK} = substr($self->{WORK}, 0 - length($self->{WORK}) + length($params{"RAW"}) + 10);
            }

        }
        else
        {
            $self->{ERROR}     = lprint("A IMPORT tag must have an end tag (in ") . $self->{FILE} . ")";
            $self->{ERRORCODE} = 14;
        }
    }

    if (!$self->{ERROR})
    {
        $work = $params{"RAW"};
        $s    = index($work, "<");
        $e    = index($work, "/>");
        if ($e ne -1)
        {
            $e++;
        }
        my ($filter) = 0;
        my ($vars)   = 0;
        my ($arrs)   = 0;

        while (($s ne -1) && ($e ne -1) && (!$self->{ERROR}))
        {
            if ($s) { $e = $e - $s }
            $tag = substr($work, $s, $e + 1);
            if ($s)
            {
                $work = substr($work, 0 - length($work) + $s);
            }
            $work = substr($work, 0 - length($work) + length($tag));
            ($cell, $tag) = $self->CleanUpTagExpr($tag);

            if (($cell ne "VARIABLE") && ($cell ne "FILTER") && ($cell ne "ARRAY"))
            {
                $self->{ERROR} =
                  lprint("An IMPORT block can only contain VARIABLEs and a FILTER (in ") . $self->{FILE} . ")";
                $self->{ERRORCODE} = 25;    #to be updated!!!!
            }
            else
            {

                $epara{"CAPTION"}       = "";
                $epara{"NAME"}          = "";
                $epara{"FIELD"}         = "";
                $epara{"WRITABLE"}      = "";
                $epara{"UPDATE"}        = "";
                $epara{"VISIBLE"}       = "";
                $epara{"CRITERIA"}      = "";
                $epara{"GROUP"}         = "";
                $epara{"GROUPCRITERIA"} = "";
                $epara{"TYPE"}          = "";

                @pararr = @{$tag};

                foreach $cell2 (@pararr)
                {
                    ($name, $value) = split(/=/, $cell2, 2);
                    $epara{"TYPE"} = $cell;

                    #my($doc) = $self;
                    #$epara{$name} = Survey::Component::Import->ParseDyn($doc, $doc->{SESSION}, $value);
                    $epara{$name} = $value;
                }

                if ($epara{"empty"} eq "no")
                {
                    $self->{ERROR} =
                      lprint(
"The IMPORT tag's elements (both VARIABLE,FILTER, and ARRAY) tag must be terminated immediately (in ")
                      . $self->{FILE} . ")";
                    $self->{ERRORCODE} = 11;    #to be updated!!!
                }
            }

            if (!$self->{ERROR})
            {
                if ($cell eq "VARIABLE")
                {
                    $vars++;
                    $s                         = $tn . "VAR" . $vars . "_";
                    $self->{ $s . "NAME" }     = $epara{"NAME"};
                    $self->{ $s . "FIELD" }    = $epara{"FIELD"};
                    $self->{ $s . "WRITABLE" } = $epara{"WRITABLE"};
                    $self->{ $s . "UPDATE" }   = $epara{"UPDATE"};
                    $self->{ $s . "CAPTION" }  = $epara{"CAPTION"};
                    $self->{ $s . "VISIBLE" }  = $epara{"VISIBLE"};
                    $params{"VARIABLES"} .= $epara{"NAME"} . ",";
                    $params{"FIELDS"}    .= $epara{"FIELD"} . ",";
                }
                if ($cell eq "ARRAY")
                {
                    $arrs++;
                    $s                      = $tn . "ARR" . $arrs . "-";
                    $self->{ $s . "NAME" }  = $epara{"NAME"};
                    $self->{ $s . "FIELD" } = $epara{"FIELD"};
                    $params{"ARRAYS"} .= $epara{"NAME"} . ",";
                    $params{"ARRFLD"} .= $epara{"FIELD"} . ",";
                }
                if ($cell eq "FILTER")
                {
                    $filter++;
                    if ($filter > 1)
                    {
                        $self->{ERROR} =
                          lprint("There can only be one FILTER tag in an IMPORT tag (in ") . $self->{FILE} . ")";
                        $self->{ERRORCODE} = 11;
                    }
                    $s                              = $tn . "FIL" . $filter . "_";
                    $self->{ $s . "CRITERIA" }      = $epara{"CRITERIA"};
                    $cri                            = $epara{"CRITERIA"};
                    $self->{ $s . "GROUP" }         = $epara{"GROUP"};
                    $gru                            = $epara{"GROUP"};
                    $self->{ $s . "GROUPCRITERIA" } = $epara{"GROUPCRITERIA"};
                    $gruc                           = $epara{"GROUPCRITERIA"};

                }
            }
            $s = index($work, "<");
            $e = index($work, "/>");
            if ($e ne -1)
            {
                $e++;
            }
        }
    }

    #erease the last useless comma

    if ($params{"VARIABLES"})
    {
        $params{"VARIABLES"} = substr($params{"VARIABLES"}, 0, length($params{"VARIABLES"}) - 1);
    }

    if ($params{"FIELDS"})
    {
        $params{"FIELDS"} = substr($params{"FIELDS"}, 0, length($params{"FIELDS"}) - 1);
    }

    if ($params{"ARRAYS"})
    {
        $params{"ARRAYS"} = substr($params{"ARRAYS"}, 0, length($params{"ARRAYS"}) - 1);
    }

    if ($params{"ARRFLD"})
    {
        $params{"ARRFLD"} = substr($params{"ARRFLD"}, 0, length($params{"ARRFLD"}) - 1);
    }

    # Should probably fill $params{"VARIABLES"} automatically with values from subtags

    if (!$self->{ERROR} && $params{"VARIABLES"})
    {
        my (@vars) = split(/,/, $params{"VARIABLES"});
        my ($var, $name);

        foreach $var (@vars)
        {
            if ((!$self->{ERROR}) && (length($var) > 8))
            {
                $self->{ERROR}     = lprint("An IMPORT name can only be 8 characters wide (in ") . $self->{FILE} . ")";
                $self->{ERRORCODE} = 21;
            }

            if (!$self->{ERROR}) { $self->CheckName($params{NAME}, "IMPORT"); }

            $name = "TAGNAME_" . $var;
            $name = "\U$name";

            if (defined($self->{$name}))
            {
                $self->{ERROR}     = lprint("An IMPORT name must be unique (in ") . $self->{FILE} . ")";
                $self->{ERRORCODE} = 20;
            }
            else
            {
                $self->{$name} = $self->{NUMTAGS};
            }
        }
    }

    $self->PlaceParams("IMPORT", $tn, %params);

    $self->{NUMTAGS}++;

    my ($flds)   = $params{"FIELDS"};
    my ($dsn)    = $params{"DBIDSN"};
    my ($usr)    = $params{"DBIUSER"};
    my ($pwd)    = $params{"DBIPASS"};
    my ($tab)    = $params{"DBITABLE"};
    my ($idf)    = $params{"IDFIELD"};
    my ($arrs)   = $params{"ARRAYS"};
    my ($arrfld) = $params{"ARRFLD"};
    my ($ses)    = $self->{SESSION};

    if ($arrs && $arrfld)
    {
        my ($res);
        my ($query) = "SELECT $arrfld";
        $query .= " FROM " . $tab;
        if ($cri) { $query .= " WHERE $cri"; }

        my ($dbh) = new Survey::DBCommon($dsn, $usr, $pwd);
        if (!$dbh->Error())
        {
            $dbh->openDB();
            $res = $dbh->execStm($query, 1);
        }

        my ($arrayid, $i);
        my (@temporaryarrays) = ();
        my (@arrayids) = split(/,/, $arrs);

        my ($line);
        foreach $line (@{$res})
        {
            my ($onefield);
            for ($i = 0 ; $i < scalar(@arrayids) ; $i++)
            {
                $onefield = @{$line}[$i];
                push(@{ $temporaryarrays[$i] }, $onefield);
            }
        }

        for ($i = 0 ; $i < scalar(@arrayids) ; $i++)
        {
            my ($arrayid)     = $arrayids[$i];
            my (@arrayassuch) = @{ $temporaryarrays[$i] };
            $ses->setArrayValue($arrayid, \@arrayassuch);
        }
        $dbh->closeDB();

    }

    1;
}

sub PrintComponent
{
    my ($crap, $self, $tagno) = @_;
    my ($doc) = $self->{DOCUMENT};
    my ($ses) = $doc->{SESSION};

    my ($dsn)  = $doc->GetTagParam($tagno, "DBIDSN");
    my ($usr)  = $doc->GetTagParam($tagno, "DBIUSER");
    my ($pwd)  = $doc->GetTagParam($tagno, "DBIPASS");
    my ($tab)  = $doc->GetTagParam($tagno, "DBITABLE");
    my ($vars) = $doc->GetTagParam($tagno, "VARIABLES");
    my ($flds) = $doc->GetTagParam($tagno, "FIELDS");
    my ($idf)  = $doc->GetTagParam($tagno, "IDFIELD");

    #my($arrs) = $doc->GetTagParam($tagno, "ARRAYS");
    #my($arrfld) = $doc->GetTagParam($tagno, "ARRFLD");

    #my(@ars) = split(/,/,$arrs);
    #my(@arf) = split(/,/,$arrfld);

    my ($dbh, $sth, $field, $fld_val, $out, $query, $v);

    my (@vs);

    if ($flds eq "")
    {
        $flds = "*";
    }

    if ($idf)
    {
        $query = "Select " . $flds . "," . $idf;
    }
    else
    {
        $query = "Select " . $flds;
    }

    $query .= " From " . $tab;

    $fld_val = $doc->GetImportFilter($tagno, 1, "CRITERIA");

    if ($fld_val ne "")
    {
        $fld_val = Survey::Component::Import->ParseDyn($doc, $ses, $fld_val);
        $query .= " Where " . $fld_val;
    }

    $fld_val = $doc->GetImportFilter($tagno, 1, "GROUP");

    if ($fld_val ne "")
    {
        $fld_val = Survey::Component::Import->ParseDyn($doc, $ses, $fld_val);
        $query .= " Group by " . $fld_val;
        $fld_val = $doc->GetImportFilter($tagno, 1, "GROUPCRITERIA");
        if ($fld_val ne "")
        {
            $fld_val = Survey::Component::Import->ParseDyn($doc, $ses, $fld_val);
            $query .= " Having " . $fld_val;
        }
    }

    my ($res);
    $dbh = new Survey::DBCommon($dsn, $usr, $pwd);
    if (!$dbh->Error())
    {
        $dbh->openDB();
        if ($idf)
        {
            $res = $dbh->execStm($query, $idf);
        }
        else
        {
            $res = $dbh->execStm($query, 1);
        }

        if ($dbh->Error())
        {
            $out = "DB ERROR";
        }

        my (@field);

        #we read only one line of the result (by now)

        my ($idfield) = "";

        @field = @{$res}[0];
        if ($idf)
        {
            $idfield = pop(@field);
        }

        my ($f);

        my (@vs)    = split(/,/, $flds);
        my (@names) = split(/,/, $vars);

        my ($i)     = 0;
        my ($sofar) = $ses->getValue("submittedsofar");

        foreach $v (@vs)
        {
            $doc->SetVisited($names[$i]);
            $ses->setValue("SUBMITTED_" . $names[$i], ${ $field[0] }[$i]);
            $i++;
            if ($sofar) { $sofar .= "\x03" }
            $sofar .= $names[$i];
        }

        $ses->setValue("submittedsofar", $sofar);

        #my($h) = 0;
        #my($p) = 0;
        #my($thisf) = "";
        #my($k) = ();
        #my(@ares) = ();

        #foreach $a (@ars)
        #{
        #  $thisf = $arf[$p];
        #  $h=0;

        #foreach $k (@{$res})
        #{
        #fetch the FIELD value from this row
        #EXECUTEME is a ref to a statemnt (look into DBCommon 4 more info)

        #   $ares[$h] = @{$k}[$dbh->{EXECUTEME}->{NAME_lc_hash}{$thisf}];
        #   $h++;
        #}
        #$ses->setArrayValue($a,\@ares);
        #$p++;
        #}

        $dbh->closeDB();

        # Should we print something on the screen here?
        # we should avoid the upload of the db-readed values

        my ($name);
        my ($tmp)      = "";
        my ($tabw)     = 0;
        my ($fldcount) = 0;

        $tmp .= "<div id=\"mdscomponent\" class=\"IMPORT\">\n";
        if ($doc->GetTagParam($tagno, "CAPTION"))
        {
            $tmp .= "<div id=\"mdsquestion\" class=\"IMPORTcap\">";
            $tmp .= $doc->GetTagParam($tagno, "CAPTION");
            $tmp .= "</div>\n";
            $tmp .= "<div id=\"mdselement\" class=\"IMPORTelm\">\n";
        }

        $i = 1;
        my ($tn) = "TAG" . $tagno . "_";

        foreach $name (@names)
        {
            my ($s)     = $tn . "VAR" . $i++ . "_";
            my ($capt)  = $doc->{ $s . "CAPTION" };
            my ($value) = $ses->getValue("SUBMITTED_$name");

            my ($visi) = $doc->GetTagParam($tagno, "VISIBLE");

            if ($visi eq "yes")
            {
                if ($tabw == 0)
                {
                    $out .= $tmp;
                    $tabw = 1;
                }

                #$out .= "<tr><td>$name</td><td>$capt</td><td>";

                my ($showcap) = $capt || $name || "?";

                $out .= "<div class=\"IMPORTline\">\n";
                $out .= "<div id=\"mdscaption\" class=\"IMPORTlabel\">";
                $out .= $showcap;
                $out .= "</div>\n";

                if ($doc->{ $s . "WRITABLE" } eq "yes")
                {
                    $out .= "<input class=\"IMPORTtext\" type=\"text\" name=\"$name\" value=\"$value\"/>";
                    my ($upstm) = "update " . $tab . " set " . $vs[$i - 2] . " = ? ";

                    if ($idf)
                    {
                        $upstm .= "WHERE $idf = $idfield";
                    }

                    if ($doc->{ $s . "UPDATE" } eq "yes")
                    {
                        $ses->setValue("update_" . $name, $upstm);
                    }
                }
                else
                {
                    $out .= "<div id=\"mdscaption\" class=\"IMPORTvalue\">";
                    $out .= $value;
                    $out .= "</div>\n";
                }
                $out .= "</div>\n";

                #$out .= "</td></tr>\n";
            }
        }

        if ($tabw == 1)
        {

            #$out .= "</table>\n";
            $out .= "</div></div>\n";
        }
    }
    else
    {
        $out = DBI::errstr;
    }

    if ($dbh->Error())
    {
        $self->{ERROR}     = "Database error: " . $dbh->{ERROR};
        $self->{ERRORCODE} = 99;
    }

    return $out;
}

sub NumberOfValues
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;

    my (@listOfVar) = split(/,/, $doc->GetTagParam($tagno, "VARIABLES"));

    return ($#listOfVar + 1);
}

sub GetValueNumerical
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;

    # This may have to be modified if both numerical and non-numerical
    # fields shall be supported

    return 0;

}

sub GetValueName
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;

    # Return the name of variable number $valuenumber

    my (@listOfVar) = split(/,/, $doc->GetTagParam($tagno, "VARIABLES"));

    return $listOfVar[$valuenumber - 1];
}

sub GetCheckValue
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;

    my ($value) = {};
    my ($name) = Survey::Component::Import->GetValueName($doc, $sub, $arg, $ses, $tagno, $valuenumber);

    if ($doc->{ "TAG" . $tagno . "_VAR" . $valuenumber . "_" . "WRITABLE" } eq "yes")
    {
        my ($fieldvalw) = $arg->ArgByName($name);
        $value->{VALUE}     = $fieldvalw;
        $value->{NAME}      = $name;
        $value->{NUMERICAL} = Survey::Component::Import->GetValueNumerical($doc, $sub, $arg, $ses, $tagno);
        if ($doc->{ "TAG" . $tagno . "_VAR" . $valuenumber . "_" . "UPDATE" } eq "yes")
        {
            my ($sqlstm) = $ses->getValue("update_" . $name);

            #	 print "<!-- $sqlstm -->\n";
            if ($sqlstm)
            {
                my ($dsn) = $doc->GetTagParam($tagno, "DBIDSN");
                my ($usr) = $doc->GetTagParam($tagno, "DBIUSER");
                my ($pwd) = $doc->GetTagParam($tagno, "DBIPASS");

                my ($dbh, $sth);

                if ($dbh = DBI->connect($dsn, $usr, $pwd, { PrintError => 0, AutoCommit => 1, RaiseError => 0 }))
                {
                    $sth = $dbh->prepare($sqlstm);
                    if ($dbh->Error())
                    {
                        $sub->{ERROR}     = "Database error: " . $dbh->{ERROR};
                        $sub->{ERRORCODE} = 99;
                        $doc->{ERROR}     = "Database error: " . $dbh->{ERROR};
                        $doc->{ERRORCODE} = 99;
                        return undef;
                    }
                    $sth->execute($fieldvalw);
                    if ($dbh->Error())
                    {
                        $sub->{ERROR}     = "Database error: " . $dbh->{ERROR};
                        $sub->{ERRORCODE} = 99;
                        $doc->{ERROR}     = "Database error: " . $dbh->{ERROR};
                        $doc->{ERRORCODE} = 99;
                        return undef;
                    }
                    $sth->finish;
                    $dbh->disconnect();
                }
            }
        }
    }
    else
    {
        if ($doc->CheckVisited($name))
        {
            $value->{VALUE} = $ses->getValue("SUBMITTED_" . $name);
        }
        else
        {
            $value->{VALUE} = $doc->GetOption("NOTDISPLAYEDVAL");
        }

        $value->{NAME} = $name;

        $value->{NUMERICAL} = Survey::Component::Import->GetValueNumerical($doc, $sub, $arg, $ses, $tagno);
    }
    return $value;
}

sub ParseVar
{
    my ($crap, $self, $out, $ses) = @_;

    my ($start)    = -1;
    my ($end)      = 0;
    my ($contents) = "";
    my ($substr);

    if (!defined($ses))
    {
        return $out;
    }

    while ((($start = index($out, "{\$")) > 0) && (!$self->{ERROR}))
    {
        $end = index($out, "\$}");
        if ($end > $start)
        {
            $substr = substr($out, $start + 2, $end - $start - 2);

            if (defined($ses->getValue("SUBMITTED_$substr")))
            {
                $contents = $ses->getValue("SUBMITTED_$substr");

                $contents = substr($out, 0, $start) . $contents;
                $contents .= substr($out, $end + 2);

                $out = $contents;
            }
            else
            {
                $self->{ERROR} =
                  lprint("The variable") . " \"$substr\" " . lprint("has not been submitted (its mine :( )");
                $self->{ERRORCODE} = 99;
            }
        }
        else
        {
            $self->{ERROR}     = lprint("Found \"{\$\" without \"\$}\"");
            $self->{ERRORCODE} = 99;
        }
    }

    return $out;
}

sub ParseCap
{
    my ($crap, $self, $out, $ses) = @_;

    my ($start) = -1;
    my ($end)   = 0;

    if (!defined($ses))
    {
        return $out;
    }

    while ((($start = index($out, "{\%")) > 0) && (!$self->{ERROR}))
    {
        my ($contents) = "";
        $end = index($out, "\%}");
        if ($end > $start)
        {
            my ($substr) = substr($out, $start + 2, $end - $start - 2);

            if (defined($ses->getValue("CAPTION_$substr")))
            {
                $contents = $ses->getValue("CAPTION_$substr");

                $contents = substr($out, 0, $start) . $contents;
                $contents .= substr($out, $end + 2);

                $out = $contents;
            }
            else
            {
                $self->{ERROR} =
                  lprint("The variable") . " \"$substr\" " . lprint("have not produced a caption (it's mine :(( )");
                $self->{ERRORCODE} = 99;
            }
        }
        else
        {
            $self->{ERROR}     = lprint("Found \"{\%\" without \"\%}\"");
            $self->{ERRORCODE} = 99;
        }
    }

    return $out;
}

sub ParseSel
{
    my ($crap, $self, $out, $ses) = @_;

    my ($start) = -1;
    my ($end)   = 0;

    if (!defined($ses))
    {
        return $out;
    }

    while ((($start = index($out, "{\!")) > 0) && (!$self->{ERROR}))
    {
        my ($contents) = "";
        $end = index($out, "\!}");
        if ($end > $start)
        {
            my ($substr) = substr($out, $start + 2, $end - $start - 2);
            my ($var, $sel) = split(/\//, $substr, 2);

            if (defined($ses->getValue("SUBMITTED_$var")))
            {
                my ($submitted) = $ses->getValue("SUBMITTED_$var");

                my (@cases) = split(/\,/, $sel);
                my ($cell, %values);

                foreach $cell (@cases)
                {
                    my ($val, $cap) = split(/\:/, $cell, 2);
                    $values{$val} = $cap;
                }

                $contents = $values{$submitted};

                $contents = substr($out, 0, $start) . $contents;
                $contents .= substr($out, $end + 2);

                $out = $contents;
            }
            else
            {
                $self->{ERROR}     = lprint("The variable") . " \"$var\" " . lprint("has not been submitted");
                $self->{ERRORCODE} = 99;
            }

        }
        else
        {
            $self->{ERROR}     = lprint("Found \"{\!\" without \"\!}\"");
            $self->{ERRORCODE} = 99;
        }
    }

    return $out;
}

sub ParseDyn
{
    my ($crap, $doc, $ses, $str) = @_;

    if (!defined($ses))
    {
        return $str;
    }

    $str = Survey::Component::Import->ParseSel($doc, $str, $ses);
    $str = Survey::Component::Import->ParseCap($doc, $str, $ses);
    $str = Survey::Component::Import->ParseVar($doc, $str, $ses);

    return $str;
}

1;

