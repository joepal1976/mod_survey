#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::Custom;
use strict;

@Survey::Component::Custom::ISA = qw(Survey::Component::Component);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub FillParams
{
    my ($crap, $self) = @_;

    @{ $self->{"ALLOWED_CUSTOM"} } =
      ("ESCAPED", "VARIABLES", "NUMERICAL", "MUSTANSWER", "MUSTMSG", "ILLEGALVAL", "CAPTION");
    @{ $self->{"ALL_CUSTOM"} } = (@{ $self->{"ALLOWED_CUSTOM"} }, "TYPE", "RAW");

    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;

    $self->{"CUSTOM_TYPE"}       = "CUSTOM";
    $self->{"CUSTOM_ESCAPED"}    = "yes";
    $self->{"CUSTOM_VARIABLES"}  = "";
    $self->{"CUSTOM_NUMERICAL"}  = "no";
    $self->{"CUSTOM_MUSTANSWER"} = "no";
    $self->{"CUSTOM_CAPTION"}    = "";
    $self->{"CUSTOM_MUSTMSG"}    = "";
    $self->{"CUSTOM_ILLEGALVAL"} = "-1";

    1;
}

sub PlaceComponent
{
    my ($crap, $self, $paramstr) = @_;

    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    my (%params, $cell, $name, $value, @pararr);

    %params = $self->GetDefaults("CUSTOM");

    @pararr = @{$paramstr};

    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "CUSTOM");
    }

    if ($params{empty} eq "yes")
    {
        $self->{ERROR}     = lprint("A CUSTOM tag cannot be immediately terminated (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 10;
    }

    if (!$self->{ERROR})
    {
        my ($endtag) = index($self->{WORK}, "</CUSTOM>");
        if ($endtag > -1)
        {
            $params{"RAW"} = substr($self->{WORK}, 0, $endtag);
            $self->{WORK} = substr($self->{WORK}, 0 - length($self->{WORK}) + length($params{"RAW"}) + 10);
            $params{"RAW"} =~ s/^[\x0a\ ]+//;
            $params{"RAW"} =~ s/^[\x0a\x0d]+//;
            $params{"RAW"} =~ s/^\x0a+//;
            $params{"RAW"} =~ s/^\ +//;
            $params{"RAW"} =~ s/[\x0a\ ]+$//;
            $params{"RAW"} =~ s/[\x0a\x0d]+$//;
            $params{"RAW"} =~ s/\x0a+$//;
            $params{"RAW"} =~ s/\ +$//;
        }
        else
        {
            $self->{ERROR}     = lprint("A CUSTOM tag must have an end tag (in ") . $self->{FILE} . ")";
            $self->{ERRORCODE} = 14;
        }
    }

    if (!$self->{ERROR} && ($params{"ESCAPED"} eq "yes"))
    {
        $_ = $params{"RAW"};
        if (/[\<\>]/)
        {
            $self->{ERROR} =
                lprint("A CUSTOM tag cannot contain")
              . " \"\&lt\;\" "
              . lprint("or")
              . " \"\&gt\;\" "
              . lprint("when ESCAPED is")
              . " \"yes\"";
            $self->{ERRORCODE} = 32;
        }
    }

    if (!$self->{ERROR} && $params{"VARIABLES"})
    {

        # New addition: Varibles in CUSTOM blocks

        my (@vars) = split(/,/, $params{"VARIABLES"});
        my ($var, $name);

        foreach $var (@vars)
        {
            if ((!$self->{ERROR}) && (length($var) > 8))
            {
                $self->{ERROR} = lprint("A CUSTOM tag name can only be 8 characters wide (in ") . $self->{FILE} . ")";
                $self->{ERRORCODE} = 21;
            }

            if (!$self->{ERROR}) { $self->CheckName($params{NAME}, "CUSTOM"); }

            $name = "TAGNAME_" . $var;
            $name = "\U$name";

            if (defined($self->{$name}))
            {
                $self->{ERROR}     = lprint("A CUSTOM tag must have unique name (in ") . $self->{FILE} . ")";
                $self->{ERRORCODE} = 20;
            }
            else
            {
                $self->{$name} = $self->{NUMTAGS};
            }
        }
    }

    $self->PlaceParams("CUSTOM", $tn, %params);

    $self->{NUMTAGS}++;

    1;
}

sub PrintComponent
{
    my ($crap, $self, $tagno) = @_;
    my ($doc) = $self->{DOCUMENT};
    my ($out) = "";

    my (@vars) = split(/,/, $doc->GetTagParam($tagno, "VARIABLES"));
    my ($var, $name);

    foreach $var (@vars)
    {
        $doc->SetVisited($var);
    }

    my ($raw) = $doc->GetTagParam($tagno, "RAW");

    if ($doc->GetTagParam($tagno, "ESCAPED") eq "yes")
    {
        $raw =~ s/\&lt\;/\</g;
        $raw =~ s/\&gt\;/\>/g;
    }

    $out .= $raw;
    $out .= "\n\n";

    return $out;
}

sub NumberOfValues
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;

    my (@vars) = split(/,/, $doc->GetTagParam($tagno, "VARIABLES"));
    my ($var, $name);

    return scalar(@vars);
}

sub GetValueNumerical
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;

    if ($doc->GetTagParam($tagno, "NUMERICAL") eq "yes")
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

sub GetValueName
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;

    my (@vars) = split(/,/, $doc->GetTagParam($tagno, "VARIABLES"));
    my ($var, $name);

    $name = "";
    if (scalar(@vars) > 0)
    {
        $name = $vars[$valuenumber - 1];
    }

    return $name;
}

sub GetCheckValue
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;

    my ($value) = {};
    my ($name) = Survey::Component::Custom->GetValueName($doc, $sub, $arg, $ses, $tagno, $valuenumber);

    my ($visited) = $doc->CheckVisited($name);

    my ($wasillegal) = 0;

    if ($visited)
    {
        if   (defined($arg->ArgByName($name))) { $value->{VALUE} = $arg->ArgByName($name); }
        else                                   { $value->{VALUE} = ""; }
    }
    else
    {
        $value->{VALUE} = $doc->GetOption("NOTDISPLAYEDVAL");
    }

    if ($value->{VALUE} eq "")
    {
        if (($doc->GetTagParam($tagno, "MUSTANSWER") eq "yes") && (!$sub->{SAVE}))
        {
            my ($tagcap) = $doc->GetTagParam($tagno, "CAPTION");
            my ($mmsg)   = $doc->GetTagParam($tagno, "MUSTMSG");
            if ($mmsg ne "")
            {
                $sub->{ERROR}     = lprint($mmsg);
                $sub->{ERRORCODE} = 11;
            }
            else
            {
                if ($doc->GetTagParam($tagno, "CAPTION"))
                {
                    $sub->{ERROR} =
                        lprint("The question with caption") 
                      . " <i>\""
                      . $doc->GetTagParam($tagno, "CAPTION")
                      . "\"</i> "
                      . lprint("must be answered.");
                }
                else
                {

                    $sub->{ERROR} = lprint("A CUSTOM question must be answered.");
                }
                $sub->{ERRORCODE} = 2;
            }
        }
        else
        {
            $value->{VALUE} = $doc->GetTagParam($tagno, "ILLEGALVAL");
            $wasillegal = 1;
        }
    }

    my ($numerical) = $doc->GetTagParam($tagno, "NUMERICAL");
    if (grep(/^$numerical$/, ("yes", "float")) && (!$wasillegal))
    {
        my ($crap) = "-13412343dd";
        my ($val);

        # check format of float (adapted from patch by MH)
        if ($numerical eq "float")
        {
            $val = $value->{VALUE};

            $val = '0' . $val if $val =~ /^[.,]/;
            if ($val =~ /^0*(\d+?(?:[,.]\d*)?)$/)
            {
                $val = $1;
                $val =~ s/[,.]/\./;
                $crap = $val;
            }
            else
            {
                $val = "";
            }
            $value->{VALUE} = $val;
        }
        else    # remove leading zeros (adapted from patch by MH)
        {

            #int removes the leading zeros itself (BugAnt)
            $val = int($value->{VALUE});
        }

        # final check if answer was numerical at all
        # if there is no error and the value isn't  int (otherwise int() return 0=false)
        # or the value is not a zero then the value isn't numerical (BugAnt)
        if ((!$sub->{ERROR}) && (!((int($value->{VALUE})) || ($value->{VALUE} eq "0"))))
        {
            $sub->{"ERROR"} =
              lprint("Answer must be numerical (to question with caption") . " \""
              . $doc->GetTagParam($tagno, "CAPTION") . "\")";
            $sub->{"ERRORCODE"} = 7;
        }
        else
        {
            $value->{VALUE} = $val;
        }
    }

    $value->{NAME} = $name;
    $value->{NUMERICAL} = Survey::Component::Custom->GetValueNumerical($doc, $sub, $arg, $ses, $tagno);

    return $value;
}

sub MakeXML
{
    my ($s, $doc, $tagno) = @_;

    my ($out) = $s->MakeTagOpener($doc, $tagno);

    $out .= $doc->GetTagParam($tagno, "RAW");
    $out .= "\n";
    $out .= $s->MakeTagCloser($doc, $tagno);

    return $out;
}

1;

