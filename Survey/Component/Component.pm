#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::Component;
use strict;

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub FillParams
{
    my ($self) = shift;
    die "OO Exception: The FillParams() method must be overridden (by all descentants of Survey::Component::Component)";
    1;
}

sub FillDefaults
{
    my ($self) = shift;
    die
      "OO Exception: The FillDefaults() method must be overridden (by all descentants of Survey::Component::Component)";
    1;
}

sub PlaceComponent
{
    my ($self) = shift;
    die
"OO Exception: The PlaceComponent() method must be overridden (by all descentants of Survey::Component::Component)";
    1;
}

sub PrintComponent
{
    my ($self) = shift;
    die
"OO Exception: The PrintComponent() method must be overridden (by all descentants of Survey::Component::Component)";
    1;
}

sub StartEnclosure
{
    my ($crap, $type) = @_;
    return "    <div id=\"mdscomponent\" class=\"$type\">\n";
}

sub EndEnclosure
{
    return "    </div>\n";
}

sub NumberOfValues
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 0;
}

sub GetValueNumerical
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 0;
}

sub GetCheckValue
{
    my ($self, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;
    return 0;
}

sub GetValueName
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;
    return $doc->GetTagParam($tagno, "NAME");
}

sub GetValueCaption
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $value) = @_;
    return $value;
}

sub GetVariableCaption
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;
    return $doc->GetTagParam($tagno, "CAPTION");
}

sub GetElementIsVariable
{
    my ($s, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 0;
}

sub GetPossibleValues
{
    my ($s, $doc, $sub, $arg, $ses, $tagno) = @_;
    my (@arr) = ();
    return \@arr;
}

sub GetFieldLength
{
    my ($self, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;
    if ($self->GetValueNumerical($doc, $sub, $arg, $ses, $tagno, $valuenumber))
    {
        return 10;    # Here assuming no integer value reaches ten billion (it can't because of 32bit)
                      # this needs check for float values.
    }
    else
    {
        return $doc->GetTagParam($tagno, "MAXLEN");
    }
}

sub MakeMultiSplit
{
    my ($s, $doc, $sub, $arg, $ses, $tagno) = @_;
    my (@arr) = ();
    return \@arr;
}

sub MakeTagHead
{
    my ($s, $doc, $tagno) = @_;

    my ($type) = $doc->GetTagParam($tagno, "TYPE");

    my (@allowed) = @{ $doc->{"ALLOWED_$type"} };
    my ($par);

    my ($out) = "<$type ";

    foreach $par (@allowed)
    {
        my ($default) = $doc->{ $type . "_" . $par };
        my ($value) = $doc->GetTagParam($tagno, "$par");
        if ($value ne $default)
        {
            $out .= "$par=\"" . $value . "\" ";
        }
    }

    return $out;
}

sub MakeTagOpener
{
    my ($s, $doc, $tagno) = @_;
    my ($out) = $s->MakeTagHead($doc, $tagno);
    $out .= ">\n";
    return $out;
}

sub MakeTagCloser
{
    my ($s, $doc, $tagno) = @_;
    my ($type) = $doc->GetTagParam($tagno, "TYPE");
    return "</$type>\n\n";
}

sub MakeXML
{
    my ($s, $doc, $tagno) = @_;

    my ($out) = $s->MakeTagHead($doc, $tagno);
    $out .= "/>\n\n";

    return $out;
}

1;

