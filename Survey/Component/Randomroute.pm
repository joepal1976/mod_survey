#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::Randomroute;
use strict;

@Survey::Component::Randomroute::ISA = qw(Survey::Component::Component);

use Survey::Language;
use Text::ParseWords;
use Survey::Component::Component;
use Survey::Debug;

use CGI;

sub FillParams
{
    my ($crap, $self) = @_;

    @{ $self->{"ALLOWED_RANDOMROUTE"} } = ("MEAN", "DISTRIBUTION");
    @{ $self->{"ALL_RANDOMROUTE"} } = (@{ $self->{"ALLOWED_RANDOMROUTE"} }, "TYPE", "RAW", "SUBTAGS");

    @{ $self->{"ALLOWED_ALTERNATIVE"} } = ("VALUE", "CONTINUE");
    @{ $self->{"ALL_ALTERNATIVE"} } = (@{ $self->{"ALLOWED_ALTERNATIVE"} }, "TYPE");

    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;

    $self->{"RANDOMROUTE_TYPE"}         = "RANDOMROUTE";
    $self->{"RANDOMROUTE_MEAN"}         = "1";
    $self->{"RANDOMROUTE_DISTRIBUTION"} = "square";
    $self->{"RANDOMROUTE_RAW"}          = "";

    1;
}

sub PlaceComponent
{
    my ($crap, $self, $paramstr) = @_;

    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    my (%params, $cell, $name, $value, @pararr);

    %params = $self->GetDefaults("RANDOMROUTE");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell, 2);
        $params{$name} = $value;
        $self->CheckParam($name, "RANDOMROUTE");
    }

    if ($params{empty} eq "yes")
    {
        $self->{ERROR}     = lprint("A RANDOMROUTE tag cannot be immediately terminated (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 10;
    }

    if (!$self->{ERROR})
    {
        my ($endtag) = index($self->{WORK}, "</RANDOMROUTE>");
        if ($endtag > -1)
        {
            $params{"RAW"} = substr($self->{WORK}, 0, $endtag - 1);
            $self->{WORK} = substr($self->{WORK}, 0 - length($self->{WORK}) + length($params{"RAW"}) + 15);

        }
        else
        {
            $self->{ERROR}     = lprint("A RANDOMROUTE tag must have an end tag (in ") . $self->{FILE} . ")";
            $self->{ERRORCODE} = 14;
        }
    }

    if ($self->{ERROR})
    {
        return;
    }

    $params{WORK} = $params{"RAW"};

    my ($e) = 1;
    my ($s) = 1;

    my (@subtags);
    my ($currsub) = 0;

    while (($e ne -1) && (!$self->{ERROR}))
    {
        $s = index($params{WORK}, "<");
        $e = index($params{WORK}, ">");

        my $ret;

        if ($e ne -1)
        {

            if ($s) { $e = $e - $s; }
            $ret = substr($params{WORK}, $s, $e + 1);
            if ($s) { $params{WORK} = substr($params{WORK}, 0 - length($params{WORK}) + $s); }
            $params{WORK} = substr($params{WORK}, 0 - length($params{WORK}) + length($ret));

            my ($subtag, $p, $type, $rest);

            if (!$self->{ERROR})
            {
                ($type, $rest) = $self->CleanUpTag($ret);

                if ($type eq "ALTERNATIVE")
                {
                    $currsub++;
                    Survey::Component::Randomroute->PlaceAlternativeTag($self, $rest, $tn, $currsub);
                }
                else
                {
                    $self->{ERROR}     = lprint("Only ALTERNATIVE tags are allowed in a RANDOMROUTE tag");
                    $self->{ERRORCODE} = 99;
                }
            }
        }
    }
    $params{"SUBTAGS"} = $currsub;

    if (!$currsub)
    {
        $self->{ERROR}     = lprint("A RANDOMROUTE tag must contain ALTERNATIVE statements.");
        $self->{ERRORCODE} = 99;
    }

    $self->SetOption("ROUTERTAG",  $self->{NUMTAGS});
    $self->SetOption("ROUTERTYPE", "RANDOM");

    $self->PlaceParams("RANDOMROUTE", $tn, %params);
    $self->{NUMTAGS}++;

    $self->SetOption("MULTIPAGE", 1);

    1;
}

sub PrintComponent
{
    my ($self) = shift;
    return "";
}

sub PlaceAlternativeTag
{
    my ($crap, $self, $paramstr, $tn, $nr) = @_;

    my ($thisparam) = $tn . "ALTERNATIVE" . $nr . "_";

    my (%params, $cell, $name, $value, @pararr, $e);

    %params = $self->GetDefaults("ALTERNATIVE");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell, 2);
        $params{$name} = $value;
        $self->CheckParam($name, "ALTERNATIVE");
    }

    if ($params{empty} eq "no")
    {
        $self->{ERROR}     = lprint("An ALTERNATIVE tag must be immediately terminated (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 11;
    }

    if ((!$self->{ERROR}) && (!$params{"CONTINUE"}))
    {
        $self->{ERROR}     = lprint("CONTINUE is a required parameter in an ALTERNATIVE tag.");
        $self->{ERRORCODE} = 99;
    }

    foreach $e (@{ $self->{"ALL_ALTERNATIVE"} })
    {
        $self->{ $thisparam . $e } = $params{$e};
    }
}

sub DoRouting
{
    my ($crap, $sub, $doc, $arg, $ses, $tagno) = @_;

    $sub->{IMPLICITCONTINUE} = 1;

    $doc->SetOption("REDIRECT", "yes");

    my ($tags) = $doc->GetTagParam($tagno, "SUBTAGS");

    my ($i) = int(rand($tags) + 1);    # Random square within $tags

    my ($continue) = $doc->GetAlternativeParam($tagno, $i, "CONTINUE");

    $doc->SetOption("CONTINUE", $continue);

    1;
}

1;
