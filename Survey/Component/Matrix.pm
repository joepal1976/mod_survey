#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Component::Matrix;
use strict;

@Survey::Component::Matrix::ISA = qw(Survey::Component::Component);

use Survey::Language;
use Text::ParseWords;
use Survey::Debug;

use CGI;

sub FillParams
{
    my ($crap, $self) = @_;

    @{ $self->{"ALLOWED_MATRIX"} } = ("NAME",
                                      "BORDER",
                                      "COLWIDTH",
                                      "STYLE",
                                      "ILLEGALVAL",
                                      "RANDOM",
                                      "MUSTANSWER",
                                      "CAPTION",
                                      "CELLSPACING",
                                      "CELLPADDING",
                                      "UNIQUECOLUMNS",
                                      "UNIQUEROWS",
                                      "MULTI");
    @{ $self->{"ALL_MATRIX"} } =
      (@{ $self->{"ALLOWED_MATRIX"} }, "RAW", "ELEMENTS", "TYPE", "MODE", "ROWS", "COLS", "MAXLEN", "FILE");

    @{ $self->{"ALLOWED_MATRIXCOLUMN"} } = ("CAPTION", "VALUE");
    @{ $self->{"ALL_MATRIXCOLUMN"} } = (@{ $self->{"ALLOWED_MATRIXCOLUMN"} });

    @{ $self->{"ALLOWED_MATRIXROW"} } = ("CAPTION", "VALUE");
    @{ $self->{"ALL_MATRIXROW"} } = (@{ $self->{"ALLOWED_MATRIXROW"} });

    1;
}

sub FillDefaults
{
    my ($crap, $self) = @_;

    $self->{"MATRIX_TYPE"}          = "MATRIX";
    $self->{"MATRIX_NAME"}          = "";
    $self->{"MATRIX_BORDER"}        = "0";
    $self->{"MATRIX_STYLE"}         = "";
    $self->{"MATRIX_ILLEGALVAL"}    = "-1";
    $self->{"MATRIX_COLWIDTH"}      = "80";
    $self->{"MATRIX_MAXLEN"}        = "80";
    $self->{"MATRIX_RANDOM"}        = "none";
    $self->{"MATRIX_MUSTANSWER"}    = "no";
    $self->{"MATRIX_CAPTION"}       = "";
    $self->{"MATRIX_UNIQUECOLUMNS"} = "no";
    $self->{"MATRIX_UNIQUEROWS"}    = "yes";
    $self->{"MATRIX_MULTI"}         = "no";
    $self->{"MATRIX_CELLSPACING"}   = "2";
    $self->{"MATRIX_CELLPADDING"}   = "2";

    1;
}

sub PlaceComponent
{
    my ($crap, $self, $paramstr) = @_;

    my ($tn) = "TAG" . $self->{NUMTAGS} . "_";

    my (%params, %epara, $cell, $cell2, $name, $value, @pararr, $work, $tag, $s, $e, $elems);

    %params = $self->GetDefaults("MATRIX");

    @pararr = @{$paramstr};
    foreach $cell (@pararr)
    {
        ($name, $value) = split(/=/, $cell);
        $params{$name} = $value;
        $self->CheckParam($name, "MATRIX");
    }

    if ($params{empty} eq "yes")
    {
        $self->{ERROR}     = lprint("A MATRIX tag cannot be immediately terminated (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 10;
    }

    if ((!$self->{ERROR}) && (!$params{NAME}))
    {
        $self->{ERROR}     = lprint("A MATRIX tag must have a name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 12;
    }

    if ((!$self->{ERROR}) && (length($params{NAME}) > 6))
    {
        $self->{ERROR}     = lprint("A MATRIX tag name can only be 6 characters wide (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 23;
    }

    if (!$self->{ERROR}) { $self->CheckName($params{NAME}, "MATRIX"); }

    if ((!$self->{ERROR}) && ($params{UNIQUECOLUMNS} eq "no") && ($params{UNIQUEROWS} eq "no"))
    {
        $self->{ERROR} =
          lprint("It's no possible to set both UNIQUECOLUMNS and UNIQUEROWS to \"no\" (in ") . $self->{FILE} . ")";
    }

    if (   (!$self->{ERROR})
        && ($params{UNIQUECOLUMNS} eq "yes")
        && ($params{UNIQUEROWS}    eq "yes")
        && ($params{MULTI}         eq "yes"))
    {
        $self->{ERROR} =
          lprint(
"It's no possible to set both UNIQUECOLUMNS and UNIQUEROWS to \"yes\" with MULTI parameter set to \"yes\" too (in ")
          . $self->{FILE} . ")" . "\n"
          . lprint("Set MULTI to \"no\" or change status of one of the two UNIQUE parameters.");
    }

    if   ($params{UNIQUECOLUMNS} eq "yes") { $params{MODE} = "col"; }
    else                                   { $params{MODE} = "row"; }

    if (!$self->{ERROR})
    {
        my ($rnd) = $params{RANDOM};
        if (!grep(/^$rnd$/, ("none", "column", "row", "both")) && $rnd)
        {
            $self->{ERROR} =
              lprint("A MATRIX's RANDOM parameter can only have the values 'none', 'column', 'row' or 'both'");
            $self->{ERRORCODE} = 99;
        }
    }

    if (!$self->{ERROR})
    {
        my ($endtag) = index($self->{WORK}, "</MATRIX>");
        if ($endtag eq 0)
        {
            $self->{ERROR} =
              lprint("A MATRIX tag must contain at least one MATRIXCOLUMN or at least one MATRIXROW (in ")
              . $self->{FILE} . ")";
            $self->{ERRORCODE} = 24;
        }
        else
        {
            if ($endtag > 0)
            {
                my ($valid) = "";
                if ($self->{WORK} =~ m/<\s*MATRIXCOLUMN/) { $valid .= "column"; }
                if ($self->{WORK} =~ m/<\s*MATRIXROW/)    { $valid .= "-row"; }
                if ($valid eq "column")
                {
                    $self->{ERROR} =
                      lprint("A MATRIX tag must contain at least one MATRIXROW (in ") . $self->{FILE} . ")";
                    $self->{ERRORCODE} = 24;    #  is it right?
                }
                elsif ($valid eq "-row")
                {
                    $self->{ERROR} =
                      lprint("A MATRIX tag must contain at least one MATRIXCOLUMN (in ") . $self->{FILE} . ")";
                    $self->{ERRORCODE} = 24;    #  is it right?
                }
                elsif ($valid eq "column-row")
                {
                    $params{"RAW"} = substr($self->{WORK}, 0, $endtag - 1);
                    $self->{WORK} = substr($self->{WORK}, 0 - length($self->{WORK}) + length($params{"RAW"}) + 10);
                }
            }
            else
            {
                $self->{ERROR}     = lprint("A MATRIX tag must have an end tag (in ") . $self->{FILE} . ")";
                $self->{ERRORCODE} = 14;
            }
        }
    }

    my ($rows) = 0;
    my ($cols) = 0;

    if (!$self->{ERROR})
    {
        $elems = 0;
        $work  = $params{"RAW"};
        $s     = index($work, "<");
        $e     = index($work, ">");

        while (($s ne -1) && ($e ne -1) && (!$self->{ERROR}))
        {
            if ($s) { $e = $e - $s }
            $tag = substr($work, $s, $e + 1);
            if ($s)
            {
                $work = substr($work, 0 - length($work) + $s);
            }
            $work = substr($work, 0 - length($work) + length($tag));
            ($cell, $tag) = $self->CleanUpTag($tag);

            if (($cell ne "MATRIXCOLUMN") && ($cell ne "MATRIXROW"))
            {
                $self->{ERROR} =
                  lprint("A MATRIX block can only contain MATRIXCOLUMNs e MATRIXROWs (in ") . $self->{FILE} . ")";
                $self->{ERRORCODE} = 25;
            }
            else
            {
                $epara{"CAPTION"} = "";
                $epara{"VALUE"}   = "";
                $epara{"TYPE"}    = "";

                @pararr = @{$tag};
                foreach $cell2 (@pararr)
                {
                    ($name, $value) = split(/=/, $cell2);
                    $epara{"TYPE"} = $cell;
                    $epara{$name} = $value;
                }

                if ($epara{"empty"} eq "no")
                {
                    $self->{ERROR} =
                      lprint(
                        "A MATRIXELEMENT (both MATRIXCOLUMN and MATRIXROW one) tag must be terminated immediately (in ")
                      . $self->{FILE} . ")";
                    $self->{ERRORCODE} = 11;
                }
            }

            if (!$self->{ERROR})
            {
                if ($cell eq "MATRIXCOLUMN")
                {
                    $cols++;
                    $s = $tn . "MC" . $cols . "_";
                }
                else
                {
                    $rows++;
                    $s = $tn . "MR" . $rows . "_";
                    if ($params{UNIQUECOLUMNS} eq "yes")
                    {
                        if (($epara{"VALUE"} eq "") || (!defined($epara{"VALUE"})))
                        {
                            $self->{ERROR}     = lprint("If UNIQUECOLUMNS, then MATRIXROW must have a value.");
                            $self->{ERRORCODE} = 99;
                        }
                    }
                }

                $self->{ $s . "CAPTION" } = $epara{"CAPTION"};
                $self->{ $s . "VALUE" }   = $epara{"VALUE"};
                $self->{ $s . "TYPE" }    = $epara{"TYPE"};

                $elems++;
            }

            $s = index($work, "<");
            $e = index($work, ">");
        }
        $params{"ELEMENTS"} = $elems;
        $params{"ROWS"}     = $rows;
        $params{"COLS"}     = $cols;

    }

    if (($elems eq 0) && (!$self->{ERROR}))
    {
        $self->{ERROR} =
          lprint("A MATRIX tag must contain at least one MATRIXELEMENT (either COLUMNs or ROWs) (in ")
          . $self->{FILE} . ")";
        $self->{ERRORCODE} = 24;
    }

    $name = "TAGNAME_" . $params{"NAME"};
    $name = "\U$name";

    if (defined($self->{$name}))
    {
        $self->{ERROR}     = lprint("A MATRIX tag must have unique name (in ") . $self->{FILE} . ")";
        $self->{ERRORCODE} = 20;
    }
    else
    {
        $self->{$name} = $self->{NUMTAGS};
    }

    $self->PlaceParams("MATRIX", $tn, %params);
    $self->{NUMTAGS}++;

    1;
}

sub GetColumnData
{
    my ($crap, $doc, $tagno) = @_;
    my ($name) = $doc->GetTagParam($tagno, "NAME");

    my ($matrixdata) = {};

    my (@variables, @columns, @rows, $i, $type, $eno, $rname);

    $eno = 1;

    my ($colcount) = $doc->GetTagParam($tagno, "COLS");
    my ($rowcount) = $doc->GetTagParam($tagno, "ROWS");

    for ($i = 1 ; $i <= $colcount ; $i++)
    {
        my ($column) = {};
        $column->{CAPTION} = $doc->GetMatrixColumnParam($tagno, $i, "CAPTION");
        $rname = $name;
        if ($eno < 10) { $rname .= 0; }
        $rname .= $eno;
        $column->{NAME} = $rname;
        push(@columns,   $column);
        push(@variables, $rname);
        $eno++;
    }

    for ($i = 1 ; $i <= $rowcount ; $i++)
    {
        my ($row) = {};
        $row->{CAPTION} = $doc->GetMatrixRowParam($tagno, $i, "CAPTION");
        $row->{VALUE}   = $doc->GetMatrixRowParam($tagno, $i, "VALUE");

        push(@rows, $row);
    }

    $matrixdata->{ROWS}      = \@rows;
    $matrixdata->{COLUMNS}   = \@columns;
    $matrixdata->{VARIABLES} = \@variables;

    return $matrixdata;
}

sub GetRowData
{
    my ($crap, $doc, $tagno) = @_;
    my ($elements) = $doc->GetTagParam($tagno, "ELEMENTS");
    my ($name)     = $doc->GetTagParam($tagno, "NAME");

    my ($matrixdata) = {};

    my (@variables, @columns, @rows, $i, $type, $eno, $rname);

    $eno = 1;

    my ($colcount) = $doc->GetTagParam($tagno, "COLS");
    my ($rowcount) = $doc->GetTagParam($tagno, "ROWS");

    for ($i = 1 ; $i <= $colcount ; $i++)
    {
        my ($column) = {};
        $column->{CAPTION} = $doc->GetMatrixColumnParam($tagno, $i, "CAPTION");
        $column->{VALUE}   = $doc->GetMatrixColumnParam($tagno, $i, "VALUE");
        push(@columns, $column);
    }

    for ($i = 1 ; $i <= $rowcount ; $i++)
    {
        my ($row) = {};
        $row->{CAPTION} = $doc->GetMatrixRowParam($tagno, $i, "CAPTION");
        $rname = $name;
        if ($eno < 10) { $rname .= 0; }
        $rname .= $eno;
        $row->{NAME} = $rname;
        push(@rows,      $row);
        push(@variables, $rname);
        $eno++;
    }

    $matrixdata->{ROWS}      = \@rows;
    $matrixdata->{COLUMNS}   = \@columns;
    $matrixdata->{VARIABLES} = \@variables;

    return $matrixdata;
}

sub PrintComponent
{
    my ($crap, $self, $tagno) = @_;
    my ($doc) = $self->{DOCUMENT};
    my $ses   = $doc->{SESSION};
    my ($sp)  = "      ";

    my $tmpl = new Survey::Template($ENV{_SURVEY_HOME} . "/templates/default/Matrix.tmpl");

    my ($thistagname) = $doc->GetTagParam($tagno, "NAME");

    my $valueErrorDescription = $ses->getValue("VALUE_ERROR_DESCRIPTION_$thistagname");

    if ($valueErrorDescription)
    {
        my $value = $ses->getValue("VALUE_ERROR_$thistagname");

        $tmpl->setVar("errorDescription", $valueErrorDescription);
        $ses->setValue("VALUE_ERROR_$thistagname",             "");
        $ses->setValue("VALUE_ERROR_DESCRIPTION_$thistagname", "");
    }

    my ($matrixdata, @rows, @columns, @variables);

    my ($type)  = "radio";
    my ($typec) = "MATRIXrb";

    if ($doc->GetTagParam($tagno, "MULTI") eq "yes") { $type = "checkbox"; $typec = "MATRIXcb"; }

    my ($cm) = 0;

    if ($doc->GetTagParam($tagno, "UNIQUECOLUMNS") eq "yes")
    {
        $matrixdata = Survey::Component::Matrix->GetColumnData($doc, $tagno);
        $cm = 1;    # we're in column mode
    }
    else
    {
        $matrixdata = Survey::Component::Matrix->GetRowData($doc, $tagno);
    }

    @rows      = @{ $matrixdata->{ROWS} };
    @columns   = @{ $matrixdata->{COLUMNS} };
    @variables = @{ $matrixdata->{VARIABLES} };

    foreach my $variable (@variables) { $doc->SetVisited($variable); }

    my ($border) = $doc->GetTagParam($tagno, "BORDER");
    if ($border eq "yes") { $border = "1"; }
    if ($border eq "no")  { $border = "0"; }

    $tmpl->setVar("border",      $border);
    $tmpl->setVar("cellspacing", $doc->GetTagParam($tagno, "CELLSPACING"));
    $tmpl->setVar("cellpadding", $doc->GetTagParam($tagno, "CELLPADDING"));

    # 270402 MH Added Caption (adapted 20030705/JP)
    if ($doc->GetTagParam($tagno, "CAPTION"))
    {
        $tmpl->setVar("caption", $doc->GetTagParam($tagno, "CAPTION"));
    }

    # 270402 MH End

    #reimplementing the random order in matrix (bugant)

    if ($doc->GetTagParam($tagno, "RANDOM") && ($doc->GetTagParam($tagno, "RANDOM") ne "none"))
    {

        #put (maybe) the question in a random order
        if (($doc->GetTagParam($tagno, "RANDOM") eq "row") || ($doc->GetTagParam($tagno, "RANDOM") eq "both"))
        {

            #reorder @row in a rand way
            my ($ord) = ($#rows + 1);
            my ($temp);

            foreach (reverse 0 .. $ord)
            {
                $temp = splice(@rows, rand $ord, 1);
                push @rows, $temp;
            }
        }

        #put (maybe) the answer in a random order
        if (($doc->GetTagParam($tagno, "RANDOM") eq "column") || ($doc->GetTagParam($tagno, "RANDOM") eq "both"))
        {

            #reorder @column in a rand way
            my ($ord) = ($#columns + 1);
            my ($temp);

            foreach (reverse 0 .. $ord)
            {
                $temp = splice(@columns, rand $ord, 1);
                push @columns, $temp;
            }
        }
    }

    $tmpl->enterLoop("FIRSTROW");

    #foreach my $column (@columns)
    for (my $col = 0 ; $col <= scalar(@columns) ; $col++)
    {

        my $styleClasses = "cell1_" . ($col + 1) . "MATRIX unevenRowMATRIX firstRowMATRIX";

        # note:
        # $col starts with 0 which stands for 1st column.
        # but 1 is uneven, so if $col is even, print uneven and vice versa

        if    ($col == 0) { $styleClasses .= " firstColMATRIX"; }
        elsif ($col & 1)  { $styleClasses .= " evenColMATRIX"; }
        else              { $styleClasses .= " unevenColMATRIX"; }

        if ($col == scalar(@columns)) { $styleClasses .= " lastColMATRIX"; }

        $tmpl->setVar("styleClasses", $styleClasses);

        if ($col > 0)
        {
            my $columnData = $columns[$col - 1];
            $tmpl->setVar("colCaption", $columnData->{CAPTION});
        }
        else
        {
            $tmpl->setVar("colCaption", "&nbsp;");
        }

        $tmpl->nextLoop();    # FIRSTROW
    }
    $tmpl->exitLoop();        # FIRSTROW

    $tmpl->enterLoop("ROWS");

    #foreach my $row (@rows)
    for (my $rowNumber = 0 ; $rowNumber < scalar(@rows) ; $rowNumber++)
    {
        my $rowData = $rows[$rowNumber];
        $tmpl->enterLoop("COLS");

        #foreach my $column (@columns)
        for (my $col = 0 ; $col <= scalar(@columns) ; $col++)
        {
            my $styleClasses = "cell" . ($rowNumber + 2) . "_" . ($col + 1) . "MATRIX";

            if   ($rowNumber & 1) { $styleClasses .= " unevenRowMATRIX"; }
            else                  { $styleClasses .= " evenRowMATRIX"; }

            if ($col == 0)
            {
                $tmpl->setVar("rowCaption", $rowData->{CAPTION});
                $styleClasses .= " firstColMATRIX";
            }
            elsif ($col & 1) { $styleClasses .= " evenColMATRIX"; }
            else             { $styleClasses .= " unevenColMATRIX"; }

            if ($col == scalar(@columns))
            {
                $styleClasses .= " lastColMATRIX";

                if   ($rowNumber & 1) { $styleClasses .= " lastColUnevenRowMATRIX"; }
                else                  { $styleClasses .= " lastColEvenRowMATRIX"; }

            }

            $tmpl->setVar("styleClasses", $styleClasses);
            $tmpl->setVar("type",         $type);
            $tmpl->setVar("typeC",        $typec);

            if ($col > 0)
            {
                my $columnData = $columns[$col - 1];

                if ($cm)
                {
                    $tmpl->setVar("name", $columnData->{NAME});

                    #bugant persist ;)
                    my ($chkpersist) = ($doc->{SESSION}->getValue("SUBMITTED_" . $columnData->{NAME}));

                    if ($chkpersist eq $rowData->{VALUE}) { $tmpl->setVar("checked", 1); }

                    #bugant persisted ;)

                    $tmpl->setVar("value", $rowData->{VALUE});
                }
                else
                {
                    $tmpl->setVar("name", $rowData->{NAME});

                    #bugant persist ;)
                    my ($thistagname) = $doc->GetTagParam($tagno, "NAME");
                    my ($chkpersist) = ($doc->{SESSION}->getValue("SUBMITTED_" . $rowData->{NAME}));

                    if ($chkpersist eq $columnData->{VALUE}) { $tmpl->setVar("checked", 1); }

                    #bugant persisted ;)

                    $tmpl->setVar("value", $columnData->{VALUE});
                }
            }
            $tmpl->nextLoop();    # COLS
        }
        $tmpl->exitLoop();        # COLS

        $tmpl->nextLoop();        # ROWS
    }
    $tmpl->exitLoop();            # ROWS

    return $tmpl->get();
}

sub NumberOfValues
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;

    my ($matrixdata);

    if ($doc->GetTagParam($tagno, "UNIQUECOLUMNS") eq "yes")
    {
        $matrixdata = Survey::Component::Matrix->GetColumnData($doc, $tagno);
    }
    else
    {
        $matrixdata = Survey::Component::Matrix->GetRowData($doc, $tagno);
    }

    my (@variables) = @{ $matrixdata->{VARIABLES} };

    return scalar(@variables);
}

sub GetValueNumerical
{
    my ($self, $doc, $sub, $arg, $ses, $tagno) = @_;

    if ($doc->GetTagParam($tagno, "MULTI") eq "yes")
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

sub GetValueName
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;
    my ($name) = $doc->GetTagParam($tagno, "NAME");
    my ($crap) = $valuenumber;
    if ($crap < 10) { $crap = "0" . $crap; }

    $name .= $crap;

    return $name;
}

sub GetCheckValue
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;

    my ($value)    = {};
    my ($name)     = $doc->GetTagParam($tagno, "NAME");
    my ($basename) = $name;

    my ($mtn) = $doc->GetTagParam($tagno, "NAME");
    my ($cpt) = $doc->GetTagParam($tagno, "CAPTION");

    my ($multi) = 0;

    if ($doc->GetTagParam($tagno, "MULTI") eq "yes")
    {
        $multi = 1;
    }

    if ($valuenumber < 10) { $name .= "0"; }
    $name .= $valuenumber;

    my ($both) = 0;

    if ($doc->GetTagParam($tagno, "UNIQUECOLUMNS") eq "yes")
    {
        if ($doc->GetTagParam($tagno, "UNIQUEROWS") eq "yes")
        {
            $both = 1;
        }
    }

    my ($visited) = $doc->CheckVisited($name);

    if ($visited)
    {
        if   (defined($arg->ArgByName($name))) { $value->{VALUE} = $arg->ArgByName($name); }
        else                                   { $value->{VALUE} = ""; }
    }
    else
    {
        $value->{VALUE} = $doc->GetOption("NOTDISPLAYEDVAL");
    }

    if (!$both)
    {

        if ($value->{VALUE} eq "")
        {
            if (($doc->GetTagParam($tagno, "MUSTANSWER") eq "yes") && (!$sub->{SAVE}))
            {
                my ($elmcap);

                if ($doc->GetTagParam($tagno, "MODE") eq "col")
                {
                    $elmcap = $doc->GetMatrixColumnParam($tagno, $valuenumber, "CAPTION");
                }
                else
                {
                    $elmcap = $doc->GetMatrixRowParam($tagno, $valuenumber, "CAPTION");
                }

                my ($tagcap) = $doc->GetTagParam($tagno, "CAPTION");
                $sub->{ERROR} =
                    lprint("The question in matrix") 
                  . " <i>\"" 
                  . $tagcap
                  . "\"</i> "
                  . lprint("with caption")
                  . " <i>\""
                  . $elmcap
                  . "\"</i> "
                  . lprint("must be answered.");
                $sub->{ERRORCODE}        = 2;
                $sub->{MATRIXWORKAROUND} = $elmcap;
            }
            else
            {
                $value->{VALUE} = $doc->GetTagParam($tagno, "ILLEGALVAL");
            }
        }
    }
    else
    {

        # OK, we want uniqueness on both axes. Tricky....

        my ($val) = $arg->ArgByName($name);

        my (@submitted) = ();
        if (!defined($doc->{"UNIQUE_$basename"})) { $doc->{"UNIQUE_$basename"} = \@submitted; }

        @submitted = @{ $doc->{"UNIQUE_$basename"} };

        if (scalar(grep(/$val/, @submitted)) > 0)
        {
            $sub->{ERROR}     = lprint("Answer must be unique on both row and col in '") . $cpt . "'";
            $sub->{ERRORCODE} = 99;
        }
        else
        {
            $value->{VALUE} = $val;
            push(@{ $doc->{"UNIQUE_$basename"} }, $val);
        }
    }

    if ($multi)
    {

        # Extra checking for multiple choices

        if (!$doc->{APPENDEDFILES})
        {

            # Ok, we now know that we are submitting a page
            # Pick the value that came from the *query string*, so
            # we know we exclude session crap.

            if (!defined($arg->{"QUERY_$name"}))
            {
                $value->{VALUE} = "";
            }
            else
            {
                $value->{VALUE} = $arg->{"QUERY_$name"};
            }
        }
    }

    $value->{NAME} = $name;
    $value->{NUMERICAL} = Survey::Component::Matrix->GetValueNumerical($doc, $sub, $arg, $ses, $tagno);

    return $value;
}

sub GetValueCaption
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $value) = @_;
    my ($matrixdata);
    my ($caption) = "";
    my (@data);

    if ($doc->GetTagParam($tagno, "UNIQUECOLUMNS") eq "yes")
    {
        $matrixdata = Survey::Component::Matrix->GetColumnData($doc, $tagno);
        @data = @{ $matrixdata->{ROWS} };
    }
    else
    {
        $matrixdata = Survey::Component::Matrix->GetRowData($doc, $tagno);
        @data = @{ $matrixdata->{COLUMNS} };
    }

    my ($cell);
    foreach $cell (@data)
    {
        my ($entval) = $cell->{VALUE};
        if ($entval eq $value)
        {
            $caption = $cell->{CAPTION};
        }
    }

    return $caption;
}

sub GetVariableCaption
{
    my ($s, $doc, $sub, $arg, $ses, $tagno, $valuenumber) = @_;
    my ($matrixdata);
    my ($caption) = "";
    my (@data);

    if ($doc->GetTagParam($tagno, "UNIQUECOLUMNS") eq "yes")
    {
        $matrixdata = Survey::Component::Matrix->GetColumnData($doc, $tagno);
        @data = @{ $matrixdata->{COLUMNS} };
    }
    else
    {
        $matrixdata = Survey::Component::Matrix->GetRowData($doc, $tagno);
        @data = @{ $matrixdata->{ROWS} };
    }

    my ($entry) = $data[$valuenumber - 1];

    return $entry->{CAPTION};
}

sub GetElementIsVariable
{
    my ($s, $doc, $sub, $arg, $ses, $tagno) = @_;
    return 1;
}

sub GetPossibleValues
{
    my ($s, $doc, $sub, $arg, $ses, $tagno) = @_;
    my (@arr) = ();

    my ($matrixdata);
    my (@data);

    if ($doc->GetTagParam($tagno, "UNIQUECOLUMNS") eq "yes")
    {
        $matrixdata = Survey::Component::Matrix->GetColumnData($doc, $tagno);
        @data = @{ $matrixdata->{ROWS} };
    }
    else
    {
        $matrixdata = Survey::Component::Matrix->GetRowData($doc, $tagno);
        @data = @{ $matrixdata->{COLUMNS} };
    }

    my ($cell);
    foreach $cell (@data)
    {
        my ($entval) = $cell->{VALUE};
        if (defined($entval) && ($entval ne ""))
        {
            my ($entha) = {};
            $entha->{VALUE}   = $entval;
            $entha->{CAPTION} = $cell->{CAPTION};
            push(@arr, $entha);
        }
    }

    return \@arr;
}

sub MakeXML
{
    my ($s, $doc, $tagno) = @_;

    my ($out) = $s->MakeTagOpener($doc, $tagno);

    my ($colcount) = $doc->GetTagParam($tagno, "COLS");
    my ($rowcount) = $doc->GetTagParam($tagno, "ROWS");

    for (my ($i) = 1 ; $i <= $colcount ; $i++)
    {
        my ($val) = $doc->GetMatrixColumnParam($tagno, $i, "VALUE");
        my ($cap) = $doc->GetMatrixColumnParam($tagno, $i, "CAPTION");

        $out .= "  <MATRIXCOLUMN CAPTION=\"$cap\" ";
        if ($val ne "")
        {
            $out .= "VALUE=\"$val\" ";
        }
        $out .= "/>\n";
    }

    for (my ($i) = 1 ; $i <= $rowcount ; $i++)
    {
        my ($val) = $doc->GetMatrixRowParam($tagno, $i, "VALUE");
        my ($cap) = $doc->GetMatrixRowParam($tagno, $i, "CAPTION");

        $out .= "  <MATRIXROW CAPTION=\"$cap\" ";
        if ($val ne "")
        {
            $out .= "VALUE=\"$val\" ";
        }
        $out .= "/>\n";
    }

    $out .= $s->MakeTagCloser($doc, $tagno);

    return $out;
}

1;

