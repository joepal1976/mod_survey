#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Statistics;

use strict;

sub Sum
{
    my ($crap)    = shift;
    my (%args)    = (@_);
    my ($illegal) = $args{"illegal"};
    my (@data)    = @{ $args{"data"} };
    my ($cell);
    my ($sum) = 0;

    foreach $cell (@data)
    {
        if ($cell ne $illegal)
        {
            $sum = $sum + $cell;
        }
    }
    return $sum;
}

sub Legal
{
    my ($crap)    = shift;
    my (%args)    = (@_);
    my ($illegal) = $args{"illegal"};
    my (@data)    = @{ $args{"data"} };
    my ($cell);
    my ($n) = 0;

    foreach $cell (@data)
    {
        if ($cell ne $illegal)
        {
            $n++;
        }
    }
    return $n;
}

sub Mean
{
    my ($crap)    = shift;
    my (%args)    = (@_);
    my ($illegal) = $args{"illegal"};
    my (@data)    = @{ $args{"data"} };

    my ($legal) = Survey::Statistics->Legal('illegal' => $illegal, 'data' => \@data);
    my ($sum)   = Survey::Statistics->Sum('illegal'   => $illegal, 'data' => \@data);

    if ($legal > 0)
    {
        return ($sum / $legal);
    }
    else
    {
        return "[no legal cases]";
    }
}

sub Median
{
    my ($crap)    = shift;
    my (%args)    = (@_);
    my ($illegal) = $args{"illegal"};
    my (@data)    = @{ $args{"data"} };

    my (@legal1, $x, $y, $cell);

    if (Survey::Statistics->Legal('illegal' => $illegal, 'data' => \@data) eq "0")
    {
        return "[no legal cases]";
    }

    foreach $cell (@data)
    {
        if ($cell ne $illegal)
        {
            push(@legal1, $cell);
        }
    }

    my (@legal) = sort { $a <=> $b } @legal1;

    if ((@legal / 2) == int(@legal / 2))
    {
        $x = $legal[int(@legal / 2) - 1];
        $y = $legal[int(@legal / 2)];
        return (($x + $y) / 2);
    }
    else
    {
        return $legal[int(@legal / 2)];
    }
}

sub Max
{
    my ($crap)    = shift;
    my (%args)    = (@_);
    my ($illegal) = $args{"illegal"};
    my (@data)    = @{ $args{"data"} };
    my ($cell)    = -10000000;
    my ($n)       = $data[0];

    if (Survey::Statistics->Legal('illegal' => $illegal, 'data' => \@data) eq "0")
    {
        return "[no legal cases]";
    }

    foreach $cell (@data)
    {
        if (($cell ne $illegal) && ($cell > $n))
        {
            $n = $cell;
        }
    }
    return $n;
}

sub Min
{
    my ($crap)    = shift;
    my (%args)    = (@_);
    my ($illegal) = $args{"illegal"};
    my (@data)    = @{ $args{"data"} };
    my ($cell)    = 10000000;
    my ($n)       = $data[0];

    if (Survey::Statistics->Legal('illegal' => $illegal, 'data' => \@data) eq "0")
    {
        return "[no legal cases]";
    }

    foreach $cell (@data)
    {
        if (($cell ne $illegal) && ($cell < $n))
        {
            $n = $cell;
        }
    }
    return $n;
}

sub StdDeviation
{
    my ($crap)    = shift;
    my (%args)    = (@_);
    my ($illegal) = $args{"illegal"};
    my (@data)    = @{ $args{"data"} };
    my ($cell, $dif);

    my ($mean)  = Survey::Statistics->Mean('illegal'  => $illegal, 'data' => \@data);
    my ($legal) = Survey::Statistics->Legal('illegal' => $illegal, 'data' => \@data);

    my ($sum) = 0;

    foreach $cell (@data)
    {
        if ($cell ne $illegal)
        {
            $dif = $cell - $mean;
            $sum = $sum + ($dif * $dif);
        }
    }

    if ($legal > 1)
    {
        return sqrt($sum / ($legal - 1));
    }
    else
    {
        return "[too few legal cases]";
    }
}

sub Frequencies
{

}

sub Correlation
{

}

1;

