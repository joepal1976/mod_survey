#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::DataAggressiveCache;

use Exporter;
use strict;

use Survey::Data;
use Survey::DataEntry;

use vars qw($AGGRESSIVE_CACHE);


sub new
{
    $AGGRESSIVE_CACHE = {};

    $AGGRESSIVE_CACHE->{TIMESTAMP} = time();

    bless($AGGRESSIVE_CACHE);
    return $AGGRESSIVE_CACHE;
}

sub isCaseRegistered
{
    my ($self, $id) = @_;

    1;
}

sub getCase
{
    my ($self, $id) = @_;

    1;
}

sub registerCase
{
    my ($self, $dataname, $carr) = @_;

    1;
}

sub registerData
{
    my ($self, $dataname, $data, $name) = @_;

    1;
}

sub readData
{
    my ($self, $dataname, $data, $name) = @_;

    1;
}

sub isDataRegistered
{
    my ($self, $name) = @_;

    1;
}

sub getData
{
    my ($self, $name) = @_;

    1;
}

BEGIN
{
    use vars qw(@ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);
    @ISA = qw(Exporter);

    @EXPORT = qw($AGGRESSIVE_CACHE);

    $AGGRESSIVE_CACHE = Survey::DataAggressiveCache->new();
}

return 1;

