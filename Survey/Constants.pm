#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Constants;

use Survey::Language;
use strict;

sub new
{
    my $crap = shift;
    my $self = {};

    bless($self);

    my ($mp_version)      = 20;
    my ($find_mp_version) = "use mod_perl; \$mp_version = \$mod_perl::VERSION; \$mp_version = int(\$mp_version * 10);";
    eval($find_mp_version);
    if (!$mp_version) { $mp_version = 20; }

    my ($evalstr) = "use Apache2 (); use Apache::compat; use Apache::Constants qw(:common);";

    if ($mp_version < 19)
    {
        $evalstr = "use Apache (); use Apache::Constants qw(:common);";
        $evalstr .= "\$REDIRECT = OK;";
    }

    if ($mp_version == 19)
    {
        $evalstr = "use Apache2 (); use Apache::compat; use Apache::Constants qw(:common);";
        $evalstr .= "\$REDIRECT = REDIRECT;";
    }

    if ($mp_version > 19)
    {
        $evalstr = "use Apache2::Const qw(:common); use Apache2::Access ();";
        $evalstr .= "\$REDIRECT = REDIRECT;";
    }

    my ($OK, $DECLINED, $AUTH_REQUIRED, $REDIRECT);

    $evalstr .= "use strict;";
    $evalstr .= "\$OK = OK;";
    $evalstr .= "\$DECLINED = DECLINED;";
    $evalstr .= "\$AUTH_REQUIRED = AUTH_REQUIRED;";

    eval($evalstr);
    if ($@) { die $@; }

    $self->{OK}            = $OK;
    $self->{DECLINED}      = $DECLINED;
    $self->{AUTH_REQUIRED} = $AUTH_REQUIRED;
    $self->{REDIRECT}      = $REDIRECT;

    return ($self);
}

1;

