#    This source code file is part of the "mod_survey" package.
#
#    Copyright (C) 2004  Joel Palmius
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program (probably in a file named "LICENSE.txt" or the like);
#    if not, write to:
#
#    Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#!/usr/bin/perl

package Survey::Submit;
use strict;

use constant MP2 => $mod_perl::VERSION < 1.99 ? 0 : 1;

use CGI::Cookie;

use Survey::Constants;

use Survey::Component::Component;
use Survey::Component::Env;
use Survey::Component::Geo;
use Survey::Component::Calculated;
use Survey::Component::Datepicker;
use Survey::Component::Constant;
use Survey::Component::Lickert;
use Survey::Component::Choice;
use Survey::Component::List;
use Survey::Component::Matrix;
use Survey::Component::Boolean;
use Survey::Component::Text;
use Survey::Component::Memo;
use Survey::Component::Newline;
use Survey::Component::Custom;
use Survey::Component::Comment;
use Survey::Component::Submit;

use Survey::Component::Ifroute;
use Survey::Component::Caseroute;
use Survey::Component::Route;
use Survey::Component::Randomroute;
use Survey::Component::Sequence;

#use Survey::Component::Import;

use Survey::Language;

sub new
{

    # added in CRU patch (MJ/20020802)
    my ($crap, $doc, $arg, $sys, $save) = @_;
    my $self = {};

    if ($ENV{"_SURVEY_USEDBI"})
    {
        eval "use Survey::Component::Import";
    }

    $self->{ERROR}     = 0;
    $self->{ERRORCODE} = 0;
    $self->{DOCUMENT}  = $doc;
    $self->{ARGUMENT}  = $arg;
    $self->{SYSTEM}    = $sys;

    # added in CRU patch (MJ/20020802)
    $self->{SAVE} = $save;

    bless($self);

    $self->FillDocument();
    $self->DoRouting();

    if (!$self->{ERROR})
    {
        if ($doc->GetOption("CHECKKEY") eq "yes")
        {
            $sys->CheckKey();
        }
        if ($sys->Error())
        {
            $self->{ERROR}     = lprint("A system error has occured");
            $self->{ERRORCODE} = 3;
        }
        else
        {
            my ($mp) = $doc->GetOption("MULTIPAGE");
            my ($lp) = $doc->GetOption("LASTPAGE");

            # Save data only if not multipage, or in last page of sequence (JP/20020524)
            if ((!$mp) || ($mp eq $lp))
            {

                # (not needed after update of FillDocument() ) $final = $self->CheckVisited();
                # added in CRU patch (MJ/20020802)
                if ($self->{SAVE})
                {
                    $self->ExportDBI();
                }
                else
                {

                    # Perform perl parsing before save, so that data can be manipulated
                    # dynamically
                    $self->ParseSubmitPerl();

                    if ($doc->GetOption("ASCIIFILE"))
                    {
                        if ($ENV{"_SURVEY_USENEWAUTO"} && $doc->GetOption("ISAUTO"))
                        {
                            $self->ExportAutoData();
                        }
                        else
                        {
                            $self->ExportASCII();
                        }
                    }

                    if ($doc->GetOption("DBITABLE"))
                    {
                        $self->ExportDBI();
                    }

                    # Check if data should be mailed too
                    Survey::Component::MailCopy->PerformMailing($doc, $self, $arg, $doc->{SESSION});

                    # There are no more pages in the chain, so we can remove the
                    # session without problem.
                    $doc->{SESSION}->clear();
                }

                # added in CRU patch (MJ/20020802)
            }
        }
    }

    if (!$self->{ERROR})
    {
        $sys->CheckSetUser();
        $self->SubmitMsg();
    }

    return ($self);
}

sub SetCookie
{
    my ($self, $name, $value) = @_;

    $self->{COOKIES}->{$name} = $value;
}

sub ParseSubmitPerl
{
    my ($self) = shift;
    my ($arg)  = $self->{ARGUMENT};
    my ($doc)  = $self->{DOCUMENT};
    my ($ses)  = $self->{DOCUMENT}->{SESSION};

    if (defined($doc->{SUBMITTAGAT}))
    {
        Survey::Component::Submit->PostParsePerl($self, $doc->{SUBMITTAGAT});
    }
}

sub DoRouting
{
    my ($self) = shift;
    my ($arg)  = $self->{ARGUMENT};
    my ($doc)  = $self->{DOCUMENT};
    my ($ses)  = $self->{DOCUMENT}->{SESSION};

    if ($doc->GetOption("MULTIPAGE"))
    {
        my ($tagno) = $doc->GetOption("ROUTERTAG");
        my ($type)  = $doc->GetOption("ROUTERTYPE");

        $self->{IMPLICITCONTINUE} = 0;

        if ($type eq "IF")
        {
            Survey::Component::Ifroute->DoRouting($self, $doc, $arg, $ses, $tagno);
        }
        if ($type eq "CASE")
        {
            Survey::Component::Caseroute->DoRouting($self, $doc, $arg, $ses, $tagno);
        }
        if ($type eq "PLAIN")
        {
            Survey::Component::Route->DoRouting($self, $doc, $arg, $ses, $tagno);
        }
        if ($type eq "RANDOM")
        {
            Survey::Component::Randomroute->DoRouting($self, $doc, $arg, $ses, $tagno);
        }
    }

    1;
}

sub SubmitMsg
{
    my ($self) = shift;
    my ($arg)  = $self->{ARGUMENT};
    my ($doc)  = $self->{DOCUMENT};
    my ($ses)  = $doc->{SESSION};

    if (defined($doc->{SUBMITTAGAT}))
    {
        if ($doc->GetTagParam($doc->{SUBMITTAGAT}, "VISIBLE") eq "yes")
        {
            print $doc->GetTagParam($doc->{SUBMITTAGAT}, "RAW");
            print "\n<br /><br />\n";
            return;

            # Note that visible SUBMIT overrides everything concerning REDIRECT and
            # CONTINUE, in other words also all forms of routing.
        }
    }

    my ($continue) = $doc->GetOption("CONTINUE");

    if ($self->{IMPLICITCONTINUE})
    {
        $continue .= "?session-id=" . $ses->{SESSION_ID} . "\&action=display";
    }

    if ($doc->GetOption("CONTINUE") && ($doc->GetOption("REDIRECT") eq "yes"))
    {

        # make fq URL, send Location header
        # the browser will be redirected to this url
        my $redirectUrl;

        # if $continue is a complete url
        # (needed to work with the CONTINUE attribute of the SURVEY-tag.
        # so it is possible to redirect to a page anywhere in the www)
        if (index($continue, "http://") >= 0 || index($continue, "https://") >= 0)
        {
            $redirectUrl = $continue;
        }
        else
        {
            $redirectUrl = $ENV{HTTPS} eq 'on' ? "https://" : "http://";
            $redirectUrl .= $ENV{HTTP_HOST};

            # get current path ($ENV{SCRIPT_NAME} looks something like: /mySurveys/earthworm/questionX.survey)
            $redirectUrl .= substr($ENV{SCRIPT_NAME}, 0, rindex($ENV{SCRIPT_NAME}, "/") + 1);
            $redirectUrl .= $continue;
        }

        # print HTTP-Header
        # use headers_out only if in Apache 2
        if (MP2)
        {
            my ($constants) = Survey::Constants->new();
            $doc->{HANDLER}->headers_out->set('Location' => $redirectUrl);
            $doc->{HANDLER}->status($constants->{REDIRECT});
        }
        else
        {
            print "Location: $redirectUrl\n\n";
        }

        print " <title>" . lprint("SURVEY: Submit successful (redirecting)") . "</title>\n";
        print " <meta HTTP-EQUIV=\"REFRESH\" CONTENT=\"0\;URL=" . $continue . "\">";
        Survey::Slask->BodyTag();
        print "    <h1 class=\"good\">Part successfully submitted</h1>\n";
        print "    If you are not automatically redirected to the next part, please ";
        print "<a href=\"" . $continue . "\">click here to continue</a>.";
    }
    else
    {
        print "    <title>" . lprint("SURVEY: Submit successful") . "</title>\n";
        print "    <style><!--\n";
        print "      .good { color: #009900; }\n";
        print "    --></style>\n";
        Survey::Slask->BodyTag();
        print "    <h1 class=\"good\">" . lprint("Submit successful") . "</h1>\n";
        print "    <h3>" . lprint("The data was successfully submitted") . ".</h3><br /> \n";
        if ($doc->GetOption("CONTINUE"))
        {
            print "    " . lprint("The author of the questionnaire has asked the program to direct you ");
            print lprint("to another page when you submitted your data. ");
            print "<a href=\""
              . $continue . "\">"
              . lprint("Click here to be proceed to that page")
              . "</a>.<br /><br />\n";
        }
        print "    " . lprint("You can go to the start page of mod_survey. ");
        print "<a href=\""
          . $ENV{"_SURVEY_ROOT_ALIAS"} . "\">"
          . lprint("Click here to proceed to that page")
          . "</a>.<br /><br />\n";
    }

    1;
}

sub FillDocument
{
    my ($self) = shift;
    my ($doc)  = $self->{DOCUMENT};
    my ($arg)  = $self->{ARGUMENT};
    my ($sys)  = $self->{SYSTEM};
    my ($ses)  = $doc->{SESSION};

    my ($i, $n, $type, @data);

    my ($sofar) = $ses->getValue("submittedsofar");

    for ($i = 0 ; $i < $doc->GetTagCount() ; $i++)
    {
        if (!$self->{ERROR})
        {
            $type = $doc->GetTagParam($i, "TYPE");

            my ($numval) = 0;
            my ($call)   = "\$numval = " . $doc->Translate($type) . "->NumberOfValues(\$doc,\$self,\$arg,\$ses,\$i);";
            eval($call);

            if ($numval > 0)
            {
                for ($n = 1 ; $n <= $numval ; $n++)
                {
                    my ($value);
                    $call =
                      "\$value = " . $doc->Translate($type) . "->GetCheckValue(\$doc,\$self,\$arg,\$ses,\$i,\$n);";
                    eval($call);

                    if ($self->{ERROR})
                    {
                        $self->{ERRORTAG}    = $i;
                        $self->{ERRORSUBTAG} = $n;
                    }

                    $value->{TAGNO} = $i;
                    $value->{TYPE}  = $type;
                    push(@data, $value);
                    if ($sofar) { $sofar .= "\x03"; }
                    $sofar .= $value->{NAME};
                    $ses->setValue("SUBMITTED_" . $value->{NAME}, $value->{VALUE});
                    $ses->setValue("CAPTION_" . $value->{NAME},   $value->{CAPTION});
                }
            }
        }
    }
    $self->{DATA} = \@data;
    $ses->setValue("submittedsofar", $sofar);
}

sub ExportASCII
{
    my ($self) = shift;
    my ($doc)  = $self->{DOCUMENT};
    my ($arg)  = $self->{ARGUMENT};

    my ($i, $type, $lineout, $crap, $d, $value);

    my (@data) = @{ $self->{DATA} };

    $lineout = $arg->ArgByName("key");
    $d       = $doc->GetOption("DELIMITER");

    foreach $value (@data)
    {
        $lineout = $lineout . $d . $value->{VALUE};
    }

    if ($doc->GetOption("DOSBR") eq "yes")
    {
        $lineout = $lineout . "\x0d\x0a";
    }
    else
    {
        $lineout = $lineout . "\n";
    }

    if (open(FIL, ">>" . $doc->GetOption("ASCIIFILE")))
    {
        print FIL $lineout;
        close(FIL);
    }
    else
    {
        $self->{ERROR}     = lprint("Could not open ") . $doc->GetOption("ASCIIFILE") . lprint("for appending. ");
        $self->{ERRORCODE} = 4;
    }

    1;
}

sub ExportAutoData
{
    my ($self) = shift;
    my ($doc)  = $self->{DOCUMENT};
    my ($arg)  = $self->{ARGUMENT};

    my ($i, $type, $lineout, $crap, $d, $b, $value);

    my (@data) = @{ $self->{DATA} };

    $lineout = $arg->ArgByName("key");

    $d = "\x01";
    $b = "\x02";

    foreach $value (@data)
    {
        $lineout = $lineout . $d . $value->{VALUE};
    }

    $lineout .= $b;

    if (open(FIL, ">>" . $doc->GetOption("ASCIIFILE")))
    {
        print FIL $lineout;
        close(FIL);
    }
    else
    {
        $self->{ERROR}     = lprint("Could not open ") . $doc->GetOption("ASCIIFILE") . lprint("for appending. ");
        $self->{ERRORCODE} = 4;
    }

    1;
}

sub ExportDBI
{
    my ($self) = shift;
    my ($doc)  = $self->{DOCUMENT};
    my ($arg)  = $self->{ARGUMENT};
    my ($save) = $self->{SAVE};               # added in CRU patch (MJ/20020802)
    my ($dsn)  = $doc->GetOption("DBIDSN");
    $dsn =~ s/\x08/\;/g;
    my ($usr) = $doc->GetOption("DBIUSER");
    my ($psw) = $doc->GetOption("DBIPASSWD");
    my ($cre) = $doc->GetOption("DBICREATE");
    my ($dbh, $i, $type, $sql, $n, $crap, $name, $value);
    my (@data) = @{ $self->{DATA} };

    #connect to the db

    if (!($dbh = DBI->connect($dsn, $usr, $psw, { PrintError => 0, AutoCommit => 1, RaiseError => 0 })))
    {
        $self->{ERROR}     = lprint("Could not connect to DBI database (error was ") . $DBI::errstr . ")";
        $self->{ERRORCODE} = 5;
    }
    else
    {

        #note: I've removed the save stuff for the incomplete survey

        $sql = "INSERT INTO " . $doc->GetOption("DBITABLE") . "(srvkey";

        foreach $name (@data)
        {
            $sql = $sql . "," . $name->{NAME};
        }

        $sql = $sql . ") VALUES (";

        #set the key

        $sql = $sql . "\'" . $arg->ArgByName("key") . "\'";

        my ($i) = 0;

        #set the value of all the other fields

        foreach $value (@data)
        {
            if ($value->{NUMERICAL})
            {
                $sql = $sql . "," . $value->{VALUE};
            }
            else
            {

                # sanity parse for DBI submit
                my ($vl) = $value->{VALUE};
                $vl =~ s/\\//g;
                $vl =~ s/\'/\\\'/g;

                $sql = $sql . ",\'" . $vl . "\'";
            }
            $i++;
        }

        #close the sql statement

        $sql .= ")";

        if (!($dbh->do($sql)))
        {
            $self->{ERROR}     = lprint("Could not execute DBI insert (error was ") . $DBI::errstr . ")";
            $self->{ERRORCODE} = 6;
        }

        $dbh->disconnect;
    }

    1;
}

sub Error
{
    my ($self) = shift;
    return $self->{ERROR};
}

sub PrintErrorDescription
{
    my $self = shift;

    my ($e)     = $self->{ERRORCODE};
    my ($found) = 0;

    my ($lb) = "<b>\&lt\;";
    my ($rb) = "\&gt\;</b>";

    print "    <br /><br />\n    ";

    # 1 - no tags
    # 2 - mustanswer
    # 3 - system error

    if ($e)
    {
        if ($e eq 1)
        {
            print "<b><i>[" . lprint("SUBMIT ERROR 1, NO TAGS TO WORK WITH") . "]</i></b> ";
            print lprint("This is one of the errors that should never occur. ");
            print lprint(
"When the survey document was parsed, no valid tags were found, therefore a submit should not have been possible. ");
            print lprint("One probable cause might be that the document has been emptied between display and submit.");
            $found = 1;
        }

        if ($e eq 2)
        {
            print "<b><i>[" . lprint("SUBMIT ERROR 2, THE QUESTION MUST BE ANSWERED") . "]</i></b> ";
            print lprint("One or more question required an answer. ");
            print lprint('You have to press "back" in your browser and answer those questions.');
            $found = 1;
        }

        if ($e eq 3)
        {
            print "<b><i>[" . lprint("SUBMIT ERROR 3, A SYSTEM ERROR HAS OCCURED") . "]</i></b> ";
            print lprint(
                   "You should never see this, since the system error should be printed instead of this submit error.");
            $found = 1;
        }

        if ($e eq 4)
        {
            print "<b><i>[" . lprint("SUBMIT ERROR 4, COULD NOT OPEN ASCIIFILE") . "]</i></b> ";
            print lprint("An error occured when the script tried to write to the output file. ");
            print lprint("Possible causes of this is that write permission was denied or that the disk is full");
            $found = 1;
        }

        if ($e eq 5)
        {
            print "<b><i>[" . lprint("SUBMIT ERROR 5, COULD NOT CONNECT TO DATABASE") . "]</i></b> ";
            print lprint("Something went wrong when the script tried to connect to the database. ");
            print lprint(
                 "A lot of things can go wrong there, so you will have to try to interpret the error description above."
            );

            $found = 1;
        }

        if ($e eq 6)
        {
            print "<b><i>[" . lprint("SUBMIT ERROR 6, COULD NOT EXECUTE DBI INSERT") . "]</i></b> ";
            print lprint("Something went wrong when the script tried to insert data into the database. ");
            print lprint(
                 "A lot of things can go wrong there, so you will have to try to interpret the error description above."
            );
            $found = 1;
        }

        if ($e eq 7)
        {
            print "<b><i>[" . lprint("SUBMIT ERROR 7, ANSWER MUST BE NUMERICAL") . "]</i></b> ";
            print lprint(
"The author of the survey file has requested that the answer to this question should be numerical, or in other words only containing numbers. "
            );
            print lprint('Please click "back" in your browser and give a numerical answer to the question.');
            $found = 1;
        }

        if ($e eq 8)
        {
            print "<b><i>[" . lprint("SUBMIT ERROR 8, ANSWER CANNOT BE HIGHER THAN") . "]</i></b> ";
            print lprint(
"The author of the survey file has requested that the answer to this question should be no higher than a certain value. "
            );
            print lprint(
'Please click "back" in your browser and give an answer which is lower than or equal to the above number.');
            $found = 1;
        }

        if ($e eq 9)
        {
            print "<b><i>[" . lprint("SUBMIT ERROR 9, ANSWER CANNOT BE LOWER THAN") . "]</i></b> ";
            print lprint(
"The author of the survey file has requested that the answer to this question should be no lower than a certain value. "
            );
            print lprint(
'Please click "back" in your browser and give an answer which is higher than or equal to the above number.');
            $found = 1;
        }
        if ($e eq 10)
        {
            print "<b><i>[" . lprint("SUBMIT ERROR 10, ANSWER NOT VALID") . "]</i></b> ";
            print lprint(
"The author of the survey file has requested that the answer you choose for one column should be no equal to the one you have chosen for the others."
            );
            print lprint('Please click "back" in your browser and change one of the answers you chose.');
            $found = 1;
        }

        if ($e eq 11)
        {
            print "<b><i>[" . lprint("SUBMIT ERROR 11, A CUSTOM QUESTION MUST BE ANSWERED") . "]</i></b> ";
            print lprint("The author of the survey has requested that a custom-made question must be answered.");
            print lprint('Please click "back" in your browser and answer this question.');
            $found = 1;
        }

        if (!$found)
        {
            print lprint("This error is not in the knowledge base, so I guess the programmer has made a mistake.");
        }
    }
    else
    {
        print "<i>[" . lprint("SUBMIT ERROR 0, NO ERROR") . "]</i> ";
        print lprint(
                "No error has occured, but the programmer has for some reason called PrintErrorDescription() anyway. ");
        print lprint("I guess the programmer has again made a mistake, something that is a very common event indeed.");
    }
    print "\n    <br /><br />\n";

    1;
}

1;

